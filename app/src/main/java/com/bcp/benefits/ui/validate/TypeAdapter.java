package com.bcp.benefits.ui.validate;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bcp.benefits.R;
import com.bcp.benefits.viewmodel.TypeDocViewModel;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TypeAdapter extends RecyclerView.Adapter<TypeAdapter.TypeViewHolder> {


    private ArrayList<TypeDocViewModel> types;
    private int positionSelected;
        private ListenerAdapter listener;

    TypeAdapter(ArrayList<TypeDocViewModel> types, int positionSelected) {
        this.types = types;
        this.positionSelected = positionSelected;
    }


    void setListener(@NonNull TypeAdapter.ListenerAdapter listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public TypeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_type_custom, parent, false);
        return new TypeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TypeViewHolder holder, int position) {
        holder.text.setText(types.get(position).getTypeDoc());
        holder.itemView.setOnClickListener(v -> listener.selectType(types.get(position)));
        holder.imageCircle.setImageResource(R.drawable.ic_circle_blue);
        holder.imageCircle.setVisibility(position == positionSelected ? View.VISIBLE : View.GONE);
    }

    @Override
    public int getItemCount() {
        return types.size();
    }

    class TypeViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_type_texto)
        TextView text;
        @BindView(R.id.img_circle)
        ImageView imageCircle;

        private TypeViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    interface ListenerAdapter {
        void selectType(TypeDocViewModel typeDocViewModel);
    }
}
