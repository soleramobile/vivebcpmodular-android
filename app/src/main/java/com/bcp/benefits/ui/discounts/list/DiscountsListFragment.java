package com.bcp.benefits.ui.discounts.list;


import android.location.Location;
import android.os.Bundle;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bcp.benefits.R;
import com.bcp.benefits.components.PaginationScrollListener;
import com.bcp.benefits.ui.BaseFragment;
import com.bcp.benefits.ui.discounts.DiscountsActivity;
import com.bcp.benefits.ui.discounts.detail.DiscountDetailActivity;
import com.bcp.benefits.util.AppPreferences;
import com.bcp.benefits.util.AppUtils;
import com.bcp.benefits.viewmodel.DiscountEnterType;
import com.bcp.benefits.viewmodel.DiscountViewModel;
import com.bcp.benefits.viewmodel.DiscountTypeViewModel;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import pe.solera.benefit.main.entity.Pagination;
import pe.solera.benefit.main.entity.SortType;

import static com.bcp.benefits.util.Constants.INDENTIFIER_FRAGMENT;
import static com.bcp.benefits.viewmodel.DiscountTypeViewModel.EDUTATION;

/**
 * Created by emontesinos on 27/05/19.
 **/
public class DiscountsListFragment extends BaseFragment implements
        DiscountAdapter.OnDiscountItemListener, DiscountListPresenter.DiscountItemsListView {

    @BindView(R.id.rv_discounts)
    RecyclerView rvDiscount;
    @BindView(R.id.btn_open_nearyou)
    TextView btnOpenNearYou;

    @BindView(R.id.swipe_refresh_discount)
    SwipeRefreshLayout swipeRefreshDiscount;

    private DiscountTypeViewModel discountType;

    private SortType sortType = SortType.VALUE;
    private String search = "";
    private int idCategory = 0;
    private int idState = 0;
    private int idDistrict = 0;
    private DiscountAdapter adapter;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private Pagination pagination;
    private Boolean validClick = true;

    @Inject
    DiscountListPresenter presenter;

    public static DiscountsListFragment newInstance(DiscountTypeViewModel discountType) {
        DiscountsListFragment fragment = new DiscountsListFragment();
        Bundle args = new Bundle();
        args.putSerializable("idTypes", discountType);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void setUpView() {
        this.presenter.setDiscountItemsListView(this);
        this.setUpRecycler();
        this.presenter.loadUrl();
        AppPreferences appPreferences = new AppPreferences(getContext());
        appPreferences.savePreference(INDENTIFIER_FRAGMENT, 1);
        discountType = (DiscountTypeViewModel) getArguments().getSerializable("idTypes");
        getViewType();
        swipeRefreshDiscount.setOnRefreshListener(() -> {
                    presenter.loadLocation();
                    adapter.clearItems();
                    pagination = null;
                }
        );
        this.presenter.loadLocation();
    }

    private void setUpRecycler() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rvDiscount.setLayoutManager(linearLayoutManager);
        adapter = new DiscountAdapter(getContext(), discountType);
        adapter.addListener(this);
        rvDiscount.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                if (pagination != null && pagination.getHasNextPage()
                        && AppUtils.isConnected(getContext()))
                    presenter.loadLocation();
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
        rvDiscount.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        validClick =true;
        getViewType();
    }

    public void setSortType(SortType sortType) {
        this.sortType = sortType;
    }

    public void setDiscountType(DiscountTypeViewModel discountType) {
        this.discountType = discountType;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public void setIdCategory(int idCategory) {
        this.idCategory = idCategory;
    }

    public void setIdState(int idState) {
        this.idState = idState;
    }

    public void setIdDistrict(int idDistrict) {
        this.idDistrict = idDistrict;
    }

    @Override
    public int getLayout() {
        return R.layout.fragment_discounts;
    }

    @OnClick(R.id.btn_open_nearyou)
    void onClickNearYou() {
        ((DiscountsActivity) Objects.requireNonNull(getActivity())).onClickNearYou();
    }

    private void getViewType() {
        if (discountType == EDUTATION)
            btnOpenNearYou.setBackground(getResources().getDrawable(R.drawable.sl_circle_yellow_gradient));

    }

    @Override
    public void onDiscountItem(DiscountViewModel discountViewModel) {
        if (validClick){
            validClick =false;
            DiscountDetailActivity.start(getActivity(), discountType, discountViewModel.getIdDiscount(), discountViewModel.getIdSubsidiary(), DiscountEnterType.LISTA);
            getActivity().overridePendingTransition(R.anim.left_in, R.anim.left_out);
        }
    }


    @Override
    public void showDiscounts(List<DiscountViewModel> discountList) {
        adapter.setItems(discountList);
    }

    @Override
    public void showPagination(Pagination pagination) {
        this.pagination = pagination;
    }

    @Override
    public void showLocation(Location location) {
        if (location != null && AppUtils.isConnected(getContext())) {
            presenter.getDiscounts(discountType, sortType, search, idCategory == -1 ? 0 : idCategory,
                    idState == -1 ? 0 : idState, idDistrict,
                    String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude()),
                    pagination != null ? pagination.getNextPage() : 0);
        }
    }


    @Override
    public void showSwipe() {
        adapter.showLoading(true);
        isLoading = true;
    }

    @Override
    public void hideSwipe() {
        adapter.showLoading(false);
        swipeRefreshDiscount.setRefreshing(false);
        isLoading = false;
    }

    @Override
    public void showUrl(String url) {
        adapter.setUrlBenefits(url);
    }

}
