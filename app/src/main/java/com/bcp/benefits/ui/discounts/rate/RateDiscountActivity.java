package com.bcp.benefits.ui.discounts.rate;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.bcp.benefits.analytics.AnalyticsModule;
import com.bcp.benefits.analytics.AnalyticsTags;
import com.google.android.flexbox.FlexWrap;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;
import com.bcp.benefits.R;
import com.bcp.benefits.ui.BaseActivity;
import com.bcp.benefits.ui.discounts.consume.SuccessMessageDialogFragment;
import com.bcp.benefits.util.AppUtils;
import com.bcp.benefits.viewmodel.DiscountCalificationOptionViewModel;
import com.bcp.benefits.viewmodel.DiscountCalificationViewModel;
import com.bcp.benefits.viewmodel.DiscountTypeViewModel;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

import static com.bcp.benefits.util.Constants.BENEFIT_NAME;
import static com.bcp.benefits.util.Constants.DISCOUNT_ID_PARAM;
import static com.bcp.benefits.util.Constants.DISCOUNT_TYPE_PARAM;
import static com.bcp.benefits.util.Constants.ID_PROVIDER;
import static com.bcp.benefits.util.Constants.REVIEW_FORM_PARAM;
import static com.bcp.benefits.util.Constants.USAGE_ID_PARAM;
import static com.bcp.benefits.viewmodel.DiscountTypeViewModel.DISCOUNT;
import static com.bcp.benefits.viewmodel.DiscountTypeViewModel.EDUTATION;

public class RateDiscountActivity extends BaseActivity implements
        RateOptionsAdapter.OnSelectCaliticationOptionListener, RateDiscountPresenter.RateDiscountView {

    private static final String PROVIDER_PARA ="" ;
    @Inject
    RateDiscountPresenter presenter;

    @BindView(R.id.rb_score)
    AppCompatRatingBar rbScore;
    @BindView(R.id.tv_question)
    TextView tvQuestion;
    @BindView(R.id.et_comment)
    EditText etComment;
    @BindView(R.id.rv_rate_options)
    RecyclerView rvRateOptions;
    @BindView(R.id.tv_title_1)
    TextView tvTitle1;
    @BindView(R.id.tv_title_2)
    TextView tvTitle2;
    @BindView(R.id.btn_send)
    TextView btnSend;
    @BindView(R.id.ll_comment_body)
    ConstraintLayout llCommentBody;
    @BindView(R.id.img_close)
    ImageView btnClose;
    @BindView(R.id.content_rate)
    ScrollView contentRate;

    private String benefitName;
    private String provider;
    private Integer discountId, usageId;
    private RateOptionsAdapter adapter;
    private ArrayList<DiscountCalificationViewModel> reviewForm;
    private DiscountCalificationOptionViewModel selectedRateOption;
    private DiscountTypeViewModel discountType;
    private AnalyticsModule analyticsModule = new AnalyticsModule(this);
    private int idProvider;

    public static void start(Activity context, DiscountTypeViewModel discountTypeModel, Integer discountId,
                             Integer usageId, ArrayList<DiscountCalificationViewModel> reviewForm, String benefitNames, int idProvider, String provider) {
        Intent starter = new Intent(context, RateDiscountActivity.class);
        starter.putExtra(DISCOUNT_TYPE_PARAM, discountTypeModel);
        starter.putExtra(DISCOUNT_ID_PARAM, discountId);
        starter.putExtra(USAGE_ID_PARAM, usageId);
        starter.putExtra(REVIEW_FORM_PARAM, reviewForm);
        starter.putExtra(BENEFIT_NAME, benefitNames);
        starter.putExtra(PROVIDER_PARA, provider);
        starter.putExtra(ID_PROVIDER, idProvider);
        context.startActivityForResult(starter, 500);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        this.loadData();
        setTheme(discountType == EDUTATION ? R.style.ConsumeEducation : R.style.ConsumeDiscount);
        super.onCreate(savedInstanceState);
    }

    private void loadData() {
        reviewForm = (ArrayList<DiscountCalificationViewModel>) getIntent().getSerializableExtra(REVIEW_FORM_PARAM);
        discountId = getIntent().getIntExtra(DISCOUNT_ID_PARAM, -1);
        usageId = getIntent().getIntExtra(USAGE_ID_PARAM, -1);
        discountType = (DiscountTypeViewModel) getIntent().getSerializableExtra(DISCOUNT_TYPE_PARAM);
        benefitName = getIntent().getStringExtra(BENEFIT_NAME);
        provider=getIntent().getStringExtra(PROVIDER_PARA);
        idProvider = getIntent().getIntExtra(ID_PROVIDER, -1);
    }

    @Override
    public void setUpView() {
        this.presenter.setView(this);
        sendToAnalytics();
        tvTitle1.setTextColor(ContextCompat.getColor(this, startFlv()));
        tvTitle2.setTextColor(ContextCompat.getColor(this, startFlv()));
        tvQuestion.setTextColor(ContextCompat.getColor(this, startFlv()));
        etComment.setBackground(getResources().getDrawable(borderFlv()));
        etComment.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE
                | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        AppUtils.setGradient(
                discountType == DISCOUNT ? getResources().getColor(R.color.green_start)
                        : getResources().getColor(R.color.orange_common),
                discountType == DISCOUNT ? getResources().getColor(R.color.green_end)
                        : getResources().getColor(R.color.orange_common),
                AppUtils.dpToPx((int) getResources().getDimension(R.dimen.size_thirteen)), btnSend);
        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(this);
        adapter = new RateOptionsAdapter(this, discountType, this);
        layoutManager.setFlexWrap(FlexWrap.WRAP);
        layoutManager.setJustifyContent(JustifyContent.FLEX_START);
        rvRateOptions.setLayoutManager(layoutManager);
        rvRateOptions.setAdapter(adapter);
        btnClose.setImageResource(closeFlv());
        rbScore.setOnRatingBarChangeListener((ratingBar, rating, fromUser) -> {
            loadRateOptions(rating);
        });

    }

    @Override
    public int getLayout() {
        return R.layout.activity_rate_discount;
    }

    @Override
    public void selectCalificationOption(DiscountCalificationOptionViewModel option) {
        this.selectedRateOption = option;
    }

    public int startFlv() {
        return resId(R.color.bg_green_start, R.color.bg_yellow_start);

    }

    public int borderFlv() {
        return resId(R.drawable.bg_white_green_corners_stroke_rate, R.drawable.bg_white_corners_yellow_stroke);
    }

    public int closeFlv() {
        return resId(R.drawable.ic_close_green, R.drawable.ic_close_orange);

    }

    private void sendToAnalytics(){
        Bundle bundle = new Bundle();
        bundle.putString(AnalyticsTags.Rate.Parameters.categoria_nombre, discountType.getValue());
        bundle.putString(AnalyticsTags.Rate.Parameters.beneficios_id, discountId.toString());
        bundle.putString(AnalyticsTags.Rate.Parameters.beneficios_nombre, AppUtils.getFirstNCharacters(benefitName));
        bundle.putString(AnalyticsTags.Rate.Parameters.beneficios_empresa, AppUtils.getFirstNCharacters(provider));
        bundle.putInt(AnalyticsTags.Discounts.Parameters.beneficios_empresa_id, idProvider);
        analyticsModule.sendToAnalytics(AnalyticsTags.Rate.Events.calificacion, bundle);
    }

    private void loadRateOptions(float rating) {
        llCommentBody.setVisibility(View.VISIBLE);
        if (rating >= 1 && !reviewForm.isEmpty()) {
            DiscountCalificationViewModel calification = reviewForm.get(((int) rating) - 1);
            adapter.addOptionList((ArrayList<DiscountCalificationOptionViewModel>) calification.getOptions());
            tvQuestion.setText(calification.question);
            if (calification.commentAvailable == 0) {
                etComment.setVisibility(View.GONE);
            } else {
                etComment.setVisibility(View.VISIBLE);
            }

        }

    }

    public int resId(int first, int second) {
        return (discountType == DISCOUNT) ? first : second;
    }

    @OnClick(R.id.img_close)
    public void onClosed() {
        onBackPressed();
        overridePendingTransition(R.anim.rigth_in, R.anim.right_out);
    }

    @OnClick(R.id.btn_send)
    public void onSend() {
        Integer selectedOptionId = null;
        if (selectedRateOption != null)
            selectedOptionId = selectedRateOption.idReviewOption;
        if (selectedOptionId != null) {
            presenter.doRateDiscount(discountType, discountId, usageId, (int) rbScore.getRating(),
                    selectedOptionId, etComment.getText().toString());
        } else {
            AppUtils.showErrorMessage(contentRate, this, getResources().getString(R.string.rate_discount_no_category));
        }

    }

    @Override
    public void showRateSuccess(Integer score) {
        sendSuccesRateToAnalytics(score);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment dialogFragment = getSupportFragmentManager().findFragmentByTag("dialog_discount_message");
        if (dialogFragment != null) {
            ft.remove(dialogFragment);
        }
        ft.addToBackStack(null);
        SuccessMessageDialogFragment newFragment = SuccessMessageDialogFragment.newInstance(SuccessMessageDialogFragment.Operation.RATE);
        newFragment.show(ft, "dialog_discount_message");

    }

    private void sendSuccesRateToAnalytics(Integer score){
        Bundle bundle = new Bundle();
        bundle.putString(AnalyticsTags.Rate.Parameters.categoria_nombre, discountType.getValue());
        bundle.putString(AnalyticsTags.Rate.Parameters.beneficios_id, discountId.toString());
        bundle.putString(AnalyticsTags.Rate.Parameters.beneficios_nombre, AppUtils.getFirstNCharacters(benefitName));
        bundle.putString(AnalyticsTags.Rate.Parameters.calificacion_categoria, selectedRateOption.description);
        bundle.putString(AnalyticsTags.Rate.Parameters.beneficios_empresa, AppUtils.getFirstNCharacters(provider));
        bundle.putString(AnalyticsTags.Rate.Parameters.contador_estrellas, score.toString());
        bundle.putInt(AnalyticsTags.Discounts.Parameters.beneficios_empresa_id, idProvider);
        analyticsModule.sendToAnalytics(AnalyticsTags.Rate.Events.calificacion_exitoso, bundle);
    }

    @Override
    public void onBackPressed() {
        AppUtils.hideKeyboard(this);
        super.onBackPressed();
        overridePendingTransition(R.anim.rigth_in, R.anim.right_out);
    }

    public void onResultDialog() {
        setResult(RESULT_OK);
        finish();
    }
}
