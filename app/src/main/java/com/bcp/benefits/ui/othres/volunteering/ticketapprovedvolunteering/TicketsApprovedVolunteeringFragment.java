package com.bcp.benefits.ui.othres.volunteering.ticketapprovedvolunteering;


import android.os.Bundle;
import android.view.View;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.bcp.benefits.R;
import com.bcp.benefits.ui.BaseFragment;
import com.bcp.benefits.viewmodel.TicketRequestVolunteeringViewModel;
import com.bcp.benefits.viewmodel.VolunteeringTicketViewModel;
import org.jetbrains.annotations.NotNull;
import java.util.ArrayList;
import butterknife.BindView;

import static com.bcp.benefits.util.Constants.TICKETS_DATA_APPROVED;

/**
 * Created by emontesinos on 02/05/19.
 **/
public class TicketsApprovedVolunteeringFragment extends BaseFragment {

    @BindView(R.id.rvApproved)
    RecyclerView rvApproved;
    @BindView(R.id.content_empty_list)
    ConstraintLayout contentEmptyList;
    private VolunteeringTicketViewModel volunteeringTicketViewModel;


    public static TicketsApprovedVolunteeringFragment newInstance(VolunteeringTicketViewModel volunteeringTicketViewModel) {
        TicketsApprovedVolunteeringFragment fragment = new TicketsApprovedVolunteeringFragment();
        Bundle args = new Bundle();
        args.putSerializable(TICKETS_DATA_APPROVED, volunteeringTicketViewModel);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            volunteeringTicketViewModel = (VolunteeringTicketViewModel) getArguments().getSerializable(TICKETS_DATA_APPROVED);
        }
    }

    @Override
    public void onViewCreated(@NotNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (volunteeringTicketViewModel.getListTicketApprove() != null && volunteeringTicketViewModel.getListTicketApprove().size() > 0)
            initRecycler();
        else
            contentEmptyList.setVisibility(View.VISIBLE);
    }


    @Override
    public void setUpView() {
    }

    @Override
    public int getLayout() {
        return R.layout.fragment_tickets_approved;
    }

    private void initRecycler() {
        ApprovedVolunteeringAdapter approvedAdapter = new ApprovedVolunteeringAdapter(getContext());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        rvApproved.setLayoutManager(mLayoutManager);
        rvApproved.setAdapter(approvedAdapter);
        approvedAdapter.addList((ArrayList<TicketRequestVolunteeringViewModel>) volunteeringTicketViewModel.getListTicketApprove());
    }

}
