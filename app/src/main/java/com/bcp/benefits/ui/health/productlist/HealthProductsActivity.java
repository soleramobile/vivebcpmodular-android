package com.bcp.benefits.ui.health.productlist;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bcp.benefits.analytics.AnalyticsModule;
import com.bcp.benefits.analytics.AnalyticsTags;
import com.google.android.material.appbar.AppBarLayout;
import com.bcp.benefits.R;
import com.bcp.benefits.ui.BaseActivity;
import com.bcp.benefits.ui.health.detailproduct.HealthProductDetailActivity;
import com.bcp.benefits.ui.health.nearyou.NearYouActivity;
import com.bcp.benefits.util.AppUtils;
import com.bcp.benefits.util.CallController;
import com.bcp.benefits.viewmodel.HealthProductViewModel;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by emontesinos on 22/05/19.
 **/

public class HealthProductsActivity extends BaseActivity implements
        HealthProductPresenter.HealthProductView, HealthProductAdapter.HealthProductItemClickListener {

    @Inject
    HealthProductPresenter presenter;
    @BindView(R.id.toolbar)
    AppBarLayout toolbar;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.title_toolbar)
    TextView titleToobal;
    @BindView(R.id.tv_names)
    TextView tvNames;
    @BindView(R.id.tv_lastName)
    TextView tvLastName;
    @BindView(R.id.rv_products)
    RecyclerView rvProducts;
    @BindView(R.id.tv_empty_list)
    TextView tvEmptyList;
    private CallController callController;
    private String healthPhone;
    private HealthProductAdapter adapter;

    private Boolean validClick = true;
    private Boolean  validClickItem = true;
    private AnalyticsModule analyticsModule = new AnalyticsModule(this);;

    public static void start(Context context) {
        Intent starter = new Intent(context, HealthProductsActivity.class);
        context.startActivity(starter);
    }

    @Override
    public void setUpView() {
        analyticsModule.sendToAnalytics(AnalyticsTags.Health.Events.salud, null);
        getToolbar();
        this.customRecycler();
        this.presenter.setView(this);
        callController = new CallController(this, null);

        if (AppUtils.isConnected(this)) {
            this.presenter.doGetUserInfo();
            this.presenter.doGetHealthPhone();
            this.presenter.doGetHealthProducts();
        } else {
            AppUtils.dialogIsConnect(this);
        }

    }

    private void customRecycler() {
        rvProducts.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        adapter = new HealthProductAdapter(this);
        rvProducts.setAdapter(adapter);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_health_products;
    }

    private void getToolbar() {
        titleToobal.setText(R.string.health_two);
        toolbar.setBackground(getResources().getDrawable(R.drawable.bg_health_blue));
        imgBack.setOnClickListener(v -> {
            onBackPressed();
            overridePendingTransition(R.anim.rigth_in, R.anim.right_out);
        });
    }

    @OnClick(R.id.btn_clinics_near_you)
    public void onBtnClinicsNearYouClicked() {
        if(validClick){
            validClick = false;
            NearYouActivity.start(this);
            overridePendingTransition(R.anim.left_in, R.anim.left_out);
        }
    }

    @OnClick(R.id.tv_callus)
    public void onTvCallusClicked() {
        if (healthPhone == null)
            return;
        callController.doCall(healthPhone);
    }

    @Override
    public void onHealthProductItemClick(HealthProductViewModel productModel) {
        if (productModel == null) return;
        if (validClickItem){
            validClickItem = false;
            if (productModel.getInUse()) {
                HealthProductDetailActivity.start(this, productModel);
              } else {
                AppUtils.openBrowser(this, productModel.getUrl());
           }
        }

    }

    @Override
    public void showNames(String name) {

        if (name != null && !name.isEmpty())
            tvNames.setText(name);
        else
            tvNames.setVisibility(View.GONE);

    }

    @Override
    public void showLastNames(String lastName) {
        if (lastName != null && !lastName.isEmpty())
            tvLastName.setText(lastName);
        else
            tvLastName.setVisibility(View.GONE);
    }

    @Override
    public void showHealthPhone(String phone) {
        healthPhone = phone;
    }

    @Override
    public void showHealthProducts(List<HealthProductViewModel> products) {
        tvEmptyList.setVisibility(View.GONE);
        adapter.setProducts(products);
    }


    @Override
    public void showEmptyHealthProducts() {
        tvEmptyList.setVisibility(View.VISIBLE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        callController.onRequestPermissionsResult(requestCode, grantResults);
    }


    @Override
    protected void onResume() {
        super.onResume();
        validClick = true;
        validClickItem = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        validClick = true;
        validClickItem = true;
    }
}
