package com.bcp.benefits.ui.discounts.rate;

import com.bcp.benefits.R;
import com.bcp.benefits.ui.MainView;
import com.bcp.benefits.viewmodel.DiscountTypeViewModel;

import javax.inject.Inject;

import pe.solera.benefit.main.usecases.executor.Action;
import pe.solera.benefit.main.usecases.usecases.DiscountUseCases;

public class RateDiscountPresenter {


    private RateDiscountView view;
    private DiscountUseCases discountUseCases;

    @Inject
    public RateDiscountPresenter(DiscountUseCases discountUseCases) {
        this.discountUseCases = discountUseCases;
    }

    public void setView(RateDiscountView view) {
        this.view = view;
    }


    void doRateDiscount(DiscountTypeViewModel discountTypeViewModel, Integer discountId, Integer usageId, Integer score, Integer rateOptionId, String comment) {
        if (this.view.validateInternet()) {
            this.view.showColor(discountTypeViewModel == DiscountTypeViewModel.DISCOUNT ? R.color.green_start : R.color.bg_yellow_start);
            this.view.showProgress();
            discountUseCases.rateDiscount(discountId, usageId, score, rateOptionId, comment, new Action.Callback<Boolean>() {
                @Override
                public void onSuccess(Boolean aBoolean) {
                    view.showRateSuccess(score);
                    view.hideProgress();
                }

                @Override
                public void onError(Throwable throwable) {
                    view.showErrorMessage(throwable.getMessage());
                    view.hideProgress();

                }
            });
        }

    }

    public interface RateDiscountView extends MainView {

        void showRateSuccess(Integer score);

    }
}
