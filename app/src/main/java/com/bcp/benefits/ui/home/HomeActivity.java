package com.bcp.benefits.ui.home;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.Intent;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bcp.benefits.R;
import com.bcp.benefits.analytics.AnalyticsModule;
import com.bcp.benefits.ui.BaseActivity;
import com.bcp.benefits.ui.campaign.CampaignActivity;
import com.bcp.benefits.ui.discounts.DiscountsActivity;
import com.bcp.benefits.ui.financial.FinancialActivity;
import com.bcp.benefits.ui.health.productlist.HealthProductsActivity;
import com.bcp.benefits.ui.home.comunication.CommunicationsActivity;
import com.bcp.benefits.ui.othres.DialogCustomGeneric;
import com.bcp.benefits.ui.othres.OtherActivity;
import com.bcp.benefits.ui.splash.SplashActivity;
import com.bcp.benefits.util.AppPreferences;
import com.bcp.benefits.util.AppUtils;
import com.bcp.benefits.viewmodel.CommunicationViewModel;
import com.bcp.benefits.viewmodel.DiscountTypeViewModel;
import com.bcp.benefits.viewmodel.HomeViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

import static com.bcp.benefits.util.AppUtils.homeModelFromJson;
import static com.bcp.benefits.util.Constants.DATE_START;
import static com.bcp.benefits.util.Constants.DURATION_ANIMATION;
import static com.bcp.benefits.util.Constants.IDENTIFIER_SESION_DIALOG;
import static com.bcp.benefits.util.Constants.ID_COMMUNICATION;
import static com.bcp.benefits.util.Constants.ID_DISCOUNTS;
import static com.bcp.benefits.util.Constants.ID_EDUCATION;
import static com.bcp.benefits.util.Constants.ID_FINANCE;
import static com.bcp.benefits.util.Constants.ID_HEALTH;
import static com.bcp.benefits.util.Constants.ID_OTHERS;
import static com.bcp.benefits.util.Constants.TOKEN_REGISTRADO;

/**
 * Created by ernestogaspard on 23/04/19.
 **/

public class HomeActivity extends BaseActivity implements HomePresenter.HomeView,
        HomeAdapter.HomeInterface {

    @BindView(R.id.img_profile)
    ImageView imgProfile;
    @BindView(R.id.txv_user_name)
    TextView txvUsername;
    @BindView(R.id.txv_user_description)
    TextView txvUserDescription;
    @BindView(R.id.rcv_home_items)
    RecyclerView rcvHomeItems;
    @BindView(R.id.iv_next_main_menu)
    ImageView imgNextCampaign;

    String communicationPush;

    private Boolean validateClick = false;
    private Boolean validateClickCampaign = false;
    private  Boolean validSesion = true;
    private boolean isBackground, showCommunications;
    private AppPreferences appPreferences;

    public static void start(Context context) {
        Intent starter = new Intent(context, HomeActivity.class);
        context.startActivity(starter);
    }

    public static void startPush(Context context, String communicationModel) {
        Intent starter = new Intent(context, HomeActivity.class);
        starter.putExtra(ID_COMMUNICATION, communicationModel);
        context.startActivity(starter);
    }

    @Inject
    HomePresenter presenter;


    @Override
    public void setUpView() {
        this.presenter.setView(this);
        imgNextCampaign.setEnabled(false);
        appPreferences = new AppPreferences(this);
        fillRecyclerWithData(homeModelFromJson(this));
        initAnimate();

        communicationPush = getIntent().getStringExtra(ID_COMMUNICATION);
        String tokenFireBase = displayFireBaseRegId();
        if (!tokenFireBase.isEmpty()) {
            presenter.doRegisterToken(tokenFireBase);
        }
        this.presenter.doGetUserFullName();
        this.presenter.doGetCommunications();
    }

    private void animUpAndDown(long time, boolean isUp) {
        imgNextCampaign.animate()
                .translationY(isUp ? -10 : 10)
                .setDuration(time)
                .setInterpolator(new DecelerateInterpolator())
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        animUpAndDown(time, !isUp);
                    }
                })
                .start();
    }

    public void initAnimate() {
        if (getResources().getString(R.string.validateAnimate).equals(getResources().getString(R.string.identifierAnimate))) {
            animUpAndDown(DURATION_ANIMATION, true);
        }
    }

    @Override
    public int getLayout() {
        return R.layout.activity_main;
    }

    private void fillRecyclerWithData(ArrayList<HomeViewModel> data) {
        rcvHomeItems.setLayoutManager(new LinearLayoutManager(this));
        RecyclerView.Adapter rcvAdapter = new HomeAdapter(this, data);
        rcvHomeItems.setAdapter(rcvAdapter);
    }

    @Override
    public void clickItem(HomeViewModel item) {
        if (!validateClick) {
            validateClick = true;
            switch (item.getId()) {
                case ID_DISCOUNTS:
                    DiscountsActivity.start(this, DiscountTypeViewModel.DISCOUNT);
                    overridePendingTransition(R.anim.left_in, R.anim.left_out);
                    break;
                case ID_EDUCATION:
                    DiscountsActivity.start(this, DiscountTypeViewModel.EDUTATION);
                    overridePendingTransition(R.anim.left_in, R.anim.left_out);
                    break;
                case ID_HEALTH:
                    HealthProductsActivity.start(this);
                    overridePendingTransition(R.anim.left_in, R.anim.left_out);
                    break;
                case ID_FINANCE:
                    FinancialActivity.start(this);
                    overridePendingTransition(R.anim.left_in, R.anim.left_out);
                    break;
                case ID_OTHERS:
                    OtherActivity.start(this);
                    overridePendingTransition(R.anim.left_in, R.anim.left_out);
                    break;
            }
        }

    }

    @OnClick(R.id.img_profile)
    public void onClickSignOff() {
        if (validSesion) {
            DialogCustomGeneric dp = DialogCustomGeneric.newInstance(IDENTIFIER_SESION_DIALOG);
            dp.show(getSupportFragmentManager(), DATE_START);
            validSesion = false;
        }
    }

    @OnClick(R.id.iv_next_main_menu)
    public void nextCampaign() {
        if (!validateClickCampaign) {
            validateClickCampaign = true;
            CampaignActivity.start(this);
            overridePendingTransition(R.anim.slide_out_up, R.anim.slide_out_down);
        }
    }

    public void dialogSignOff() {
        this.presenter.doLogout();
    }

    @Override
    public void goToLogin() {
        SplashActivity.start(this);
        finish();
    }

    public void updateFlagSesion(){
        validSesion = true;
    }

    @Override
    public void showUserFullName(String name) {
        txvUsername.setText(name);
    }

    @Override
    public void showCommunications(List<CommunicationViewModel> communications) {
        imgNextCampaign.setEnabled(true);
        ArrayList<CommunicationViewModel> ordenado = AppUtils.encontrarPrimeroCommunicatons((ArrayList<CommunicationViewModel>) communications, appPreferences, communicationPush);
        if (!ordenado.isEmpty()) {
            CommunicationsActivity.start(this, ordenado);
            overridePendingTransition(R.anim.slide_out_up, R.anim.slide_out_down);
            showCommunications = true;
        } else {
            showCommunications = false;
        }
    }

    @Override
    public void saveRegisterToken(Boolean exito) {
        appPreferences.savePreference(TOKEN_REGISTRADO, exito);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        communicationPush = intent.getStringExtra("idCommunication");
        super.onNewIntent(intent);
    }

    private String displayFireBaseRegId() {
        return appPreferences.getString("regId");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        validateClick = false;
        validateClickCampaign = false;
        showCommunications = false;
        if (!isBackground) {
            communicationPush = null;
            presenter.doGetCommunications();
        }
        isBackground = false;
    }

    @Override
    protected void onStop() {
        super.onStop();
        validateClick = false;
        validateClickCampaign = false;
        if (showCommunications) {
            isBackground = true;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        isBackground = false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        moveTaskToBack(true);
    }
}
