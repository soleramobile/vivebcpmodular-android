package com.bcp.benefits.ui.othres.assistanceconfirmation.confirmation;

import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bcp.benefits.R;
import com.bcp.benefits.ui.BaseActivity;
import com.bcp.benefits.ui.othres.assistanceconfirmation.ConfirmationVolunteeringPresenter;
import com.bcp.benefits.ui.othres.assistanceconfirmation.confirmation.adapter.CollaboratorAdapter;
import com.bcp.benefits.ui.othres.assistanceconfirmation.confirmation.dialog.ConfirmationDialogFragment;
import com.bcp.benefits.ui.othres.assistanceconfirmation.confirmation.dialog.DialogTCommentary;
import com.bcp.benefits.ui.othres.assistanceconfirmation.listcapaignvolunteeting.ListCampaignActivity;
import com.bcp.benefits.util.AppUtils;
import com.bcp.benefits.viewmodel.CampaignVolunteeringViewModel;
import com.bcp.benefits.viewmodel.CollaboratorViewModel;
import com.bcp.benefits.viewmodel.ListCollaboratorViewModel;
import com.bcp.benefits.viewmodel.LocalVolunteeringViewModel;
import com.google.android.material.appbar.AppBarLayout;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

import static com.bcp.benefits.util.Constants.DATA_CAMPAIGN_VOLUNTEERING;
import static com.bcp.benefits.util.Constants.DATA_LOCAL_VOLUNTEERING;
import static com.bcp.benefits.util.Constants.DATE_START;
import static com.bcp.benefits.util.Constants.EMPTY;

/**
 * Created by emontesinos on 13/01/20.
 */

public class ConfirmationVolunteeringActivity extends BaseActivity implements ConfirmationVolunteeringView, CollaboratorAdapter.OnItemClickCollaboratorListener, ConfirmationDialogFragment.ConfirmationListener,
        TextWatcher {


    @Inject
    ConfirmationVolunteeringPresenter presenter;

    @BindView(R.id.img_back)
    ImageButton imgBack;
    @BindView(R.id.title_toolbar)
    TextView titleToolbar;
    @BindView(R.id.toolbar)
    AppBarLayout toolbar;

    @BindView(R.id.rv_collaborator)
    RecyclerView rvCollaborator;

    @BindView(R.id.txt_title_campaign)
    TextView txtTitleCampaign;
    @BindView(R.id.txt_sub_descriptin)
    TextView txtSubDescription;

    @BindView(R.id.ed_search)
    AppCompatEditText edSearch;
    @BindView(R.id.contentListEmpty)
    ConstraintLayout contentListEmpty;

    @BindView(R.id.contentConfirmation)
    ConstraintLayout contentConfirmation;

    @BindView(R.id.btnConfirmAssistance)
    Button btnConfirmAssistance;

    @BindView(R.id.swipe_Refresh_Collaborator)
    SwipeRefreshLayout swipeRefreshCollaborator;
    @BindView(R.id.img_close_search_v)
    ImageView imgCloseSearch;

    private String textSearch = EMPTY;
    private CollaboratorAdapter collaboratorAdapter;
    private LocalVolunteeringViewModel data;


    private ListCollaboratorViewModel modelDefault;
    private CampaignVolunteeringViewModel dataCampaign;
    private int positionItem = 0;
    private String closeAttendance;
    private String message;
    private Integer idVolunteerType;

    public static void start(Context context, LocalVolunteeringViewModel data, Integer idVolunteerType, CampaignVolunteeringViewModel campaignVolunteeringSelectedUpdate, String closeAttendance) {
        Intent intent = new Intent(context, ConfirmationVolunteeringActivity.class);
        intent.putExtra(DATA_LOCAL_VOLUNTEERING, data);
        intent.putExtra(DATA_CAMPAIGN_VOLUNTEERING, campaignVolunteeringSelectedUpdate);
        intent.putExtra("idVolunteerType", idVolunteerType);
        intent.putExtra("closeAttendance", closeAttendance);
        context.startActivity(intent);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_confirmation_volunteering;
    }

    @Override
    public void setUpView() {
        getToolbar();
        this.presenter.setView(this);

        edSearch.addTextChangedListener(this);
        data = (LocalVolunteeringViewModel) getIntent().getSerializableExtra(DATA_LOCAL_VOLUNTEERING);
        dataCampaign = (CampaignVolunteeringViewModel) getIntent().getSerializableExtra(DATA_CAMPAIGN_VOLUNTEERING);
        closeAttendance = (String) getIntent().getSerializableExtra("closeAttendance");
        idVolunteerType = (Integer) getIntent().getSerializableExtra("idVolunteerType");
        txtTitleCampaign.setText(dataCampaign.getName() + "-" + data.getName());
        txtSubDescription.setText(data.getDate());
        rvCollaborator.setLayoutManager(new LinearLayoutManager(this));
        collaboratorAdapter = new CollaboratorAdapter(this, closeAttendance);
        rvCollaborator.setAdapter(collaboratorAdapter);
        collaboratorAdapter.setListener(this);
        if (AppUtils.isConnected(this)) {
            this.presenter.getListCollaboratorsVolunteering(String.valueOf(data.getId()),idVolunteerType, textSearch, true);
        } else {
            contentListEmpty.setVisibility(View.VISIBLE);
            rvCollaborator.setVisibility(View.GONE);
            showErrorConnect(getResources().getString(R.string.exception_no_internet_error));
        }

        if (closeAttendance.equals("1")) {
            btnConfirmAssistance.setEnabled(false);
            AppUtils.setGradient(getResources().getColor(R.color.gray), getResources().getColor(R.color.gray),
                    AppUtils.dpToPx((int) getResources().getDimension(R.dimen.size_thirteen)), btnConfirmAssistance);
        }

        swipeRefreshCollaborator.setOnRefreshListener(() -> this.presenter.getListCollaboratorsVolunteering(String.valueOf(data.getId()), idVolunteerType, textSearch, true)
        );
    }

    public void getToolbar() {
        this.presenter.setView(this);
        titleToolbar.setText(R.string.confirmation);
        toolbar.setBackground(getResources().getDrawable(R.drawable.bg_pink_confirmation));
        imgBack.setOnClickListener(v -> {
            onBackPressed();
            overridePendingTransition(R.anim.rigth_in, R.anim.right_out);
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (closeAttendance.equals("1")) {
            btnConfirmAssistance.setEnabled(false);
            AppUtils.setGradient(getResources().getColor(R.color.gray), getResources().getColor(R.color.gray),
                    AppUtils.dpToPx((int) getResources().getDimension(R.dimen.size_thirteen)), btnConfirmAssistance);
        }
    }

    @Override
    public void showSuccessAccept(Boolean response) {
        filterListCollaborator(String.valueOf(data.getId()), idVolunteerType, textSearch, false);
    }

    @Override
    public void showListCollaboratorsVolunteering(List<ListCollaboratorViewModel> collaboratorViewModels) {
        swipeRefreshCollaborator.setRefreshing(false);
        if (collaboratorViewModels.isEmpty()) {
            contentListEmpty.setVisibility(View.VISIBLE);
            rvCollaborator.setVisibility(View.GONE);
            btnConfirmAssistance.setVisibility(View.GONE);
        } else {
            contentListEmpty.setVisibility(View.GONE);
            rvCollaborator.setVisibility(View.VISIBLE);
            collaboratorAdapter.addList((collaboratorViewModels));
        }

    }

    @Override
    public void showMessage(String message) {
        this.message = message;
    }

    @Override
    public void showErrorService() {
        if (!rvCollaborator.isComputingLayout())
            collaboratorAdapter.modifyItem(positionItem, modelDefault);
    }

    @Override
    public void showErrorServiceList() {
        swipeRefreshCollaborator.setRefreshing(false);
        contentListEmpty.setVisibility(View.VISIBLE);
        rvCollaborator.setVisibility(View.GONE);

    }

    @Override
    public void showStatusAssistance(Boolean status) {
        if (status)
            AppUtils.showErrorMessageSucces(contentConfirmation, this, "Asistencia de voluntariado cerrada.");
        else
            AppUtils.showErrorMessageSucces(contentConfirmation, this, "No tiene permisos para solicitar este servicio");
    }

    @Override
    public void onItemCollaboratorClick(ListCollaboratorViewModel entity, int value, int position) {
        modelDefault = entity;
        positionItem = position;
        presenter.collaboratorsConfirmation(String.valueOf(entity.getIdVolunteerCollaborator()), String.valueOf(value), entity.getCommentary());
    }

    @Override
    public void onItemCommentaryClick(ListCollaboratorViewModel entity, int value, int position) {
        modelDefault = entity;
        positionItem = position;
        DialogTCommentary dp = DialogTCommentary.newInstance(entity);
        dp.show(getSupportFragmentManager(), DATE_START);
    }


    public void sendComment(ListCollaboratorViewModel model, String commentary) {
        presenter.collaboratorsConfirmation(String.valueOf(model.getIdVolunteerCollaborator()), String.valueOf(model.getAttendance()), commentary);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        textSearch = s.toString();
        if (s.toString().length() < 1) {
            filterListCollaborator(String.valueOf(data.getId()), idVolunteerType, EMPTY, false);
            imgCloseSearch.setVisibility(View.GONE);

        } else {
            filterListCollaborator(String.valueOf(data.getId()), idVolunteerType, textSearch, false);
            imgCloseSearch.setVisibility(View.VISIBLE);
        }

    }

    @OnClick(R.id.img_close_search_v)
    public void onDeleteSearch() {
        filterListCollaborator(String.valueOf(data.getId()), idVolunteerType, EMPTY, false);
        edSearch.setText("");

    }

    @OnClick(R.id.btnConfirmAssistance)
    public void showConfirmationDialog() {
        ConfirmationDialogFragment dialog = ConfirmationDialogFragment.newInstance(message, this);
        dialog.show(getSupportFragmentManager(), "Second confirmation");
    }

    private void filterListCollaborator(String idVolunteering, Integer idVolunteerType, String search, Boolean progress) {
        this.presenter.getListCollaboratorsVolunteering(idVolunteering, idVolunteerType, search, progress);
    }

    @Override
    public void click(boolean click) {
        if (click) {
            presenter.getStatusAssistance(String.valueOf(data.getIdVolunteer()), "1");
            btnConfirmAssistance.setEnabled(false);
            AppUtils.setGradient(getResources().getColor(R.color.gray), getResources().getColor(R.color.gray),
                    AppUtils.dpToPx((int) getResources().getDimension(R.dimen.size_thirteen)), btnConfirmAssistance);
            ListCampaignActivity.start(this);
        }
    }
}
