package com.bcp.benefits.ui.discounts.consume;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Base64;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;

import com.bcp.benefits.R;
import com.bcp.benefits.analytics.AnalyticsModule;
import com.bcp.benefits.analytics.AnalyticsTags;
import com.bcp.benefits.components.CustomTypefaceSpan;
import com.bcp.benefits.ui.BaseActivity;
import com.bcp.benefits.ui.discounts.rate.RateDiscountActivity;
import com.bcp.benefits.util.AppUtils;
import com.bcp.benefits.util.Constants;
import com.bcp.benefits.viewmodel.ConsumedDiscountViewModel;
import com.bcp.benefits.viewmodel.DiscountCalificationViewModel;
import com.bcp.benefits.viewmodel.DiscountTypeViewModel;
import com.bcp.benefits.viewmodel.UserViewModel;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

import static com.bcp.benefits.util.Constants.CAPTION_PARAM;
import static com.bcp.benefits.util.Constants.CAPTION_VALUE_PARAM;
import static com.bcp.benefits.util.Constants.DISCOUNT_ID_PARAM;
import static com.bcp.benefits.util.Constants.DISCOUNT_TYPE_PARAM;
import static com.bcp.benefits.util.Constants.ID_PROVIDER;
import static com.bcp.benefits.util.Constants.PROVIDER_PARAM;
import static com.bcp.benefits.util.Constants.SUBSIDIARY_ID_PARAM;
import static com.bcp.benefits.viewmodel.DiscountTypeViewModel.DISCOUNT;
import static com.bcp.benefits.viewmodel.DiscountTypeViewModel.EDUTATION;

public class ConsumeDiscountActivity extends BaseActivity implements ConsumePresenter.ConsumeView {

    @Inject
    ConsumePresenter presenter;


    @BindView(R.id.flContent)
    ConstraintLayout rlRoot;
    @BindView(R.id.tv_caption)
    TextView tvCaption;
    @BindView(R.id.tv_provider)
    TextView tvProvider;
    @BindView(R.id.tv_fullname)
    TextView tvFullname;
    @BindView(R.id.tv_doc_number)
    TextView tvDocNumber;
    @BindView(R.id.iv_barcode)
    ImageView ivBarcode;
    @BindView(R.id.tv_dni)
    TextView tvDni;
    @BindView(R.id.btn_rate)
    TextView btnRate;
    @BindView(R.id.ll_url_usage)
    LinearLayout llUrlUsage;
    @BindView(R.id.tv_usage_message)
    TextView tvUsageMessage;
    @BindView(R.id.btn_usage_action)
    TextView btnUsageAction;
    @BindView(R.id.iv_close)
    ImageView btnClose;

    private Integer discountId;
    private ConsumedDiscountViewModel consumedDiscountModel;
    private DiscountTypeViewModel discountType;
    private String provider;

    String captionValue;
    String caption;
    Integer subsidiaryId;
    private Boolean validClick = true;
    private int idProvider;

    private AnalyticsModule analyticsModule = new AnalyticsModule(this);

    public static void start(Activity context, DiscountTypeViewModel discountType, Integer discountId,
                             Integer subsidiaryId, String captionValue, String caption, int idProvider, String provider) {
        Intent starter = new Intent(context, ConsumeDiscountActivity.class);
        starter.putExtra(DISCOUNT_ID_PARAM, discountId);
        starter.putExtra(SUBSIDIARY_ID_PARAM, subsidiaryId);
        starter.putExtra(CAPTION_VALUE_PARAM, captionValue);
        starter.putExtra(CAPTION_PARAM, caption);
        starter.putExtra(PROVIDER_PARAM, provider);
        starter.putExtra(DISCOUNT_TYPE_PARAM, discountType);
        starter.putExtra(ID_PROVIDER, idProvider);
        context.startActivityForResult(starter, 400);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        this.loadData();
        setTheme(discountType == EDUTATION ? R.style.AppThemeEducation : R.style.ConsumeDiscount);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setUpView() {
        this.presenter.setView(this);
        getDescriptionCaptionTextCustom();
        btnClose.setImageDrawable(getResources().getDrawable(closeFlv()));
        tvProvider.setText(provider);
        buttonFlv();
        rlRoot.setBackground(getResources().getDrawable(gradientBgFlv()));
        tvFullname.setTextColor(ContextCompat.getColor(this, startFlv()));
        tvProvider.setTextColor(ContextCompat.getColor(this, startFlv()));

        presenter.doGetUserInfo();
        presenter.doConsumeDiscount(discountId, subsidiaryId);
        updateBackgroundColor();
    }

    private void sentToAnalytics() {
        if (discountType == DISCOUNT) {
            sendDiscountToAnalytics();
        } else {
            sendEducationToAnalytics();
        }
    }

    private void sendDiscountToAnalytics() {
        Bundle bundle = new Bundle();
        bundle.putString(AnalyticsTags.Discounts.Parameters.beneficios_id, String.valueOf(discountId));
        bundle.putString(AnalyticsTags.Discounts.Parameters.beneficios_nombre, AppUtils.getFirstNCharacters(caption));
        bundle.putString(AnalyticsTags.Discounts.Parameters.beneficios_empresa, AppUtils.getFirstNCharacters(provider));
        bundle.putString(AnalyticsTags.Discounts.Parameters.beneficios_tipo, consumedDiscountModel.idType.getName());
        bundle.putInt(AnalyticsTags.Discounts.Parameters.beneficios_empresa_id, idProvider);
        analyticsModule.sendToAnalytics(AnalyticsTags.Discounts.Events.descuento_exitoso, bundle);
    }

    private void sendEducationToAnalytics() {
        Bundle bundle = new Bundle();
        bundle.putString(AnalyticsTags.Education.Parameters.beneficios_id, String.valueOf(discountId));
        bundle.putString(AnalyticsTags.Education.Parameters.beneficios_nombre, AppUtils.getFirstNCharacters(caption));
        bundle.putString(AnalyticsTags.Education.Parameters.beneficios_empresa, AppUtils.getFirstNCharacters(provider));
        bundle.putString(AnalyticsTags.Education.Parameters.beneficios_tipo, consumedDiscountModel.idType.getName());
        analyticsModule.sendToAnalytics(AnalyticsTags.Education.Events.educacion_exitoso, bundle);
    }

    private void loadData() {
        discountType = (DiscountTypeViewModel) getIntent().getSerializableExtra(DISCOUNT_TYPE_PARAM);
        discountId = getIntent().getIntExtra(DISCOUNT_ID_PARAM, 0);
        subsidiaryId = getIntent().getIntExtra(SUBSIDIARY_ID_PARAM, 0);
        provider = getIntent().getStringExtra(PROVIDER_PARAM);
        captionValue = getIntent().getStringExtra(CAPTION_VALUE_PARAM);
        caption = getIntent().getStringExtra(CAPTION_PARAM);
        idProvider = getIntent().getIntExtra(ID_PROVIDER, -1);
    }

    private void updateBackgroundColor() {
        AppUtils.setGradient(
                discountType == DISCOUNT ? getResources().getColor(R.color.green_start)
                        : getResources().getColor(R.color.orange_common),
                discountType == DISCOUNT ? getResources().getColor(R.color.green_end)
                        : getResources().getColor(R.color.orange_common),
                AppUtils.dpToPx((int) getResources().getDimension(R.dimen.size_thirteen)), btnRate);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_consume_discount;
    }

    private void getDescriptionCaptionTextCustom() {
        int lengthValue = 0;
        if (captionValue != null) {
            lengthValue = captionValue.length();
        }

        StringBuilder sbCaption = new StringBuilder();

        if (captionValue != null && !captionValue.isEmpty()) {
            sbCaption.append(captionValue);
        }

        if (caption != null && !caption.isEmpty()) {
            if (captionValue != null && !captionValue.isEmpty())
                sbCaption.append(Constants.SPACE);
            sbCaption.append(caption);
        }

        String completeCaption = sbCaption.toString();
        Typeface font = ResourcesCompat.getFont(this, R.font.breviabold);
        SpannableString spannableString = new SpannableString(completeCaption);
        spannableString.setSpan(new CustomTypefaceSpan(font), 0, lengthValue, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        int color = startFlv();
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, color)), 0, lengthValue, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        tvCaption.setText(spannableString);


    }

    private void doUsageAction(int idType, String url) {
        llUrlUsage.setVisibility(View.VISIBLE);
        ivBarcode.setVisibility(View.GONE);
        tvUsageMessage.setText(String.format(getString(R.string.consume_discount_usage_message), provider));
        btnUsageAction.setOnClickListener(view -> {
            if (idType == 3) {
                ConsumeDiscountPdfViewActivity.start(this, discountType, url, provider);
            } else if (idType == 4) {
                AppUtils.openBrowser(this, url);
            }
        });
    }

    @Override
    public void showConsumeSuccess(ConsumedDiscountViewModel consumedDiscount) {
        consumedDiscountModel = consumedDiscount;
        sentToAnalytics();
        switch (consumedDiscount.idType.getTypeId()) {
            case 1:
                tvFullname.setTextSize(TypedValue.COMPLEX_UNIT_SP, 22f);
                tvFullname.setGravity(Gravity.CENTER_HORIZONTAL);
                tvDocNumber.setVisibility(View.GONE);
                tvDni.setVisibility(View.VISIBLE);
                ivBarcode.setVisibility(View.GONE);
                break;
            case 2:
                ivBarcode.setVisibility(View.VISIBLE);
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = false;
                options.inPreferredConfig = Bitmap.Config.RGB_565;
                options.inDither = true;
                options.outHeight = 48;
                options.outWidth = 48;
                String barcode = consumedDiscount.barCode.replace("data:image/png;base64,", "");
                byte[] decodedString = Base64.decode(barcode, Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length, options);
                if (decodedByte != null) {
                    ivBarcode.setImageBitmap(Bitmap.createScaledBitmap(decodedByte, ivBarcode.getWidth(), ivBarcode.getHeight(), false));
                }
                break;
            default:
                ivBarcode.setVisibility(View.GONE);
                if (consumedDiscount.usageUrl != null) {
                    doUsageAction(consumedDiscount.idType.getTypeId(), consumedDiscount.usageUrl);
                }
                break;
        }
    }

    @Override
    public void showUserInfo(UserViewModel user) {
        StringBuilder sbFullname = new StringBuilder();
        if (user.name != null && !user.name.isEmpty())
            sbFullname.append(user.name);

        if (user.firstSurname != null && !user.firstSurname.isEmpty()) {
            sbFullname.append(Constants.SPACE);
            sbFullname.append(user.firstSurname);
        }

        if (user.secondSurname != null && !user.secondSurname.isEmpty()) {
            sbFullname.append(Constants.SPACE);
            sbFullname.append(user.secondSurname);
        }

        tvFullname.setText(sbFullname.toString());

        if (user.documentNumber.length() == 8) {
            // tvDni.setText("DNI " + user.documentNumber);
            tvDni.setText(user.documentNumber);
            tvDocNumber.setText("DNI " + user.documentNumber);
        } else {
            tvDni.setText("CE " + user.documentNumber);
            //tvDocNumber.setText("CE " + user.documentNumber);
            tvDocNumber.setText(user.documentNumber);
        }
    }

    @OnClick(R.id.iv_close)
    public void onIvCloseClicked() {
        onBackPressed();
        overridePendingTransition(R.anim.rigth_in, R.anim.right_out);
    }

    @OnClick(R.id.btn_rate)
    public void onBtnRateClicked() {
        if (consumedDiscountModel == null || consumedDiscountModel.idUsage == null)
            return;
        if (validClick) {
            RateDiscountActivity.start(this, discountType, discountId, consumedDiscountModel.idUsage, (ArrayList<DiscountCalificationViewModel>) consumedDiscountModel.reviewForm, caption, idProvider, provider);
            validClick = false;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 500) {
            setResult(RESULT_OK);
            finish();
        }


    }

    public int resId(int first, int second) {
        return (discountType == DISCOUNT) ? first : second;
    }

    public int startFlv() {
        return resId(R.color.green_end, R.color.bg_yellow_start);
    }

    public int gradientBgFlv() {
        return resId(R.drawable.bg_consume_discount, R.drawable.bg_yellow_discount);
    }

    public int buttonFlv() {
        return resId(R.drawable.bg_green_button, R.drawable.bg_button_discount_yellow);
    }

    public int closeFlv() {
        return resId(R.drawable.ic_close_green, R.drawable.ic_close_orange);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        validClick = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        validClick = true;
    }
}
