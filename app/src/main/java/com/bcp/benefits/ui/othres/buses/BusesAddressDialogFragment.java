package com.bcp.benefits.ui.othres.buses;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bcp.benefits.R;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pe.solera.benefit.main.entity.Address;

public class BusesAddressDialogFragment extends DialogFragment implements BusAddressAdapter.AddressSelected {
    private List<Address> addressList;
    private AddressType addressType;

    @BindView(R.id.constraintLayout9)
    View contentHeader;
    @BindView(R.id.text_title_generic)
    TextView textTitle;
    @BindView(R.id.iv_close)
    ImageView imageClose;
    @BindView(R.id.rv_type)
    RecyclerView recyclerAddress;
    private BusAddressAdapter addressAdapter;
    private AddressClickListener addressClickListener;
    private Address addressSelected;

    private BusesAddressDialogFragment(List<Address> addressList, AddressType addressType,
                                       AddressClickListener addressClickListener,
                                       Address addressSelected) {
        this.addressList = addressList;
        this.addressType = addressType;
        this.addressClickListener = addressClickListener;
        this.addressSelected = addressSelected;
    }

    public static BusesAddressDialogFragment newInstance(List<Address> addressList,
                                                         AddressType addressType,
                                                         AddressClickListener addressClickListener,
                                                         Address addressSelected) {
        return new BusesAddressDialogFragment(addressList, addressType, addressClickListener,
                addressSelected);
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            Drawable backgroundColor = new ColorDrawable(ContextCompat.getColor(
                    Objects.requireNonNull(getContext()), android.R.color.transparent));
            Objects.requireNonNull(dialog.getWindow()).setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            dialog.getWindow().setBackgroundDrawable(backgroundColor);
        }
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_identy_simple, container, false);
        ButterKnife.bind(this, view);
        this.setUpView();
        return view;
    }

    @OnClick(R.id.iv_close)
    public void clickClose() {
        dismiss();
    }

    private void setUpView() {
        contentHeader.setBackgroundResource(R.drawable.bg_buses_dialog_blue);
        textTitle.setText(addressType == AddressType.START ? R.string.select_start_address : R.string.select_end_address);
        imageClose.setImageDrawable(this.getContext().getDrawable(R.drawable.ic_close_buses_blue));
        setUpRecyclerView();
    }

    private void setUpRecyclerView() {
        addressAdapter = new BusAddressAdapter(this);
        recyclerAddress.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerAddress.setAdapter(addressAdapter);
        addressAdapter.setAddressList(addressList, addressSelected != null ? getPosition() : -1);
    }

    private int getPosition() {
        for (Address address : addressList) {
            if (address.getIdSubsidiary().equals(addressSelected.getIdSubsidiary()))
                return addressList.indexOf(address);
        }
        return -1;
    }

    @Override
    public void selectedAddress(Address address) {
        if (addressType == AddressType.START)
            addressClickListener.selectedAddress(address);
        else
            addressClickListener.selectedArrivalAddress(address);
        this.dismiss();
    }

    public interface AddressClickListener {
        void selectedAddress(Address address);

        void selectedArrivalAddress(Address address);
    }

    public enum AddressType {
        START, END
    }
}
