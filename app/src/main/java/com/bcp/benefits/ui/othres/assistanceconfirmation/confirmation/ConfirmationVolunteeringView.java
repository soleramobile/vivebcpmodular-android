package com.bcp.benefits.ui.othres.assistanceconfirmation.confirmation;

import com.bcp.benefits.ui.MainView;
import com.bcp.benefits.viewmodel.CollaboratorViewModel;
import com.bcp.benefits.viewmodel.ListCollaboratorViewModel;

import java.util.List;

/**
 * Created by emontesinos on 13/01/20.
 */

public interface ConfirmationVolunteeringView extends MainView {
    void showSuccessAccept(Boolean response);

    void showListCollaboratorsVolunteering(List<ListCollaboratorViewModel> collaboratorViewModels);

    void showMessage(String message);

    void showErrorService();

    void showErrorServiceList();

    void showStatusAssistance(Boolean status);
}
