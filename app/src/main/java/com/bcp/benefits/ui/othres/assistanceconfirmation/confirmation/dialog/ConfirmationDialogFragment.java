package com.bcp.benefits.ui.othres.assistanceconfirmation.confirmation.dialog;
/* 
   Created by: Flavia Figueroa
               07/02/2020
         Solera Mobile
*/


import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.bcp.benefits.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ConfirmationDialogFragment extends DialogFragment {
    @BindView(R.id.etMessage)
    TextView etMessage;

    private ConfirmationListener listener;
    private String message;

    public static ConfirmationDialogFragment newInstance(String message, ConfirmationListener listener) {

        Bundle args = new Bundle();
        args.putString("message", message);
        ConfirmationDialogFragment fragment = new ConfirmationDialogFragment();
        fragment.listener = listener;
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.layout_dialog_confirm_assistance, container, false);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable((Color.TRANSPARENT)));
        ButterKnife.bind(this, v);
        message = getArguments().getString("message");

        etMessage.setText(message);
        return v;
    }

    @OnClick(R.id.imgBtnClose)
    void dismissDialog(){
        dismiss();
    }

    @OnClick(R.id.btnReConfirm)
    void confirm(){
        listener.click(true);
        dismiss();
    }

    public interface ConfirmationListener{
        void click(boolean click);
    }
}
