package com.bcp.benefits.ui.campaign;

import android.content.Context;

import androidx.annotation.NonNull;

import pe.solera.benefit.main.entity.Campaign;
import pe.solera.benefit.main.usecases.executor.Action;
import pe.solera.benefit.main.usecases.usecases.CampaignUseCase;

import java.util.List;

import javax.inject.Inject;

import com.bcp.benefits.ui.MainView;
import com.bcp.benefits.viewmodel.CampaignViewModel;

/**
 * Created by emontesinos on 13/05/19.
 **/

public class CampaignPresenter {

    private CampaignView view;
    private Context context;

    private final CampaignUseCase campaignUseCase;

    @Inject
    public CampaignPresenter(CampaignUseCase campaignUseCase,Context context) {
        this.campaignUseCase = campaignUseCase;
        this.context=context;
    }

    public void setView(@NonNull CampaignView view) {
        this.view = view;
    }

    void getCampaigns() {
        if (this.view.validateInternet()) {
            this.view.showProgress();
            campaignUseCase.getCampaigns(new Action.Callback<List<Campaign>>() {
                @Override
                public void onSuccess(List<Campaign> campaigns) {
                    if (!campaigns.isEmpty()) {
                        view.showCampaign(CampaignViewModel.toCampaignList(campaigns));
                        view.hideProgress();
                    } else {
                        view.showEmptyList();
                        view.hideProgress();
                    }

                }

                @Override
                public void onError(Throwable throwable) {
                    view.showErrorMessage(throwable.getMessage());
                    view.hideProgress();
                }
            });
        }
    }

    public interface CampaignView extends MainView {
        void showCampaign(List<CampaignViewModel> campaignModels);

        void showEmptyList();
    }
}
