package com.bcp.benefits.ui.othres;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

import com.bcp.benefits.R;
import com.bcp.benefits.ui.home.HomeActivity;
import com.bcp.benefits.ui.othres.maternity.MaternityActivity;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.bcp.benefits.util.Constants.IDENTIFIER_DIALOG;
import static com.bcp.benefits.util.Constants.IDENTIFIER_SESION_DIALOG;
import static com.bcp.benefits.util.Constants.IDENTIFIR_MOTHER_DIALOG;
/**
 * Created by emontesinos on 13/05/19.
 **/

public class DialogCustomGeneric extends DialogFragment {

    public static DialogCustomGeneric newInstance(String dialogIdentifier) {
        DialogCustomGeneric fragment = new DialogCustomGeneric();
        Bundle args = new Bundle();
        args.putString(IDENTIFIER_DIALOG, dialogIdentifier);
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.btn_register_generic)
    TextView btnAcept;
    @BindView(R.id.img_close_generic)
    ImageView imgClose;
    @BindView(R.id.text_title_generic)
    TextView title;
    @BindView(R.id.txt_negative)
    TextView btnNegative;
    @BindView(R.id.content_dialog)
    ConstraintLayout contentDialog;

    private String identifierDialog;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_custom_generic, container, false);
        ButterKnife.bind(this, v);
        assert getArguments() != null;
        identifierDialog = getArguments().getString(IDENTIFIER_DIALOG);
        dialogSelect();
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow()
                .getAttributes().windowAnimations = R.style.DialogTheme;
    }

    @NotNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        Objects.requireNonNull(dialog.getWindow()).requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            Drawable backgroundColor = new ColorDrawable(ContextCompat.getColor
                    (Objects.requireNonNull(getContext()), android.R.color.transparent));
            Objects.requireNonNull(dialog.getWindow()).setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            dialog.getWindow().setBackgroundDrawable(backgroundColor);
        }
    }

    @OnClick({R.id.img_close_generic,R.id.txt_negative})
    public void onDialogDismiss() {
        dismiss();
        ((HomeActivity) Objects.requireNonNull(getActivity())).updateFlagSesion();
    }

    @OnClick(R.id.btn_register_generic)
    public void onRegister() {
        switch (identifierDialog){
            case IDENTIFIR_MOTHER_DIALOG:
                MaternityActivity.start(getContext());
                break;
            case IDENTIFIER_SESION_DIALOG:
                ((HomeActivity) Objects.requireNonNull(getActivity())).dialogSignOff();
                dismiss();
                break;
        }
    }

    private void dialogSelect(){
        switch (identifierDialog){
            case IDENTIFIER_SESION_DIALOG:
                contentDialog.setBackgroundResource(R.drawable.bg_type_custom_blue);
                btnAcept.setBackgroundResource(R.drawable.bg_black_blue_logout);
                btnNegative.setBackgroundResource(R.drawable.bg_black_blue_logout_corner);
                btnNegative.setTextColor(getResources().getColor(R.color.blue_color));
                break;
        }

    }
}
