package com.bcp.benefits.ui.health.productlist;

import androidx.annotation.NonNull;

import java.util.List;

import com.bcp.benefits.ui.MainView;
import com.bcp.benefits.viewmodel.HealthProductViewModel;

import javax.inject.Inject;

import pe.solera.benefit.main.entity.HealthProduct;
import pe.solera.benefit.main.entity.User;
import pe.solera.benefit.main.usecases.executor.Action;
import pe.solera.benefit.main.usecases.usecases.AppUseCase;
import pe.solera.benefit.main.usecases.usecases.HealthUseCase;
import pe.solera.benefit.main.usecases.usecases.UserUseCase;

/**
 * Created by emontesinos on 22/05/19.
 **/

public class HealthProductPresenter {

    private HealthProductView view;
    private final UserUseCase userUseCase;
    private final AppUseCase appUseCase;
    private final HealthUseCase healthUseCase;

    @Inject
    public HealthProductPresenter(UserUseCase userUseCase, AppUseCase appUseCase,
                                  HealthUseCase healthUseCase) {
        this.userUseCase = userUseCase;
        this.appUseCase = appUseCase;
        this.healthUseCase = healthUseCase;
    }


    public void setView(@NonNull HealthProductView view) {
        this.view = view;
    }

    public void doGetUserInfo() {
        if (this.view.validateInternet()) {
            this.view.showProgressColorhealth();
            this.view.showProgress();
            this.userUseCase.getUser(new Action.Callback<User>() {
                @Override
                public void onSuccess(User user) {
                    updateDataUser(user.getFirstSurname(), user.getSecondSurname());
                    view.showNames(user.getName());
                    view.hideProgress();
                }

                @Override
                public void onError(Throwable throwable) {
                    view.showErrorMessage(throwable.getMessage());
                    view.hideProgress();
                }
            });
        }
    }

    private void updateDataUser(String name, String lastName) {
        view.showLastNames(name + " " + (lastName != null ? lastName : ""));
    }

    public void doGetHealthPhone() {
        if (this.view.validateInternet()) {
            this.view.showProgressColorhealth();
            view.showProgress();
            this.appUseCase.getHealthPhone(new Action.Callback<String>() {
                @Override
                public void onSuccess(String healthPhone) {
                    view.showHealthPhone(healthPhone);
                    view.hideProgress();
                }

                @Override
                public void onError(Throwable throwable) {
                    view.showErrorMessage(throwable.getMessage());
                    view.hideProgress();
                }
            });
        }

    }

    public void doGetHealthProducts() {
        if (this.view.validateInternet()) {
            this.view.showProgressColorhealth();
            view.showProgress();
            this.healthUseCase.getHealthProducts(new Action.Callback<List<HealthProduct>>() {
                @Override
                public void onSuccess(List<HealthProduct> healthProducts) {
                    if (healthProducts.isEmpty())
                        view.showEmptyHealthProducts();
                    else
                        view.showHealthProducts(
                                HealthProductViewModel.toHealthProductViewModel(healthProducts));
                    view.hideProgress();
                }

                @Override
                public void onError(Throwable throwable) {
                    view.showErrorMessage(throwable.getMessage());
                    view.hideProgress();
                }
            });
        }

    }


    public interface HealthProductView extends MainView {
        void showNames(String name);

        void showLastNames(String lastName);

        void showHealthPhone(String number);

        void showHealthProducts(List<HealthProductViewModel> response);

        void showEmptyHealthProducts();
    }

}
