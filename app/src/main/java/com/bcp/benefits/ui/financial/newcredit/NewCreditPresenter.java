package com.bcp.benefits.ui.financial.newcredit;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bcp.benefits.R;
import com.bcp.benefits.ui.MainView;
import com.bcp.benefits.viewmodel.CalculateViewModel;
import com.bcp.benefits.viewmodel.ConditionViewModel;
import com.bcp.benefits.viewmodel.PeriodViewModel;
import com.bcp.benefits.viewmodel.ProductFinancialViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import pe.solera.benefit.main.entity.Calculate;
import pe.solera.benefit.main.usecases.executor.Action;
import pe.solera.benefit.main.usecases.usecases.FinancialUseCase;

/**
 * Created by emontesinos on 21/05/19.
 **/
public class NewCreditPresenter {

    private NewCreditView view;
    private final FinancialUseCase financialUseCase;

    private List<ProductFinancialViewModel> productFinancialViewModelList;

    @Inject
    public NewCreditPresenter(FinancialUseCase financialUseCase) {
        this.financialUseCase = financialUseCase;
    }

    public void setView(@NonNull NewCreditView view) {
        this.view = view;

    }

    public void setProductFinancialList(List<ProductFinancialViewModel> productFinancialList) {
        this.productFinancialViewModelList = productFinancialList;
        setSelected(0);
    }

    private void showPeriods(List<PeriodViewModel> periodModels, String periodUnit) {
        if (!periodModels.isEmpty()) {
            view.showPeriods(periodModels, periodUnit);
        }
    }

    public void calculate(int idProduct, Integer idPeriod, Double amount,
                          @Nullable Double creditAmount, boolean isNewCredit,
                          List<ConditionViewModel> conditions, int position) {
        if (this.view.validateInternet()) {
            this.view.showColor(R.color.home_finance_start);
            view.showProgress();
            this.financialUseCase.calculate(idProduct, idPeriod, amount, creditAmount, isNewCredit,
                    ConditionViewModel.toCondition(conditions), new Action.Callback<Calculate>() {
                        @Override
                        public void onSuccess(Calculate calculate) {
                            view.hideProgress();
                            view.calculateProduct(new CalculateViewModel(calculate), position);
                        }

                        @Override
                        public void onError(Throwable throwable) {
                            view.showErrorMessage(throwable.getMessage());
                            view.hideProgress();
                        }
                    });
        }
    }

    public void preSelected(int idProduct, Integer idPeriodSelectedIfExist, Double amount,
                            @Nullable Double creditAmount, boolean isNewCredit,
                            List<ConditionViewModel> conditionsIfExist) {
        if (this.view.validateInternet()) {
            this.view.showColor(R.color.home_finance_start);
            this.view.showProgress();
            this.financialUseCase.calculate(idProduct, idPeriodSelectedIfExist, amount, creditAmount,
                    isNewCredit, ConditionViewModel.toCondition(conditionsIfExist), new Action.Callback<Calculate>() {
                        @Override
                        public void onSuccess(Calculate calculate) {
                            view.calculateProduct(new CalculateViewModel(calculate), 0);
                            view.hideProgress();
                        }

                        @Override
                        public void onError(Throwable throwable) {
                            view.hideProgress();
                            view.showErrorMessage(throwable.getMessage());
                        }
                    });
        }
    }

    public void request(int idProduct, Integer idPeriod, Double amount, Double optionalAmount,
                        boolean isNewCredit, List<ConditionViewModel> conditions) {
        if (this.view.validateInternet()) {
            this.view.showColor(R.color.home_finance_start);
            view.showProgress();
            this.financialUseCase.request(idProduct, idPeriod, amount, optionalAmount, isNewCredit,
                    ConditionViewModel.toCondition(conditions), new Action.Callback<String>() {
                        @Override
                        public void onSuccess(String s) {
                            view.showMessageSuccess();
                            view.hideProgress();
                        }

                        @Override
                        public void onError(Throwable throwable) {
                            view.showErrorMessage(throwable.getMessage());
                            view.hideProgress();
                        }
                    });
        }
    }

    public void showCreditTypes() {
        List<String> creditTypes = new ArrayList<>();
        for (ProductFinancialViewModel product : productFinancialViewModelList) {
            creditTypes.add(product.getName());
        }
        view.showTypeCredits(creditTypes);
    }

    public void setSelected(int position) {
        if (!this.productFinancialViewModelList.isEmpty()) {
            ProductFinancialViewModel productFinancialViewModel =
                    this.productFinancialViewModelList.get(position);
            view.setSelected(productFinancialViewModel);
            showPeriods(productFinancialViewModel.getPeriodOptions(),
                    productFinancialViewModel.getPeriodUnit());
        }
    }

    public interface NewCreditView extends MainView {
        void calculateProduct(CalculateViewModel calculateModel, int position);

        void showMessageSuccess();

        void setSelected(ProductFinancialViewModel productFinancialViewModel);

        void showPeriods(List<PeriodViewModel> periodModels, String periodUnit);

        void showTypeCredits(List<String> strings);
    }
}
