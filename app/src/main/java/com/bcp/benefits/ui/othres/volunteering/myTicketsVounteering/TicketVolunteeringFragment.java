package com.bcp.benefits.ui.othres.volunteering.myTicketsVounteering;

import android.graphics.Typeface;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import com.bcp.benefits.R;
import com.bcp.benefits.components.CustomTypefaceSpan;
import com.bcp.benefits.ui.campaign.CampaignActivity;
import com.bcp.benefits.ui.othres.golden.mytickets.CarouselLinearLayout;
import com.bcp.benefits.viewmodel.TicketVolunteeringViewModel;
import com.squareup.picasso.Picasso;
import org.jetbrains.annotations.NotNull;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import static com.bcp.benefits.util.Constants.SCALE;
import static com.bcp.benefits.util.Constants.VOLUNTEERING_DATA;

/**
 * Created by emontesinos on 02/05/19.
 **/
public class TicketVolunteeringFragment extends Fragment {

    @BindView(R.id.txt_state_ticket)
    TextView stateTicket;
    @BindView(R.id.ivImg)
    ImageView imgTicket;
    @BindView(R.id.tvDurationText)
    TextView durationTicket;
    @BindView(R.id.line_golden)
    View lineGolden;
    @BindView(R.id.line_decorate_one)
    View lineDecorateOne;
    @BindView(R.id.line_horizontal)
    View lineHorizontal;
    @BindView(R.id.rlv_activate)
    RelativeLayout rlvActivate;
    @BindView(R.id.tvActivate)
    TextView tvActivate;
    @BindView(R.id.txt_message)
    TextView txtMessage;
    @BindView(R.id.rlv_content_title)
    RelativeLayout rlvContentTitle;
    @BindView(R.id.rlv_content_view)
    RelativeLayout rlvContentView;
    @BindView(R.id.txt_title_model)
    TextView txtTitleModel;

    private TicketVolunteeringViewModel ticketVolunteering;

    public static Fragment newInstance(MyTicketsVoulunteeringFragment context, TicketVolunteeringViewModel ticketVolunteeringViewModel) {
        Bundle b = new Bundle();
        b.putSerializable(VOLUNTEERING_DATA, ticketVolunteeringViewModel);
        return Fragment.instantiate(context.getContext(), TicketVolunteeringFragment.class.getName(), b);
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        assert this.getArguments() != null;
        float scale = this.getArguments().getFloat(SCALE);
        ticketVolunteering = (TicketVolunteeringViewModel) this.getArguments().getSerializable(VOLUNTEERING_DATA);
        LinearLayout linearLayout = (LinearLayout) inflater.inflate(R.layout.fragment_image, container, false);
        ButterKnife.bind(this, linearLayout);
        CarouselLinearLayout root = linearLayout.findViewById(R.id.root_container);
        root.setScaleBoth(scale);
        txtTitleModel.setText(R.string.ticket_volunteering);
        rlvContentTitle.setBackground(getResources().getDrawable(R.drawable.bg_orage_volunteering_gradient_corner_top));
        tvActivate.setBackground(getResources().getDrawable(R.drawable.bg_orage_volunteering_gradient_corner_botton));
        tvActivate.setText("ACTIVAR DESDE CAMPAÑAS");
        lineGolden.setBackgroundColor(getResources().getColor(R.color.bg_orange_start_volunteering));
        lineDecorateOne.setBackground(getResources().getDrawable(R.drawable.horizontal_dashed_line_orange_volunteering));
        lineHorizontal.setBackground(getResources().getDrawable(R.drawable.horizontal_dashed_line_gray_volunteering));
        stateTicket.setText(ticketVolunteering.getNameTicket_status());

        if(ticketVolunteering.getNameTicket_status().equals("Aprobado")){
            stateTicket.setTextColor(getResources().getColor(R.color.color_volunte));
        }else {
            stateTicket.setTextColor(getResources().getColor(R.color.black_400));
        }
        Typeface font = ResourcesCompat.getFont(getContext(), R.font.breviabold);
        SpannableString spannableString = new SpannableString("Para el " + ticketVolunteering.getDateRequest());
        spannableString.setSpan(new CustomTypefaceSpan(font), 7, spannableString.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        durationTicket.setText(spannableString);
        Typeface fontTwo = ResourcesCompat.getFont(getContext(), R.font.breviabold);
        SpannableString spannableStringTwo = new SpannableString(getResources().getString(R.string.txt_message_volunteering));
        spannableStringTwo.setSpan(new CustomTypefaceSpan(fontTwo), 0, spannableStringTwo.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        txtMessage.setText(spannableStringTwo);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        params.setMargins(50, 0, 50, 0);
        txtMessage.setLayoutParams(params);

        durationTicket.setText(spannableString);
        if (ticketVolunteering.getIdTicketStatus() == 1){
            durationTicket.setVisibility(View.GONE);
            lineGolden.setVisibility(View.VISIBLE);
            txtMessage.setVisibility(View.VISIBLE);
            lineHorizontal.setVisibility(View.VISIBLE);
            stateTicket.setVisibility(View.GONE);
            rlvActivate.setVisibility(View.VISIBLE);
        } else {
            lineGolden.setVisibility(View.GONE);
            txtMessage.setVisibility(View.GONE);
            rlvActivate.setVisibility(View.GONE);
            stateTicket.setVisibility(View.VISIBLE);
            lineHorizontal.setVisibility(View.GONE);
            durationTicket.setVisibility(View.VISIBLE);
        }

        if (!ticketVolunteering.getUrlImage().isEmpty()){
            Picasso.get().load(ticketVolunteering.getUrlImage()).into(imgTicket);
        }
        return linearLayout;
    }

}
