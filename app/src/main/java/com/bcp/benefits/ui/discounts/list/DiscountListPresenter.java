package com.bcp.benefits.ui.discounts.list;

import android.content.Context;
import android.location.Location;

import com.google.android.gms.location.LocationServices;
import com.bcp.benefits.ui.MainFragment;
import com.bcp.benefits.viewmodel.DiscountTypeViewModel;
import com.bcp.benefits.viewmodel.DiscountViewModel;

import java.util.List;

import javax.inject.Inject;

import pe.solera.benefit.main.entity.DiscountList;
import pe.solera.benefit.main.entity.Pagination;
import pe.solera.benefit.main.entity.SortType;
import pe.solera.benefit.main.usecases.executor.Action;
import pe.solera.benefit.main.usecases.usecases.AppUseCase;
import pe.solera.benefit.main.usecases.usecases.DiscountUseCases;


public class DiscountListPresenter {

    private DiscountItemsListView discountItemsListView;

    private final DiscountUseCases discountUseCases;
    private final Context context;
    private final AppUseCase appUseCase;

    @Inject
    public DiscountListPresenter(DiscountUseCases discountUseCases,
                                 Context context, AppUseCase appUseCase) {
        this.discountUseCases = discountUseCases;
        this.context = context;
        this.appUseCase = appUseCase;
    }

    void setDiscountItemsListView(DiscountItemsListView discountItemsListView) {
        this.discountItemsListView = discountItemsListView;
    }
//consultar
    void getDiscounts(DiscountTypeViewModel type, SortType sort, String search,
                      Integer idCategory, Integer idState, Integer idDistrict,
                      String latitude, String longitude, int page) {
        if (this.discountItemsListView.validateInternet()) {
            discountItemsListView.showSwipe();
            this.discountUseCases.getDiscountByType(DiscountTypeViewModel.getDiscountType(type), sort,
                    search, idCategory, idState, idDistrict, latitude, longitude, page, 0,
                    new Action.Callback<DiscountList>() {
                        @Override
                        public void onSuccess(DiscountList discountList) {
                            discountItemsListView.showDiscounts(
                                    DiscountViewModel.toDiscountListViewModels(discountList, type));
                            discountItemsListView.hideSwipe();
                            discountItemsListView.showPagination(discountList.getPagination());
                        }

                        @Override
                        public void onError(Throwable throwable) {
                            discountItemsListView.showErrorMessage(throwable.getMessage());
                            discountItemsListView.hideSwipe();
                        }
                    });
        }
    }

    void loadUrlAppDiscount() {
        try {

        } catch (Exception ex) {

        }
    }

    void loadLocation() {
        try {
            LocationServices.getFusedLocationProviderClient(context).getLastLocation()
                    .addOnSuccessListener(location -> {
                        discountItemsListView.showLocation(location);
                        discountItemsListView.hideProgress();
                    })
                    .addOnFailureListener(error -> {
                        discountItemsListView.showErrorMessage(error.getMessage());
                        discountItemsListView.hideProgress();
                    });
        } catch (SecurityException ex) {
            discountItemsListView.showErrorMessage(ex.getMessage());
        }
    }

    void loadUrl(){
        this.appUseCase.getUrlBenefits(new Action.Callback<String>(){
            @Override
            public void onSuccess(String s) {
                discountItemsListView.showUrl(s);
            }

            @Override
            public void onError(Throwable throwable) {
                discountItemsListView.showErrorMessage(throwable.getMessage());
            }
        });
    }

    public interface DiscountItemsListView extends MainFragment {
        void showSwipe();

        void hideSwipe();

        void showUrl(String url);

        void showDiscounts(List<DiscountViewModel> discountList);

        void showPagination(Pagination pagination);

        void showLocation(Location location);

    }
}
