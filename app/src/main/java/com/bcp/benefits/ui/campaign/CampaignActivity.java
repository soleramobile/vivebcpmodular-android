package com.bcp.benefits.ui.campaign;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewpager.widget.ViewPager;

import com.bcp.benefits.R;
import com.bcp.benefits.analytics.AnalyticsModule;
import com.bcp.benefits.analytics.AnalyticsTags;
import com.bcp.benefits.components.RadioGroupComponent;
import com.bcp.benefits.ui.BaseActivity;
import com.bcp.benefits.util.AppUtils;
import com.bcp.benefits.viewmodel.CampaignViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

import static com.bcp.benefits.util.Constants.DURATION_ANIMATION;
import static com.bcp.benefits.util.Constants.ID_CAMPAING;

/**
 * Created by emontesinos on 13/05/19.
 **/

public class CampaignActivity extends BaseActivity implements CampaignPresenter.CampaignView
        , CampaignPagerAdapter.OnClickListener {

    @Inject
    CampaignPresenter presenter;

    @BindView(R.id.pager)
    ViewPager mPager;
    @BindView(R.id.content_campaign_empty)
    ConstraintLayout campaignEmpty;
    @BindView(R.id.content_general)
    ConstraintLayout contentGeneral;

    @BindView(R.id.iv_back_main_menu)
    ImageView ivBackMain;
    @BindView(R.id.indicator)
    RadioGroupComponent mRgBanners;

    private ViewGroup viewGroup;
    private String campaignId;
    private AnalyticsModule analyticsModule = new AnalyticsModule(this);;

    public static void start(Context context) {
        Intent intent = new Intent(context, CampaignActivity.class);
        context.startActivity(intent);
    }

    public static void startSpecific(Context context, String idCampaign) {
        Intent starter = new Intent(context, CampaignActivity.class);
        starter.putExtra(ID_CAMPAING, idCampaign);
        context.startActivity(starter);
    }

    @Override
    public void setUpView() {
        analyticsModule.sendToAnalytics(AnalyticsTags.Campaigns.Events.campana, null);
        this.presenter.setView(this);
        this.presenter.getCampaigns();
        campaignId = getIntent().getStringExtra(ID_CAMPAING);
        getPositionIndicator();
        initAnimateImg();
    }

    public void initAnimateImg() {
        if (getResources().getString(R.string.validateAnimate).equals(getResources().getString(R.string.identifierAnimate))) {
            animUpAndDown(DURATION_ANIMATION, true);
            ivBackMain.setPadding(0, 0, 0, 24);
        }
    }

    @Override
    public int getLayout() {
        return R.layout.activity_campaign;
    }

    private void showPreviousAndNextPager() {
        mPager.setPageMargin(AppUtils.dpToPx(-42));
        mPager.setHorizontalFadingEdgeEnabled(true);
        mPager.setFadingEdgeLength(AppUtils.dpToPx(40));
    }

    @Override
    public void showCampaign(List<CampaignViewModel> campaignModels) {
        this.createRadioGroups((ArrayList<CampaignViewModel>) campaignModels);
        mPager.setAdapter(new CampaignPagerAdapter(CampaignActivity.this, (ArrayList<CampaignViewModel>) campaignModels, this));
        showPreviousAndNextPager();
        viewGroup = (ViewGroup) ((ViewGroup) this.findViewById(android.R.id.content)).getChildAt(0);
        if (campaignId != null) {
            int hallado = 0;
            for (int i = 0; i < campaignModels.size(); i++) {
                if (campaignId.equals(campaignModels.get(i).getIdCampaign().toString())) {
                    hallado = i;
                }
            }
            mPager.setCurrentItem(hallado);
        }
    }

    @Override
    public void showEmptyList() {
        campaignEmpty.setVisibility(View.VISIBLE);
        contentGeneral.setBackground(getResources().getDrawable(R.drawable.bg_campaign));
    }

    @OnClick(R.id.iv_back_main_menu)
    public void nextCampaign() {
        finish();
        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_top_down);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {
                if (data!=null){
                int idResult = data.getIntExtra("idCampania",0);
                    campaignId = String.valueOf(idResult);
                }
                presenter.getCampaigns();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void createRadioGroups(ArrayList<CampaignViewModel> listItem) {
        mRgBanners.removeAllViews();
        for (CampaignViewModel item : listItem) {
            AppUtils.addRadioButton(this, mRgBanners, listItem.indexOf(item),
                    listItem.indexOf(item) == 0, 10);
        }
        mRgBanners.setChecked(0);
    }

    private void getPositionIndicator() {
        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {


            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mRgBanners.setChecked(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }

        });

    }

    @Override
    public void messageError() {
        AppUtils.showErrorMessage(viewGroup, this, getResources().getString(R.string.message_error_aguinaldo_product_empty));
    }

    @Override
    public void messageErrorEmpty() {
        AppUtils.showErrorMessage(viewGroup, this, getResources().getString(R.string.message_error_aguinaldo_detail));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_top_down);

    }

    private void animUpAndDown(long time, boolean isUp) {
        ivBackMain.animate()
                .translationY(isUp ? -10 : 10)
                .setDuration(time)
                .setInterpolator(new DecelerateInterpolator())
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        animUpAndDown(time, !isUp);
                    }
                })
                .start();
    }
}
