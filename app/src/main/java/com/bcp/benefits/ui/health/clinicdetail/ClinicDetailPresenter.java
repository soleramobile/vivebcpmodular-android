package com.bcp.benefits.ui.health.clinicdetail;

import androidx.annotation.NonNull;

import javax.inject.Inject;

import com.bcp.benefits.ui.MainView;
import com.bcp.benefits.viewmodel.ClinicViewModel;

import pe.solera.benefit.main.entity.Clinic;
import pe.solera.benefit.main.usecases.executor.Action;
import pe.solera.benefit.main.usecases.usecases.HealthUseCase;

public class ClinicDetailPresenter {


    private ClinicDetailView view;
    private final HealthUseCase healthUseCase;

    @Inject
    public ClinicDetailPresenter(HealthUseCase healthUseCase) {
        this.healthUseCase = healthUseCase;
    }

    public void setView(@NonNull ClinicDetailView view) {
        this.view = view;
    }

    void doGetClinic(Integer clinicId, Integer productId) {
        if (this.view.validateInternet()) {
            this.view.showProgressColorhealth();
            view.showProgress();
            this.healthUseCase.getClinicDetail(clinicId, productId, new Action.Callback<Clinic>() {
                @Override
                public void onSuccess(Clinic clinic) {
                    view.showClinic(new ClinicViewModel(clinic));
                    view.hideProgress();
                }

                @Override
                public void onError(Throwable throwable) {
                    view.hideProgress();
                    view.showErrorMessage(throwable.getMessage());
                }
            });
        }
    }

    public interface ClinicDetailView extends MainView {
        void showClinic(ClinicViewModel clinicModel);
    }
}
