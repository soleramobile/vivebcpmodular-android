package com.bcp.benefits.ui.campaign.dynamic.referredlist;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageButton;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.AppBarLayout;
import com.bcp.benefits.R;
import com.bcp.benefits.ui.BaseActivity;
import com.bcp.benefits.viewmodel.DynamicReferredViewModel;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

import static com.bcp.benefits.util.Constants.ID_CAMPAIGN_PARAM;

public class ReferredActivity extends BaseActivity implements ReferredPresenter.ReferredView {

    @Inject
    ReferredPresenter presenter;
    @BindView(R.id.rcv_fields)
    RecyclerView rcvFields;
    @BindView(R.id.toolbar)
    AppBarLayout toolbar;
    @BindView(R.id.img_back)
    ImageButton imgBack;

    @BindView(R.id.content_empty_referred)
    ConstraintLayout contentEmptyReferred;

    private ReferredAdapter adapter;
    private int campaignId;

    public static void start(Context context, int idCampaign) {
        Intent starter = new Intent(context, ReferredActivity.class);
        starter.putExtra(ID_CAMPAIGN_PARAM, idCampaign);
        context.startActivity(starter);
    }

    @Override
    public void setUpView() {
        presenter.setView(this);
        campaignId = getIntent().getIntExtra(ID_CAMPAIGN_PARAM, 0);
        presenter.getReferredList(campaignId);
        getToolbar();
        adapter = new ReferredAdapter(this, null);
        rcvFields.setLayoutManager(new LinearLayoutManager(this));
        rcvFields.setAdapter(adapter);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_referred;
    }

    @Override
    public void showReferredList(List<DynamicReferredViewModel> dynamicReferredList) {
        adapter.setDynamicReferredList(dynamicReferredList);
    }

    @Override
    public void showReferredListEmpty() {
        contentEmptyReferred.setVisibility(View.VISIBLE);
        rcvFields.setVisibility(View.GONE);
    }

    public void getToolbar() {
        toolbar.setBackgroundColor(getResources().getColor(R.color.white));
        imgBack.setImageDrawable(getResources().getDrawable(R.drawable.ic_back_gray));
        imgBack.setOnClickListener(v -> {
            finish();
            overridePendingTransition(R.anim.rigth_in, R.anim.right_out);
        });
    }
}
