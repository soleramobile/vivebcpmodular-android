package com.bcp.benefits.ui.discounts.detail;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bcp.benefits.R;
import com.bcp.benefits.viewmodel.DiscountTypeViewModel;
import com.bcp.benefits.viewmodel.SubsidiaryViewModel;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;


public class SubsidiariesAdapter extends RecyclerView.Adapter<SubsidiariesAdapter.SubsidiaryVH> {

    private OnSelectSubsidiaryListener onSelectSubsidiaryListener;
    private ArrayList<SubsidiaryViewModel> subsidiaries;
    private SubsidiariesActivity.SubsidiaryOption subsidiaryOption;
    private DiscountTypeViewModel discountType;

    public SubsidiariesAdapter(SubsidiariesActivity.SubsidiaryOption subsidiaryOption, DiscountTypeViewModel discountType) {
        this.subsidiaryOption = subsidiaryOption;
        this.discountType = discountType;
    }

    public void addList(ArrayList<SubsidiaryViewModel> subsidiaries) {
        this.subsidiaries = subsidiaries;
        notifyDataSetChanged();
    }

    public void addListener(OnSelectSubsidiaryListener onSelectSubsidiaryListener) {
        this.onSelectSubsidiaryListener = onSelectSubsidiaryListener;
    }

    @NotNull
    @Override
    public SubsidiaryVH onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_subsidiaries_item, parent, false);
        return new SubsidiaryVH(v);
    }

    @Override
    public void onBindViewHolder(@NotNull SubsidiaryVH holder, int position) {
        holder.bind(subsidiaries.get(position));

    }

    @Override
    public int getItemCount() {
        return subsidiaries == null ? 0 : subsidiaries.size();
    }

    class SubsidiaryVH extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvName;
        TextView tvAddress;
        TextView tvPhone;
        ImageView imgPhone;

        SubsidiaryVH(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            tvName = itemView.findViewById(R.id.tv_subsidiary_name);
            tvAddress = itemView.findViewById(R.id.tv_subsidiary_address);
            tvPhone = itemView.findViewById(R.id.tv_subsidiary_phone);
            imgPhone = itemView.findViewById(R.id.img_phone);
        }

        public void bind(SubsidiaryViewModel subsidiary) {
            if (discountType == DiscountTypeViewModel.EDUTATION)
                imgPhone.setImageDrawable(itemView.getResources().getDrawable(R.drawable.ic_call_yellow_two));
            if (subsidiary.getName() != null) {
                tvName.setVisibility(View.VISIBLE);
                tvName.setText(subsidiary.getName());
            } else
                tvName.setVisibility(View.GONE);

            if (subsidiary.getAddress() != null) {
                tvAddress.setVisibility(View.VISIBLE);
                tvAddress.setText(subsidiary.getAddress());
            } else
                tvAddress.setVisibility(View.GONE);

            if (subsidiaryOption == SubsidiariesActivity.SubsidiaryOption.HOW_TO_GET) {
                tvPhone.setVisibility(View.GONE);
                imgPhone.setVisibility(View.GONE);
            } else {
                if (subsidiary.getPhone() != null) {
                    tvPhone.setVisibility(View.VISIBLE);
                    imgPhone.setVisibility(View.VISIBLE);
                    tvPhone.setText(subsidiary.getPhone());
                } else {
                    tvPhone.setVisibility(View.GONE);
                    imgPhone.setVisibility(View.GONE);
                }
            }
        }

        @Override
        public void onClick(View view) {
            if (onSelectSubsidiaryListener != null)
                onSelectSubsidiaryListener.onSelectSubsidiary(subsidiaries.get(getAdapterPosition()));
        }
    }

    interface OnSelectSubsidiaryListener {
        void onSelectSubsidiary(SubsidiaryViewModel subsidiary);
    }
}
