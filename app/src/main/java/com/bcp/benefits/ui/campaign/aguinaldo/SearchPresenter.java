package com.bcp.benefits.ui.campaign.aguinaldo;

import com.bcp.benefits.ui.MainView;
import com.bcp.benefits.viewmodel.SubsidiaryViewModel;

import java.util.List;
import javax.inject.Inject;
import androidx.annotation.NonNull;
import pe.solera.benefit.main.entity.Subsidiary;
import pe.solera.benefit.main.usecases.executor.Action;
import pe.solera.benefit.main.usecases.usecases.CampaignUseCase;

public class SearchPresenter {


    private final CampaignUseCase campaignUseCase;

    private SearchView view;

    @Inject
    public SearchPresenter(final CampaignUseCase campaignUseCase) {
        this.campaignUseCase = campaignUseCase;
    }
    public void setView(@NonNull SearchView view) {
        this.view = view;
    }

    public void requestSuggestions(int idCampaign, int idLocation, String name) {
        if (this.view.validateInternet()) {
            campaignUseCase.requestSuggestion(idCampaign, idLocation, name, new Action.Callback<List<Subsidiary>>() {
                @Override
                public void onSuccess(List<Subsidiary> subsidiaries) {
                    view.showRefreshSubsidiariesList(SubsidiaryViewModel.toSubsidiaryList(subsidiaries));
                }

                @Override
                public void onError(Throwable throwable) {
                    view.showErrorMessage(throwable.getMessage());

                }
            });
        }

    }

    public interface SearchView extends MainView {

        void showRefreshSubsidiariesList(List<SubsidiaryViewModel> subsidiaryViewModels);


    }

}
