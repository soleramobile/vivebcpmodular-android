package com.bcp.benefits.ui.health.nearyou.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bcp.benefits.R;
import com.bcp.benefits.viewmodel.HealthAttentionTypeViewModel;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AttentionTypeAdapter extends RecyclerView.Adapter<AttentionTypeAdapter.AttentionTypeVH> {


    private List<HealthAttentionTypeViewModel> attentionTypes;
    private OnSelectAttentionTypeListener listener;
    private int selectedItem = 0;
    private Context context;

    public AttentionTypeAdapter(Context context, OnSelectAttentionTypeListener listener) {
        this.context = context;
        this.listener = listener;
    }

    public void addList(List<HealthAttentionTypeViewModel> attentionTypes) {
        this.attentionTypes = attentionTypes;
        notifyDataSetChanged();
    }

    @NotNull
    @Override
    public AttentionTypeVH onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_health_products_select, parent, false);
        return new AttentionTypeVH(v);
    }

    @Override
    public void onBindViewHolder(@NotNull AttentionTypeVH holder, int position) {
        holder.tvAttentionTypeName.setText(attentionTypes.get(position).getDescription());
        if (selectedItem == position) {
            holder.tvAttentionTypeName.setBackgroundResource(R.drawable.bg_health_blue_corner_two);
            holder.tvAttentionTypeName.setTextColor(ContextCompat.getColor(context, R.color.white));
            holder.tvAttentionTypeName.setTextAppearance(holder.itemView.getContext(), R.style.FontBold);
        } else {
            holder.tvAttentionTypeName.setBackgroundResource(R.drawable.bg_white_corners_skyblue_stroke_four);
            holder.tvAttentionTypeName.setTextColor(ContextCompat.getColor(context, R.color.gray_600));
            holder.tvAttentionTypeName.setTextAppearance(holder.itemView.getContext(), R.style.FontRegular);
        }
    }

    @Override
    public int getItemCount() {
        return attentionTypes != null ? attentionTypes.size() : 0;
    }


    class AttentionTypeVH extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.tv_item)
        TextView tvAttentionTypeName;

        public AttentionTypeVH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            tvAttentionTypeName.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            if (listener == null)
                return;

            if (selectedItem != -1)
                notifyItemChanged(selectedItem);

            if (selectedItem != getAdapterPosition()) {
                selectedItem = getAdapterPosition();
                notifyItemChanged(selectedItem);
                listener.selectAttentionType(attentionTypes.get(getAdapterPosition()));
            }
        }
    }

    public interface OnSelectAttentionTypeListener {
        void selectAttentionType(HealthAttentionTypeViewModel category);
    }
}
