package com.bcp.benefits.ui.financial.newcredit;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bcp.benefits.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by emontesinos on 5/06/19.
 **/
public class TypeCreditAdapter extends RecyclerView.Adapter<TypeCreditAdapter.TypeCreditViewHolder> {


    private ArrayList<String> types;
    private ListenerAdapter listener;
    private int selectedItem = -1;

    TypeCreditAdapter(ArrayList<String> types) {
        this.types = types;
    }

    void setListener(@NonNull TypeCreditAdapter.ListenerAdapter listener) {
        this.listener = listener;
    }


    public void setSelectedItem(String type) {
        if (type == null) return;
        int index = types.indexOf(type);
        if (selectedItem != index) {
            selectedItem = index;
            notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public TypeCreditViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_type_custom, parent, false);
        return new TypeCreditViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TypeCreditViewHolder holder, int position) {
        holder.titulo.setText(types.get(position));
        if (selectedItem == position) {
            holder.imgCircle.setBackground(holder.itemView.getContext().getResources().getDrawable(R.drawable.bg_cicle_orange_type));

        } else {
            holder.imgCircle.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return types.size();
    }

    public class TypeCreditViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.txt_type_texto)
        TextView titulo;
        @BindView(R.id.img_circle)
        ImageView imgCircle;

        private TypeCreditViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);

        }


        @Override
        public void onClick(View v) {
            if (listener == null)
                return;

            if (selectedItem != -1)
                notifyItemChanged(selectedItem);
            selectedItem = getAdapterPosition();
            notifyItemChanged(selectedItem);
            listener.selectType(types.get(getAdapterPosition()), getAdapterPosition());
        }
    }

    interface ListenerAdapter {
        void selectType(String type, int position);
    }
}
