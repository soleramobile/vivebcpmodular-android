package com.bcp.benefits.ui.othres.licenses.mylicences;

import android.os.Bundle;
import androidx.viewpager.widget.ViewPager;
import android.util.DisplayMetrics;
import android.view.View;
import org.jetbrains.annotations.NotNull;
import java.util.Objects;
import butterknife.BindView;
import com.bcp.benefits.R;
import com.bcp.benefits.ui.BaseFragment;
import com.bcp.benefits.viewmodel.GoldenTicketViewModel;
import static com.bcp.benefits.util.Constants.TICKETS_DATA;

/**
 * Created by emontesinos on 08/05/19.
 **/

public class MysLicencesFragment extends BaseFragment {

    @BindView(R.id.my_view_pager_ticket)
    ViewPager pager;

    public final static int LOOPS = 1;
    private GoldenTicketViewModel goldenTicketResponse;
    public static int count;
    public static final int FIRST_PAGE = 0;

    public static MysLicencesFragment newInstance(GoldenTicketViewModel goldenTicketModel) {
        MysLicencesFragment fragment = new MysLicencesFragment();
        Bundle args = new Bundle();
        args.putSerializable(TICKETS_DATA, goldenTicketModel);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void setUpView() {

    }

    @Override
    public int getLayout() {
        return R.layout.fragment_my_tickets;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            goldenTicketResponse = (GoldenTicketViewModel) getArguments().getSerializable(TICKETS_DATA);
            assert goldenTicketResponse != null;
            count = goldenTicketResponse.getOwned().size();
        }
    }

    @Override
    public void onViewCreated(@NotNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        DisplayMetrics metrics = new DisplayMetrics();
        Objects.requireNonNull(getActivity()).getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int pageMargin = ((metrics.widthPixels / 6) * 2);
        pager.setPageMargin(-pageMargin);
        CarouselLicencePagerAdapter adapter = new CarouselLicencePagerAdapter(this, getChildFragmentManager(), goldenTicketResponse.getOwned());
        pager.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        pager.addOnPageChangeListener(adapter);
        pager.setCurrentItem(FIRST_PAGE);
    }
}
