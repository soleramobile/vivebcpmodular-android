package com.bcp.benefits.ui.financial.requestproduct;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bcp.benefits.R;
import com.bcp.benefits.ui.MainView;
import com.bcp.benefits.viewmodel.CalculateViewModel;
import com.bcp.benefits.viewmodel.ConditionViewModel;

import java.util.List;

import javax.inject.Inject;

import pe.solera.benefit.main.entity.Calculate;
import pe.solera.benefit.main.usecases.executor.Action;
import pe.solera.benefit.main.usecases.usecases.FinancialUseCase;

/**
 * Created by emontesinos on 21/05/19.
 **/

public class AprovedcreditPresenter {

    private AprovedcreditView view;
    private final FinancialUseCase financialUseCase;

    @Inject
    public AprovedcreditPresenter(FinancialUseCase financialUseCase) {
        this.financialUseCase = financialUseCase;
    }

    public void setView(@NonNull AprovedcreditView view) {
        this.view = view;

    }

    public void calculate(int idProduct, Integer idPeriod, Double amount,
                          @Nullable Double creditAmount, boolean isNewCredit,
                          List<ConditionViewModel> conditions) {
        if (this.view.validateInternet()) {
            this.view.showColor(R.color.home_finance_start);
            this.view.showProgress();
            this.financialUseCase.calculate(idProduct, idPeriod, amount, creditAmount, isNewCredit,
                    ConditionViewModel.toCondition(conditions), new Action.Callback<Calculate>() {
                        @Override
                        public void onSuccess(Calculate calculate) {
                            view.hideProgress();
                            view.calculateProduct(new CalculateViewModel(calculate));
                        }

                        @Override
                        public void onError(Throwable throwable) {
                            view.showErrorMessage(throwable.getMessage());
                            view.hideProgress();
                        }
                    });
        }
    }

    public void preSelected(int idProduct, Integer idPeriodSelectedIfExist, Double amount,
                            @Nullable Double creditAmount, boolean isNewCredit,
                            List<ConditionViewModel> conditionsIfExist) {
        if (this.view.validateInternet()) {
            this.view.showColor(R.color.home_finance_start);
            this.view.showProgress();
            this.financialUseCase.calculate(idProduct, idPeriodSelectedIfExist, amount, creditAmount,
                    isNewCredit, ConditionViewModel.toCondition(conditionsIfExist), new Action.Callback<Calculate>() {
                        @Override
                        public void onSuccess(Calculate calculate) {
                            view.calculateProduct(new CalculateViewModel(calculate));
                            view.hideProgress();
                        }

                        @Override
                        public void onError(Throwable throwable) {
                            view.hideProgress();
                            view.showErrorMessage(throwable.getMessage());
                        }
                    });
        }
    }

    public void request(int idProduct, Integer idPeriod, Double amount, Double optionalAmount,
                        boolean isNewCredit, List<ConditionViewModel> conditions) {
        if (this.view.validateInternet()) {
            this.view.showColor(R.color.home_finance_start);
            this.view.showProgress();
            this.financialUseCase.request(idProduct, idPeriod, amount, optionalAmount, isNewCredit,
                    ConditionViewModel.toCondition(conditions), new Action.Callback<String>() {
                        @Override
                        public void onSuccess(String s) {
                            view.showMessageSuccess();
                            view.hideProgress();
                        }

                        @Override
                        public void onError(Throwable throwable) {
                            view.showErrorMessage(throwable.getMessage());
                            view.hideProgress();
                        }
                    });
        }
    }

    public interface AprovedcreditView extends MainView {
        void calculateProduct(CalculateViewModel calculateModel);

        void showMessageSuccess();
    }
}
