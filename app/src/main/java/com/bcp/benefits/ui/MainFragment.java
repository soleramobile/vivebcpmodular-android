package com.bcp.benefits.ui;

import androidx.annotation.StringRes;

public interface MainFragment {
    void showProgress();

    void hideProgress();

    void showErrorMessage(@StringRes int message);

    void showErrorMessage(String message);

    Boolean validateInternet();
}
