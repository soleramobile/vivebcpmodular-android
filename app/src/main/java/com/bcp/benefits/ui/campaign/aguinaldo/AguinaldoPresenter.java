package com.bcp.benefits.ui.campaign.aguinaldo;

import com.bcp.benefits.ui.MainView;

import java.util.ArrayList;
import javax.inject.Inject;
import androidx.annotation.NonNull;
import pe.solera.benefit.main.usecases.executor.Action;
import pe.solera.benefit.main.usecases.usecases.CampaignUseCase;

public class AguinaldoPresenter {

    private final CampaignUseCase campaignUseCase;
    private AguinaldoView view;

    @Inject
    public AguinaldoPresenter(CampaignUseCase campaignUseCase) {
        this.campaignUseCase = campaignUseCase;
    }
    public void setView(@NonNull AguinaldoView view) {
        this.view = view;
    }


    void request(int idCampaign, int idSubsidiary, ArrayList<Integer> productIds) {
        if (this.view.validateInternet()) {
            this.view.showProgress();
            campaignUseCase.requestAguinaldo(idCampaign, idSubsidiary, productIds, new Action.Callback<String>() {
                @Override
                public void onSuccess(String s) {
                    view.showMessageSuccess();
                    view.hideProgress();

                }

                @Override
                public void onError(Throwable throwable) {
                    view.showErrorMessage(throwable.getMessage());
                    view.hideProgress();

                }
            });

        }
    }

    public interface AguinaldoView extends MainView {
        void showMessageSuccess();

    }
}
