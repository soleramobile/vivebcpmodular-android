package com.bcp.benefits.ui.tutorial;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.bcp.benefits.R;
import com.bcp.benefits.ui.BaseFragment;
import com.bcp.benefits.viewmodel.TutorialViewModel;
import com.squareup.picasso.Picasso;

import java.util.Objects;

import butterknife.BindView;

/**
 * Created by emontesinos.
 **/

public class TutorialFragment extends BaseFragment {
    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String TUTORIAL_VIEW_MODEL_DATA = "TUTORIAL_VIEW_MODEL_DATA";

    @BindView(R.id.txt_title_tutorial)
    TextView title;
    @BindView(R.id.img)
    ImageView img;
    @BindView(R.id.constraintLayout)
    ConstraintLayout ctrLay;

    private TutorialViewModel tutorialViewModel;

    public static TutorialFragment newInstance(TutorialViewModel tutorialViewModel, int sectionNumber) {
        TutorialFragment fragment = new TutorialFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        args.putSerializable(TUTORIAL_VIEW_MODEL_DATA, tutorialViewModel);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void setUpView() {
        this.loadData();
        title.setText(tutorialViewModel.getTitle());
        Picasso.get().load(Integer.parseInt(tutorialViewModel.getImage()))
                .into(img);
        ctrLay.setBackgroundResource(Integer.parseInt(tutorialViewModel.getBackground()));
    }

    private void loadData() {
        tutorialViewModel = (TutorialViewModel) Objects.requireNonNull(getArguments())
                .getSerializable(TUTORIAL_VIEW_MODEL_DATA);
    }

    @Override
    public int getLayout() {
        return R.layout.fragment_tutorial;
    }
}
