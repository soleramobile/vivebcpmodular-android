package com.bcp.benefits.ui.health.nearyou.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bcp.benefits.R;
import com.bcp.benefits.viewmodel.HealthProductViewModel;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HealthProductsAdapter extends RecyclerView.Adapter<HealthProductsAdapter.ProductVH> {

    private ArrayList<HealthProductViewModel> products;
    private OnSelectProductListener listener;
    private int selectedItem = 0;
    private Context context;

    public HealthProductsAdapter(Context context, OnSelectProductListener listener) {
        this.context = context;
        this.listener = listener;
    }

    public void addList(ArrayList<HealthProductViewModel> products) {
        this.products = products;
        notifyDataSetChanged();
    }

    public void setSelectedItem(HealthProductViewModel product) {
        if (products == null) return;
        int index = products.indexOf(product);
        if (selectedItem != index) {
            selectedItem = index;
            notifyDataSetChanged();
        }
    }

    @NotNull
    @Override
    public ProductVH onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_health_products_select, parent, false);
        return new ProductVH(v);
    }

    @Override
    public void onBindViewHolder(@NotNull ProductVH holder, int position) {
        if (selectedItem == position) {
            holder.tvProductName.setBackgroundResource(R.drawable.bg_health_blue_corner_two);
            holder.tvProductName.setTextColor(ContextCompat.getColor(context, R.color.white));
            holder.tvProductName.setTextAppearance(holder.itemView.getContext(), R.style.FontBold);
        } else {
            holder.tvProductName.setBackgroundResource(R.drawable.bg_white_corners_skyblue_stroke_four);
            holder.tvProductName.setTextColor(ContextCompat.getColor(context,
                    R.color.gray_600));
            holder.tvProductName.setTextAppearance(holder.itemView.getContext(), R.style.FontRegular);
        }
        holder.tvProductName.setText(products.get(position).getProductName());
    }

    @Override
    public int getItemCount() {
        return products != null ? products.size() : 0;
    }

    class ProductVH extends RecyclerView.ViewHolder implements View.OnClickListener {


        @BindView(R.id.tv_item)
        TextView tvProductName;

        public ProductVH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            tvProductName.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (listener == null)
                return;

            if (selectedItem != -1)
                notifyItemChanged(selectedItem);

            if (selectedItem != getAdapterPosition()) {
                selectedItem = getAdapterPosition();
                notifyItemChanged(selectedItem);
                listener.selectProduct(products.get(getAdapterPosition()), getAdapterPosition());
            }
        }
    }

    public interface OnSelectProductListener {
        void selectProduct(HealthProductViewModel model, int position);
    }
}
