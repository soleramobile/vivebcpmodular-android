package com.bcp.benefits.ui.financial;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bcp.benefits.R;
import com.bcp.benefits.viewmodel.BannerViewModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by emontesinos on 20/05/19.
 **/
public class BannerFinanceAdapter extends RecyclerView.Adapter<BannerFinanceAdapter.BannerViewHolder> {
    private List<BannerViewModel> list;
    Context context;

    public BannerFinanceAdapter(Context context) {
        this.list = new ArrayList<>();
        this.context = context;
    }

    public void setList(List<BannerViewModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public BannerFinanceAdapter.BannerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_banner_finacial, viewGroup, false);
        return new BannerFinanceAdapter.BannerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BannerFinanceAdapter.BannerViewHolder holder, int position) {
        BannerViewModel data = list.get(position);
        Picasso.get().load(data.getLink()).placeholder(R.drawable.default_scroll_handle_bottom).into(holder.imageBanner);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class BannerViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_banner_financial)
        ImageView imageBanner;

        public BannerViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
