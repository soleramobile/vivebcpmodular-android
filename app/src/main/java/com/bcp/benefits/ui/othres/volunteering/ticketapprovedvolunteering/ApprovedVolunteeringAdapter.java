package com.bcp.benefits.ui.othres.volunteering.ticketapprovedvolunteering;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.bcp.benefits.R;
import com.bcp.benefits.ui.othres.volunteering.VolunteeringTicketActivity;
import com.bcp.benefits.viewmodel.TicketRequestVolunteeringViewModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by emontesinos on 03/05/19.
 **/
public class ApprovedVolunteeringAdapter extends RecyclerView.Adapter<ApprovedVolunteeringAdapter.ApprovedViewHolder> {


    private ArrayList<TicketRequestVolunteeringViewModel> ticketRequestVolunteeringViewModes;
    private final Context context;

    void addList(ArrayList<TicketRequestVolunteeringViewModel> ticketRequestVolunteeringViewModes) {
        this.ticketRequestVolunteeringViewModes = ticketRequestVolunteeringViewModes;
        notifyDataSetChanged();
    }

    ApprovedVolunteeringAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public ApprovedViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_list_approved, viewGroup, false);
        return new ApprovedViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ApprovedViewHolder holder, int position) {
        TicketRequestVolunteeringViewModel approvalModel = ticketRequestVolunteeringViewModes.get(position);
        holder.tvName.setText(approvalModel.getFullNameCollaborator());
        holder.tvDate.setText(approvalModel.getDateRequest());
        holder.tvDurationDetail.setText(approvalModel.getNameVolunteer_time());
        holder.tvPosition.setText(approvalModel.getFunctionName());
        holder.tvDurationDetail2.setText(approvalModel.getTypeVolunteerTime());
        holder.tvAccept.setTextColor(context.getResources().getColor(R.color.green_accept));
        holder.lineY.setBackgroundColor(context.getResources().getColor(R.color.bg_orange_end_volunteering));
        holder.rvTitleContent.setBackground(context.getResources().getDrawable(R.drawable.bg_orange_gradient_corner_volunteering_approved));
        holder.tvAccept.setOnClickListener(view -> ((VolunteeringTicketActivity) context).approveTicketVolunteering(approvalModel.getIdVolunteerCollaborator()));
        holder.tvCancel.setOnClickListener(view -> ((VolunteeringTicketActivity) context).cancelTicketVolunteering(approvalModel.getIdVolunteerCollaborator()));

    }

    @Override
    public int getItemCount() {
        return ticketRequestVolunteeringViewModes.size();
    }

    class ApprovedViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvDurationDetailprincipal)
        TextView tvDurationDetail;
        @BindView(R.id.tvName)
        TextView tvName;
        @BindView(R.id.tvPosition)
        TextView tvPosition;
        @BindView(R.id.tvDate)
        TextView tvDate;
        @BindView(R.id.tvDuration_detail)
        TextView tvDurationDetail2;
        @BindView(R.id.tvAccept)
        TextView tvAccept;
        @BindView(R.id.tvCancel)
        TextView tvCancel;
        @BindView(R.id.rv_title_content)
        RelativeLayout rvTitleContent;
        @BindView(R.id.line_y)
        View lineY;


        ApprovedViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }
}
