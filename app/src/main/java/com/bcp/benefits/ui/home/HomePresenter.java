package com.bcp.benefits.ui.home;

import javax.inject.Inject;

import com.bcp.benefits.ui.MainView;
import com.bcp.benefits.viewmodel.CommunicationViewModel;

import java.util.List;

import pe.solera.benefit.main.entity.Communication;
import pe.solera.benefit.main.entity.User;
import pe.solera.benefit.main.usecases.executor.Action;
import pe.solera.benefit.main.usecases.usecases.AppUseCase;
import pe.solera.benefit.main.usecases.usecases.UserUseCase;

/**
 * Created by ernestogaspard on 23/04/19.
 **/

public class HomePresenter {

    private HomeView view;

    private final UserUseCase userUseCase;
    private final AppUseCase appUseCase;

    @Inject
    public HomePresenter(UserUseCase userUseCase,AppUseCase appUseCase) {
        this.userUseCase = userUseCase;
        this.appUseCase = appUseCase;
    }

    public void setView(HomeView view) {
        this.view = view;
    }

    public void doGetUserFullName() {
        if (this.view.validateInternet()) {
            this.view.showProgress();
            userUseCase.getUser(new Action.Callback<User>() {
                @Override
                public void onSuccess(User user) {
                    view.showUserFullName(user.getName());
                    view.hideProgress();
                }

                @Override
                public void onError(Throwable throwable) {

                    view.showErrorMessage(throwable.getMessage());
                    view.hideProgress();
                }
            });
        }
    }

    public void doLogout() {
        if (this.view.validateInternet()) {
            this.view.showProgress();
            userUseCase.logout(new Action.Callback<Boolean>() {
                @Override
                public void onSuccess(Boolean aBoolean) {
                    view.goToLogin();
                    view.hideProgress();
                }

                @Override
                public void onError(Throwable throwable) {
                    view.showErrorMessage(throwable.getMessage());
                    view.hideProgress();

                }
            });
        }

    }

    public void doGetCommunications() {
        if (this.view.validateInternet()) {
            this.view.showProgress();
            userUseCase.getAllCommunication(new Action.Callback<List<Communication>>() {
                @Override
                public void onSuccess(List<Communication> communications) {
                    view.showCommunications(CommunicationViewModel.toOwnedList(communications));
                    view.hideProgress();

                }

                @Override
                public void onError(Throwable throwable) {
                    view.showErrorMessage(throwable.getMessage());
                    view.hideProgress();

                }
            });
        }

    }

    public void doRegisterToken(String token){
       view.showProgress();
        appUseCase.fetchToken(token, new Action.Callback<Boolean>() {
             @Override
             public void onSuccess(Boolean aBoolean) {
                 view.saveRegisterToken(true);
             }
             @Override
             public void onError(Throwable throwable) {
                 view.saveRegisterToken(false);
             }
         });

    }


    public interface HomeView extends MainView {

        void goToLogin();
        void showUserFullName(String name);
        void showCommunications(List<CommunicationViewModel> communicationViewModels);
        void saveRegisterToken(Boolean token);

    }
}
