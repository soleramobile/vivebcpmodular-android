package com.bcp.benefits.ui.othres.golden.mytickets;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import com.bcp.benefits.R;
import com.bcp.benefits.components.CustomTypefaceSpan;
import com.bcp.benefits.ui.othres.golden.GoldenTicketActivity;
import com.bcp.benefits.ui.othres.golden.request.RequestTicketActivity;
import com.bcp.benefits.util.AppUtils;
import com.bcp.benefits.viewmodel.OwnedViewModel;
import com.squareup.picasso.Picasso;
import org.jetbrains.annotations.NotNull;
import java.util.Objects;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import static com.bcp.benefits.util.Constants.HEADNAME;
import static com.bcp.benefits.util.Constants.IDTICKET;
import static com.bcp.benefits.util.Constants.OWNED;
import static com.bcp.benefits.util.Constants.RESULT_TICKET;
import static com.bcp.benefits.util.Constants.SCALE;
import static com.bcp.benefits.util.Constants.SHOWRADIO;

/**
 * Created by emontesinos on 02/05/19.
 **/
public class TicketFragment extends Fragment {

    @BindView(R.id.txt_state_ticket)
    TextView stateTicket;
    @BindView(R.id.ivImg)
    ImageView imgTicket;
    @BindView(R.id.tvDurationText)
    TextView durationTicket;
    @BindView(R.id.line_golden)
    View lineGolden;
    @BindView(R.id.line_horizontal)
    View lineHorizontal;
    @BindView(R.id.rlv_activate)
    RelativeLayout rlvActivate;
    @BindView(R.id.tvActivate)
    TextView tvActivate;
    @BindView(R.id.txt_message)
    TextView txtMessage;
    @BindView(R.id.rlv_content_title)
    RelativeLayout rlvContentTitle;
    @BindView(R.id.rlv_content_view)
    RelativeLayout rlvContentView;
    private Boolean validClick = true;

    private OwnedViewModel ownedModel;

    public static Fragment newInstance(MyTicketsFragment context, OwnedViewModel ownedModel) {
        Bundle b = new Bundle();
        b.putSerializable(OWNED, ownedModel);
        return Fragment.instantiate(context.getContext(), TicketFragment.class.getName(), b);
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        assert this.getArguments() != null;
        float scale = this.getArguments().getFloat(SCALE);
        ownedModel = (OwnedViewModel) this.getArguments().getSerializable(OWNED);
        LinearLayout linearLayout = (LinearLayout) inflater.inflate(R.layout.fragment_image, container, false);
        ButterKnife.bind(this, linearLayout);
        CarouselLinearLayout root = linearLayout.findViewById(R.id.root_container);
        root.setScaleBoth(scale);

        if (ownedModel.getShowButton()) {
            durationTicket.setText(AppUtils.getStyleText(getContext(), 0, ownedModel.getDurationText(), ownedModel.getDurationText().length()));
        } else {
            rlvActivate.setVisibility(View.INVISIBLE);
            lineGolden.setVisibility(View.GONE);
            txtMessage.setVisibility(View.GONE);
            lineHorizontal.setVisibility(View.GONE);
            stateTicket.setVisibility(View.VISIBLE);

            Typeface font = ResourcesCompat.getFont(getContext(), R.font.breviabold);
            SpannableString spannableString = new SpannableString("Para el " + ownedModel.getUsageDateFormatted());
            spannableString.setSpan(new CustomTypefaceSpan(font), 7, spannableString.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            durationTicket.setText(spannableString);

            durationTicket.setMovementMethod(LinkMovementMethod.getInstance());
            stateTicket.setText(ownedModel.getStatusText());
        }
        if (!ownedModel.getImage().isEmpty()){
            Picasso.get().load(ownedModel.getImage()).into(imgTicket);
        }
        return linearLayout;
    }

    @OnClick({R.id.rlv_content_title, R.id.rlv_content_view})
    void onClickTicket() {
        if (validClick) {
            validClick = false;
            if (ownedModel.getShowButton()) {
                getIntent();
            } else {
                if (ownedModel.isReleaseable()) {
                    ((GoldenTicketActivity) Objects.requireNonNull(getActivity())).releaseTicket(ownedModel.getIdTicket());
                }
            }
        }
    }

    @OnClick(R.id.rlv_activate)
    void onClickTicketActivate() {
        if (ownedModel.getShowButton()) {
            if (validClick){
                validClick = false;
                getIntent();
            }
        }

    }

    private void getIntent() {
        Intent starter = new Intent(getContext(), RequestTicketActivity.class);
        starter.putExtra(HEADNAME, ownedModel.getDepartmentHeadName());
        starter.putExtra(IDTICKET, ownedModel.getIdTicket());
        starter.putExtra(SHOWRADIO, ownedModel.getShowDurationDetail());
        Objects.requireNonNull(getActivity()).startActivityForResult(starter, RESULT_TICKET);
    }

    @Override
    public void onResume() {
        super.onResume();
        validClick = true;
    }

}
