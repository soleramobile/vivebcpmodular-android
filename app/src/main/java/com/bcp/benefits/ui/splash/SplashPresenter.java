package com.bcp.benefits.ui.splash;

import com.bcp.benefits.BuildConfig;
import com.bcp.benefits.R;
import com.bcp.benefits.ui.MainView;

import javax.inject.Inject;

import pe.solera.benefit.main.entity.Param;
import pe.solera.benefit.main.entity.User;
import pe.solera.benefit.main.usecases.executor.Action;
import pe.solera.benefit.main.usecases.usecases.AppUseCase;
import pe.solera.benefit.main.usecases.usecases.UserUseCase;

/**
 * Created by emontesinos.
 **/

public class SplashPresenter {

    private final AppUseCase appUseCase;
    private final UserUseCase userUseCase;
    private SplashView view;

    @Inject
    public SplashPresenter(AppUseCase appUseCase, UserUseCase userUseCase) {
        this.appUseCase = appUseCase;
        this.userUseCase = userUseCase;
    }

    public void setView(SplashView view) {
        this.view = view;
    }

    void configApp() {
        if (this.view.validateInternet()) {
            this.appUseCase.fetchParams(new Action.Callback<Param>() {
                @Override
                public void onSuccess(Param param) {
                    int lastVersion = Integer.parseInt(param.appVersion.replace(".", ""));
                    int currentVersion = Integer.parseInt(BuildConfig.VERSION_NAME.replace(".",""));

                    if (lastVersion > currentVersion) {
                        view.openUrl(param.appUrl);
                        return;
                    }

                    if (param.isBannerWatching())
                        validateLogin();
                    else
                        view.goBanner();
                }

                @Override
                public void onError(Throwable throwable) {
                    throwable.printStackTrace();
                    view.showErrorMessage(R.string.exception_generic);
                }
            });
        }
    }

     void updateLastVersion() {
        if (this.view.validateInternet()) {
           this.appUseCase.getLastVersion(new Action.Callback<String>() {
               @Override
               public void onSuccess(String s) {
                   int actualVersion = Integer.parseInt(s.replace(".", ""));
                   int version = Integer.parseInt(BuildConfig.VERSION_NAME.replace(".",""));
                   if (version >= actualVersion){
                       configApp();
                   }
                   else {
                       view.openUrl("https://descarga-vivebcp.solera.pe/");
                   }
               }

               @Override
               public void onError(Throwable throwable) {

               }
           });
        }
    }

    private void validateLogin() {
        if (this.view.validateInternet()) {
            this.userUseCase.getUser(new Action.Callback<User>() {
                @Override
                public void onSuccess(User user) {
                    if (user != null)
                        view.goToLogin();
                    else
                        view.goToValidate();
                }

                @Override
                public void onError(Throwable throwable) {
                    view.showErrorMessage(throwable.getMessage());
                }
            });
        }
    }

    interface SplashView extends MainView {
        void goToLogin();

        void goBanner();

        void goToValidate();

        void openUrl(String url);
    }
}
