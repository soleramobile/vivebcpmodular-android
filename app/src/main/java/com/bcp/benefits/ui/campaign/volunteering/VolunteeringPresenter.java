package com.bcp.benefits.ui.campaign.volunteering;

import androidx.annotation.NonNull;

import com.bcp.benefits.ui.MainView;
import com.bcp.benefits.viewmodel.CampaignViewModel;
import com.bcp.benefits.viewmodel.VolunteeringViewModel;

import java.util.List;

import javax.inject.Inject;

import pe.solera.benefit.main.entity.Campaign;
import pe.solera.benefit.main.entity.Volunteering;
import pe.solera.benefit.main.usecases.executor.Action;
import pe.solera.benefit.main.usecases.usecases.CampaignUseCase;
import retrofit2.http.Body;

/**
 * Created by emontesinos on 13/05/19.
 **/

public class VolunteeringPresenter {

    private VolunteeringView view;

    private final CampaignUseCase campaignUseCase;

    @Inject
    public VolunteeringPresenter(CampaignUseCase campaignUseCase) {
        this.campaignUseCase = campaignUseCase;
    }

    public void setView(@NonNull VolunteeringView view) {
        this.view = view;
    }

    void getListVolunteering(String idCampaing) {
       if (this.view.validateInternet()) {
            this.view.showProgress();
            campaignUseCase.getListVolunteering(idCampaing, new Action.Callback<List<Volunteering>>() {
                @Override
                public void onSuccess(List<Volunteering> volunteerings) {

                    if (volunteerings.size()>0) {
                            view.showListVolunteering(VolunteeringViewModel.toVolunteeringList(volunteerings));
                            view.hideProgress();
                        } else {
                            view.showEmptyList();
                            view.hideProgress();
                        }

                }

                @Override
                public void onError(Throwable throwable) {
                    view.showErrorMessage(throwable.getMessage());
                    view.hideProgress();
                }
            });
        }
    }


    void requestVolunteering(String idVolunteering, Integer enabledLocal, Integer enabledRole, Integer idRoleVolunteer, Integer idLocalVolunteer) {
        if (this.view.validateInternet()) {
            this.view.showProgress();
            campaignUseCase.requestSelectedVolunteering(idVolunteering, enabledLocal, enabledRole,idRoleVolunteer, idLocalVolunteer, new Action.Callback<Boolean>() {
                @Override
                public void onSuccess(Boolean aBoolean) {
                    view.showSuccessVolunteering(aBoolean);
                    view.hideProgress();
                }

                @Override
                public void onError(Throwable throwable) {
                    view.showErrorMessage(throwable.getMessage());
                    view.hideProgress();
                }
            });
        }
    }

    public interface VolunteeringView extends MainView {
        void showListVolunteering(List<VolunteeringViewModel> volunteeringViewModels);
        void showSuccessVolunteering(Boolean response);
        void showEmptyList();
    }

}
