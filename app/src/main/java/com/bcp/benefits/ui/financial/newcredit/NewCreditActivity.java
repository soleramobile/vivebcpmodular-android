package com.bcp.benefits.ui.financial.newcredit;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.bcp.benefits.R;
import com.bcp.benefits.components.PrefixEditText;
import com.bcp.benefits.ui.BaseActivity;
import com.bcp.benefits.ui.financial.ConditionAdapter;
import com.bcp.benefits.ui.financial.DialogFinanceGeneric;
import com.bcp.benefits.ui.financial.PeriodAdapter;
import com.bcp.benefits.util.AppUtils;
import com.bcp.benefits.viewmodel.CalculateViewModel;
import com.bcp.benefits.viewmodel.ConditionViewModel;
import com.bcp.benefits.viewmodel.LastActionViewModel;
import com.bcp.benefits.viewmodel.PeriodViewModel;
import com.bcp.benefits.viewmodel.ProductFinancialViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

import static com.bcp.benefits.util.Constants.DATE_START;

/**
 * Created by emontesinos on 21/05/19.
 **/

public class NewCreditActivity extends BaseActivity implements
        ConditionAdapter.OnItemClickListenerCondition, PeriodAdapter.OnItemClickListenerPeriod,
        NewCreditPresenter.NewCreditView, DialogTypeCredit.OnSelectIdTypeCreditListener {

    @Inject
    NewCreditPresenter presenter;
    @BindView(R.id.text_list_credit)
    TextView tvCreditType;
    @BindView(R.id.et_amount_required)
    PrefixEditText etAmountRequired;
    @BindView(R.id.et_credit_amount)
    PrefixEditText etCreditAmount;
    @BindView(R.id.txt_title_credit)
    TextView tvTitle;
    @BindView(R.id.recycler_months)
    RecyclerView recyclerMonths;
    @BindView(R.id.et_interest)
    EditText etInterest;
    @BindView(R.id.recycler_conditions)
    RecyclerView recyclerConditions;
    @BindView(R.id.text_mesage_monthly_fee)
    TextView tvDisclaimer;
    @BindView(R.id.text_monthly_fee)
    TextView tvAmountCalculate;
    @BindView(R.id.text_interest_rate)
    TextView titleInterest;
    @BindView(R.id.tv_credit_amount)
    TextView tvTitleCreditAmount;
    @BindView(R.id.content_credit_amount)
    LinearLayout contentCreditAmount;
    @BindView(R.id.content_list_coditions)
    ConstraintLayout contentConditions;
    @BindView(R.id.containerOptional)
    View containerOptional;
    @BindView(R.id.id_scroll)
    NestedScrollView idScroll;


    private ArrayList<ProductFinancialViewModel> list;
    private ProductFinancialViewModel itemSelected;
    private PeriodAdapter periodAdapter;
    private ConditionAdapter conditionAdapter;
    private int positionClick;
    private String type;

    public static void start(Context context, String serializableList) {
        Intent starter = new Intent(context, NewCreditActivity.class);
        starter.putExtra("list", serializableList);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        idScroll.setVisibility(View.VISIBLE);
    }

    @Override
    public void setUpView() {
        this.presenter.setView(this);
        idScroll.setVisibility(View.INVISIBLE);
        loadList();
        setupAdapters();
        setDefaultAmount();
        if (AppUtils.isConnected(this)) {
            this.presenter.setProductFinancialList(list);
        } else {
            AppUtils.dialogIsConnect(this);
            idScroll.setVisibility(View.VISIBLE);
        }

        this.etAmountRequired.setSelection(this.etAmountRequired.getText().length());
        this.etCreditAmount.setSelection(this.etCreditAmount.getText().length());
        AppUtils.updateTextWithFormat(etCreditAmount);
        AppUtils.updateTextWithFormat(etAmountRequired);

        etAmountRequired.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                AppUtils.hideKeyboard(NewCreditActivity.this);
                calculateFinances(positionClick);
                return true;
            }
            return false;
        });
    }

    private void loadList() {
        Gson gson = new Gson();
        list = gson.fromJson(getIntent().getStringExtra("list"),
                new TypeToken<List<ProductFinancialViewModel>>() {
                }.getType());
    }

    @Override
    public int getLayout() {
        return R.layout.activity_new_credit;
    }

    @OnClick(R.id.text_list_credit)
    public void onClickTypeCredit() {
        presenter.showCreditTypes();
    }

    @Override
    public void showTypeCredits(List<String> strings) {
        if (type == null){
            type = strings.get(0);
        }

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment dialogFragment = getSupportFragmentManager().findFragmentByTag("dialog");
        if (dialogFragment != null) {
            ft.remove(dialogFragment);
        }
        ft.addToBackStack(null);
        DialogTypeCredit newFragment = DialogTypeCredit.newInstance((ArrayList<String>) strings,type);
        newFragment.show(ft, "dialog");

    }

    private void setupAdapters() {
        DividerItemDecoration itemDecoration = new DividerItemDecoration(this, DividerItemDecoration.HORIZONTAL);
        itemDecoration.setDrawable(Objects.requireNonNull(ActivityCompat.getDrawable(this, R.drawable.divider_months)));
        recyclerMonths.addItemDecoration(itemDecoration);
        recyclerMonths.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));

        DividerItemDecoration itemDecoration2 = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        itemDecoration2.setDrawable(Objects.requireNonNull(ActivityCompat.getDrawable(this, R.drawable.divider_months)));
        recyclerConditions.addItemDecoration(itemDecoration2);
        recyclerConditions.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));

        periodAdapter = new PeriodAdapter();
        periodAdapter.setListener(this);
        recyclerMonths.setAdapter(periodAdapter);

        conditionAdapter = new ConditionAdapter();
        conditionAdapter.setListener(this);
        recyclerConditions.setAdapter(conditionAdapter);
    }

    private void setDefaultAmount() {
        etAmountRequired.setText("5,500");
    }

    @Override
    public void setSelected(ProductFinancialViewModel entity) {
        itemSelected = entity;
        setUpControls();
        this.conditionAdapter.setList(entity.getConditions());
    }

    private void setUpControls() {
        tvCreditType.setText(itemSelected.getName());

        if (itemSelected.getConditions() != null && !itemSelected.getConditions().isEmpty()) {
            recyclerConditions.setVisibility(View.VISIBLE);
        } else {
            recyclerConditions.setVisibility(View.GONE);
        }

        if (itemSelected.getPeriodUnit() != null && !itemSelected.getPeriodUnit().isEmpty()) {
            recyclerMonths.setVisibility(View.VISIBLE);
        } else {
            recyclerMonths.setVisibility(View.GONE);
        }

        if (itemSelected.getDisclaimer() != null) {
            tvDisclaimer.setVisibility(View.VISIBLE);
            tvDisclaimer.setText(itemSelected.getDisclaimer());
        } else {
            tvDisclaimer.setVisibility(View.GONE);
        }

        if (itemSelected.getInterest() != null) {
            titleInterest.setVisibility(View.VISIBLE);
            etInterest.setVisibility(View.VISIBLE);
            etInterest.setText(getString(R.string.value_interest, itemSelected.getInterest()));

        } else {
            titleInterest.setVisibility(View.GONE);
            etInterest.setVisibility(View.GONE);
        }

        if (itemSelected.getIdProduct() == 3) {
            contentCreditAmount.setVisibility(View.VISIBLE);
            tvTitleCreditAmount.setVisibility(View.VISIBLE);
            tvTitleCreditAmount.setText("Valor del inmueble");
        } else {
            contentCreditAmount.setVisibility(View.GONE);
            tvTitleCreditAmount.setVisibility(View.GONE);
        }

        if (itemSelected.getIdProduct() == 3) {
            contentConditions.setVisibility(View.GONE);
            containerOptional.setVisibility(View.GONE);
        } else {
            contentConditions.setVisibility(View.VISIBLE);
            containerOptional.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showPeriods(List<PeriodViewModel> periodModels, String periodUnit) {
        this.periodAdapter.setListPeriod(periodModels, periodUnit);
        this.presenter.preSelected(itemSelected.getIdProduct(), getIdPeriodSelectedIfExist(),
                getAmountRequired(), getCreditAmountIfExists(), true,
                conditionAdapter.getList());
    }

    @Override
    public void calculateProduct(CalculateViewModel calculateModel, int position) {
        tvAmountCalculate.setText(AppUtils.formatPrice(calculateModel.getAmount()));
        tvDisclaimer.setText(calculateModel.getTcea());
        etInterest.setText(getString(R.string.value_interest, calculateModel.getInterest()));
        periodAdapter.setPositionSelected((position));
    }

    @Override
    public void onItemClickCondition(ConditionViewModel entity, int position) {
        LastActionViewModel lastActionViewModel = new LastActionViewModel();
        lastActionViewModel.setId(LastActionViewModel.CONDITION);
        lastActionViewModel.setAction(position + "," + entity.isChecked());
        positionClick = periodAdapter.getPositionSelected();
        calculateFinances(periodAdapter.getPositionSelected());

    }

    @Override
    public void onItemClickPeriod(PeriodViewModel entity, int previousPosition, int newPosition) {
        LastActionViewModel lastActionViewModel = new LastActionViewModel();
        lastActionViewModel.setId(LastActionViewModel.PERIOD);
        lastActionViewModel.setAction(previousPosition);
        periodAdapter.setPositionSelected(newPosition);
        positionClick = newPosition;
        calculateFinances(newPosition);
    }

    private void calculateFinances(int newPosition) {
        this.presenter.calculate(itemSelected.getIdProduct(),
                periodAdapter.getPeriodSelected().getIdPeriod(),
                getAmountRequired(), getCreditAmountIfExists(), true,
                conditionAdapter.getList(), newPosition);
    }


    @OnClick(R.id.content_amount_required)
    void onClickVehicular() {
        AppUtils.showSoftKeyboard(etAmountRequired, this);
        etAmountRequired.setCursorVisible(true);
    }

    @OnClick(R.id.content_credit_amount)
    void onClickCreditAmount() {
        AppUtils.showSoftKeyboard(etCreditAmount, this);
        etCreditAmount.setCursorVisible(true);
    }

    @OnClick(R.id.text_btn_request)
    void onClickRequest() {
        if (validateData() && AppUtils.isConnected(this)) {
            presenter.request(itemSelected.getIdProduct(), getIdPeriodSelectedIfExist(),
                    getAmountRequired(), getCreditAmountIfExists(), true,
                    conditionAdapter.getList());
        }
    }

    private boolean validateData() {
        if (etAmountRequired.getText().toString()
                .replace(",", "")
                .trim()
                .isEmpty()) {
            Toast.makeText(this, R.string.financial_amount_required, Toast.LENGTH_SHORT).show();
            return false;
        }
        String number = etAmountRequired.getText().toString()
                .replace(",", "")
                .trim();
        double amount = Double.parseDouble(number.isEmpty() ? "0" : number);
        if (amount < 5500) {
            Toast.makeText(this, R.string.financial_atleast_5500, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    @Override
    public void showMessageSuccess() {
        DialogFinanceGeneric dialogFinanceGeneric = DialogFinanceGeneric.newInstance();
        dialogFinanceGeneric.show(getSupportFragmentManager(), DATE_START);
    }

    @OnClick(R.id.ivClose)
    void onClickClose() {
        onBackPressed();
        overridePendingTransition(R.anim.rigth_in, R.anim.right_out);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private Double getAmountRequired() {
        if (etAmountRequired.getVisibility() == View.GONE)
            return null;
        if (TextUtils.isEmpty(etAmountRequired.getText())) {
            return null;
        }

        String number = etAmountRequired.getText().toString()
                .replace(",", "")
                .trim();
        return Double.parseDouble(number.isEmpty() ? "0" : number);
    }

    private Double getCreditAmountIfExists() {
        if (etCreditAmount.getVisibility() == View.GONE)
            return null;
        if (TextUtils.isEmpty(etCreditAmount.getText().toString()
                .replace(",", "")
        )) {
            etCreditAmount.setText(R.string.amount_default);
        }
        String number = etCreditAmount.getText().toString()
                .replace(",", "")
                .trim();
        return Double.parseDouble(number.isEmpty() ? "0" : number);
    }

    private Integer getIdPeriodSelectedIfExist() {
        if (recyclerMonths.getVisibility() == View.GONE)
            return null;
        return periodAdapter.getPeriodSelected().getIdPeriod();
    }

    @Override
    public void onSelectTypeCredit(String type, int position) {
        if (type != null){
            this.type = type;
            LastActionViewModel lastActionViewModel = new LastActionViewModel();
            lastActionViewModel.setId(LastActionViewModel.CREDIT_TYPE);
            lastActionViewModel.setAction(position);
            presenter.setSelected(position);
        }else {
            this.type = type;
            LastActionViewModel lastActionViewModel = new LastActionViewModel();
            lastActionViewModel.setId(LastActionViewModel.CREDIT_TYPE);
            lastActionViewModel.setAction(position);
            presenter.setSelected(0);
        }

    }
}
