package com.bcp.benefits.ui.discounts.rate;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bcp.benefits.R;
import com.bcp.benefits.viewmodel.DiscountCalificationOptionViewModel;
import com.bcp.benefits.viewmodel.DiscountTypeViewModel;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import static com.bcp.benefits.viewmodel.DiscountTypeViewModel.DISCOUNT;


public class RateOptionsAdapter extends RecyclerView.Adapter<RateOptionsAdapter.OptionVH> {

    private Context context;
    private OnSelectCaliticationOptionListener onSelectCaliticationOptionListener;
    private ArrayList<DiscountCalificationOptionViewModel> discountCalificationOption;
    private int selectedItem = -1;
    private DiscountTypeViewModel discountType;

    public RateOptionsAdapter(Context context, DiscountTypeViewModel discountType, OnSelectCaliticationOptionListener onSelectCaliticationOptionListener) {
        this.context = context;
        this.discountType = discountType;
        this.onSelectCaliticationOptionListener = onSelectCaliticationOptionListener;
    }

    public void addOptionList(ArrayList<DiscountCalificationOptionViewModel> discountCalificationOption) {
        this.discountCalificationOption = discountCalificationOption;
        notifyDataSetChanged();
    }

    @NotNull
    @Override
    public OptionVH onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_discounts_category, parent, false);
        return new OptionVH(v);
    }

    @Override
    public void onBindViewHolder(@NotNull OptionVH holder, int position) {
        holder.tvOptionName.setText(discountCalificationOption.get(position).description);
        if (selectedItem == position) {
            holder.tvOptionName.setBackgroundResource(gradientFlv());
            holder.tvOptionName.setTextColor(ContextCompat.getColor(context, R.color.white));
            holder.tvOptionName.setTextAppearance(holder.itemView.getContext(), R.style.FontBold);
        } else {
            holder.tvOptionName.setBackgroundResource(bgFlv());
            holder.tvOptionName.setTextColor(ContextCompat.getColor(context, R.color.gray_600));
            holder.tvOptionName.setTextAppearance(holder.itemView.getContext(), R.style.FontRegular);
        }
    }

    private int resId(int first, int second) {
        return (discountType == DISCOUNT) ? first : second;
    }

    private int gradientFlv() {
        return resId(R.drawable.bg_green_gradient_button, R.drawable.bg_yellow_gradient_button);
    }

    private int bgFlv() {
        return resId(R.drawable.bg_white_green_corners_stroke_rate, R.drawable.bg_white_corners_yellow_stroke);
    }


    @Override
    public int getItemCount() {
        return discountCalificationOption != null ? discountCalificationOption.size() : 0;
    }

    class OptionVH extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvOptionName;

        OptionVH(View itemView) {
            super(itemView);
            tvOptionName = itemView.findViewById(R.id.tv_category);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (onSelectCaliticationOptionListener == null)
                return;

            if (selectedItem != -1)
                notifyItemChanged(selectedItem);

            if (selectedItem == getAdapterPosition()) {
                selectedItem = -1;
                notifyItemChanged(getAdapterPosition());
                if (onSelectCaliticationOptionListener != null)
                    onSelectCaliticationOptionListener.selectCalificationOption(null);
            } else {
                selectedItem = getAdapterPosition();
                notifyItemChanged(selectedItem);
                onSelectCaliticationOptionListener.selectCalificationOption(discountCalificationOption.get(getAdapterPosition()));
            }
        }
    }

    interface OnSelectCaliticationOptionListener {
        void selectCalificationOption(DiscountCalificationOptionViewModel option);
    }
}
