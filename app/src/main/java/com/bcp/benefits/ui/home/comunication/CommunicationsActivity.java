package com.bcp.benefits.ui.home.comunication;


import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

import com.bcp.benefits.R;
import com.bcp.benefits.components.RadioGroupComponent;
import com.bcp.benefits.ui.BaseActivity;
import com.bcp.benefits.util.AppUtils;
import com.bcp.benefits.viewmodel.CommunicationViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by emontesinos on 12/07/19.
 **/

public class CommunicationsActivity extends BaseActivity {


    private static final String COMMUNICATIONS_PARAM = "communications";
    @BindView(R.id.vp_slides)
    CustomPager vpSlides;
    @BindView(R.id.rg_pages)
    RadioGroupComponent rgPages;
    @BindView(R.id.arrow_left)
    ImageView arrowLeft;
    @BindView(R.id.arrow_right)
    ImageView arrowRight;

    private ArrayList<CommunicationViewModel> communications;
    private FragmentPagerAdapter adapter;
    private int positionUpdate = 0;

    public static void start(Context context, ArrayList<CommunicationViewModel> communications) {
        Intent starter = new Intent(context, CommunicationsActivity.class);
        starter.putExtra(COMMUNICATIONS_PARAM, communications);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT == 26) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }

    @Override
    public void setUpView() {

        communications = (ArrayList<CommunicationViewModel>) getIntent().getSerializableExtra(COMMUNICATIONS_PARAM);

        if (communications.size() > 1) {
            arrowRight.setVisibility(View.VISIBLE);
        } else {
            arrowRight.setVisibility(View.GONE);
        }
        adapter = new FragmentPagerAdapter(getSupportFragmentManager());
        vpSlides.setAdapter(adapter);
        this.adapter.setTutorialViewModels(communications);
        showPreviousAndNextPager();
        this.createRadioGroup(communications);
        vpSlides.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                showHideArrow(position);
                rgPages.setChecked(position);
                positionUpdate = position;

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }

        });

    }

    private void showPreviousAndNextPager() {
        vpSlides.setPageMargin(AppUtils.dpToPx(20));
    }


    @Override
    public int getLayout() {
        return R.layout.activity_communications;
    }


    private void showHideArrow(int position) {
        if (position != 0 && position != communications.size() - 1) {
            arrowLeft.setVisibility(View.VISIBLE);
            arrowRight.setVisibility(View.VISIBLE);
        }
        if (position == communications.size() - 1) {
            arrowRight.setVisibility(View.GONE);
            arrowLeft.setVisibility(View.VISIBLE);
        }
        if (position == 0) {
            arrowLeft.setVisibility(View.GONE);
            arrowRight.setVisibility(View.VISIBLE);
        }
        if (communications.size() == 1) {
            arrowLeft.setVisibility(View.GONE);
            arrowRight.setVisibility(View.GONE);
        }
    }

    private void createRadioGroup(List<CommunicationViewModel> list) {
        rgPages.setColor(R.drawable.bg_selected_paginator, R.drawable.bg_circle_gray);
        rgPages.setVisibility(list.isEmpty() || list.size() == 1 ? View.GONE : View.VISIBLE);
        for (CommunicationViewModel item : list) {
            AppUtils.addRadioButton(this, rgPages, list.indexOf(item),
                    list.indexOf(item) == 0, 10);
        }
        rgPages.setChecked(0);
    }

    public void finishCommunication() {
        finish();
        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_top_down);
    }

    @OnClick(R.id.iv_close)
    public void onViewClickedClose() {
        finishCommunication();
    }

    @OnClick(R.id.arrow_right)
    public void onClickArrowRight() {
        vpSlides.setCurrentItem(positionUpdate + 1);

    }

    @OnClick(R.id.arrow_left)
    public void onClickArrowLeft() {
        vpSlides.setCurrentItem(positionUpdate - 1);

    }


}
