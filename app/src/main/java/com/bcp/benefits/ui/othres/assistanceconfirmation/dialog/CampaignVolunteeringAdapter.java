package com.bcp.benefits.ui.othres.assistanceconfirmation.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.bcp.benefits.R;
import com.bcp.benefits.viewmodel.CampaignVolunteeringViewModel;
import org.jetbrains.annotations.NotNull;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by emontesinos on 13/01/20.
 */

public class CampaignVolunteeringAdapter extends RecyclerView.Adapter<CampaignVolunteeringAdapter.CampaignVolunteeringViewHolder> {

    private ArrayList<CampaignVolunteeringViewModel> list = new ArrayList<>();
    private OnItemClickCampaignListener listener;
    private int selectedItem = -1;
    private Context context;

     CampaignVolunteeringAdapter(Context context) {
        this.context = context;
    }

     void addCategories(ArrayList<CampaignVolunteeringViewModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public void setSelectedItem(CampaignVolunteeringViewModel campaignVolunteeringViewModel) {
        if (campaignVolunteeringViewModel == null) return;
        int index = list.indexOf(campaignVolunteeringViewModel);
        if (selectedItem != index) {
            selectedItem = index;
            notifyDataSetChanged();
        }
    }

    @NotNull
    @Override
    public CampaignVolunteeringAdapter.CampaignVolunteeringViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_type_custom, parent, false);
        return new CampaignVolunteeringViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NotNull CampaignVolunteeringViewHolder holder, int position) {
        holder.text.setText( list.get(position).getName());
        if (selectedItem == position) {
            holder.imgCircle.setVisibility(View.VISIBLE);
            holder.imgCircle.setBackground(context.getResources().getDrawable(R.drawable.bg_cicle_green));
        } else {
            holder.imgCircle.setVisibility(View.INVISIBLE);
        }
    }

    public void setListener(OnItemClickCampaignListener listener) {
        this.listener = listener;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class CampaignVolunteeringViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        @BindView(R.id.txt_type_texto)
        TextView text;
        @BindView(R.id.img_circle)
        ImageView imgCircle;


         CampaignVolunteeringViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (listener == null)
                return;

            if (selectedItem != -1)
                notifyItemChanged(selectedItem);


            if (selectedItem == getAdapterPosition()) {
                selectedItem = -1;
                notifyItemChanged(getAdapterPosition());
                if (listener != null)
                    listener.onItemCampaignClick(null, getAdapterPosition());
            } else {
                selectedItem = getAdapterPosition();
                notifyItemChanged(selectedItem);
                listener.onItemCampaignClick(list.get(getAdapterPosition()), getAdapterPosition());
            }

        }
    }

    public interface OnItemClickCampaignListener {
        void onItemCampaignClick(CampaignVolunteeringViewModel entity, int position);
    }
}
