package com.bcp.benefits.ui.campaign.aguinaldo;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bcp.benefits.R;
import com.bcp.benefits.viewmodel.CampaignProductViewModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by emontesinos on 15/05/19.
 **/

public class AguinaldoAdapter extends RecyclerView.Adapter<AguinaldoAdapter.AguinaldoViewHolder> {

    private ArrayList<CampaignProductViewModel> list;
    private ListenerViewAdapterDetail listener;
    Context context;
    private int selectedItem = -1;
    private boolean selectProducts = false;

    AguinaldoAdapter(Context context, ArrayList<CampaignProductViewModel> list, ListenerViewAdapterDetail listener, boolean selectProducts) {
        this.list = list;
        this.context = context;
        this.listener = listener;
        this.selectProducts = selectProducts;
    }

    @NonNull
    @Override
    public AguinaldoViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_product_bonus, viewGroup, false);
        return new AguinaldoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AguinaldoViewHolder holder, int position) {
        CampaignProductViewModel campaign = list.get(position);

        holder.titleProduct.setText(campaign.getTitle());
        if (campaign.getImage() != null && !campaign.getImage().isEmpty()) {
            Picasso.get().load(campaign.getImage()).placeholder(R.drawable.ic_place_campam).into(holder.imgProductBonus);

        } else {
            holder.imgProductBonus.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_place_campam));
        }


        String cad = "";
        for (int i = 0; i < campaign.getDescriptions().size(); i++) {
            cad = cad + campaign.getDescriptions().get(i) + "\n";
        }
        holder.descriptionProduct.setText(cad);
        holder.moreCampaign.setOnClickListener(v -> listener.goDialogDetail(list.get(position)));

        if (position == selectedItem) {
            listener.selectProduct(campaign);
            holder.imgSelect.setVisibility(View.VISIBLE);
        } else {
            holder.imgSelect.setVisibility(View.GONE);
        }
        holder.itemView.setOnClickListener(v -> {
            if (!selectProducts)
                return;
            if (selectedItem != position) {
                selectedItem = position;
                notifyDataSetChanged();
            } else {
                notifyDataSetChanged();

            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class AguinaldoViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_product_bonus)
        ImageView imgProductBonus;
        @BindView(R.id.img_check_select)
        ImageView imgSelect;
        @BindView(R.id.txt_title_product)
        TextView titleProduct;
        @BindView(R.id.txt_description_product)
        TextView descriptionProduct;
        @BindView(R.id.txt_more_campaign)
        TextView moreCampaign;

        public AguinaldoViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    interface ListenerViewAdapterDetail {
        void goDialogDetail(CampaignProductViewModel entity);

        void selectProduct(CampaignProductViewModel select);
    }

}
