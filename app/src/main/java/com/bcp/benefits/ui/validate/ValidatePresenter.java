package com.bcp.benefits.ui.validate;

import android.content.Context;
import android.util.Patterns;

import androidx.annotation.NonNull;

import com.bcp.benefits.R;
import com.bcp.benefits.ui.MainView;
import com.bcp.benefits.viewmodel.TypeDocViewModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Pattern;

import javax.inject.Inject;

import pe.solera.benefit.main.usecases.executor.Action;
import pe.solera.benefit.main.usecases.usecases.UserUseCase;

/**
 * Created by emontesinos.
 **/

public class ValidatePresenter {

    private RegisterView view;
    private final UserUseCase userUseCase;
    private final Context context;

    @Inject
    public ValidatePresenter(UserUseCase userUseCase, Context context) {
        this.userUseCase = userUseCase;
        this.context = context;
    }

    public void setView(@NonNull RegisterView view) {
        this.view = view;
    }
    void loadRegister(String codeEmploy, String birthDate, String numberDoc, String emailPersonal,
                      String numberMobile, Integer docType) {
        this.view.showProgress();
        if (validate(codeEmploy, birthDate, numberDoc, emailPersonal, numberMobile)) {
            if (this.view.validateInternet()) {
                this.view.showProgress();
                this.userUseCase.validate(numberDoc, docType, codeEmploy, birthDate, emailPersonal,
                        numberMobile, new Action.Callback<Boolean>() {
                            @Override
                            public void onSuccess(Boolean isValid) {
                                view.goHome();
                                view.hideProgress();
                            }

                            @Override
                            public void onError(Throwable throwable) {
                                view.showErrorMessage(throwable.getMessage());
                                view.activateButton();
                                view.hideProgress();
                            }
                        });
            }
        } else
            view.hideProgress();

    }

    private Boolean validate(String codeEmploy, String birthDate, String numberDoc,
                             String emailPersonal, String numberMobile) {
        if (codeEmploy.isEmpty()) {
            view.fieldEmpty(context.getString(R.string.mesage_error_code_worker),
                    R.id.edit_code_employ);
            view.activateButton();
            return false;
        } else {
            view.field(R.id.edit_code_employ);
        }
        if (codeEmploy.length() < 5) {
            view.fieldEmpty(context.getString(R.string.message_error_code_worker_length),
                    R.id.edit_code_employ);
            view.activateButton();
            return false;
        } else {
            view.field(R.id.edit_code_employ);
        }
        if (birthDate.isEmpty()) {
            view.fieldEmpty(context.getString(R.string.mesage_error_birthDate), 0);
            view.activateButton();
            return false;
        } else {
            view.field(R.id.et_employee_birthday);
        }
        if (numberDoc.isEmpty()) {
            view.fieldEmpty(context.getString(R.string.mesage_error_number_doc), R.id.edit_number_doc);
            view.activateButton();
            return false;
        } else {
            view.field(R.id.edit_number_doc);
        }
        if (emailPersonal.isEmpty()) {
            view.fieldEmpty(context.getString(R.string.mesage_error_email_personal), R.id.edit_email_personal);
            view.activateButton();
            return false;
        } else {
            view.field(R.id.edit_email_personal);
        }

        if (!validarEmail(emailPersonal)) {
            view.fieldEmpty(context.getString(R.string.mesage_error_email_format), R.id.edit_email_personal);
            view.activateButton();
            return false;
        } else {
            view.field(R.id.edit_email_personal);
        }

        if (numberMobile.isEmpty()) {
            view.fieldEmpty(context.getString(R.string.mesage_error_number_phone), R.id.edit_number_mobile);
            view.activateButton();
            return false;
        } else {
            view.field(R.id.edit_number_mobile);
        }
        if (numberMobile.length() < 9) {
            view.fieldEmpty(context.getString(R.string.message_error_phone_number_size), R.id.edit_number_mobile);
            view.activateButton();
            return false;
        } else {
            view.field(R.id.edit_number_mobile);
        }

        return true;
    }

    void loadTypes() {
        ArrayList<String> types = new ArrayList<>(Arrays.asList(context.getResources().getStringArray(R.array.type_doc)));
        ArrayList<TypeDocViewModel> typesDoc = new ArrayList<>();
        for (String value : types) {
            typesDoc.add(new TypeDocViewModel(value, types.indexOf(value) + 1));
        }
        view.showTypes(typesDoc);
    }

    private boolean validarEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    public interface RegisterView extends MainView {

        void goHome();

        void fieldEmpty(String message, int field);

        void field(int field);

        void showTypes(ArrayList<TypeDocViewModel> types);

        void activateButton();
    }
}
