package com.bcp.benefits.ui.campaign.productDetail;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

import com.bcp.benefits.R;
import com.bcp.benefits.components.AdjustImage;
import com.bcp.benefits.viewmodel.CampaignProductViewModel;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.bcp.benefits.util.Constants.DETAIL_PARAM;

public class DialogProductDetail extends DialogFragment {

    public static DialogProductDetail newInstance(CampaignProductViewModel productModel) {
        DialogProductDetail fragments = new DialogProductDetail();
        Bundle args = new Bundle();
        args.putSerializable(DETAIL_PARAM, productModel);
        fragments.setArguments(args);
        return fragments;
    }

    @BindView(R.id.txt_title_product)
    TextView tvTitle;
    @BindView(R.id.img_detail_product)
    AdjustImage ivImage;
    @BindView(R.id.txt_description_product)
    TextView tvDescription;
    @BindView(R.id.img_close_detail)
    ImageView closeDetail;

    private CampaignProductViewModel detailInfo;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            detailInfo = (CampaignProductViewModel) getArguments().getSerializable(DETAIL_PARAM);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_product_detail, container, false);
        ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (detailInfo.getTitle() != null && !detailInfo.getTitle().isEmpty()) {
            tvTitle.setText(detailInfo.getTitle());
        } else {
            tvTitle.setVisibility(View.GONE);
        }
        if (detailInfo.getDescriptions() != null && !detailInfo.getDescriptions().isEmpty()) {
            String cad = "";
            for (int i = 0; i < detailInfo.getDescriptions().size(); i++) {
                cad = cad + detailInfo.getDescriptions().get(i) + "\n" + "\n";
            }
            tvDescription.setText(cad);
        } else {
            tvDescription.setVisibility(View.GONE);
        }

        if (detailInfo.getImage() != null && !detailInfo.getImage().isEmpty()) {
            Picasso.get().load(detailInfo.getImage()).placeholder(R.drawable.ic_place_campam).into(ivImage);
        } else {
            ivImage.setImageDrawable(getResources().getDrawable(R.drawable.ic_place_campam));
        }
    }

    @NotNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        Objects.requireNonNull(dialog.getWindow()).requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            Drawable backgroundColor = new ColorDrawable(ContextCompat.getColor
                    (Objects.requireNonNull(getContext()), android.R.color.transparent));
            Objects.requireNonNull(dialog.getWindow()).setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            dialog.getWindow().setBackgroundDrawable(backgroundColor);
        }
    }

    @OnClick(R.id.img_close_detail)
    public void onClickClose() {
        dismiss();
    }
}
