package com.bcp.benefits.ui.othres.assistanceconfirmation.dialog;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bcp.benefits.R;
import com.bcp.benefits.viewmodel.LocalVolunteeringViewModel;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by emontesinos on 13/01/20.
 */

public class LocalVolunteeringDialogFragment extends DialogFragment implements LocalVolunteeringAdapter.OnItemClickLocalListener {


    @BindView(R.id.rv_type)
    RecyclerView rvType;
    @BindView(R.id.iv_close)
    ImageView ivClose;
    @BindView(R.id.constraintLayout9)
    ConstraintLayout contentDialog;
    @BindView(R.id.text_title_generic)
    TextView textGeneric;

    private OnSelectLocalListener onSelectLocalListener;
    private ArrayList<LocalVolunteeringViewModel> listCampaignVolunteering = new ArrayList<>();
    private LocalVolunteeringViewModel campaignVolunteeringSelected;

    public static LocalVolunteeringDialogFragment newInstance(LocalVolunteeringViewModel stateSelected, ArrayList<LocalVolunteeringViewModel> types
            , OnSelectLocalListener onSelectLocalListener
    ) {
        LocalVolunteeringDialogFragment fragment = new LocalVolunteeringDialogFragment();
        Bundle args = new Bundle();
        fragment.onSelectLocalListener = onSelectLocalListener;
        args.putSerializable("dataLocalVolunteering", types);
        args.putSerializable("localSelected", stateSelected);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow()
                .getAttributes().windowAnimations = R.style.DialogTheme;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            Drawable backgroundColor = new ColorDrawable(ContextCompat.getColor(
                    Objects.requireNonNull(getContext()), android.R.color.transparent));
            Objects.requireNonNull(dialog.getWindow()).setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            dialog.getWindow().setBackgroundDrawable(backgroundColor);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_identity_types, container, false);
        ButterKnife.bind(this, v);

        listCampaignVolunteering = (ArrayList<LocalVolunteeringViewModel>) getArguments().getSerializable("dataLocalVolunteering");
        campaignVolunteeringSelected = (LocalVolunteeringViewModel) getArguments().getSerializable("localSelected");

        return v;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        textGeneric.setText(R.string.by_selected_Lo);
        ivClose.setImageResource(R.drawable.ic_close_white);
        contentDialog.setBackground(getResources().getDrawable(R.drawable.bg_type_custom_green));
        ivClose.setOnClickListener(v -> getDialog().dismiss());
        rvType.setLayoutManager(new LinearLayoutManager(getContext()));
        LocalVolunteeringAdapter localVolunteeringAdapter = new LocalVolunteeringAdapter(getContext());
        DividerItemDecoration itemDecoration = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        rvType.addItemDecoration(itemDecoration);
        rvType.setAdapter(localVolunteeringAdapter);
        localVolunteeringAdapter.addCategories(listCampaignVolunteering);
        localVolunteeringAdapter.setSelectedItem(campaignVolunteeringSelected);
        localVolunteeringAdapter.setListener(this);

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        Objects.requireNonNull(dialog.getWindow()).requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onItemLocalClick(LocalVolunteeringViewModel entity, int position) {
        onSelectLocalListener.thirdCombo(entity, position);
        dismiss();

    }

    public interface OnSelectLocalListener {
        void thirdCombo(LocalVolunteeringViewModel entity, int position);
    }
}
