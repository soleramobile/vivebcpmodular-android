package com.bcp.benefits.ui.othres.licenses.mylicences;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import org.jetbrains.annotations.NotNull;
import java.util.List;
import com.bcp.benefits.R;
import com.bcp.benefits.ui.othres.golden.mytickets.CarouselLinearLayout;
import com.bcp.benefits.viewmodel.OwnedViewModel;
/**
 * Created by emontesinos on 08/05/19.
 **/
public class CarouselLicencePagerAdapter extends FragmentPagerAdapter implements ViewPager.OnPageChangeListener{

    final static float BIG_SCALE = 1.0f;
    private final static float SMALL_SCALE = 0.7f;
    private final static float DIFF_SCALE = BIG_SCALE - SMALL_SCALE;
    private final MysLicencesFragment context;
    private final FragmentManager fragmentManager;
    private final List<OwnedViewModel> ownedModels;

    public CarouselLicencePagerAdapter(MysLicencesFragment context, FragmentManager fm, List<OwnedViewModel> ownedModels) {
        super(fm);
        this.fragmentManager = fm;
        this.context = context;
        this.ownedModels = ownedModels;
    }

    @Override
    public Fragment getItem(int position) {
        try {
            position = position % MysLicencesFragment.count;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return LicenceFragment.newInstance(context,ownedModels.get(position));

    }

    public int getItemPosition(@NotNull Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        int count = 0;
        try {
            count = MysLicencesFragment.count * MysLicencesFragment.LOOPS;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return count;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        try {
            if (positionOffset >= 0f && positionOffset <= 1f) {
                CarouselLinearLayout cur = getRootView(position);
                CarouselLinearLayout next = getRootView(position + 1);
                cur.setScaleBoth(BIG_SCALE - DIFF_SCALE * positionOffset);
                next.setScaleBoth(SMALL_SCALE + DIFF_SCALE * positionOffset);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @SuppressWarnings("ConstantConditions")
    private CarouselLinearLayout getRootView(int position) {
        return (CarouselLinearLayout) fragmentManager.findFragmentByTag(
                this.getFragmentTag(position)).getView().findViewById(R.id.root_container);
    }

    private String getFragmentTag(int position) {
        return "android:switcher:" + context.pager.getId() + ":" + position;
    }
}
