package com.bcp.benefits.ui.othres;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import org.jetbrains.annotations.NotNull;
import com.bcp.benefits.ui.othres.golden.mytickets.MyTicketsFragment;
import com.bcp.benefits.ui.othres.golden.ticketsapproved.TicketsApprovedFragment;
import com.bcp.benefits.ui.othres.licenses.licenceapproved.LicenceApprovedFragment;
import com.bcp.benefits.ui.othres.licenses.mylicences.MysLicencesFragment;
import com.bcp.benefits.viewmodel.GoldenTicketViewModel;

import static com.bcp.benefits.util.Constants.IDENTIFIER_TICKET;

/**
 * Created by emontesinos on 02/05/19.
 **/

public class GenericPagerAdapter extends FragmentStatePagerAdapter {

    private GoldenTicketViewModel goldenTicketModel;
    private int identifierBenefit;

    public GenericPagerAdapter(FragmentManager fm, int identifierBenefit) {
        super(fm);
        this.identifierBenefit = identifierBenefit;
    }

    public void addGoldenTicketResponse(GoldenTicketViewModel goldenTicketModel){
        this.goldenTicketModel = goldenTicketModel;
        notifyDataSetChanged();
    }

    @Override
    public Fragment getItem(int position) {
        if (identifierBenefit == IDENTIFIER_TICKET){
            if(position == 0)
                return MyTicketsFragment.newInstance(goldenTicketModel);
            else
                return TicketsApprovedFragment.newInstance(goldenTicketModel);
        }else {
            if(position == 0)
                return MysLicencesFragment.newInstance(goldenTicketModel);
            else
                return LicenceApprovedFragment.newInstance(goldenTicketModel);
        }
    }

    @Override
    public int getItemPosition(@NotNull Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return goldenTicketModel==null?0:2;
    }
}
