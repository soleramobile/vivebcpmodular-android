package com.bcp.benefits.ui.discounts.detail;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

import com.bcp.benefits.R;
import com.bcp.benefits.util.AppUtils;
import com.bcp.benefits.viewmodel.DiscountTypeViewModel;
import com.bcp.benefits.viewmodel.DiscountViewModel;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DiscountDetailDialogFragment extends DialogFragment {

    public static DiscountDetailDialogFragment newInstance(DiscountTypeViewModel discountTypeViewModel, OnSelectSuccesListener onSelectSuccesListener,DiscountViewModel discountViewModel) {
        DiscountDetailDialogFragment fragment = new DiscountDetailDialogFragment();
        fragment.onSelectSuccesListener = onSelectSuccesListener;
        fragment.discountViewModel = discountViewModel;
        Bundle args = new Bundle();
        args.putSerializable("discountTypeViewModel", discountTypeViewModel);
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.btn_register_generic)
    TextView btnAccept;
    @BindView(R.id.img_close_generic)
    ImageView imgClose;
    @BindView(R.id.text_title_generic)
    TextView title;
    @BindView(R.id.txt_negative)
    TextView btnNegative;
    @BindView(R.id.content_dialog)
    ConstraintLayout contentDialog;


    private DiscountViewModel discountViewModel;
    private DiscountTypeViewModel discountTypeViewModel;
    private OnSelectSuccesListener onSelectSuccesListener;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_custom_generic, container, false);
        ButterKnife.bind(this, v);
        discountTypeViewModel = (DiscountTypeViewModel) getArguments().getSerializable("discountTypeViewModel");
        dialogSelect();
        return v;
    }

    @NotNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        Objects.requireNonNull(dialog.getWindow()).requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            Drawable backgroundColor = new ColorDrawable(ContextCompat.getColor
                    (Objects.requireNonNull(getContext()), android.R.color.transparent));
            Objects.requireNonNull(dialog.getWindow()).setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            dialog.getWindow().setBackgroundDrawable(backgroundColor);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow()
                .getAttributes().windowAnimations = R.style.DialogTheme;
    }

    @OnClick({R.id.img_close_generic, R.id.txt_negative})
    public void onDialogDismiss() {
        dismiss();

    }

    @OnClick(R.id.btn_register_generic)
    public void onRegister() {
        if (discountTypeViewModel == DiscountTypeViewModel.DISCOUNT ||
                discountTypeViewModel == DiscountTypeViewModel.EDUTATION) {
            onSelectSuccesListener.onSuccess();
            dismiss();
        }
    }

    private void dialogSelect() {
        if (discountTypeViewModel == DiscountTypeViewModel.DISCOUNT) {
            title.setText(R.string.dialog_use_discount_text);
            imgClose.setImageResource(R.drawable.ic_close_white);
            contentDialog.setBackgroundResource(R.drawable.bg_type_custom_green);
            AppUtils.setGradient(getResources().getColor(R.color.green_start), getResources().getColor(R.color.green_end),
                    AppUtils.dpToPx((int) getResources().getDimension(R.dimen.size_thirteen)), btnAccept);
            btnNegative.setBackgroundResource(R.drawable.bg_white_corners_green_stroke_two);
        } else if (discountTypeViewModel == DiscountTypeViewModel.EDUTATION) {
            title.setText(R.string.dialog_use_discount_text);
            imgClose.setImageResource(R.drawable.ic_close_white_yellow);
            contentDialog.setBackgroundResource(R.drawable.bg_type_custom_yellow);
            btnAccept.setBackgroundResource(R.drawable.bg_button_discount_yellow);
            btnNegative.setBackgroundResource(R.drawable.bg_white_corners_yellow_stroke);
        }
    }

    public interface OnSelectSuccesListener {
        void onSuccess();
    }
}
