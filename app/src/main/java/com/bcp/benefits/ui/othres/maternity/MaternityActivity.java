package com.bcp.benefits.ui.othres.maternity;


import android.content.Context;
import android.content.Intent;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.yarolegovich.discretescrollview.DiscreteScrollView;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import com.bcp.benefits.R;
import com.bcp.benefits.components.RadioGroupComponent;
import com.bcp.benefits.ui.BaseActivity;
import com.bcp.benefits.util.AppUtils;

/**
 * Created by emontesinos on 30/04/19.
 **/

public class MaternityActivity extends BaseActivity implements MaternityPresenter.MaternityView,
        DiscreteScrollView.OnItemChangedListener,
        View.OnClickListener {

    @Inject
    MaternityPresenter presenter;

    @BindView(R.id.toolbar)
    ConstraintLayout toolbar;
    @BindView(R.id.title_toolbar)
    TextView titleToobal;
    @BindView(R.id.img_back)
    ImageButton imgBack;
    @BindView(R.id.recycler_maternity)
    DiscreteScrollView rvMaternity;
    @BindView(R.id.rg_banners)
    RadioGroupComponent mRgBanners;


    public static void start(Context context) {
        Intent intent = new Intent(context, MaternityActivity.class);
        context.startActivity(intent);
    }

    @Override
    public void setUpView() {
        this.presenter.setView(this);
        getToolbar();
        this.presenter.goListaMaternity();


    }

    private void createRadioGroups(ArrayList<String> list) {
        for (String item : list) {
            AppUtils.addRadioButton(this, mRgBanners, list.indexOf(item),
                    list.indexOf(item) == 0, 10);
        }
        mRgBanners.setChecked(0);
    }

    public void getToolbar() {
        titleToobal.setText(R.string.maternity);
        imgBack.setOnClickListener(v -> {
            onBackPressed();
            overridePendingTransition(R.anim.rigth_in, R.anim.right_out);
        });
    }

    @Override
    public int getLayout() {
        return R.layout.activity_maternity;
    }


    @Override
    public void onClick(View v) {

    }

    @Override
    public void onCurrentItemChanged(@Nullable RecyclerView.ViewHolder viewHolder, int adapterPosition) {
        mRgBanners.setChecked(adapterPosition);

    }

    @Override
    public void showBenenitsMaternity(ArrayList<String> list) {
        MaternityAdapter adapter = new MaternityAdapter(this, list);
        rvMaternity.setAdapter(adapter);
        rvMaternity.addOnItemChangedListener(this);
        this.createRadioGroups(list);

    }
}
