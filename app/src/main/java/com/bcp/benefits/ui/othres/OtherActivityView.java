package com.bcp.benefits.ui.othres;

import com.bcp.benefits.ui.MainView;

/**
 * Created by emontesinos on 30/04/19.
 **/

public interface  OtherActivityView extends MainView {
    void showFlag(int response);
}
