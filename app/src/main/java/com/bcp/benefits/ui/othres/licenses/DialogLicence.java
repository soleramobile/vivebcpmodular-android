package com.bcp.benefits.ui.othres.licenses;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

import com.bcp.benefits.R;
import com.bcp.benefits.ui.othres.licenses.requestlicence.LicenceRequestActivity;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.bcp.benefits.util.Constants.IDENTIFIER_DIALOG;
import static com.bcp.benefits.util.Constants.IDENTIFIER_LICENCE_APROVAL_DIALOG;
import static com.bcp.benefits.util.Constants.IDENTIFIER_LICENCE_REFUSE_DIALOG;
import static com.bcp.benefits.util.Constants.IDENTIFIR_LICENCE_FORCE_APROVAL_DIALOG;
import static com.bcp.benefits.util.Constants.ID_CAMPAIGN_LICENCE;
import static com.bcp.benefits.util.Constants.MESSAGE_SERVICE;

public class DialogLicence extends DialogFragment {

    public static DialogLicence newInstance(String dialogIdentifier,int idLicence) {
        DialogLicence fragment = new DialogLicence();
        Bundle args = new Bundle();
        args.putString(IDENTIFIER_DIALOG, dialogIdentifier);
        args.putInt(ID_CAMPAIGN_LICENCE, idLicence);
        fragment.setArguments(args);
        return fragment;
    }

    public static DialogLicence newInstance(String dialogIdentifier,String message) {
        DialogLicence fragment = new DialogLicence();
        Bundle args = new Bundle();
        args.putString(IDENTIFIER_DIALOG, dialogIdentifier);
        args.putString(MESSAGE_SERVICE, message);
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.btn_register_generic)
    Button btnAcept;
    @BindView(R.id.img_close_generic)
    ImageView imgClose;
    @BindView(R.id.text_title_generic)
    TextView title;
    @BindView(R.id.txt_negative)
    Button btnNegative;
    @BindView(R.id.content_dialog)
    ConstraintLayout contentDialog;

    private String identifierDialog;
    private int id;
    private String message;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_custom_generic, container, false);
        ButterKnife.bind(this, v);
        assert getArguments() != null;
        identifierDialog = getArguments().getString(IDENTIFIER_DIALOG);
        id = getArguments().getInt(ID_CAMPAIGN_LICENCE);
        message = getArguments().getString(MESSAGE_SERVICE);
        dialogSelect();
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow()
                .getAttributes().windowAnimations = R.style.DialogTheme;
    }

    @NotNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        Objects.requireNonNull(dialog.getWindow()).requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            Drawable backgroundColor = new ColorDrawable(ContextCompat.getColor
                    (Objects.requireNonNull(getContext()), android.R.color.transparent));
            Objects.requireNonNull(dialog.getWindow()).setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            dialog.getWindow().setBackgroundDrawable(backgroundColor);
        }
    }

    @OnClick({R.id.img_close_generic,R.id.txt_negative})
    public void onDialogDismiss() {
        dismiss();
    }

    @OnClick(R.id.btn_register_generic)
    public void onRegister() {
        switch (identifierDialog){
            case IDENTIFIR_LICENCE_FORCE_APROVAL_DIALOG:
                ((LicenceRequestActivity) Objects.requireNonNull(getActivity())).requestSuccess();
                break;
            case IDENTIFIER_LICENCE_REFUSE_DIALOG:
                ((LicencesActivity) Objects.requireNonNull(getActivity())).cancelDialogLicence(id);
                break;
            case IDENTIFIER_LICENCE_APROVAL_DIALOG:
                ((LicencesActivity) Objects.requireNonNull(getActivity())).approveDialogLicence(id);
                break;
        }
    }

    private void dialogSelect(){
        switch (identifierDialog){
           case IDENTIFIR_LICENCE_FORCE_APROVAL_DIALOG:
                title.setText(R.string.title_mi_banco);
                imgClose.setVisibility(View.GONE);
                break;

           case IDENTIFIER_LICENCE_REFUSE_DIALOG:
                title.setText(R.string.refuse_permissionn);
               imgClose.setImageResource(R.drawable.ic_close_green);
                btnAcept.setText(R.string.accept);
                break;

           case IDENTIFIER_LICENCE_APROVAL_DIALOG:
                title.setText(R.string.approved_permissionn);
               imgClose.setImageResource(R.drawable.ic_close_green);
                break;
        }


    }
}
