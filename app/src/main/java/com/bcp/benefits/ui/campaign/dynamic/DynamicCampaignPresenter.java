package com.bcp.benefits.ui.campaign.dynamic;


import androidx.annotation.NonNull;

import pe.solera.benefit.main.entity.CampaignDynamic;
import pe.solera.benefit.main.usecases.executor.Action;
import pe.solera.benefit.main.usecases.usecases.CampaignUseCase;

import java.util.List;

import javax.inject.Inject;

import com.bcp.benefits.ui.MainView;
import com.bcp.benefits.viewmodel.CampaignDynamicViewModel;
import com.bcp.benefits.viewmodel.DynamicFieldValueViewModel;

/**
 * Created by emontesinos on 20/05/19.
 **/
public class DynamicCampaignPresenter {


    private DinamicCampaignView view;

    private CampaignUseCase campaignUseCase;

    @Inject
    DynamicCampaignPresenter(CampaignUseCase campaignUseCase) {
        this.campaignUseCase = campaignUseCase;
    }

    public void setView(@NonNull DinamicCampaignView view) {
        this.view = view;

    }

    public void getCompaign(int id) {
        if (this.view.validateInternet()) {
            this.view.showProgress();
            campaignUseCase.getCampaignById(id, new Action.Callback<CampaignDynamic>() {
                @Override
                public void onSuccess(CampaignDynamic campaignDynamic) {

                    view.showForm(new CampaignDynamicViewModel(campaignDynamic));
                    view.hideProgress();

                }

                @Override
                public void onError(Throwable throwable) {

                    view.showErrorMessage(throwable.getMessage());
                    view.hideProgress();

                }
            });
        }

    }

    void refer(int campaignId, List<DynamicFieldValueViewModel> valueList) {
        if (this.view.validateInternet()) {
            this.view.showProgress();
            campaignUseCase.referCampaign(campaignId,
                    DynamicFieldValueViewModel.toDynamicList(valueList), new Action.Callback<String>() {
                        @Override
                        public void onSuccess(String s) {
                            view.onReferSuccess();
                            view.hideProgress();
                        }

                        @Override
                        public void onError(Throwable throwable) {
                            view.showErrorMessage(throwable.getMessage());
                            view.hideProgress();
                        }
                    });
        }

    }

    public interface DinamicCampaignView extends MainView {
        void showForm(CampaignDynamicViewModel list);

        void onReferSuccess();
    }
}
