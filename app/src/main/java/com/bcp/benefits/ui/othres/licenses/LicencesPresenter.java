package com.bcp.benefits.ui.othres.licenses;

import androidx.annotation.NonNull;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import com.bcp.benefits.viewmodel.ForApprovalViewModel;
import com.bcp.benefits.viewmodel.GoldenTicketViewModel;
import com.bcp.benefits.viewmodel.OwnedViewModel;
/**
 * Created by emontesinos on 07/05/19.
 **/

public class LicencesPresenter {

    private LicencesView view;

    @Inject
    LicencesPresenter() { }
    public void setView(@NonNull LicencesView view) {
        this.view = view;
    }

    void doApproveTicket(final int idTicket, boolean force){
        view.successApprove("mensaje");

    }

    void doCancelTicket(final int idTicket){
        view.successCancel("mensaje");

    }

    void goLicences(){
        GoldenTicketViewModel ticketModels = new GoldenTicketViewModel();
        ticketModels.setDepartmentHead(true);
        List<ForApprovalViewModel> lisApproved = new ArrayList<>();
        lisApproved.add(new ForApprovalViewModel());
        lisApproved.add(new ForApprovalViewModel());
        lisApproved.add(new ForApprovalViewModel());
        lisApproved.add(new ForApprovalViewModel());
        List<OwnedViewModel> lisOwne = new ArrayList<>();


        OwnedViewModel owe1 = new OwnedViewModel();
        owe1.setShowButton(true);
        owe1.setDepartmentHeadName("medic");
        owe1.setUsageDateFormatted("Para el Viernes 26 de Mayo");
        owe1.setShowDurationDetail(true);

        OwnedViewModel owe2 = new OwnedViewModel();
        owe2.setShowButton(true);
        owe2.setDepartmentHeadName("cumpleanios");
        owe2.setUsageDateFormatted("Para el Viernes 26 de Mayo");
        owe2.setShowDurationDetail(true);


        OwnedViewModel owe3 = new OwnedViewModel();
        owe3.setShowButton(true);
        owe3.setDepartmentHeadName("muerte");
        owe3.setUsageDateFormatted("Para el Viernes 26 de Mayo");
        owe3.setShowDurationDetail(true);


        OwnedViewModel owe4 = new OwnedViewModel();
        owe4.setShowButton(true);
        owe4.setDepartmentHeadName("maternidad");
        owe4.setUsageDateFormatted("Para el Viernes 26 de Mayo");
        owe4.setShowDurationDetail(true);



        OwnedViewModel owe5 = new OwnedViewModel();
        owe5.setShowButton(false);
        owe5.setStatusText("approved");
        owe5.setReleaseable(true);
        owe5.setUsageDateFormatted("Para el Viernes 26 de Mayo");

        OwnedViewModel owe6 = new OwnedViewModel();
        owe6.setShowButton(false);
        owe6.setReleaseable(true);
        owe6.setUsageDateFormatted("Para el Viernes 26 de Mayo");
        owe6.setStatusText("pending");

        lisOwne.add(owe1);
        lisOwne.add(owe2);
        lisOwne.add(owe3);
        lisOwne.add(owe4);
        lisOwne.add(owe5);
        lisOwne.add(owe6);
        ticketModels.setForApproval(lisApproved);
        ticketModels.setOwned(lisOwne);
        ticketModels.setDepartmentHead(true);

        view.showLicences(ticketModels);
    }

    public interface  LicencesView {
        void showLicences(GoldenTicketViewModel goldenTicketModel);
        void successApprove(String message);
        void successCancel(String message);
    }
}
