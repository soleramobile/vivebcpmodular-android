package com.bcp.benefits.ui.tutorial;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

import com.bcp.benefits.viewmodel.TutorialViewModel;

/**
 * Created by emontesinos.
 **/

class TutorialPagerAdapter extends FragmentStatePagerAdapter {
    private List<TutorialViewModel> tutorialViewModels;

    TutorialPagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
        this.tutorialViewModels = new ArrayList<>();
    }

    void setTutorialViewModels(List<TutorialViewModel> tutorialViewModels) {
        this.tutorialViewModels = tutorialViewModels;
        notifyDataSetChanged();
    }

    @Override
    public Fragment getItem(int position) {
        return TutorialFragment.newInstance(tutorialViewModels.get(position), position);
    }

    @Override
    public int getCount() {
        return tutorialViewModels.size();
    }

}
