package com.bcp.benefits.ui.discounts.list;

import android.content.Context;
import android.graphics.Typeface;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bcp.benefits.R;
import com.bcp.benefits.components.CustomTypefaceSpan;
import com.bcp.benefits.ui.discounts.propose.ProposeDiscountActivity;
import com.bcp.benefits.util.AppUtils;
import com.bcp.benefits.util.Constants;
import com.bcp.benefits.viewmodel.DiscountTypeViewModel;
import com.bcp.benefits.viewmodel.DiscountViewModel;
import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.bcp.benefits.viewmodel.DiscountTypeViewModel.DISCOUNT;
import static com.bcp.benefits.viewmodel.DiscountTypeViewModel.EDUTATION;

/**
 * Created by emontesinos on 27/05/19.
 **/
public class DiscountAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<DiscountViewModel> items;
    private DiscountTypeViewModel discountType;
    private Context context;
    private OnDiscountItemListener onDiscountItemListener;
    private boolean isLoading = true;
    private boolean showFace = false;
    private Typeface fontBold;
    private Typeface fontRegular;

    private String urlBenefits;

    DiscountAdapter(Context context, DiscountTypeViewModel discountType) {
        this.items = new ArrayList<>();
        this.discountType = discountType;
        this.context = context;
        this.fontBold = ResourcesCompat.getFont(context, R.font.breviabold);
        this.fontRegular = ResourcesCompat.getFont(context, R.font.breviaregular);

    }

    public void setUrlBenefits(String urlBenefits) {
        this.urlBenefits = urlBenefits;
    }

    void addListener(OnDiscountItemListener onDiscountItemListener) {
        this.onDiscountItemListener = onDiscountItemListener;
    }

    void setItems(List<DiscountViewModel> items) {
        this.items.addAll(items);
        notifyItemRangeChanged(items.size() - 1, items.size());
    }

    void clearItems() {
        this.items = new ArrayList<>();
        notifyDataSetChanged();
    }

    void showLoading(boolean isShow) {
        this.isLoading = isShow;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == items.size())
            return DiscountViewModel.TypeViewHolder.LOADING.getValue();
        showFace = items.size() == 1;
        return items.get(position).getTypeViewHolder().getValue();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        switch (DiscountViewModel.TypeViewHolder.valueOf(viewType)) {
            case DISCOUNT:
                View viewDiscount = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_discount_list,
                        viewGroup, false);
                return new DiscountViewHolder(viewDiscount);
            case PROPOSE:
                View viewPropose = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_propose_discount,
                        viewGroup, false);
                return new ProposeDiscountViewHolder(viewPropose);
            case ALERT:
                View viewAlert = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_acount_discount,
                        viewGroup, false);
                return new AlertDiscountViewHolder(viewAlert);
            default:
                View viewLoading = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_progress,
                        viewGroup, false);
                return new LoadingViewHolder(viewLoading);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (DiscountViewModel.TypeViewHolder.valueOf(getItemViewType(position))) {
            case DISCOUNT:
                if (items.isEmpty())
                    return;
                DiscountViewModel discountViewModel = items.get(position == items.size() ? position - 1 : position);
                if (holder instanceof DiscountViewHolder)
                    ((DiscountViewHolder) holder).bind(discountViewModel);
                break;
            case PROPOSE:
                break;
            case LOADING:
                if (holder instanceof LoadingViewHolder)
                    ((LoadingViewHolder) holder).showLoading(isLoading);
                break;
            default:
                break;
        }

    }

    @Override
    public int getItemCount() {
        return items.size() + 1;
    }

    class DiscountViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_photo)
        ImageView ivPhoto;
        @BindView(R.id.tv_caption)
        TextView tvCaption;
        @BindView(R.id.tv_provider)
        TextView tvProvider;
        @BindView(R.id.tv_group)
        TextView tvGroup;
        @BindView(R.id.rb_score)
        AppCompatRatingBar rbScore;
        @BindView(R.id.tv_num_usage)
        TextView tvNumUsage;
        @BindView(R.id.tv_num_review)
        TextView tvNumReview;
        @BindView(R.id.tv_uso)
        TextView tvUso;


        DiscountViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void bind(DiscountViewModel discountViewModel) {
            if (discountViewModel.getImage() != null && !discountViewModel.getImage().isEmpty()) {
                Picasso.get().load(discountViewModel.getImage()).placeholder(R.drawable.placeholder_discount_item).into(ivPhoto);
            } else {
                ivPhoto.setImageDrawable(context.getResources().getDrawable(R.drawable.placeholder_discount_item));
            }
            validateVisible(discountViewModel);
            itemView.setOnClickListener(v -> onDiscountItemListener.onDiscountItem(discountViewModel));
            tvGroup.setText(discountViewModel.getGroup());
            tvProvider.setText(discountViewModel.getProvider());
            getCaption(discountViewModel);
            rbScore.setRating(discountViewModel.getScore());
            tvNumUsage.setText(discountViewModel.getUsage(itemView.getContext()));
            tvNumReview.setText(discountViewModel.getReview(itemView.getContext()));
            tvUso.setText(discountViewModel.getRestriction());

        }

        private void validateVisible(DiscountViewModel discountViewModel) {
            tvCaption.setVisibility(discountViewModel.getValue() != null
                    && !discountViewModel.getValue().isEmpty() ? View.VISIBLE
                    : View.GONE);
            tvProvider.setVisibility(discountViewModel.getProvider() != null
                    && !discountViewModel.getProvider().isEmpty() ? View.VISIBLE : View.GONE);
            tvGroup.setVisibility(discountViewModel.isShowGroup() ? View.VISIBLE : View.GONE);
            tvNumUsage.setVisibility(discountViewModel.getCanBeUsed() ? View.VISIBLE : View.GONE);
            tvNumReview.setVisibility(discountViewModel.getCanBeReviewed() ? View.VISIBLE : View.GONE);
            tvUso.setVisibility(discountViewModel.getRestriction() != null
                    && !discountViewModel.getRestriction().isEmpty() ? View.VISIBLE : View.GONE);
        }

        private void getCaption(DiscountViewModel discountViewModel) {

            StringBuilder sbDescription = new StringBuilder();

            if (discountViewModel.getValue() != null && !discountViewModel.getValue().isEmpty())
                sbDescription.append(discountViewModel.getValue());
            if (discountViewModel.getCaption() != null && !discountViewModel.getCaption().isEmpty()) {
                if (discountViewModel.getValue() != null && !discountViewModel.getValue().isEmpty())
                    sbDescription.append(Constants.SPACE);
                sbDescription.append(discountViewModel.getCaption());
            }

            String description = sbDescription.toString();

            if (!description.isEmpty()) {
                SpannableStringBuilder ss = new SpannableStringBuilder(description);
                if (discountViewModel.getValue() != null && !discountViewModel.getValue().isEmpty()) {
                    ss.setSpan(new CustomTypefaceSpan(fontBold), 0, discountViewModel.getValue().length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
                    ss.setSpan(new RelativeSizeSpan(1.25f), 0, discountViewModel.getValue().length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
                    int color = colorSkinFlv();

                    ss.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, color)), 0, discountViewModel.getValue().length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);

                    if (discountViewModel.getCaption() != null && !discountViewModel.getCaption().isEmpty())
                        ss.setSpan(new CustomTypefaceSpan(fontRegular), discountViewModel.getValue().length() + 1, description.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
                } else {
                    if (discountViewModel.getCaption() != null && !discountViewModel.getCaption().isEmpty())
                        ss.setSpan(new CustomTypefaceSpan(fontRegular), 0, description.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
                }
                tvCaption.setVisibility(View.VISIBLE);
                tvCaption.setText(ss);
            } else {
                tvCaption.setVisibility(View.GONE);
            }
        }

        private int colorSkinFlv() {
            return resId(R.color.bg_green_start, R.color.bg_yellow_start);
        }

        private int resId(int first, int second) {
            return (discountType == DISCOUNT) ? first : second;
        }
    }

    class ProposeDiscountViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.btn_message_action)
        TextView btnActionPropose;
        @BindView(R.id.img_question)
        View iconFace;
        private Boolean validClick = true;

        ProposeDiscountViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            if (discountType == EDUTATION)
                btnActionPropose.setBackground(itemView.getContext()
                        .getResources().getDrawable(R.drawable.bg_button_yellow_light));
            iconFace.setVisibility(showFace ? View.VISIBLE : View.GONE);
        }


        @OnClick(R.id.btn_message_action)
        void onClickPropose() {
            if (validClick) {
                validClick = false;
                ProposeDiscountActivity.start(itemView.getContext(), discountType);
            }
        }
    }

    class LoadingViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.pg_loading)
        AVLoadingIndicatorView loadingIndicatorView;

        LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            if (discountType == EDUTATION)
                loadingIndicatorView.setIndicatorColor(itemView.getContext()
                        .getResources().getColor(R.color.yellow_light));
        }

        void showLoading(boolean isShow) {
            loadingIndicatorView.setVisibility(isShow ? View.VISIBLE : View.GONE);
        }
    }

    class AlertDiscountViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.btn_open_app)
        TextView btnOpenApp;

        AlertDiscountViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            btnOpenApp.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (!urlBenefits.equals("")) {
                AppUtils.openUrl(itemView.getContext(), urlBenefits);
            } else {
                Toast.makeText(context, "Intentalo más tarde", Toast.LENGTH_SHORT).show();
            }
        }
    }


    public interface OnDiscountItemListener {
        void onDiscountItem(DiscountViewModel discount);
    }


}
