package com.bcp.benefits.ui.othres;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.bcp.benefits.R;
import com.bcp.benefits.util.AppUtils;
import com.bcp.benefits.viewmodel.OtherBenefitViewModel;

import static com.bcp.benefits.util.AppUtils.applyBackgroundColor;
import static com.bcp.benefits.util.AppUtils.applyBackgroundColorOther;
import static com.bcp.benefits.util.Constants.BORDER_COLOR;
import static com.bcp.benefits.util.Constants.END_COLOR;
import static com.bcp.benefits.util.Constants.START_COLOR;

public class OtherAdapter extends RecyclerView.Adapter<OtherAdapter.OtherViewholder> {

    private final Context context;
    private ArrayList<OtherBenefitViewModel> listOthers;
    private ListenerViewAdapter listener;


    void addList(ArrayList<OtherBenefitViewModel> listOthers) {
        this.listOthers = listOthers;
        notifyDataSetChanged();
    }

    OtherAdapter(Context context) {
        this.context = context;
    }

    void setListener(@NonNull ListenerViewAdapter listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public OtherViewholder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_other, viewGroup, false);
        return new OtherViewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OtherViewholder holder, int position) {
        OtherBenefitViewModel otherBenefit = listOthers.get(position);
        holder.txvOtherTitle.setText(otherBenefit.getTitle());
        holder.imgOtherIcon.setImageDrawable(AppUtils.getImage(context, otherBenefit.getIcon()));
        holder.itemView.setOnClickListener(v -> listener.goView(otherBenefit));
        String stColor = (String) otherBenefit.getColors().get(START_COLOR);
        String endColor = (String) otherBenefit.getColors().get(END_COLOR);
        String bdColor = (String) otherBenefit.getColors().get(BORDER_COLOR);
        applyBackgroundColorOther(holder.content, holder.itemView.getContext(), stColor, endColor, bdColor, 36f);
    }

    @Override
    public int getItemCount() {
        return listOthers.size();
    }

    static class OtherViewholder extends RecyclerView.ViewHolder {

        final Context context;
        @BindView(R.id.txv_item_title_other)
        TextView txvOtherTitle;
        @BindView(R.id.img_item_icon_other)
        ImageView imgOtherIcon;
        @BindView(R.id.ctr_home_item)
        View content;

        private OtherViewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            context = itemView.getContext();
        }
    }

    interface ListenerViewAdapter {
        void goView(OtherBenefitViewModel benefit);
    }
}
