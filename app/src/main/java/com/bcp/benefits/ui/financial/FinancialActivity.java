package com.bcp.benefits.ui.financial;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bcp.benefits.analytics.AnalyticsModule;
import com.bcp.benefits.analytics.AnalyticsTags;
import com.google.android.material.appbar.AppBarLayout;
import com.google.gson.Gson;
import com.bcp.benefits.R;
import com.bcp.benefits.components.RadioGroupComponent;
import com.bcp.benefits.ui.BaseActivity;
import com.bcp.benefits.ui.financial.newcredit.NewCreditActivity;
import com.bcp.benefits.ui.financial.requestproduct.AprovedCreditActivity;
import com.bcp.benefits.util.AppUtils;
import com.bcp.benefits.util.CallController;
import com.bcp.benefits.viewmodel.BannerViewModel;
import com.bcp.benefits.viewmodel.ProductFinancialViewModel;
import com.yarolegovich.discretescrollview.DiscreteScrollView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by emontesinos on 21/05/19.
 **/
public class FinancialActivity extends BaseActivity implements FinancialPresenter.FinancialView,
        DiscreteScrollView.OnItemChangedListener, ProductFinancialAdapter.OnItemClickListener {

    @Inject
    FinancialPresenter presenter;

    @BindView(R.id.toolbar)
    AppBarLayout toolbar;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.title_toolbar)
    TextView titleToolbar;
    @BindView(R.id.content_list_credit)
    ConstraintLayout contentCredit;
    @BindView(R.id.rv_banner_financial)
    DiscreteScrollView rvBanner;
    @BindView(R.id.rg_banners_count)
    RadioGroupComponent rgBannersCount;
    @BindView(R.id.txt_message_empty_banner)
    TextView txtEmptyBanner;
    @BindView(R.id.txt_title_credit)
    TextView titleCredit;
    @BindView(R.id.rv_list_credit)
    RecyclerView rvCredit;
    @BindView(R.id.txt_description_title_credit)
    TextView descriptionTitleCredit;
    @BindView(R.id.text_description_credit)
    TextView descriptionCredit;
    @BindView(R.id.txt_request)
    TextView txtRequest;
    @BindView(R.id.tv_call_question)
    TextView tvCallQuestion;
    @BindView(R.id.scrollContent)
    View contentFinance;

    private ProductFinancialAdapter financialAdapter;
    private BannerFinanceAdapter bannerFinanceAdapter;
    private List<ProductFinancialViewModel> all;
    private CallController callController;
    private String financialPhone;

    private List<BannerViewModel> bannerModelsbaners;
    private Boolean validClick = true;
    private Boolean validClickTwo = true;
    private AnalyticsModule analyticsModule = new AnalyticsModule(this);;

    public static void start(Context context) {
        Intent starter = new Intent(context, FinancialActivity.class);
        context.startActivity(starter);
    }

    @Override
    public void setUpView() {
        analyticsModule.sendToAnalytics(AnalyticsTags.Finance.Events.financiero, null);
        this.presenter.setView(this);
        this.configRecyclerViews();
        callController = new CallController(this, null);

        if (AppUtils.isConnected(this)) {
            this.presenter.getFinancialPhone();
            this.presenter.goListBanner();
            this.presenter.loadCredits();
        } else {
            AppUtils.dialogIsConnect(this);
            contentFinance.setVisibility(View.VISIBLE);
        }

        getToolbar();

    }

    private void configRecyclerViews() {
        rvCredit.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.HORIZONTAL, false));
        financialAdapter = new ProductFinancialAdapter();
        financialAdapter.setListener(this);
        rvCredit.setAdapter(financialAdapter);

        bannerFinanceAdapter = new BannerFinanceAdapter(this);
        rvBanner.setAdapter(bannerFinanceAdapter);
        rvBanner.addOnItemChangedListener(this);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_financial;
    }

    public void getToolbar() {
        titleToolbar.setText(R.string.finance);
        toolbar.setBackground(getResources().getDrawable(R.drawable.bg_orange_toolbar));
        imgBack.setOnClickListener(v -> {
            onBackPressed();
            overridePendingTransition(R.anim.rigth_in, R.anim.right_out);
        });
    }

    @Override
    public void showListBanner(List<BannerViewModel> bannerModels) {
        this.bannerModelsbaners = bannerModels;
        rvBanner.setVisibility(bannerModels.isEmpty() ? View.GONE : View.VISIBLE);
        txtEmptyBanner.setVisibility(View.GONE);
        bannerFinanceAdapter.setList(bannerModels);
        this.createRadioGroup(bannerModels);
        this.rgBannersCount.setVisibility((bannerModels.isEmpty() || bannerModels.size() == 1) ? View.GONE : View.VISIBLE);
        if (bannerModels.isEmpty())
            showErrorBanner(getString(R.string.dont_exist_publicidad));
    }

    @Override
    public void showEmptyList() {
        contentCredit.setVisibility(View.GONE);
    }

    @Override
    public void showFinancialPhone(String phone) {
        this.financialPhone = phone;
    }

    @Override
    public void showOwnedListFinancial(List<ProductFinancialViewModel> listProduct) {
        contentFinance.setVisibility(View.VISIBLE);
        contentCredit.setVisibility(listProduct.isEmpty() ? View.GONE : View.VISIBLE);
        this.financialAdapter.setList(listProduct);
    }

    @Override
    public void showAllFinancialList(List<ProductFinancialViewModel> list) {
        this.all = list;
    }

    @Override
    public void onCurrentItemChanged(@Nullable RecyclerView.ViewHolder viewHolder, int adapterPosition) {
        if (bannerModelsbaners != null && !bannerModelsbaners.isEmpty()) {
            rgBannersCount.setChecked(adapterPosition);
        }

    }

    private void createRadioGroup(List<BannerViewModel> list) {
        if (list != null && !list.isEmpty()) {
            rgBannersCount.setColor(R.drawable.ic_indicator_orange, R.drawable.ic_indicator_unselect_gray);
            for (BannerViewModel item : list) {
                AppUtils.addRadioButton(this, rgBannersCount, list.indexOf(item),
                        list.indexOf(item) == 0, 10);
            }
            rgBannersCount.setChecked(0);
        }

    }

    @OnClick(R.id.txt_request)
    public void OnClickRequest() {
        if (AppUtils.isConnected(this)) {
            if (validClickTwo){
                if (!all.get(0).getPeriodOptions().isEmpty()) {
                Gson gson = new Gson();
                NewCreditActivity.start(this, gson.toJson(all));
                validClickTwo =false;
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
                }else {
                    Toast.makeText(this, getResources().getString(R.string.mesage_error_period), Toast.LENGTH_SHORT).show();
                }

            }

        } else {
            AppUtils.dialogIsConnect(this);
        }

    }

    @Override
    public void onItemClick(ProductFinancialViewModel entity, int position) {
        if (validClick){
            if (!entity.getPeriodOptions().isEmpty()) {
            AprovedCreditActivity.start(this, entity);
            validClick = false;} else {
                Toast.makeText(this, getResources().getString(R.string.mesage_error_period), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @OnClick(R.id.tv_call_question)
    void onClickQuestion() {
        if (financialPhone != null && !financialPhone.isEmpty())
            callController.doCall(financialPhone);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        callController.onRequestPermissionsResult(requestCode, grantResults);
    }

    @Override
    public void showErrorBanner(String error) {
        rvBanner.setVisibility(View.VISIBLE);
        txtEmptyBanner.setVisibility(View.VISIBLE);
        txtEmptyBanner.setText(error);
    }

    @Override
    protected void onResume() {
        super.onResume();
        validClick =true;
        validClickTwo =true;
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        validClick =true;
        validClickTwo =true;
    }
}
