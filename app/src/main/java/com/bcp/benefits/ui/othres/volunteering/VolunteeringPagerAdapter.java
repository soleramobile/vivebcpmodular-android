package com.bcp.benefits.ui.othres.volunteering;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import com.bcp.benefits.ui.othres.volunteering.myTicketsVounteering.MyTicketsVoulunteeringFragment;
import com.bcp.benefits.ui.othres.volunteering.ticketapprovedvolunteering.TicketsApprovedVolunteeringFragment;
import com.bcp.benefits.viewmodel.VolunteeringTicketViewModel;
import org.jetbrains.annotations.NotNull;

/**
 * Created by emontesinos on 02/05/19.
 **/

public class VolunteeringPagerAdapter extends FragmentStatePagerAdapter {

    private VolunteeringTicketViewModel volunteeringTicketViewModel;

     VolunteeringPagerAdapter(FragmentManager fm) {
        super(fm);
    }

     void addGoldenTicketResponse(VolunteeringTicketViewModel volunteeringTicketViewModel){
        this.volunteeringTicketViewModel = volunteeringTicketViewModel;
        notifyDataSetChanged();
    }

    @NotNull
    @Override
    public Fragment getItem(int position) {
            if(position == 0)
                return MyTicketsVoulunteeringFragment.newInstance(volunteeringTicketViewModel);
            else
                return TicketsApprovedVolunteeringFragment.newInstance(volunteeringTicketViewModel);

    }

    @Override
    public int getItemPosition(@NotNull Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return volunteeringTicketViewModel==null?0:2;
    }
}
