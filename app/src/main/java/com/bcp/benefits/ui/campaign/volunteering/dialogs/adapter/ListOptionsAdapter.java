package com.bcp.benefits.ui.campaign.volunteering.dialogs.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bcp.benefits.R;
import com.bcp.benefits.viewmodel.ListRoleViewModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

/* 
   Created by: Flavia Figueroa
               13/02/2020
         Solera Mobile
*/


public class ListOptionsAdapter extends RecyclerView.Adapter<ListOptionsAdapter.ListOptionsViewHolder> {

    List<ListRoleViewModel> list;
    private OptionsListener listener;
    private String type;

    public void addList(List<ListRoleViewModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public ListOptionsAdapter(List<ListRoleViewModel> list, String type) {
        this.list = list;
        this.type = type;
    }

    public void setListener(@NonNull ListOptionsAdapter.OptionsListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public ListOptionsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_type_custom, parent, false);
        return new ListOptionsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListOptionsViewHolder holder, int position) {
        holder.text.setText(list.get(position).getName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.select(list.get(position), type);
                Log.d("CLICK DETECTED: ",
                        "Selección: " +
                              "\nNombre -> " + list.get(position).getName() +
                              "\nID     -> " + list.get(position).getIdRoleVolunteer() +
                              "\nTipo   -> " + type);
//                holder.imageCircle.setVisibility(position == positionSelected ? View.VISIBLE : View.GONE);
            }
        });
        holder.imageCircle.setImageResource(R.drawable.bg_circle_sky_blue);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ListOptionsViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_type_texto)
        TextView text;
        @BindView(R.id.ctrMainContainer)
        ConstraintLayout ctrMainContainer;
        @BindView(R.id.img_circle)
        ImageView imageCircle;

        public ListOptionsViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OptionsListener {
        void select(ListRoleViewModel listRoleViewModel, String type);
    }
}
