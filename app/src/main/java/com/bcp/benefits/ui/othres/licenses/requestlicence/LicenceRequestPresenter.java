package com.bcp.benefits.ui.othres.licenses.requestlicence;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import javax.inject.Inject;
import com.bcp.benefits.viewmodel.BossViewModel;

/**
 * Created by emontesinos on 08/05/19.
 **/
public class LicenceRequestPresenter {

    private LicenceRequestView view;

    @Inject
    public LicenceRequestPresenter() {
    }

    public void setView(@NonNull LicenceRequestView view) {
        this.view = view;
    }

    void loadBoss() {
        ArrayList<BossViewModel> bossModelList = new ArrayList<>();
        bossModelList.add(new BossViewModel(0,"Mariana"));
        bossModelList.add(new BossViewModel(1,"Gabriela"));
        bossModelList.add(new BossViewModel(1,"Agustina"));
        bossModelList.add(new BossViewModel(1,"Aldana"));
        this.view.showBoss(bossModelList);
    }

    void doRequest(int idTicket, Integer idDurationDetail, String usageDate, Integer idUserDepartmentHead) {
        this.view.successRequest("Deseas Aceptar");
    }



    public interface LicenceRequestView {
        void showBoss(ArrayList<BossViewModel> bossModelList);
        void successRequest(String message);
    }
}
