package com.bcp.benefits.ui.discounts;

import androidx.annotation.NonNull;

import com.bcp.benefits.ui.MainView;
import com.bcp.benefits.viewmodel.DiscountCategoryViewModel;
import com.bcp.benefits.viewmodel.StateViewModel;

import java.util.List;

import javax.inject.Inject;

import pe.solera.benefit.main.entity.DiscountCategory;
import pe.solera.benefit.main.entity.State;
import pe.solera.benefit.main.usecases.executor.Action;
import pe.solera.benefit.main.usecases.usecases.AppUseCase;
import pe.solera.benefit.main.usecases.usecases.DiscountUseCases;

/**
 * Created by emontesinos on 27/05/19.
 **/
public class DiscountsPresenter {

    private DiscountsView view;
    private final AppUseCase appUseCase;
    private final DiscountUseCases discountUseCases;

    @Inject
    public DiscountsPresenter(AppUseCase appUseCase, DiscountUseCases discountUseCases) {
        this.appUseCase = appUseCase;
        this.discountUseCases = discountUseCases;
    }

    public void setView(@NonNull DiscountsView view) {
        this.view = view;

    }


    public void doGetProvinces() {
        if (this.view.validateInternet()) {
            this.view.showProgress();
            this.appUseCase.getProvinces(new Action.Callback<List<State>>() {
                @Override
                public void onSuccess(List<State> stateList) {
                    view.showProvinces(StateViewModel.toStateViewModel(stateList));
                    view.hideProgress();
                }

                @Override
                public void onError(Throwable throwable) {
                    view.showErrorMessage(throwable.getMessage());
                    view.hideProgress();
                }
            });
        }
    }

    public void doGetDiscountCategories(int type) {
        if (this.view.validateInternet()) {
            view.showProgress();
            this.discountUseCases.getDiscountCategories(type, new Action.Callback<List<DiscountCategory>>() {
                @Override
                public void onSuccess(List<DiscountCategory> discountCategoryList) {
                    view.showDiscountCategories(DiscountCategoryViewModel
                            .toDiscountCategoryViewModelList(discountCategoryList));
                    view.hideProgress();
                }

                @Override
                public void onError(Throwable throwable) {
                    view.showErrorMessage(throwable.getMessage());
                    view.hideProgress();
                }
            });
        }
    }


    public interface DiscountsView extends MainView {

        void showProvinces(List<StateViewModel> provinces);
        void showDiscountCategories(List<DiscountCategoryViewModel> discountCategories);

    }
}
