package com.bcp.benefits.ui.othres.assistanceconfirmation.dialog;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bcp.benefits.R;
import com.bcp.benefits.viewmodel.ListTypeVolunteerViewModel;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

/* 
   Created by: Flavia Figueroa
               25/02/2020
         Solera Mobile
*/


public class TypeVolunteerDialogFragment extends DialogFragment implements TypeVolunteerAdapter.OnItemClickTypeVolunteerListener {
    @BindView(R.id.rv_type)
    RecyclerView rvType;
    @BindView(R.id.iv_close)
    ImageView ivClose;
    @BindView(R.id.constraintLayout9)
    ConstraintLayout contentDialog;
    @BindView(R.id.text_title_generic)
    TextView textGeneric;

    private OnSelectTypeVolunteerListener listener;
    private ArrayList<ListTypeVolunteerViewModel> listTypesVolunteer = new ArrayList<>();
    private ListTypeVolunteerViewModel listTypeVolunteerSelected;

    public static TypeVolunteerDialogFragment newInstance(ListTypeVolunteerViewModel stateSelected, ArrayList<ListTypeVolunteerViewModel> types
            , OnSelectTypeVolunteerListener onSelectDepartment
    ) {
        TypeVolunteerDialogFragment fragment = new TypeVolunteerDialogFragment();
        Bundle args = new Bundle();
        fragment.listener = onSelectDepartment;
        args.putSerializable("dataCampaignVolunteering", types);
        args.putSerializable("volunteeringSelected", stateSelected);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow()
                .getAttributes().windowAnimations = R.style.DialogTheme;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            Drawable backgroundColor = new ColorDrawable(ContextCompat.getColor(
                    Objects.requireNonNull(getContext()), android.R.color.transparent));
            Objects.requireNonNull(dialog.getWindow()).setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            dialog.getWindow().setBackgroundDrawable(backgroundColor);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_identity_types, container, false);
        ButterKnife.bind(this, v);
        listTypesVolunteer = (ArrayList<ListTypeVolunteerViewModel>) getArguments().getSerializable("dataCampaignVolunteering");
        listTypeVolunteerSelected = (ListTypeVolunteerViewModel) getArguments().getSerializable("volunteeringSelected");
        return v;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        textGeneric.setText(R.string.by_selected_vo);
        ivClose.setImageResource(R.drawable.ic_close_white);
        contentDialog.setBackground(getResources().getDrawable(R.drawable.bg_type_custom_green));
        ivClose.setOnClickListener(v -> getDialog().dismiss());
        rvType.setLayoutManager(new LinearLayoutManager(getContext()));
        TypeVolunteerAdapter typeVolunteeringAdapter = new TypeVolunteerAdapter(getContext());
        DividerItemDecoration itemDecoration = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        rvType.addItemDecoration(itemDecoration);
        rvType.setAdapter(typeVolunteeringAdapter);
        typeVolunteeringAdapter.setListener(this);
        typeVolunteeringAdapter.addCategories(listTypesVolunteer);
        typeVolunteeringAdapter.setSelectedItem(listTypeVolunteerSelected);


    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        Objects.requireNonNull(dialog.getWindow()).requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onItemTypeVolunteerClick(ListTypeVolunteerViewModel entity, int position) {
        listener.firstCombo(entity, position);
        dismiss();
    }

    public interface OnSelectTypeVolunteerListener {
        void firstCombo(ListTypeVolunteerViewModel entity, int position);
    }
}
