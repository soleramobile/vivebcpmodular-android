package com.bcp.benefits.ui.health.nearyou.fragment;


import android.animation.Animator;
import android.app.AlertDialog;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.TextAppearanceSpan;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.bcp.benefits.R;
import com.bcp.benefits.ui.BaseFragment;
import com.bcp.benefits.ui.health.clinicdetail.ClinicDetailActivity;
import com.bcp.benefits.util.AppUtils;
import com.bcp.benefits.util.LocationController;
import com.bcp.benefits.viewmodel.ClinicViewModel;
import com.bcp.benefits.viewmodel.HealthAttentionTypeViewModel;
import com.bcp.benefits.viewmodel.HealthProductViewModel;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

import static com.bcp.benefits.util.Constants.ID_MAPS;
import static com.bcp.benefits.util.Constants.ID_WAZE;

public class MapNearFragment extends BaseFragment implements OnMapReadyCallback,
        LocationController.LocationControllerListener, GoogleMap.InfoWindowAdapter, MapNearYouPresenter.MapNearYouView {
    public static final String SEARCH = "SEARCH";
    public static final String PRODUCT_VIEW_MODEL = "PRODUCT_VIEW_MODEL";
    public static final String ATTENTION_TYPE = "ATTENTION_TYPE";

    @Inject
    MapNearYouPresenter presenter;
    @BindView(R.id.content_marker_custom)
    ConstraintLayout contentMarkerCustom;
    @BindView(R.id.content_info)
    ConstraintLayout contentInfo;
    @BindView(R.id.tv_title)
    TextView title;
    @BindView(R.id.tv_sub_title_1)
    TextView subTitle;
    @BindView(R.id.tv_sub_title_2)
    TextView subTitleTwo;
    @BindView(R.id.tv_callus)
    TextView tvCallus;
    @BindView(R.id.tv_how_to_get)
    TextView tvHowToGet;

    private String search = "";
    private HealthAttentionTypeViewModel healthAttentionTypeViewModel;
    private HealthProductViewModel healthProductViewModel;

    public static MapNearFragment newInstance(String search,
                                              HealthProductViewModel healthProductViewModel,
                                              HealthAttentionTypeViewModel healthAttentionTypeViewModel) {
        MapNearFragment fragment = new MapNearFragment();
        Bundle args = new Bundle();
        args.putString(SEARCH, search);
        args.putSerializable(PRODUCT_VIEW_MODEL, healthProductViewModel);
        args.putSerializable(ATTENTION_TYPE, healthAttentionTypeViewModel);
        fragment.setArguments(args);
        return fragment;
    }

    private GoogleMap mMap;
    private MapView mMapView;
    private Location queryLocation;
    private LocationController locationController;
    private HashMap<Marker, ClinicViewModel> markerMap = new HashMap<>();
    private ClinicViewModel clinicProduct;


    @Override
    public void setUpView() {
        if (getArguments() == null) return;
        this.loadData();
        this.presenter.setView(this);
    }

    private void loadData() {
        this.search = getArguments().getString(SEARCH);
        this.healthAttentionTypeViewModel = (HealthAttentionTypeViewModel)
                getArguments().getSerializable(ATTENTION_TYPE);
        this.healthProductViewModel = (HealthProductViewModel)
                getArguments().getSerializable(PRODUCT_VIEW_MODEL);
    }

    @Override
    public void onViewCreated(@NonNull View rootView, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(rootView, savedInstanceState);
        mMapView = rootView.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();
        try {
            MapsInitializer.initialize(Objects.requireNonNull(getContext()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        mMapView.getMapAsync(this);


    }

    @Override
    public int getLayout() {
        return R.layout.fragment_map;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.setOnMyLocationButtonClickListener(() -> false);
        mMap.setInfoWindowAdapter(this);
        LatLng peru = new LatLng(-12.084155, -76.975721);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(peru, 5.0f));

    }

    @Override
    public View getInfoWindow(Marker marker) {
        clinicProduct = markerMap.get(marker);
        Animation animations = AnimationUtils.loadAnimation(getContext(), R.anim.traslate_in);
        Animation animationsTwo = AnimationUtils.loadAnimation(getContext(), R.anim.traslate_on);
        contentMarkerCustom.animate()
                .alpha(1)
                .setDuration(100)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        if (contentMarkerCustom.getVisibility() == View.VISIBLE) {
                            contentMarkerCustom.setVisibility(View.GONE);
                            contentMarkerCustom.startAnimation(animationsTwo);
                        } else {
                            contentMarkerCustom.startAnimation(animations);
                            contentMarkerCustom.setVisibility(View.VISIBLE);
                            title.setText(clinicProduct.getName());
                            contentInfo.setVisibility(View.VISIBLE);
                            tvCallus.setVisibility(View.INVISIBLE);


                            tvHowToGet.setTextColor(getResources().getColor(R.color.home_health_start));
                            tvHowToGet.setBackground(getResources().getDrawable(R.drawable.bg_white_corners_skyblue_stroke_two));
                            tvHowToGet.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_location_info_mini_skyblue, 0, 0, 0);
                            String textStyleOne = getContext().getString(R.string.co_pago, clinicProduct.getAmount());
                            String textStyle = getContext().getString(R.string.text_percentage, clinicProduct.getCoverage());
                            int color = R.color.home_health_start;
                            SpannableString spannableString = new SpannableString(textStyleOne);
                            SpannableString spannableStringTwo = new SpannableString(textStyle);
                            spannableString.setSpan(new TextAppearanceSpan(getContext(), R.style.FontBold), 7, spannableString.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
                            spannableStringTwo.setSpan(new TextAppearanceSpan(getContext(), R.style.FontBold), 11, spannableStringTwo.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
                            spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getContext(), color)), 7, spannableString.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
                            spannableStringTwo.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getContext(), color)), 11, spannableStringTwo.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
                            subTitle.setText(spannableString);
                            subTitleTwo.setText(spannableStringTwo);

                        }

                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                        //Do nothing
                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {
                        //Do nothing
                    }
                })
                .start();

        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }

    private void getClinics(String search, HealthProductViewModel filterProduct, HealthAttentionTypeViewModel type) {
        if (mMap != null)
            mMap.clear();

        markerMap.clear();

        String latitude = null, longitude = null;
        if (queryLocation != null) {
            latitude = String.valueOf(queryLocation.getLatitude());
            longitude = String.valueOf(queryLocation.getLongitude());
        }
        presenter.doGetClinics(search, filterProduct.getIdProduct(), type.getIdAttentionType(), latitude, longitude);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        locationController.onRequestPermissionsResult(requestCode, grantResults);
    }

    @Override
    public void onLocationPermissionDenied() {

    }

    @Override
    public void onGpsUnavailable() {

    }

    @Override
    public void onGetLocationCompleted(Location location) throws SecurityException {
        queryLocation = location;
        mMap.setMyLocationEnabled(true);
        LatLng currentLocation = new LatLng(location.getLatitude(), location.getLongitude());
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 16.0f));
        locationController.disconnect();
        getClinics(search, healthProductViewModel, healthAttentionTypeViewModel);
    }

    @Override
    public void onLocationUpdate(Location location) {

    }

    @Override
    public void onError(LocationController.LocationManagerError error) {

    }

    @Override
    public void onResume() {
        super.onResume();
        locationController = new LocationController(getContext(), 1000, this);
        locationController.connect();
        mMapView.onResume();
        contentMarkerCustom.setVisibility(View.GONE);
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public void showClinics(List<ClinicViewModel> clinics) {
        LatLng closestLocation = null;
        Float distance = null;
        int drawable = R.drawable.ic_marker_sky_blue;

        for (ClinicViewModel clinic : clinics) {
            try {
                if (clinic.getLatitude() != 0.0 && clinic.getLongitude() != 0.0) {
                    LatLng latLng = new LatLng(clinic.getLatitude(), clinic.getLongitude());
                    MarkerOptions markerOptions = new MarkerOptions();
                    markerOptions.position(latLng);
                    markerOptions.icon(AppUtils.bitmapDescriptorFromVector(getContext(),
                            drawable));

                    Marker marker = mMap.addMarker(markerOptions);
                    markerMap.put(marker, clinic);

                    if (closestLocation != null) {
                        float results[] = new float[1];
                        Location.distanceBetween(queryLocation.getLatitude(), queryLocation.getLongitude(),
                                latLng.latitude, latLng.longitude, results);
                        if (results[0] < distance) {
                            distance = results[0];
                            closestLocation = latLng;
                        }
                    } else {
                        float results[] = new float[1];
                        Location.distanceBetween(queryLocation.getLatitude(), queryLocation.getLongitude(),
                                latLng.latitude, latLng.longitude, results);
                        distance = results[0];
                        closestLocation = latLng;
                    }
                }
            } catch (Exception ex) {
                /*Do nothing*/
            }
        }

        if (closestLocation != null) {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            builder.include(new LatLng(queryLocation.getLatitude(), queryLocation.getLongitude()));
            builder.include(closestLocation);
            LatLngBounds bounds = builder.build();

            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 150);
            mMap.animateCamera(cu);
        }

    }

    @OnClick(R.id.tv_how_to_get)
    public void onTvHowToGetClicked() {
        if (clinicProduct != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle(R.string.subsidiaries_select_nav_app_subtitle)
                    .setItems(R.array.navigation_apps, (dialog, which) -> {
                        dialog.dismiss();
                        if (which == 0) {
                            selectGoogleMaps();
                        } else if (which == 1) {
                            selectWaze();
                        }

                    });

            builder.create();

            builder.show();
        }


    }

    @OnClick(R.id.content_marker_custom)
    public void onClickDetailClinic() {
        if (clinicProduct != null)
            ClinicDetailActivity.start(getContext(), clinicProduct.getIdClinic(), healthProductViewModel.getIdProduct());
        }


    private void selectWaze() {
        String intentUriWaze = "waze://?ll=" + clinicProduct.getLatitude() + ", " + clinicProduct.getLongitude() + "&navigate=yes";
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(intentUriWaze));

        if (mapIntent.resolveActivity(getContext().getPackageManager()) != null) {
            startActivity(mapIntent);
        } else {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + ID_WAZE)));
        }
    }

    private void selectGoogleMaps() {
        String position = clinicProduct.getLatitude() + "," + clinicProduct.getLongitude();
        Uri gmmIntentUri = Uri.parse("google.navigation:q=" + position + "&mode=d");
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");

        if (mapIntent.resolveActivity(getContext().getPackageManager()) != null) {
            startActivity(mapIntent);
        } else {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + ID_MAPS)));
        }
    }

}
