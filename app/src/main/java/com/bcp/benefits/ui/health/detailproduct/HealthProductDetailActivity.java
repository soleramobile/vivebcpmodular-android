package com.bcp.benefits.ui.health.detailproduct;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.SpannableStringBuilder;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bcp.benefits.R;
import com.bcp.benefits.ui.BaseActivity;
import com.bcp.benefits.util.CallController;
import com.bcp.benefits.viewmodel.HealthProductViewModel;

import butterknife.BindView;
import butterknife.OnClick;

import static com.bcp.benefits.util.Constants.PRODUCT_DETAIL;

/**
 * Created by emontesinos on 22/05/19.
 **/

public class HealthProductDetailActivity extends BaseActivity {

    @BindView(R.id.tv_product_type)
    TextView tvProductType;
    @BindView(R.id.rv_items)
    RecyclerView rvItems;
    @BindView(R.id.text_link_plan)
    TextView txtLink;


    private CallController callController;
    private HealthProductViewModel healthProduct;

    public static void start(Context context, HealthProductViewModel healthProduct) {
        Intent starter = new Intent(context, HealthProductDetailActivity.class);
        starter.putExtra(PRODUCT_DETAIL, healthProduct);
        context.startActivity(starter);
    }


    @Override
    public void setUpView() {
        healthProduct = (HealthProductViewModel) getIntent().getSerializableExtra(PRODUCT_DETAIL);
        callController = new CallController(this, null);
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(txtLink.getText());
        spannableStringBuilder.setSpan(new UnderlineSpan(), 0, txtLink.getText().length(), 0);
        txtLink.setText(spannableStringBuilder);
        if (healthProduct != null) {
            tvProductType.setText(healthProduct.getProductType());
            HealthProductViewModel list = new HealthProductViewModel(1, "",
                    healthProduct.getItems());
            HealthProductDetailRowAdapter adapter = new HealthProductDetailRowAdapter(list.getItems());
            rvItems.setLayoutManager(new LinearLayoutManager(this));
            rvItems.setAdapter(adapter);

        }
        if (healthProduct != null && (healthProduct.getProductLink() == null
                || healthProduct.getProductLink().isEmpty()))
            txtLink.setVisibility(View.GONE);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_health_product_detail;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        callController.onRequestPermissionsResult(requestCode, grantResults);
    }
    //EN CASO DE REJECT
    @OnClick(R.id.iv_close)
    public void onIvCloseClicked() {

        finish();
    }

    @OnClick(R.id.btn_callus)
    public void onBtnCallusClicked() {
        assert healthProduct.getPhone() != null;
        callController.doCall(healthProduct.getPhone());
    }

    @OnClick(R.id.text_link_plan)
    public void onClickLink() {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(healthProduct.getProductLink()));
        startActivity(i);
    }
}
