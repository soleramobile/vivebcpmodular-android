package com.bcp.benefits.ui.validate;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.bcp.benefits.BuildConfig;
import com.bcp.benefits.R;
import com.bcp.benefits.analytics.AnalyticsModule;
import com.bcp.benefits.analytics.AnalyticsTags;
import com.bcp.benefits.components.DateTextWatcher;
import com.bcp.benefits.components.TextViewWithDrawable;
import com.bcp.benefits.ui.BaseActivity;
import com.bcp.benefits.ui.home.HomeActivity;
import com.bcp.benefits.util.AppPreferences;
import com.bcp.benefits.util.AppUtils;
import com.bcp.benefits.util.Constants;
import com.bcp.benefits.viewmodel.TypeDocViewModel;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

import static com.bcp.benefits.util.Constants.DATE_START;
import static com.bcp.benefits.util.Constants.LOGIN_GO;
import static com.bcp.benefits.util.Constants.VALIDATE_VIEW;
import static com.bcp.benefits.util.Constants.YEAR_MONTH_DAY;

/**
 * Created by emontesinos.
 **/

public class ValidateActivity extends BaseActivity implements
        ValidatePresenter.RegisterView, TypesDialogFragment.OnSelectIdTypeListener,
        DatePickerDialogCustom.EditNameDialogListener {

    @BindView(R.id.edit_code_employ)
    EditText codeEmploy;
    @BindView(R.id.edit_number_doc)
    EditText numberDoc;
    @BindView(R.id.edit_email_personal)
    EditText emailPersonal;
    @BindView(R.id.edit_number_mobile)
    EditText numberMobile;
    @BindView(R.id.btn_validate)
    Button btnValidate;
    @BindView(R.id.tv_identify_type)
    TextViewWithDrawable tvIdentifyType;
    @BindView(R.id.content_cons)
    ConstraintLayout contentCons;
    @BindView(R.id.et_employee_birthday)
    EditText etEmployeeBirthDay;
    @BindView(R.id.tvVersionName)
    TextView tvVersionName;

    @Inject
    ValidatePresenter presenter;

    private Boolean validateClick = false;

    private AppPreferences appPreferences;
    private TextWatcher textWatcher;
    private int documentType;
    private AnalyticsModule analyticsModule = new AnalyticsModule(this);

    public static void start(Context context) {
        Intent intent = new Intent(context, ValidateActivity.class);
        context.startActivity(intent);
    }

    @Override
    public void setUpView() {
        this.documentType = TypesDialogFragment.DNI;
        this.presenter.setView(this);
        appPreferences = new AppPreferences(ValidateActivity.this);
        setDni();
        settingEditTexts();
        AppUtils.setGradient(
                getResources().getColor(R.color.orange_common),
                getResources().getColor(R.color.orange_common),
                AppUtils.dpToPx((int) getResources().getDimension(R.dimen.size_thirteen)), btnValidate);
        tvVersionName.setText(BuildConfig.VERSION_NAME);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_validate;
    }

    @OnClick(R.id.btn_validate)
    void OnClickNext() {
        if (!validateClick) {
            validateClick = true;
            this.presenter.loadRegister(codeEmploy.getText().toString(),
                    etEmployeeBirthDay.getText().toString(),
                    numberDoc.getText().toString(),
                    emailPersonal.getText().toString(),
                    numberMobile.getText().toString(),
                    documentType);
        }

    }

    public void goHome() {
        Bundle bundle = new Bundle();
        bundle.putString(AnalyticsTags.Login.Parameters.login_tipo, AnalyticsTags.Login.Values.login_principal);
        analyticsModule.sendToAnalytics(AnalyticsTags.Login.Events.login_exitoso, bundle);

        appPreferences.savePreference(VALIDATE_VIEW, LOGIN_GO);
        HomeActivity.start(this);
        finish();
    }

    @Override
    public void fieldEmpty(String message, int field) {
        if (field > 0)
            findViewById(field).setBackgroundResource(R.drawable.bg_white_corner_error);
        AppUtils.showErrorMessage(contentCons, this, message);
    }

    @Override
    public void field(int field) {
        findViewById(field).setBackgroundResource(R.drawable.bg_gray_corner_gray);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        moveTaskToBack(true);
    }

    @OnClick(R.id.tv_identify_type)
    public void onSelectIdType() {
        AppUtils.hideSoftKeyboard(this);
        this.presenter.loadTypes();
    }

    @Override
    public void onSelectIdType(int idType) {
        this.documentType = idType;
        int limit;
        if (TypesDialogFragment.DNI == idType) {
            limit = 8;
            setDni();
            AppUtils.showSoftKeyboard(numberDoc, this);
        } else {
            limit = 12;
            setCE();
        }
        numberDoc.setText("");
        numberDoc.requestFocus();
        textWatcher = AppUtils.hideSoftKeyboardLimit(this, numberDoc, limit, textWatcher);
        AppUtils.showSoftKeyboard(numberDoc, this);
    }

    private void settingEditTexts() {
        new DateTextWatcher(etEmployeeBirthDay);
        numberDoc.setOnEditorActionListener((textView, actionId, keyEvent) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                AppUtils.hideSoftKeyboard(ValidateActivity.this);
            }
            return false;
        });
    }


    private void setDni() {
        tvIdentifyType.setText(getString(R.string.id_type_dni));
        appPreferences.savePreference(Constants.TYPE, getString(R.string.id_type_dni));
        AppUtils.changeEditTextLength(numberDoc, 8);
        numberDoc.setInputType(InputType.TYPE_CLASS_NUMBER);
    }

    private void setCE() {
        tvIdentifyType.setText(getString(R.string.id_type_ce));
        appPreferences.savePreference(Constants.TYPE, getString(R.string.id_type_ce));
        AppUtils.changeEditTextLength(numberDoc, 12);
        numberDoc.setInputType(InputType.TYPE_CLASS_TEXT);
    }


    @OnClick(R.id.et_employee_birthday)
    public void onClickContent() {
        if (etEmployeeBirthDay.getText().toString().isEmpty()) {
            DatePickerDialogCustom datePickerDialogCustom = DatePickerDialogCustom.newInstance
                    (1, Calendar.getInstance().getTime(),
                            100, Constants.MODO_COMPLETO);
            datePickerDialogCustom.setListener(this);
            datePickerDialogCustom.show(getSupportFragmentManager(), DATE_START);
        } else {
            try {
                @SuppressLint("SimpleDateFormat")
                Date dateIni = new SimpleDateFormat(YEAR_MONTH_DAY).
                        parse(etEmployeeBirthDay.getText().toString());

                DatePickerDialogCustom datePickerDialogCustom = DatePickerDialogCustom.newInstance
                        (1, dateIni, 100,
                                Constants.MODO_COMPLETO);
                datePickerDialogCustom.setListener(this);
                datePickerDialogCustom.show(getSupportFragmentManager(), DATE_START);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    @OnClick(R.id.content_cons)
    public void onClickBirthday() {
        AppUtils.hideKeyboard(this);
    }

    @Override
    public void onFinishEditDialog(Date date, int type, Boolean anual, int modo) {
        if (type == 1) {
            saveDate(etEmployeeBirthDay, date);
        }
    }

    @SuppressLint("SimpleDateFormat")
    private void saveDate(EditText editDate, Date date) {
        editDate.setText(DateFormat.getDateInstance(DateFormat.SHORT, Locale.FRANCE).format(date));
    }

    @Override
    public void showTypes(ArrayList<TypeDocViewModel> types) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment dialogFragment = getSupportFragmentManager().findFragmentByTag("dialog");
        if (dialogFragment != null) {
            ft.remove(dialogFragment);
        }
        ft.addToBackStack(null);
        TypesDialogFragment newFragment = TypesDialogFragment.newInstance(types, documentType - 1);
        newFragment.show(ft, "dialog");
    }

    @Override
    public void activateButton() {
        validateClick = false;
    }
}
