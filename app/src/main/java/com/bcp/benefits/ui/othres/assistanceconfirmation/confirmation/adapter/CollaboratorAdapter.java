package com.bcp.benefits.ui.othres.assistanceconfirmation.confirmation.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.appcompat.widget.SwitchCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.bcp.benefits.R;
import com.bcp.benefits.viewmodel.CollaboratorViewModel;
import com.bcp.benefits.viewmodel.ListCollaboratorViewModel;

import org.jetbrains.annotations.NotNull;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by emontesinos on 13/01/20.
 */

public class CollaboratorAdapter extends RecyclerView.Adapter<CollaboratorAdapter.CampaignVolunteeringViewHolder> {

    private List<ListCollaboratorViewModel> list = new ArrayList<>();
    private OnItemClickCollaboratorListener listener;
    private Context context;
    private String closeAttendance;

    public CollaboratorAdapter(Context context, String closeAttendance) {
        this.context = context;
        this.closeAttendance = closeAttendance;
    }

    public void addList(List<ListCollaboratorViewModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }



    public void modifyItem(int position, ListCollaboratorViewModel model) {
        list.set(position,model);
        notifyItemChanged(position);
    }

    @NotNull
    @Override
    public CollaboratorAdapter.CampaignVolunteeringViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_confirmation_collaborator, parent, false);
        return new CampaignVolunteeringViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NotNull CampaignVolunteeringViewHolder holder, int position) {
        ListCollaboratorViewModel collaborator = list.get(position);

        if (closeAttendance.equals("1")){
            holder.swtSelected.setClickable(false);
            holder.imgCommentaryCollaborators.setEnabled(false);
        }
            holder.txNameCollaborators.setText(collaborator.getCollaboratorName());

            holder.txtNumberDocCollaborators.setText(collaborator.getDocumentNumber());
            if (collaborator.getCommentary().isEmpty()){
                holder.imgCommentaryCollaborators.setImageDrawable(context.getDrawable(R.drawable.ic_comment));
            }else {
                holder.imgCommentaryCollaborators.setImageDrawable(context.getDrawable(R.drawable.ic_coment_selected));
            }
            if (collaborator.getAttendance()==1){
                holder.swtSelected.setChecked(true);
            }else {
                holder.swtSelected.setChecked(false);
            }
            holder.swtSelected.setOnCheckedChangeListener((buttonView, isChecked) -> {
                if (isChecked){
                    listener.onItemCollaboratorClick(collaborator,1,position);
                }else {
                    listener.onItemCollaboratorClick(collaborator,0,position);
                }
            });

            holder.imgCommentaryCollaborators.setOnClickListener(v -> listener.onItemCommentaryClick(collaborator,collaborator.getAttendance(),position));


    }

    public void setListener(OnItemClickCollaboratorListener listener) {
        this.listener = listener;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class CampaignVolunteeringViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.txt_name_collaborators)
        TextView txNameCollaborators;
        @BindView(R.id.txt_number_doc_collaborators)
        TextView txtNumberDocCollaborators;
        @BindView(R.id.swt_selected)
        SwitchCompat swtSelected;
        @BindView(R.id.img_commentary_collaborators)
        ImageView imgCommentaryCollaborators;
        @BindView(R.id.container)
        LinearLayoutCompat container;

         CampaignVolunteeringViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


    }

    public interface OnItemClickCollaboratorListener {
        void onItemCollaboratorClick(ListCollaboratorViewModel entity, int value, int position);
        void onItemCommentaryClick(ListCollaboratorViewModel entity, int value, int position);
    }
}
