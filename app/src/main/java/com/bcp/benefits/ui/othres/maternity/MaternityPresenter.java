package com.bcp.benefits.ui.othres.maternity;

import androidx.annotation.NonNull;

import java.util.ArrayList;

import javax.inject.Inject;
/**
 * Created by emontesinos on 30/04/19.
 **/

public class MaternityPresenter {

    private MaternityView view;

    @Inject
    MaternityPresenter() { }
    public void setView(@NonNull MaternityView view) {
        this.view = view;

    }

    public void goListaMaternity(){
        ArrayList<String> liscampain = new ArrayList<>();
        liscampain.add("1");
        liscampain.add("2");
        liscampain.add("3");
        liscampain.add("4");
        view.showBenenitsMaternity(liscampain);
    }

    public interface MaternityView {
        void showBenenitsMaternity(ArrayList<String> list);
    }
}
