package com.bcp.benefits.ui.othres.golden;

import androidx.annotation.NonNull;
import pe.solera.benefit.main.entity.Ticket;
import pe.solera.benefit.main.usecases.executor.Action;
import pe.solera.benefit.main.usecases.usecases.TicketUseCase;

import javax.inject.Inject;

import com.bcp.benefits.ui.MainView;
import com.bcp.benefits.viewmodel.GoldenTicketViewModel;

/**
 * Created by emontesinos on 30/04/19.
 **/

public class GoldenTicketPresenter {


    private GoldenTicketView view;


    private final TicketUseCase ticketUseCase;

    @Inject
    GoldenTicketPresenter(TicketUseCase ticketUseCase) {
        this.ticketUseCase = ticketUseCase;
    }


    public void setView(@NonNull GoldenTicketView view) {
        this.view = view;
    }

    void doApproveTicket(final int idTicket, boolean force) {
        if (this.view.validateInternet()) {
            this.view.showProgress();
            ticketUseCase.aprovelTickets(idTicket, force, new Action.Callback<String>() {
                @Override
                public void onSuccess(String s) {
                    view.successApprove(s);
                    view.hideProgress();
                }

                @Override
                public void onError(Throwable throwable) {
                    view.showErrorMessage(throwable.getMessage());
                    view.hideProgress();
                }
            });
        }

    }

    void doCancelTicket(final int idTicket) {
        if (this.view.validateInternet()) {
            this.view.showProgress();
            ticketUseCase.cancelTickets(idTicket, new Action.Callback<String>() {
                @Override
                public void onSuccess(String s) {
                    view.successCancel(s);
                    view.hideProgress();
                }

                @Override
                public void onError(Throwable throwable) {
                    view.showErrorMessage(throwable.getMessage());
                    view.hideProgress();
                }
            });
        }

    }

    void doGetTickets() {
        if (this.view.validateInternet()) {
            this.view.showProgress();
            ticketUseCase.getTicket(new Action.Callback<Ticket>() {
                @Override
                public void onSuccess(Ticket ticket) {
                    view.hideProgress();
                    view.showTickets(new GoldenTicketViewModel(ticket));
                }

                @Override
                public void onError(Throwable throwable) {
                    view.showErrorMessage(throwable.getMessage());
                    view.hideProgress();

                }
            });
        }

    }

    void doReleaseTicket(int idTicket){
        if (this.view.validateInternet()) {
            this.view.showProgress();
            ticketUseCase.releaseTickets(idTicket, new Action.Callback<String>() {
                @Override
                public void onSuccess(String s) {
                    geTickets();
                    view.hideProgress();
                }

                @Override
                public void onError(Throwable throwable) {
                    view.showErrorMessage(throwable.getMessage());
                   view.hideProgress();
                }
            });
        }

    }

    private void geTickets(){
        if (this.view.validateInternet()) {
            this.view.showProgress();
            ticketUseCase.getTicket(new Action.Callback<Ticket>() {
                @Override
                public void onSuccess(Ticket ticket) {
                    view.showTickets(new GoldenTicketViewModel(ticket));
                    view.hideProgress();
                }

                @Override
                public void onError(Throwable throwable) {
                    view.showErrorMessage(throwable.getMessage());
                    view.hideProgress();
                }
            });
        }
    }

    public interface GoldenTicketView extends MainView {
        void showTickets(GoldenTicketViewModel goldenTicketModel);

        void successApprove(String message);

        void successCancel(String message);
    }
}
