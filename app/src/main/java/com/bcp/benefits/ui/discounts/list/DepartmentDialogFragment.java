package com.bcp.benefits.ui.discounts.list;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bcp.benefits.R;
import com.bcp.benefits.viewmodel.DiscountTypeViewModel;
import com.bcp.benefits.viewmodel.StateViewModel;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.bcp.benefits.util.Constants.ID_TYPE;
import static com.bcp.benefits.util.Constants.LIST_TYPE_DEPARTMENT;

public class DepartmentDialogFragment extends DialogFragment implements DepartmentAdapter.OnItemClickDepartmentListener {


    @BindView(R.id.rv_type)
    RecyclerView rvType;
    @BindView(R.id.iv_close)
    ImageView ivClose;
    @BindView(R.id.constraintLayout9)
    ConstraintLayout contentDialog;
    @BindView(R.id.text_title_generic)
    TextView textGeneric;

    private OnSelectDepartmentListener onSelectDepartment;
    private ArrayList<StateViewModel> stateViewModels;
    private DiscountTypeViewModel type;
    private int provinceSelected;
    private StateViewModel provincSelected;

    public static DepartmentDialogFragment newInstance(StateViewModel stateSelected, ArrayList<StateViewModel> types,
                                                       DiscountTypeViewModel idType
            , OnSelectDepartmentListener onSelectDepartment
    ) {
        DepartmentDialogFragment fragment = new DepartmentDialogFragment();
        Bundle args = new Bundle();
        fragment.onSelectDepartment = onSelectDepartment;
        args.putSerializable(LIST_TYPE_DEPARTMENT, types);
        args.putSerializable(ID_TYPE, idType);
        args.putSerializable("stateSelected", stateSelected);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow()
                .getAttributes().windowAnimations = R.style.DialogTheme;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            Drawable backgroundColor = new ColorDrawable(ContextCompat.getColor(
                    Objects.requireNonNull(getContext()), android.R.color.transparent));
            Objects.requireNonNull(dialog.getWindow()).setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            dialog.getWindow().setBackgroundDrawable(backgroundColor);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_identity_types, container, false);
        ButterKnife.bind(this, v);
        stateViewModels = (ArrayList<StateViewModel>) getArguments().getSerializable(LIST_TYPE_DEPARTMENT);
        provincSelected = (StateViewModel) getArguments().getSerializable("stateSelected");
        type = (DiscountTypeViewModel) getArguments().getSerializable(ID_TYPE);
        return v;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        textGeneric.setText(R.string.select_department);
        dialogSelect(type);
        ivClose.setOnClickListener(v -> getDialog().dismiss());
        rvType.setLayoutManager(new LinearLayoutManager(getContext()));
        DepartmentAdapter departmentAdapter = new DepartmentAdapter(getContext(), type);
        DividerItemDecoration itemDecoration = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        rvType.addItemDecoration(itemDecoration);
        rvType.setAdapter(departmentAdapter);
        departmentAdapter.addCategories(stateViewModels);
        departmentAdapter.setSelectedItem(provincSelected);
        departmentAdapter.setListener(this);

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        Objects.requireNonNull(dialog.getWindow()).requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onItemDepartmentClick(StateViewModel entity, int position) {

        onSelectDepartment.onSelectDepartment(entity, position);
        dismiss();

    }

    public interface OnSelectDepartmentListener {
        void onSelectDepartment(StateViewModel entity, int position);
    }

    private void dialogSelect(DiscountTypeViewModel type) {
        switch (type) {
            case DISCOUNT:
                ivClose.setImageResource(R.drawable.ic_close_white);
                contentDialog.setBackground(getResources().getDrawable(R.drawable.bg_type_custom_green));
                break;
            case EDUTATION:
                ivClose.setImageResource(R.drawable.ic_close_white_yellow);
                contentDialog.setBackground(getResources().getDrawable(R.drawable.bg_type_custom_yellow));
                break;
        }


    }
}
