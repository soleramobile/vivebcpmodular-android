package com.bcp.benefits.ui.othres.volunteering;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.bcp.benefits.R;
import com.bcp.benefits.analytics.AnalyticsModule;
import com.bcp.benefits.analytics.AnalyticsTags;
import com.bcp.benefits.ui.BaseActivity;
import com.bcp.benefits.ui.tutorial.NonSwipeableViewPager;
import com.bcp.benefits.viewmodel.VolunteeringTicketViewModel;
import com.google.android.material.appbar.AppBarLayout;

import javax.inject.Inject;

import butterknife.BindView;

import static com.bcp.benefits.util.Constants.DATE_START;
import static com.bcp.benefits.util.Constants.GOLDEN_TICKETS;
import static com.bcp.benefits.util.Constants.IDENTIFIER_TICKETS_VOLUNTEERING_APROVAL_DIALOG;
import static com.bcp.benefits.util.Constants.IDENTIFIER_TICKETS_VOLUNTEERING_REFUSE_DIALOG;
import static com.bcp.benefits.util.Constants.RESULT_TICKET;
import static com.bcp.benefits.util.Constants.TICKETS_TO_APPROVED;

/**
 * Created by emontesinos on 30/04/19.
 **/

public class VolunteeringTicketActivity extends BaseActivity implements VolunteetingTicketPresenter.VolunteetingTicketView {

    @Inject
    VolunteetingTicketPresenter presenter;

    @BindView(R.id.toolbar)
    AppBarLayout toolbar;
    @BindView(R.id.img_back)
    ImageButton imgBack;
    @BindView(R.id.title_toolbar)
    TextView titleToobal;
    @BindView(R.id.radioGroup)
    RadioGroup mRadioGroup;
    @BindView(R.id.rbMyTickets)
    RadioButton rbMyTickets;
    @BindView(R.id.rbForApproved)
    RadioButton rbForApproved;
    private AnalyticsModule analyticsModule = new AnalyticsModule(this);


    @BindView(R.id.viewPager_ticket)
    NonSwipeableViewPager vpGoldenTicketsItems;
    private int selector;

    public static void start(Context context) {
        Intent intent = new Intent(context, VolunteeringTicketActivity.class);
        context.startActivity(intent);
    }

    @Override
    public void setUpView() {
        this.presenter.setView(this);
        sendToAnalytics();
        setToolbar();
        setUpPager();
        getVolunteeringTickets();
    }

    @Override
    public int getLayout() {
        return R.layout.activity_golden_ticket;
    }

    private void sendToAnalytics(){
        analyticsModule.sendToAnalytics(AnalyticsTags.Others.Events.ticketVoluntariado, null);
    }


    private void setToolbar() {
        titleToobal.setText(R.string.ticket_volunteering_two);
        rbForApproved.setTextColor(getResources().getColor(R.color.bg_orange_end_volunteering));
        toolbar.setBackground(getResources().getDrawable(R.drawable.bg_golden_volunteering));
        rbMyTickets.setText(getResources().getString(R.string.txt_title_tickets_volu));
        rbForApproved.setText(getResources().getString(R.string.txt_title_tickets_apro));
        rbForApproved.setBackground(getResources().getDrawable(R.drawable.rbtn_selector_volunteeting));
        rbMyTickets.setBackground(getResources().getDrawable(R.drawable.rbtn_selector_volunteeting));
        imgBack.setOnClickListener(v -> {
            onBackPressed();
            overridePendingTransition(R.anim.rigth_in, R.anim.right_out);
        });
    }

    private void getVolunteeringTickets() {
        this.presenter.doGetTicketsVolunteering();
    }

    private void setUpPager() {
        vpGoldenTicketsItems.setPagingEnabled(false);

        mRadioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            if (checkedId == R.id.rbMyTickets) {
                vpGoldenTicketsItems.setCurrentItem(GOLDEN_TICKETS);
                selector = GOLDEN_TICKETS;
                rbMyTickets.setTextColor(getResources().getColor(R.color.white));
                rbForApproved.setTextColor(getResources().getColor(R.color.bg_orange_end_volunteering));
            } else if (checkedId == R.id.rbForApproved) {
                vpGoldenTicketsItems.setCurrentItem(TICKETS_TO_APPROVED);
                selector = TICKETS_TO_APPROVED;
                rbMyTickets.setTextColor(getResources().getColor(R.color.bg_orange_end_volunteering));
                rbForApproved.setTextColor(getResources().getColor(R.color.white));
            }
        });
    }

    @Override
    public void showTicketsVolunteering(VolunteeringTicketViewModel goldenTicketModel) {
        VolunteeringPagerAdapter genericPagerAdapter = new VolunteeringPagerAdapter(getSupportFragmentManager());
        vpGoldenTicketsItems.setAdapter(genericPagerAdapter);
        genericPagerAdapter.addGoldenTicketResponse(goldenTicketModel);
        if (selector == GOLDEN_TICKETS) {
            mRadioGroup.check(R.id.rbMyTickets);
            vpGoldenTicketsItems.setCurrentItem(GOLDEN_TICKETS);
        } else {
            mRadioGroup.check(R.id.rbForApproved);
            vpGoldenTicketsItems.setCurrentItem(TICKETS_TO_APPROVED);

        }
        mRadioGroup.getChildAt(1).setEnabled(goldenTicketModel.isValidateTipeUser());
        if(!goldenTicketModel.isValidateTipeUser()){
            rbForApproved.setTextColor(getResources().getColor(R.color.gray));
        }
    }

    @Override
    public void successApproveVolunteering(Boolean message) {
        getVolunteeringTickets();
    }

    @Override
    public void successCancelVolunteering(Boolean message) {
        getVolunteeringTickets();
    }

    public void approveTicketVolunteering(final int idTicket) {
        DialogTicketVolunteering dp = DialogTicketVolunteering.newInstance(IDENTIFIER_TICKETS_VOLUNTEERING_APROVAL_DIALOG, idTicket);
        dp.show(getSupportFragmentManager(), DATE_START);
    }

    public void cancelTicketVolunteering(final int idTicket) {
        DialogTicketVolunteering dp = DialogTicketVolunteering.newInstance(IDENTIFIER_TICKETS_VOLUNTEERING_REFUSE_DIALOG, idTicket);
        dp.show(getSupportFragmentManager(), DATE_START);
    }

    public void approveVolunteeringDialogTicket(final String idTicket) {
        this.presenter.doApproveTicketVolunteering(idTicket);
    }

    public void cancelVolunteeringDialogTicket(final String idTicket) {
        this.presenter.doCancelTicketVolunteering(idTicket);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_TICKET) {
            if (resultCode == Activity.RESULT_OK || resultCode == Activity.RESULT_CANCELED) {
                getVolunteeringTickets();
            }
        }
    }
}
