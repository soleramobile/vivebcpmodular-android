package com.bcp.benefits.ui.othres.volunteering;

import androidx.annotation.NonNull;

import com.bcp.benefits.ui.MainView;
import com.bcp.benefits.viewmodel.VolunteeringTicketViewModel;
import javax.inject.Inject;
import pe.solera.benefit.main.entity.VolunteeringTicket;
import pe.solera.benefit.main.usecases.executor.Action;
import pe.solera.benefit.main.usecases.usecases.TicketVolunteeringUseCase;

/**
 * Created by emontesinos on 30/04/19.
 **/

public class VolunteetingTicketPresenter {

    private VolunteetingTicketView view;
    private final TicketVolunteeringUseCase ticketVolunteeringUseCase;

    @Inject
    VolunteetingTicketPresenter(TicketVolunteeringUseCase TicketVolunteeringUseCase) {
        this.ticketVolunteeringUseCase = TicketVolunteeringUseCase;
    }


    public void setView(@NonNull VolunteetingTicketView view) {
        this.view = view;
    }

    void doApproveTicketVolunteering(final String  idTicket) {
        if (this.view.validateInternet()) {
            this.view.showProgress();
          ticketVolunteeringUseCase.aprovelTicketsVolunteering(idTicket, new Action.Callback<Boolean>() {
              @Override
              public void onSuccess(Boolean aBoolean) {
                  view.hideProgress();
                   view.successApproveVolunteering(aBoolean);
              }

              @Override
              public void onError(Throwable throwable) {
                  view.showErrorMessage(throwable.getMessage());
                  view.hideProgress();
              }
          });
        }

    }

    void doCancelTicketVolunteering(final String idTicket) {
        if (this.view.validateInternet()) {
            this.view.showProgress();
            ticketVolunteeringUseCase.cancelTicketsVolunteering(idTicket, new Action.Callback<Boolean>() {
                @Override
                public void onSuccess(Boolean aBoolean) {
                    view.hideProgress();
                    view.successCancelVolunteering(aBoolean);

                }

                @Override
                public void onError(Throwable throwable) {
                    view.showErrorMessage(throwable.getMessage());
                    view.hideProgress();
                }
            });
        }

    }

    void doGetTicketsVolunteering() {
        if (this.view.validateInternet()) {
            this.view.showProgress();
            ticketVolunteeringUseCase.getTicketVolunteering(new Action.Callback<VolunteeringTicket>() {
                @Override
                public void onSuccess(VolunteeringTicket volunteeringTicket) {
                    view.hideProgress();
                    view.showTicketsVolunteering(new VolunteeringTicketViewModel(volunteeringTicket));

                }

                @Override
                public void onError(Throwable throwable) {
                    view.showErrorMessage(throwable.getMessage());
                    view.hideProgress();
                }
            });
        }

    }


    public interface VolunteetingTicketView extends MainView {
        void showTicketsVolunteering(VolunteeringTicketViewModel volunteeringTicketViewModel);
        void successApproveVolunteering(Boolean response);
        void successCancelVolunteering(Boolean response);
    }
}
