package com.bcp.benefits.ui.campaign.volunteering.dialogs;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bcp.benefits.R;
import com.bcp.benefits.ui.campaign.volunteering.dialogs.adapter.ListOptionsAdapter;
import com.bcp.benefits.viewmodel.ListRoleViewModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.bcp.benefits.util.Constants.LIST_TO_DIALOG_OPTIONS;
import static com.bcp.benefits.util.Constants.TITLE_TO_DIALOG_OPTIONS;
import static com.bcp.benefits.util.Constants.TYPE_SELECTION;


/* 
   Created by: Flavia Figueroa
               13/02/2020
         Solera Mobile
*/


public class OptionsDialogFragment extends DialogFragment implements ListOptionsAdapter.OptionsListener {

    @BindView(R.id.tvTitleDialog)
    TextView titleDialog;
    @BindView(R.id.rvOptions)
    RecyclerView rvOptions;

    private String title;
    private List<ListRoleViewModel> listRole;
    private String type;
    private SelectedIdListener listener;

    public static OptionsDialogFragment newInstance(String title, List<ListRoleViewModel> list, String type) {
        Bundle args = new Bundle();
        OptionsDialogFragment fragment = new OptionsDialogFragment();
        args.putString(TITLE_TO_DIALOG_OPTIONS, title);
        args.putString(LIST_TO_DIALOG_OPTIONS, new Gson().toJson(list));
        args.putString(TYPE_SELECTION, type);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_fragment_options, container, false);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable((Color.TRANSPARENT)));
        ButterKnife.bind(this, v);
        setUp();
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        uiPreferences();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        listener = (SelectedIdListener) context;
    }

    private void setUp() {
        if (getArguments() != null) {
            Type type;
            this.title = (String) getArguments().getSerializable(TITLE_TO_DIALOG_OPTIONS);
            type = new TypeToken<List<ListRoleViewModel>>() {
                }.getType();
            this.listRole = new Gson().fromJson(getArguments().getString(LIST_TO_DIALOG_OPTIONS), type);
            this.type = (String) getArguments().getSerializable(TYPE_SELECTION);
        }
    }

    private void uiPreferences(){
        this.setCancelable(false);
        this.titleDialog.setText(title);
        this.rvOptions.setLayoutManager(new LinearLayoutManager(getContext()));
        ListOptionsAdapter optionsAdapter = new ListOptionsAdapter(listRole, type);
        this.rvOptions.setAdapter(optionsAdapter);
        optionsAdapter.setListener(this);
    }

    @OnClick(R.id.imgBtnClose)
    void closeDialog() {
        dismiss();
    }

    @Override
    public void select(ListRoleViewModel listRoleViewModel, String type) {
        listener.selectedId(listRoleViewModel.getIdRoleVolunteer(), listRoleViewModel.getName(), type);
        dismiss();
    }

    public interface SelectedIdListener {
        void selectedId(int id, String name, String type);
    }
}
