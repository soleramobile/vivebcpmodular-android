package com.bcp.benefits.ui.othres.assistanceconfirmation.dialog;
/* 
   Created by: Flavia Figueroa
               25/02/2020
         Solera Mobile
*/


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bcp.benefits.R;
import com.bcp.benefits.viewmodel.ListLocalViewModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListLocalAdapter extends RecyclerView.Adapter<ListLocalAdapter.ListLocalViewHolder> {

    private ArrayList<ListLocalViewModel> list = new ArrayList<>();
    private ListLocalListener listener;
    private int selectedItem = -1;
    private Context context;

    public ListLocalAdapter(Context context) {
        this.context = context;
    }

    void addCategories(ArrayList<ListLocalViewModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public void setSelectedItem(ListLocalViewModel localVolunteeringViewModel) {
        if (localVolunteeringViewModel == null) return;
        int index = list.indexOf(localVolunteeringViewModel);
        if (selectedItem != index) {
            selectedItem = index;
            notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public ListLocalAdapter.ListLocalViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_type_custom, parent, false);
        return new ListLocalViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListLocalAdapter.ListLocalViewHolder holder, int position) {
        holder.text.setText(list.get(position).getLocal());
        if (selectedItem == position) {
            holder.imgCircle.setVisibility(View.VISIBLE);
            holder.imgCircle.setBackground(context.getResources().getDrawable(R.drawable.bg_cicle_green));
        } else {
            holder.imgCircle.setVisibility(View.INVISIBLE);
        }
    }

    public void setListener(ListLocalListener listener) {
        this.listener = listener;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ListLocalViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        @BindView(R.id.txt_type_texto)
        TextView text;
        @BindView(R.id.img_circle)
        ImageView imgCircle;

        public ListLocalViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (listener == null)
                return;

            if (selectedItem != -1)
                notifyItemChanged(selectedItem);


            if (selectedItem == getAdapterPosition()) {
                selectedItem = -1;
                notifyItemChanged(getAdapterPosition());
                if (listener != null)
                    listener.listLocalClick(null, getAdapterPosition());
            } else {
                selectedItem = getAdapterPosition();
                notifyItemChanged(selectedItem);
                listener.listLocalClick(list.get(getAdapterPosition()), getAdapterPosition());
            }

        }
    }

    public interface ListLocalListener {
        void listLocalClick(ListLocalViewModel entity, int position);
    }
}
