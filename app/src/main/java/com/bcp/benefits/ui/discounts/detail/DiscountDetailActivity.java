package com.bcp.benefits.ui.discounts.detail;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;

import com.bcp.benefits.R;
import com.bcp.benefits.analytics.AnalyticsModule;
import com.bcp.benefits.analytics.AnalyticsTags;
import com.bcp.benefits.components.CustomTypefaceSpan;
import com.bcp.benefits.ui.BaseActivity;
import com.bcp.benefits.ui.discounts.consume.ConsumeDiscountActivity;
import com.bcp.benefits.ui.discounts.consume.ConsumeDiscountPdfViewActivity;
import com.bcp.benefits.ui.discounts.detail.dialogterms.DialogTerms;
import com.bcp.benefits.util.AppUtils;
import com.bcp.benefits.util.CallController;
import com.bcp.benefits.util.Constants;
import com.bcp.benefits.viewmodel.BotonAction;
import com.bcp.benefits.viewmodel.DiscountEnterType;
import com.bcp.benefits.viewmodel.DiscountTypeViewModel;
import com.bcp.benefits.viewmodel.DiscountViewModel;
import com.bcp.benefits.viewmodel.SubsidiaryViewModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

import static com.bcp.benefits.util.Constants.DATE_START;
import static com.bcp.benefits.util.Constants.DISCOUNT_PARAM;
import static com.bcp.benefits.util.Constants.DISCOUNT_TYPE_PARAM;
import static com.bcp.benefits.util.Constants.DISCOUNT_TYPE_PARAMA;
import static com.bcp.benefits.util.Constants.ENTER_TYPE;
import static com.bcp.benefits.util.Constants.ID_MAPS;
import static com.bcp.benefits.util.Constants.ID_WAZE;
import static com.bcp.benefits.util.Constants.SUBSIDIARY_PARAM;
import static com.bcp.benefits.viewmodel.DiscountTypeViewModel.DISCOUNT;
import static com.bcp.benefits.viewmodel.DiscountTypeViewModel.EDUTATION;

public class DiscountDetailActivity extends BaseActivity implements
        DiscountDetailPresenter.DiscountDetailView, DiscountDetailDialogFragment.OnSelectSuccesListener {
    @Inject
    DiscountDetailPresenter presenter;
    @BindView(R.id.iv_photo)
    ImageView ivPhoto;
    @BindView(R.id.tv_caption_value)
    TextView tvCaptionValue;
    @BindView(R.id.tv_provider)
    TextView tvProvider;
    @BindView(R.id.tv_description)
    TextView tvDescription;
    @BindView(R.id.rb_score)
    AppCompatRatingBar rbScore;
    @BindView(R.id.btn_use_discount)
    TextView btnUseDiscount;
    @BindView(R.id.tv_webpage)
    TextView tvWebPage;
    @BindView(R.id.linear_qualification)
    LinearLayout rlCaliticationContent;
    @BindView(R.id.tv_how_to_get)
    TextView btnHowToGet;
    @BindView(R.id.tv_callus)
    TextView btnCallUs;
    @BindView(R.id.btn_promotions)
    TextView tvPromotions;
    @BindView(R.id.iv_close)
    ImageButton ivBack;
    @BindView(R.id.tv_terms)
    TextView tvTerms;
    @BindView(R.id.view_line_two)
    View lineTwo;
    @BindView(R.id.content_discount)
    ConstraintLayout contentDiscount;
    @BindView(R.id.content_detail_discount)
    ConstraintLayout contentDetailGenereral;

    private CallController callController;
    private DiscountViewModel selectedDiscount;
    private DiscountTypeViewModel discountType;
    private ArrayList<SubsidiaryViewModel> subsidiariesForCall = new ArrayList<>();
    private ArrayList<SubsidiaryViewModel> subsidiariesForHowToGet = new ArrayList<>();
    private int colorSkin;
    private String termsConditions;


    private Boolean validClick = true;
    private Boolean validClickthree = true;
    private Boolean validClickTwo = true;
    private Boolean validFour = true;
    private AnalyticsModule analyticsModule = new AnalyticsModule(this);

    private String enterType;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        this.loadData();
        setTheme(discountType == EDUTATION ? R.style.AppThemeEducation : R.style.AppThemeDiscount);
        super.onCreate(savedInstanceState);
    }

    private void loadData() {
        discountType = (DiscountTypeViewModel) getIntent().getSerializableExtra(DISCOUNT_TYPE_PARAMA);
    }

    public static void start(Activity context, DiscountTypeViewModel discountType, Integer discountId, Integer subsidiaryId, DiscountEnterType enterType) {
        Intent starter = new Intent(context, DiscountDetailActivity.class);
        starter.putExtra(DISCOUNT_PARAM, discountId);
        starter.putExtra(SUBSIDIARY_PARAM, subsidiaryId);
        starter.putExtra(DISCOUNT_TYPE_PARAMA, discountType);
        starter.putExtra(ENTER_TYPE, enterType.getType());
        context.startActivityForResult(starter, 450);
    }

    @Override
    public void setUpView() {
        this.presenter.setView(this);
        Integer discountId = getIntent().getIntExtra(DISCOUNT_PARAM, -1);
        Integer subsidiaryId = getIntent().getIntExtra(SUBSIDIARY_PARAM, -1);
        enterType = getIntent().getStringExtra(ENTER_TYPE);
        discountType = (DiscountTypeViewModel) getIntent().getSerializableExtra(DISCOUNT_TYPE_PARAM);
        colorSkin = ContextCompat.getColor(this, colorSkinFlv());
        btnHowToGet.setTextColor(colorSkin);
        btnCallUs.setTextColor(colorSkin);
        Drawable btnUseDrawable = fetchDrawable(userBtnFlv());
        btnHowToGet.setBackground(fetchDrawable(bgFlv()));
        btnCallUs.setBackground(fetchDrawable(bgFlv()));
        ivBack.setImageResource(backFlv());
        btnCallUs.setCompoundDrawablesWithIntrinsicBounds(phoneFlv(), 0, 0, 0);
        btnHowToGet.setCompoundDrawablesWithIntrinsicBounds(markerFlv(), 0, 0, 0);

        btnUseDrawable.mutate();
        btnUseDiscount.setBackground(btnUseDrawable);
        callController = new CallController(this, null);

        presenter.doGetDiscountDetail(discountType, discountId, subsidiaryId);

    }

    @Override
    public void errorDiscountNotFound() {
        new CountDownTimer(3000, 100) {
            @Override
            public void onTick(long millisUntilFinished) {
                //Do nothing
            }

            @Override
            public void onFinish() {
                onBackPressed();
            }
        }.start();
    }

    @Override
    public int getLayout() {
        return R.layout.activity_discount_detail;
    }

    @Override
    public void showDetail(DiscountViewModel discountViewModel) {

        sendToAnalytics(discountViewModel);

        selectedDiscount = discountViewModel;
        if (discountViewModel.getImage() != null && !discountViewModel.getImage().isEmpty()) {
            Picasso.get().load(discountViewModel.getImage()).placeholder(R.drawable.placeholder_discount_detail).into(ivPhoto);
        } else {
            ivPhoto.setImageDrawable(getResources().getDrawable(R.drawable.placeholder_discount_detail));
        }

        int lengthValue = 0;
        if (discountViewModel.getValue() != null)
            lengthValue = discountViewModel.getValue().length();

        StringBuilder sbCaption = new StringBuilder();

        if (discountViewModel.getValue() != null && !discountViewModel.getValue().isEmpty())
            sbCaption.append(discountViewModel.getValue());

        if (discountViewModel.getCaption() != null && !discountViewModel.getCaption().isEmpty()) {
            if (discountViewModel.getValue() != null && !discountViewModel.getValue().isEmpty())
                sbCaption.append(Constants.SPACE);
            sbCaption.append(discountViewModel.getCaption());
        }

        String caption = sbCaption.toString();
        Typeface font = ResourcesCompat.getFont(this, R.font.breviabold);
        SpannableString spannableString = new SpannableString(caption);
        spannableString.setSpan(new CustomTypefaceSpan(font), 0, lengthValue, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        spannableString.setSpan(new ForegroundColorSpan(colorSkin), 0, lengthValue, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        tvCaptionValue.setText(spannableString);

        tvProvider.setText(discountViewModel.getProvider());
        tvDescription.setText(discountViewModel.getDescription());

        if (discountViewModel.getWebpage() != null && !discountViewModel.getWebpage().isEmpty()) {

            SpannableStringBuilder ssb = new SpannableStringBuilder(discountViewModel.getWebpage());
            ssb.setSpan(new UnderlineSpan(), 0, discountViewModel.getWebpage().length(), 0);
            tvWebPage.setText(ssb);
            tvWebPage.setOnClickListener(view -> {
                sendInteractionToAnalytics(tvWebPage.getText().toString(), BotonAction.WEB);
                tvWebPage.setClickable(false);
                AppUtils.openBrowser(DiscountDetailActivity.this, discountViewModel.getWebpage());
                tvWebPage.setClickable(true);
            });
        } else
            tvWebPage.setVisibility(View.GONE);

        if (discountViewModel.getCanBeReviewed() != null && discountViewModel.getCanBeReviewed()) {
            rlCaliticationContent.setVisibility(View.VISIBLE);
            if (discountViewModel.getScore() != 0) {
                rbScore.setRating(discountViewModel.getScore());
            } else
                rbScore.setRating(0);
        } else {
            rlCaliticationContent.setVisibility(View.GONE);
        }

        if (discountViewModel.getCanBeUsed() != null && discountViewModel.getCanBeUsed())
            btnUseDiscount.setVisibility(View.VISIBLE);
        else
            btnUseDiscount.setVisibility(View.GONE);

        if (discountViewModel.getTerms() != null) {
            termsConditions = discountViewModel.getTerms();
        } else {
            tvTerms.setVisibility(View.GONE);
            lineTwo.setVisibility(View.GONE);
        }

        if (discountViewModel.getSubsidiaries() != null && discountViewModel.getSubsidiaries().size() == 1) {
            if (discountViewModel.getSubsidiaries().get(0).getLatitude() == null
                    || discountViewModel.getSubsidiaries().get(0).getLongitude() == null) {
                btnHowToGet.setVisibility(View.INVISIBLE);
            }
        }

        if (discountViewModel.getSubsidiaries() != null && !discountViewModel.getSubsidiaries().isEmpty()) {
            //check for calling
            for (SubsidiaryViewModel item : discountViewModel.getSubsidiaries()) {
                if (item.getPhone() != null && !item.getPhone().isEmpty()) {
                    subsidiariesForCall.add(item);
                }
            }

            //check for how to get
            for (SubsidiaryViewModel item : discountViewModel.getSubsidiaries()) {
                if (item.getLatitude() != null && !item.getLongitude().isEmpty() &&
                        item.getLatitude() != null && !item.getLongitude().isEmpty()) {
                    subsidiariesForHowToGet.add(item);
                }
            }

            if (subsidiariesForCall.size() <= 0)
                btnCallUs.setVisibility(View.INVISIBLE);

            if (subsidiariesForHowToGet.size() <= 0)
                btnHowToGet.setVisibility(View.INVISIBLE);

            if (subsidiariesForCall.size() <= 0 && subsidiariesForHowToGet.size() <= 0) {
                btnHowToGet.setVisibility(View.INVISIBLE);
                btnCallUs.setVisibility(View.INVISIBLE);
            }
        } else {
            btnHowToGet.setVisibility(View.INVISIBLE);
            btnCallUs.setVisibility(View.INVISIBLE);
        }
        showContent();
        if (discountViewModel.getPdfUrl() == null) {
            tvPromotions.setVisibility(View.GONE);
            return;
        } else {
            tvPromotions.setVisibility(View.VISIBLE);
        }

        if (discountViewModel.getPdfUrl().contains(".pdf")) {
            tvPromotions.setOnClickListener(view -> {
                sendInteractionToAnalytics(tvPromotions.getText().toString(), BotonAction.DESCARGAR);
                ConsumeDiscountPdfViewActivity.start(this, discountType, discountViewModel.getPdfUrl(), discountViewModel.getProvider());
            });
        } else {
            tvPromotions.setOnClickListener(view -> {
                sendInteractionToAnalytics(tvPromotions.getText().toString(), BotonAction.WEB);
                AppUtils.openBrowser(DiscountDetailActivity.this, discountViewModel.getPdfUrl());
            });
        }
    }

    private void showContent() {
        contentDetailGenereral.setVisibility(View.VISIBLE);
        contentDiscount.setVisibility(View.VISIBLE);
    }

    private void sendToAnalytics(DiscountViewModel discountViewModel){
        if (discountType == DISCOUNT){
            sendToAnalyticsDiscount(discountViewModel);
        }else {
            sendToAnalyticsEducation(discountViewModel);
        }
    }

    private void sendToAnalyticsDiscount(DiscountViewModel discountViewModel){
        Bundle bundle = new Bundle();
        bundle.putString(AnalyticsTags.Discounts.Parameters.beneficios_id, String.valueOf(discountViewModel.getIdDiscount()));
        bundle.putString(AnalyticsTags.Discounts.Parameters.beneficios_nombre, AppUtils.getFirstNCharacters(discountViewModel.getCaption()));
        bundle.putString(AnalyticsTags.Discounts.Parameters.beneficios_empresa, AppUtils.getFirstNCharacters(discountViewModel.getProvider()));
        bundle.putInt(AnalyticsTags.Discounts.Parameters.beneficios_empresa_id, discountViewModel.getIdProvider());
        bundle.putString(AnalyticsTags.Discounts.Parameters.pantalla_tipo, enterType);
        analyticsModule.sendToAnalytics(AnalyticsTags.Discounts.Events.descuento_detalle, bundle);
    }

    private void sendToAnalyticsEducation(DiscountViewModel discountViewModel){
        Bundle bundle = new Bundle();
        bundle.putString(AnalyticsTags.Education.Parameters.beneficios_id, String.valueOf(discountViewModel.getIdDiscount()));
        bundle.putString(AnalyticsTags.Education.Parameters.beneficios_nombre, AppUtils.getFirstNCharacters(discountViewModel.getCaption()));
        bundle.putString(AnalyticsTags.Education.Parameters.beneficios_empresa, AppUtils.getFirstNCharacters(discountViewModel.getProvider()));
        bundle.putInt(AnalyticsTags.Education.Parameters.beneficios_empresa_id, discountViewModel.getIdProvider());
        bundle.putString(AnalyticsTags.Education.Parameters.pantalla_tipo, enterType);
        analyticsModule.sendToAnalytics(AnalyticsTags.Education.Events.educacion_detalle, bundle);
    }

    public Drawable fetchDrawable(int id) {
        return getResources().getDrawable(id);
    }

    public int resId(int first, int second) {
        return (discountType == DISCOUNT) ? first : second;
    }

    public int colorSkinFlv() {
        return resId(R.color.bg_green_start, R.color.bg_yellow_start);
    }

    public int userBtnFlv() {
        return resId(R.drawable.bg_green_gradient_button, R.drawable.bg_yellow_gradient_button);
    }

    public int bgFlv() {
        return resId(R.drawable.bg_white_green_corners_stroke_detail, R.drawable.bg_white_corners_yellow_stroke_detail);
    }

    public int backFlv() {
        return resId(R.drawable.ic_back_green, R.drawable.ic_back_yellow_detail);
    }

    public int phoneFlv() {
        return resId(R.drawable.ic_call_green, R.drawable.ic_call_yellow_two);
    }

    public int markerFlv() {
        return resId(R.drawable.ic_location_green, R.drawable.ic_location_yellow);
    }

    @OnClick(R.id.tv_how_to_get)
    public void onTvHowToGetClicked() {
        sendInteractionToAnalytics(btnHowToGet.getText().toString(), BotonAction.LLEGAR);
        if (validClick) {
            if (subsidiariesForHowToGet.size() > 1) {
                validClick = false;
                SubsidiariesActivity.start(this, discountType, SubsidiariesActivity.SubsidiaryOption.HOW_TO_GET, subsidiariesForHowToGet);
            } else {
                validClick = false;
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(R.string.subsidiaries_select_nav_app_subtitle)
                        .setItems(R.array.navigation_apps, (dialog, which) -> {
                            dialog.dismiss();
                            if (which == 0) {
                                selectGoogleMaps();
                            } else if (which == 1) {
                                selectWaze();
                            }

                        });
                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        validClick = true;
                    }
                });

                builder.create();

                builder.show();
            }
        }
    }

    @OnClick(R.id.iv_close)
    public void onIvBackClicked() {
        onBackPressed();
        overridePendingTransition(R.anim.rigth_in, R.anim.right_out);
    }

    @OnClick(R.id.tv_callus)
    public void onTvCallusClicked() {
        sendInteractionToAnalytics(btnCallUs.getText().toString(), BotonAction.LLAMAR);
        if (validClickTwo) {
            if (subsidiariesForHowToGet.size() > 1) {
                validClickTwo = false;
                SubsidiariesActivity.start(this, discountType, SubsidiariesActivity.SubsidiaryOption.CALL_US, subsidiariesForCall);

            } else {
                validClickTwo = false;
                callController.doCall(subsidiariesForCall.get(0).getPhone());
            }

        }
    }

    @OnClick(R.id.tv_terms)
    public void onTvTermsClicked() {
        if (validClickthree) {
            DialogTerms dp = DialogTerms.newInstance(discountType, termsConditions);
            dp.show(getSupportFragmentManager(), DATE_START);
            validClickthree = false;
        }
    }

    @OnClick(R.id.btn_use_discount)
    public void onBtnUseDiscountClicked() {
        if (AppUtils.isConnected(this)) {
            if (validFour) {
                DiscountDetailDialogFragment discountDetailDialogFragment = DiscountDetailDialogFragment.newInstance(discountType, this,selectedDiscount);
                discountDetailDialogFragment.show(getSupportFragmentManager(), DATE_START);
                validFour = false;
            }

        } else {
            AppUtils.dialogIsConnect(this);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        callController.onRequestPermissionsResult(requestCode, grantResults);
    }

    private void sendInteractionToAnalytics(String btnText, BotonAction action){
        Bundle bundle = new Bundle();
        bundle.putString(AnalyticsTags.TransversalElements.Parameters.categoria_nombre, discountType.getValue());
        bundle.putString(AnalyticsTags.TransversalElements.Parameters.boton_accion, action.getAction());
        bundle.putString(AnalyticsTags.TransversalElements.Parameters.boton_nombre, btnText);
        bundle.putString(AnalyticsTags.TransversalElements.Parameters.pantalla_tipo, enterType);
        bundle.putString(AnalyticsTags.TransversalElements.Parameters.beneficios_id, String.valueOf(selectedDiscount.getIdDiscount()));
        bundle.putString(AnalyticsTags.TransversalElements.Parameters.beneficios_nombre, AppUtils.getFirstNCharacters(selectedDiscount.getCaption()));
        bundle.putString(AnalyticsTags.TransversalElements.Parameters.beneficios_empresa, AppUtils.getFirstNCharacters(selectedDiscount.getProvider()));
        bundle.putInt(AnalyticsTags.TransversalElements.Parameters.beneficios_empresa_id, selectedDiscount.getIdProvider());
        analyticsModule.sendToAnalytics(AnalyticsTags.TransversalElements.Events.btn_interactuar, bundle);
    }

    private void selectWaze() {
        String intentUriWaze = "waze://?ll=" + subsidiariesForHowToGet.get(0).getLatitude() + ", " + subsidiariesForHowToGet.get(0).getLongitude() + "&navigate=yes";
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(intentUriWaze));

        if (mapIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(mapIntent);
        } else {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + ID_WAZE)));
        }
    }

    private void selectGoogleMaps() {
        String position = subsidiariesForHowToGet.get(0).getLatitude() + "," + subsidiariesForHowToGet.get(0).getLongitude();
        Uri gmmIntentUri = Uri.parse("google.navigation:q=" + position + "&mode=d");
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");

        if (mapIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(mapIntent);
        } else {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + ID_MAPS)));
        }
    }

    @Override
    public void onSuccess() {
        if (AppUtils.isConnected(this)) {
            ConsumeDiscountActivity.start(DiscountDetailActivity.this, discountType, selectedDiscount.getIdDiscount(),
                    selectedDiscount.getIdSubsidiary(), selectedDiscount.getValue(), selectedDiscount.getCaption(), selectedDiscount.getIdProvider(),
                    selectedDiscount.getProvider());
        } else {
            AppUtils.dialogIsConnect(this);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 400) {
            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        validClick = true;
        validClickTwo = true;

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        validClick = true;
        validClickTwo = true;
    }

    public void updateFlag() {
        validClickthree = true;
        validFour = true;
    }
}
