package com.bcp.benefits.ui.othres.golden.request;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import org.jetbrains.annotations.NotNull;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.bcp.benefits.R;
import com.bcp.benefits.viewmodel.BossViewModel;

/**
 * Created by emontesinos on 02/05/19.
 **/

public class BossAdapter extends RecyclerView.Adapter<BossAdapter.BossVH> {

    private ArrayList<BossViewModel> bosses;
    private final OnSelectBossItemListener onSelectBossItemListener;

     public BossAdapter(OnSelectBossItemListener onSelectBossItemListener) {
        this.onSelectBossItemListener = onSelectBossItemListener;
    }

    public void addList(ArrayList<BossViewModel> bosses) {
        this.bosses = bosses;
        notifyDataSetChanged();
    }

    @NotNull
    @Override
    public BossVH onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_name_generic, parent, false);
        return new BossVH(v);
    }

    @Override
    public void onBindViewHolder(@NotNull BossVH holder, int position) {
        holder.tvBossName.setText(bosses.get(position).name);
    }

    @Override
    public int getItemCount() {
        return bosses==null?0:bosses.size();
    }

    class BossVH extends RecyclerView.ViewHolder{
        @BindView(R.id.tv_boss_name)
        TextView tvBossName;
        BossVH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(view -> {
                if(onSelectBossItemListener!=null)
                    onSelectBossItemListener.selectBossItem(bosses.get(getAdapterPosition()));
            });
        }
    }

    public interface OnSelectBossItemListener{
        void selectBossItem(BossViewModel boss);
    }
}
