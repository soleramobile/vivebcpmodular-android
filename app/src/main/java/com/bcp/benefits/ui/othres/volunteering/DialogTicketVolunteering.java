package com.bcp.benefits.ui.othres.volunteering;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

import com.bcp.benefits.R;
import com.bcp.benefits.util.AppUtils;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.bcp.benefits.util.Constants.IDENTIFIER_DIALOG;
import static com.bcp.benefits.util.Constants.IDENTIFIER_TICKETS_VOLUNTEERING_APROVAL_DIALOG;
import static com.bcp.benefits.util.Constants.IDENTIFIER_TICKETS_VOLUNTEERING_REFUSE_DIALOG;
import static com.bcp.benefits.util.Constants.ID_CAMPAIGN_TICKET;
import static com.bcp.benefits.util.Constants.MESSAGE_SERVICE;

public class DialogTicketVolunteering extends DialogFragment {

    public static DialogTicketVolunteering newInstance(String dialogIdentifier, int idTicket) {
        DialogTicketVolunteering fragment = new DialogTicketVolunteering();
        Bundle args = new Bundle();
        args.putString(IDENTIFIER_DIALOG, dialogIdentifier);
        args.putInt(ID_CAMPAIGN_TICKET, idTicket);
        fragment.setArguments(args);
        return fragment;
    }

    public static DialogTicketVolunteering newInstance(String dialogIdentifier, String message) {
        DialogTicketVolunteering fragment = new DialogTicketVolunteering();
        Bundle args = new Bundle();
        args.putString(IDENTIFIER_DIALOG, dialogIdentifier);
        args.putString(MESSAGE_SERVICE, message);
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.btn_register_generic)
    TextView btnAcept;
    @BindView(R.id.img_close_generic)
    ImageView imgClose;
    @BindView(R.id.text_title_generic)
    TextView title;
    @BindView(R.id.txt_negative)
    TextView btnNegative;
    @BindView(R.id.content_dialog)
    ConstraintLayout contentDialog;
    @BindView(R.id.linear_selection)
    LinearLayout linearSelection;
    @BindView(R.id.txt_dilog_message)
    TextView dilogMessage;
    @BindView(R.id.btn_register_generic_two)
    TextView btnTwo;


    private String identifierDialog;
    private int id;
    private String message;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_custom_generic, container, false);
        ButterKnife.bind(this, v);

        assert getArguments() != null;
        identifierDialog = getArguments().getString(IDENTIFIER_DIALOG);
        id = getArguments().getInt(ID_CAMPAIGN_TICKET);
        message = getArguments().getString(MESSAGE_SERVICE);
        dialogSelect();
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
    }

    @NotNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        Objects.requireNonNull(dialog.getWindow()).requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            Drawable backgroundColor = new ColorDrawable(ContextCompat.getColor
                    (Objects.requireNonNull(getContext()), android.R.color.transparent));
            Objects.requireNonNull(dialog.getWindow()).setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            dialog.getWindow().setBackgroundDrawable(backgroundColor);
        }
    }

    @OnClick({R.id.img_close_generic, R.id.txt_negative})
    public void onDialogDismiss() {
        dismiss();
    }

    @OnClick(R.id.btn_register_generic)
    public void onRegister() {
        switch (identifierDialog) {
            case IDENTIFIER_TICKETS_VOLUNTEERING_APROVAL_DIALOG:
                ((VolunteeringTicketActivity) Objects.requireNonNull(getActivity())).approveVolunteeringDialogTicket(String.valueOf(id));
                dismiss();
                break;
            case IDENTIFIER_TICKETS_VOLUNTEERING_REFUSE_DIALOG:
                ((VolunteeringTicketActivity) Objects.requireNonNull(getActivity())).cancelVolunteeringDialogTicket(String.valueOf(id));
                dismiss();
                break;
        }
    }


    private void dialogSelect() {
        switch (identifierDialog) {
            case IDENTIFIER_TICKETS_VOLUNTEERING_APROVAL_DIALOG:
                title.setText(R.string.approved_ticket_dialog_accept);
                imgClose.setImageResource(R.drawable.ic_close_golden);
                contentDialog.setBackground(getResources().getDrawable(R.drawable.bg_orage_volunteering_gradient_corner_top));
                AppUtils.setGradient(getResources().getColor(R.color.bg_orange_end_volunteering), getResources().getColor(R.color.bg_orange_end_volunteering),
                        AppUtils.dpToPx((int) getResources().getDimension(R.dimen.size_thirteen)), btnAcept);
                btnNegative.setBackground(getResources().getDrawable(R.drawable.bg_volunteering_negative_border));

                break;
            case IDENTIFIER_TICKETS_VOLUNTEERING_REFUSE_DIALOG:
                title.setText(R.string.approved_ticket_dialog_cancel);
                imgClose.setImageResource(R.drawable.ic_close_golden);
                contentDialog.setBackground(getResources().getDrawable(R.drawable.bg_orage_volunteering_gradient_corner_top));
                AppUtils.setGradient(getResources().getColor(R.color.bg_orange_end_volunteering), getResources().getColor(R.color.bg_orange_end_volunteering),
                        AppUtils.dpToPx((int) getResources().getDimension(R.dimen.size_thirteen)), btnAcept);
                btnNegative.setBackground(getResources().getDrawable(R.drawable.bg_volunteering_negative_border));
                break;
        }
    }
}
