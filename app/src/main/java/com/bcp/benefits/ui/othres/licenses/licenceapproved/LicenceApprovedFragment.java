package com.bcp.benefits.ui.othres.licenses.licenceapproved;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import org.jetbrains.annotations.NotNull;
import java.util.ArrayList;
import butterknife.BindView;
import com.bcp.benefits.R;
import com.bcp.benefits.ui.BaseFragment;
import com.bcp.benefits.viewmodel.ForApprovalViewModel;
import com.bcp.benefits.viewmodel.GoldenTicketViewModel;

import static com.bcp.benefits.util.Constants.TICKETS_DATA_APPROVED;
/**
 * Created by emontesinos on 08/05/19.
 **/

public class LicenceApprovedFragment extends BaseFragment {


    public static LicenceApprovedFragment newInstance(GoldenTicketViewModel goldenTicketModel) {
        LicenceApprovedFragment fragment = new LicenceApprovedFragment();
        Bundle args = new Bundle();
        args.putSerializable(TICKETS_DATA_APPROVED, goldenTicketModel);
        fragment.setArguments(args);
        return fragment;
    }


    @BindView(R.id.rvApproved)
    RecyclerView rvApproved;
    @BindView(R.id.tv_empty_list)
    TextView tvEmptyList;
    private GoldenTicketViewModel goldenTicketResponse;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            goldenTicketResponse = (GoldenTicketViewModel) getArguments().getSerializable(TICKETS_DATA_APPROVED);
        }
    }

    @Override
    public void onViewCreated(@NotNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(goldenTicketResponse.getForApproval()!=null && goldenTicketResponse.getForApproval().size()>0)
            initRecycler();
        else
            tvEmptyList.setVisibility(View.VISIBLE);
    }

    @Override
    public void setUpView() {
    }

    @Override
    public int getLayout() {
        return R.layout.fragment_tickets_approved;
    }

    private void initRecycler() {
        LicenceApprovedAdapter approvedAdapter = new LicenceApprovedAdapter(getContext());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        rvApproved.setLayoutManager(mLayoutManager);
        rvApproved.setAdapter(approvedAdapter);
        approvedAdapter.addList((ArrayList<ForApprovalViewModel>) goldenTicketResponse.getForApproval());
    }
}
