package com.bcp.benefits.ui.campaign.volunteering.detail;


import android.app.Activity;
import android.content.Intent;
import android.widget.ImageView;
import android.widget.TextView;
import com.bcp.benefits.R;
import com.bcp.benefits.ui.BaseActivity;
import com.bcp.benefits.viewmodel.VolunteeringViewModel;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;
import butterknife.BindView;
import butterknife.OnClick;
import static com.bcp.benefits.util.Constants.ID_VOLUNTEERING_DETAIL;

public class DetailVolunteeringActivity extends BaseActivity implements VolunteeringDetailPresenter.VolunteeringDetailView {


    @BindView(R.id.imgVolunteeringDetail)
    ImageView imgVolunteering;
    @BindView(R.id.txt_title_volunteering_detail)
    TextView title;
    @BindView(R.id.txt_address_detail)
    TextView address;
    @BindView(R.id.txt_date_volunteering_detail)
    TextView dateString;
    @BindView(R.id.txt_capacity)
    TextView capacity;
    @BindView(R.id.txt_stock_detail)
    TextView stock;
    @BindView(R.id.txt_name_encargado)
    TextView txtNameEncargado;
    @BindView(R.id.txt_description_status)
    TextView txtDescriptionStatus;





    @Inject
    VolunteeringDetailPresenter presenter;


    public static void start(Activity context, int idVolunteering) {
        Intent starter = new Intent(context, DetailVolunteeringActivity.class);
        starter.putExtra(ID_VOLUNTEERING_DETAIL, idVolunteering);
        context.startActivity(starter);
    }


    @Override
    public void setUpView() {
        this.presenter.setView(this);
        int idVolunteering = getIntent().getIntExtra(ID_VOLUNTEERING_DETAIL, 0);
        presenter.getVolunteeringDetail(idVolunteering);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_detail_volunteering;
    }


    @Override
    public void showSuccessVolunteeringDetail(VolunteeringViewModel response) {
        title.setText(response.getNameVolunteer());
        if (response.getLocal() != null){
            address.setText(response.getLocal());
        }else {
            address.setText(response.getAddress());
        }
        dateString.setText(response.getDate());
        capacity.setText(getResources().getString(R.string.txt_con_capacity, String.valueOf(response.getCapacity())));
        stock.setText(response.getTime()+" - "+response.getNameVolunteerTime());
        if (response.getRole() != null){
            txtNameEncargado.setText(response.getRole());
        }else {
            txtNameEncargado.setText(response.getNameCollaboratorResponsable());
        }
        txtDescriptionStatus.setText(response.getMessage());
      if (!response.getImageLocal().isEmpty()){
            Picasso.get().load(response.getImageLocal()).placeholder(R.drawable.ic_place_campam).into(imgVolunteering);
        }

    }

    @OnClick(R.id.imgCloseDetailVolunteering)
    public void onClickCloseDetail(){
        onBackPressed();
    }
}
