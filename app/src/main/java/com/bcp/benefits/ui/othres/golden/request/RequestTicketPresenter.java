package com.bcp.benefits.ui.othres.golden.request;

import androidx.annotation.NonNull;
import pe.solera.benefit.main.entity.Boss;
import pe.solera.benefit.main.usecases.executor.Action;
import pe.solera.benefit.main.usecases.usecases.TicketUseCase;

import java.util.List;

import javax.inject.Inject;

import com.bcp.benefits.ui.MainView;
import com.bcp.benefits.viewmodel.BossViewModel;

/**
 * Created by emontesinos on 06/05/19.
 **/

public class RequestTicketPresenter {

    private RequestTicketView view;

    private final TicketUseCase ticketUseCase;

    @Inject
    public RequestTicketPresenter(TicketUseCase ticketUseCase) {
        this.ticketUseCase = ticketUseCase;

    }

    public void setView(@NonNull RequestTicketView view) {
        this.view = view;
    }

    void doGetBosses(String searchedName) {
        if (this.view.validateInternet()) {
            //this.view.showProgress();
            ticketUseCase.getBosses(searchedName, new Action.Callback<List<Boss>>() {
                @Override
                public void onSuccess(List<Boss> bosses) {
                    view.showRefreshBossesList(BossViewModel.toBossList(bosses));
                    view.hideProgress();

                }

                @Override
                public void onError(Throwable throwable) {
                    view.showErrorMessage(throwable.getMessage());
                    view.hideProgress();

                }
            });
        }

    }

    void doRequest(int idTicket, Integer idDurationDetail, String usageDate, Integer idUserDepartmentHead) {
        if (this.view.validateInternet()) {
            this.view.showProgress();
            ticketUseCase.requestTicket(idTicket, idDurationDetail, usageDate, idUserDepartmentHead, new Action.Callback<String>() {
                @Override
                public void onSuccess(String s) {
                    view.successRequest(s);
                    view.hideProgress();
                }

                @Override
                public void onError(Throwable throwable) {
                    view.showErrorMessage(throwable.getMessage());
                    view.hideProgress();

                }
            });
        }

    }



    public interface RequestTicketView extends MainView {
        void successRequest(String message);
        void showRefreshBossesList(List<BossViewModel> viewModels);
    }
}
