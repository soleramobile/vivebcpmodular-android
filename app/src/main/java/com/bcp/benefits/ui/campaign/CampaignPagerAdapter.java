package com.bcp.benefits.ui.campaign;


import android.app.Activity;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.PagerAdapter;

import com.bcp.benefits.R;
import com.bcp.benefits.analytics.AnalyticsModule;
import com.bcp.benefits.analytics.AnalyticsTags;
import com.bcp.benefits.ui.campaign.aguinaldo.AguinaldoDetailActivity;
import com.bcp.benefits.ui.campaign.campaigndetail.CampaignDetailActivity;
import com.bcp.benefits.ui.campaign.dynamic.DynamicCampaignActivity;
import com.bcp.benefits.ui.campaign.productDetail.DialogProductDetail;
import com.bcp.benefits.ui.campaign.volunteering.VolunteeringActivity;
import com.bcp.benefits.ui.campaign.volunteering.detail.DetailVolunteeringActivity;
import com.bcp.benefits.ui.discounts.DiscountsActivity;
import com.bcp.benefits.ui.discounts.detail.DiscountDetailActivity;
import com.bcp.benefits.util.AppUtils;
import com.bcp.benefits.viewmodel.CampaignProductViewModel;
import com.bcp.benefits.viewmodel.CampaignViewModel;
import com.bcp.benefits.viewmodel.DiscountEnterType;
import com.bcp.benefits.viewmodel.DiscountTypeViewModel;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.bcp.benefits.util.Constants.DATE_START;


/**
 * Created by emontesinos on 13/05/19.
 **/
public class CampaignPagerAdapter extends PagerAdapter {


    @BindView(R.id.img_campaign)
    ImageView imgCampaign;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvSubTitle)
    TextView tvSubtitle;
    @BindView(R.id.txt_description_campaign)
    TextView tvCaption;
    @BindView(R.id.img_used)
    ImageView imgUsed;
    @BindView(R.id.img_used_two)
    ImageView imgUsedTwo;
    @BindView(R.id.btn_accept)
    TextView btnAcceptOne;
    @BindView(R.id.btn_accept_two)
    TextView btnAcceptTwo;
    @BindView(R.id.tvMessageVolunteer)
    TextView tvMessageVolunteer;

    private ArrayList<CampaignViewModel> campaignModels;
    private LayoutInflater inflater;
    private Activity context;
    private CampaignViewModel campaignModel;
    private OnClickListener listener;
    private long mLastClickTime = 0;
    private AnalyticsModule analyticsModule = new AnalyticsModule(context);


    CampaignPagerAdapter(Activity context, ArrayList<CampaignViewModel> campaignModels, OnClickListener listener) {
        this.context = context;
        this.campaignModels = campaignModels;
        this.listener = listener;
        inflater = LayoutInflater.from(context);
    }

    @NotNull
    @Override
    public Object instantiateItem(@NotNull ViewGroup view, int position) {
        View myImageLayout = inflater.inflate(R.layout.row_campaign, view, false);
        ButterKnife.bind(this, myImageLayout);
        campaignModel = campaignModels.get(position);

        if (campaignModel.getListInfo() != null) {

            if (campaignModel.getListInfo().getCaption() != null && !campaignModel.getListInfo().getCaption().isEmpty())
                tvCaption.setText(campaignModel.getListInfo().getCaption());
            else
                tvCaption.setVisibility(View.GONE);

            if (campaignModel.getListInfo().getImage() != null && !campaignModel.getListInfo().getImage().isEmpty()) {
                Picasso.get().load(campaignModel.getListInfo().getImage()).placeholder(R.drawable.ic_place_campam).into(imgCampaign);
            } else {
                imgCampaign.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_place_campam));
            }

            if (campaignModel.getMessageVolunteer() != null && !campaignModel.getMessageVolunteer().equals("")){
                tvMessageVolunteer.setVisibility(View.VISIBLE);
                tvMessageVolunteer.setText(campaignModel.getMessageVolunteer());
            }

            tvTitle.setText(campaignModel.getListInfo().getTitle());
            tvSubtitle.setText(campaignModel.getListInfo().getSubTitle());
            btnAcceptOne.setText(campaignModel.getListInfo().getButtonTextInactive());


            typeCampaign(campaignModels.get(position));
            btnAcceptOne.setOnClickListener(onClickCampaign(campaignModels.get(position)));
            btnAcceptTwo.setOnClickListener(OnClickListenerButtonTwo(campaignModels.get(position)));
        }
        view.addView(myImageLayout, 0);
        return myImageLayout;
    }

    @Override
    public void destroyItem(@NotNull ViewGroup container, int position, @NotNull Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return campaignModels.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }
    private View.OnClickListener onClickCampaign(CampaignViewModel campaignModel) {
        return (view) -> {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                return;
            }

            sendToAnalytics(campaignModel);

            switch (campaignModel.getIdCampaignType().getIdType()) {
                case 1:
                    if (campaignModel.getButtons().get(0).isActive()) {
                        for (int h = 0; h < campaignModel.getProducts().size(); h++) {
                            int id1 = campaignModel.getProducts().get(h).getIdCampaignProduct();
                            int id2 = campaignModel.getUsageDetail().getProducts().get(0);
                            if (id1 == id2) {
                                DialogProductDetail.newInstance(campaignModel.getProducts().get(h))
                                        .show(((AppCompatActivity) context).getSupportFragmentManager(),
                                                DATE_START);
                            }
                        }
                    } else {
                        if (AppUtils.isConnected(context)) {
                            if (campaignModel.getButtons().size() == 1) {
                                AguinaldoDetailActivity.start(context, campaignModel.getDetailInfo(),
                                        (ArrayList<CampaignProductViewModel>) campaignModel.getProducts(),
                                        campaignModel.getDetailLocation(), campaignModel.getIdCampaign(),
                                        campaignModel.getUsageDetail(), false, false,
                                        true, campaignModel.getButtons().get(0).isActive(),
                                        campaignModel.getButtons().get(1).isActive());
                            } else {
                                AguinaldoDetailActivity.start(context, campaignModel.getDetailInfo(),
                                        (ArrayList<CampaignProductViewModel>) campaignModel.getProducts(),
                                        campaignModel.getDetailLocation(), campaignModel.getIdCampaign(),
                                        campaignModel.getUsageDetail(), campaignModel.getListInfo().isUsage(),
                                        campaignModel.isShowLocations(), campaignModel.isSelectProducts(),
                                        campaignModel.getButtons().get(0).isActive(),
                                        campaignModel.getButtons().get(1).isActive());
                            }
                        } else {
                            AppUtils.dialogIsConnect(context);
                        }

                    }
                    break;

                case 2:
                    CampaignDetailActivity.start(context, campaignModel.getDetailInfo());
                    break;
                case 3:

                    if (campaignModel.getIdDiscountType() == 0)
                        return;
                    DiscountTypeViewModel discountType;
                    if (campaignModel.getIdDiscountType() == 1)
                        discountType = DiscountTypeViewModel.DISCOUNT;
                    else
                        discountType = DiscountTypeViewModel.EDUTATION;

                    if (AppUtils.isConnected(context)) {
                        if (campaignModel.getIdDiscount() != null) {
                            DiscountDetailActivity.start(context, discountType, campaignModel.getIdDiscount(), campaignModel.getIdSubsidiary(), DiscountEnterType.CAMPANA);
                        } else if (campaignModel.getIdDiscountCategory() != null) {
                            DiscountsActivity.startWithFilters(context, discountType, campaignModel.getIdDiscountCategory(),
                                    campaignModel.getIdProvince(), campaignModel.getIdDistrict());
                        }
                    } else {
                        AppUtils.dialogIsConnect(context);
                    }


                    break;
                case 4:
                    if (AppUtils.isConnected(context)) {
                        DynamicCampaignActivity.start(context, campaignModel.getIdCampaign());
                    } else {
                        AppUtils.dialogIsConnect(context);
                    }

                    break;

                case 5:
                    if (AppUtils.isConnected(context)) {
                        if (!campaignModel.getButtons().get(0).isActive()) {
                            VolunteeringActivity.start(context, campaignModel.getIdCampaign(),campaignModel.getListInfo().getTitle(),campaignModel.getListInfo().getCaption(), campaignModel.getEnableRole(), campaignModel.getEnableLocal());
                        } else {
                            DetailVolunteeringActivity.start(context, campaignModel.getButtons().get(0).getIdVolunteer());
                        }

                    } else {
                        AppUtils.dialogIsConnect(context);
                    }
                    break;

            }

            mLastClickTime = SystemClock.elapsedRealtime();
        };
    }

    private void sendToAnalytics(CampaignViewModel campaignModel){
        Bundle bundle = new Bundle();
        bundle.putString(AnalyticsTags.Campaigns.Parameters.campana_id, campaignModel.getIdCampaign().toString());
        bundle.putString(AnalyticsTags.Campaigns.Parameters.campana_clase_id, String.valueOf(campaignModel.getIdCampaignType().getIdType()));
        bundle.putString(AnalyticsTags.Campaigns.Parameters.campana_clase, campaignModel.getIdCampaignType().getName());
        bundle.putString(AnalyticsTags.Campaigns.Parameters.campana_nombre, AppUtils.getFirstNCharacters(campaignModel.getListInfo().getTitle()));
        analyticsModule.sendToAnalytics(AnalyticsTags.Campaigns.Events.campana_detalle, bundle);
    }

    private View.OnClickListener OnClickListenerButtonTwo(CampaignViewModel campaign) {

        return (view) -> {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                return;
            }
            if (campaign.getIdCampaignType().getIdType() == 1) {
                if (campaign.getButtons().get(0).isActive()) {
                    AguinaldoDetailActivity.start(context, campaign.getDetailInfo(),
                            (ArrayList<CampaignProductViewModel>) campaign.getProducts(),
                            campaign.getDetailLocation(), campaign.getIdCampaign(),
                            campaign.getUsageDetail(), campaign.getListInfo().isUsage(),
                            campaign.isShowLocations(), campaign.isSelectProducts(),
                            campaign.getButtons().get(0).isActive(),
                            campaign.getButtons().get(1).isActive());
                } else {
                    if (campaign.getProducts().isEmpty()) {
                        listener.messageError();
                    } else {
                        listener.messageErrorEmpty();
                    }
                }

            }

            if (campaign.getIdCampaignType().getIdType() == 5) {
                if (AppUtils.isConnected(context)) {
                    if (!campaign.getButtons().get(1).isActive()) {
                        VolunteeringActivity.start(context, campaign.getIdCampaign(),campaign.getListInfo().getTitle(),campaign.getListInfo().getCaption(), campaignModel.enableRole, campaignModel.enableLocal);
                    } else {
                        DetailVolunteeringActivity.start(context, campaign.getButtons().get(1).getIdVolunteer());
                    }
                } else {
                    AppUtils.dialogIsConnect(context);
                }
            }

            mLastClickTime = SystemClock.elapsedRealtime();
        };
    }

    private void typeCampaign(CampaignViewModel campaignType) {

        switch (campaignType.getIdCampaignType().getIdType()) {
            case 1:
                if (!campaignModel.getButtons().isEmpty()) {
                    btnAcceptOne.setText(campaignModel.getButtons().get(0).getText());

                    if (campaignModel.getButtons().get(0).isActive()) {
                        imgUsed.setVisibility(View.VISIBLE);
                        btnAcceptOne.setBackgroundResource(R.drawable.sl_enter_campaign_button);
                    } else {
                        imgUsed.setVisibility(View.INVISIBLE);
                        btnAcceptOne.setBackgroundResource(R.drawable.sl_disable_campaign_button);
                    }
                    if (campaignModel.getButtons().size() == 2) {
                        btnAcceptTwo.setVisibility(View.VISIBLE);
                        btnAcceptTwo.setText(campaignModel.getButtons().get(1).getText());
                        if (campaignModel.getButtons().get(1).isActive()) {
                            imgUsedTwo.setVisibility(View.VISIBLE);
                            btnAcceptTwo.setBackgroundResource(R.drawable.sl_enter_campaign_button);
                        } else {
                            imgUsedTwo.setVisibility(View.INVISIBLE);
                            btnAcceptTwo.setBackgroundResource(R.drawable.sl_disable_campaign_button);
                        }
                        if (!campaignModel.isShowLocations()) {
                            btnAcceptTwo.setVisibility(View.GONE);
                            imgUsedTwo.setVisibility(View.INVISIBLE);
                        }
                    } else {
                        btnAcceptTwo.setVisibility(View.GONE);
                        imgUsedTwo.setVisibility(View.INVISIBLE);
                    }
                }

                if (campaignModel.getProducts().isEmpty()) {
                    btnAcceptOne.setVisibility(View.GONE);
                    imgUsed.setVisibility(View.INVISIBLE);
                }
                break;


            case 2:
            case 3:
            case 4:
                btnAcceptTwo.setVisibility(View.GONE);
                btnAcceptOne.setBackgroundResource(R.drawable.sl_disable_campaign_button);
                break;
            case 5:
                if (!campaignType.getButtons().isEmpty()) {
                    if (campaignType.getButtons().size()>1){
                        if (campaignType.getButtons().get(0).isActive()) {
                            btnAcceptOne.setText(campaignModel.getButtons().get(0).getText());
                            btnAcceptTwo.setText(campaignModel.getButtons().get(1).getText());
                            btnAcceptOne.setBackgroundResource(R.drawable.sl_enter_campaign_button);
                            btnAcceptTwo.setVisibility(View.VISIBLE);
                            imgUsed.setVisibility(View.VISIBLE);
                            if (campaignType.getButtons().get(1).isActive()) {
                                btnAcceptTwo.setBackgroundResource(R.drawable.sl_enter_campaign_button);
                                imgUsedTwo.setVisibility(View.VISIBLE);
                            } else {
                                btnAcceptTwo.setBackgroundResource(R.drawable.sl_disable_campaign_button);
                            }

                        } else {
                            btnAcceptTwo.setVisibility(View.GONE);
                            btnAcceptOne.setBackgroundResource(R.drawable.sl_disable_campaign_button);
                        }

                    }else {
                        btnAcceptOne.setText(campaignModel.getButtons().get(0).getText());
                        btnAcceptTwo.setVisibility(View.GONE);
                        imgUsedTwo.setVisibility(View.GONE);
                        if (campaignType.getButtons().get(0).isActive()) {
                            btnAcceptOne.setBackgroundResource(R.drawable.sl_enter_campaign_button);
                            btnAcceptOne.setVisibility(View.VISIBLE);
                            imgUsed.setVisibility(View.VISIBLE);
                        } else {
                            imgUsed.setVisibility(View.GONE);
                            btnAcceptTwo.setBackgroundResource(R.drawable.sl_disable_campaign_button);
                        }
                    }

                }else {
                    btnAcceptTwo.setVisibility(View.GONE);
                    btnAcceptOne.setVisibility(View.GONE);
                }
                break;


        }

    }

    interface OnClickListener {
        void messageError();

        void messageErrorEmpty();
    }

}
