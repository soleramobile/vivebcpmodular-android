package com.bcp.benefits.ui.othres.maternity;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.bcp.benefits.R;

/**
 * Created by emontesinos on 16/05/19.
 **/

public class MaternityAdapter extends RecyclerView.Adapter<MaternityAdapter.MaternityViewHolder> {

    private ArrayList<String> list;
    Context context;

    public MaternityAdapter(Context context, ArrayList<String> list) {
        this.list = list;
        this.context = context;
    }


    @NonNull
    @Override
    public MaternityViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_product_bonus, viewGroup, false);
        return new MaternityAdapter.MaternityViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MaternityViewHolder holder, int position) {
        String data = list.get(position);

       // holder.imgMaternity.setImageResource(R.drawable.);
        holder.txtDetailMoterbity.setOnClickListener(v -> MaternityDetailActivity.start(context));

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MaternityViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.img_product_bonus)
        ImageView imgMaternity;

        @BindView(R.id.txt_more_campaign)
        TextView txtDetailMoterbity;

        public MaternityViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
