package com.bcp.benefits.ui.campaign.aguinaldo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bcp.benefits.R;
import com.bcp.benefits.viewmodel.SubsidiaryViewModel;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchAdaper extends RecyclerView.Adapter<SearchAdaper.SearchViewHolder> {

    private ArrayList<SubsidiaryViewModel> list;
    private OnItemClickListener listener;

    private Context context;

    public SearchAdaper(Context context) {
        this.context = context;
        this.list = new ArrayList<>();
    }

    public void addList(ArrayList<SubsidiaryViewModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @NotNull
    @Override
    public SearchAdaper.SearchViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_location_search, parent, false);
        return new SearchViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NotNull SearchViewHolder holder, int position) {
        holder.bind(list.get(position));
    }


    @Override
    public int getItemCount() {
        return list != null ? list.size() : 0;
    }

    class SearchViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.edit_search_row)
        TextView adress;

        public SearchViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            adress.setOnClickListener(this);
            //itemView.setOnClickListener(this);
        }

        public void bind(SubsidiaryViewModel subsidiary) {
            adress.setText(subsidiary.getAgency());
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.edit_search_row:
                    listener.onItemClickSelect(list.get(getAdapterPosition()));
                    break;
            }
        }
    }


    public interface OnItemClickListener {
        void onItemClickSelect(SubsidiaryViewModel entity);
    }
}
