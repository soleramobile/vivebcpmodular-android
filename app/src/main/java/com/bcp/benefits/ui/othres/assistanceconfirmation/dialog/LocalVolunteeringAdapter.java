package com.bcp.benefits.ui.othres.assistanceconfirmation.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bcp.benefits.R;
import com.bcp.benefits.viewmodel.LocalVolunteeringViewModel;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by emontesinos on 13/01/20.
 */
public class LocalVolunteeringAdapter extends RecyclerView.Adapter<LocalVolunteeringAdapter.CampaignVolunteeringViewHolder> {

    private ArrayList<LocalVolunteeringViewModel> list = new ArrayList<>();
    private OnItemClickLocalListener listener;
    private int selectedItem = -1;
    private Context context;

     LocalVolunteeringAdapter(Context context) {
        this.context = context;
    }

     void addCategories(ArrayList<LocalVolunteeringViewModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public void setSelectedItem(LocalVolunteeringViewModel localVolunteeringViewModel) {
        if (localVolunteeringViewModel == null) return;
        int index = list.indexOf(localVolunteeringViewModel);
        if (selectedItem != index) {
            selectedItem = index;
            notifyDataSetChanged();
        }
    }

    @NotNull
    @Override
    public LocalVolunteeringAdapter.CampaignVolunteeringViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_type_custom, parent, false);
        return new CampaignVolunteeringViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NotNull CampaignVolunteeringViewHolder holder, int position) {
        holder.text.setText( list.get(position).getName());
        if (selectedItem == position) {
            holder.imgCircle.setVisibility(View.VISIBLE);
            holder.imgCircle.setBackground(context.getResources().getDrawable(R.drawable.bg_cicle_green));
        } else {
            holder.imgCircle.setVisibility(View.INVISIBLE);
        }
    }

    public void setListener(OnItemClickLocalListener listener) {
        this.listener = listener;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class CampaignVolunteeringViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        @BindView(R.id.txt_type_texto)
        TextView text;
        @BindView(R.id.img_circle)
        ImageView imgCircle;


         CampaignVolunteeringViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (listener == null)
                return;

            if (selectedItem != -1)
                notifyItemChanged(selectedItem);


            if (selectedItem == getAdapterPosition()) {
                selectedItem = -1;
                notifyItemChanged(getAdapterPosition());
                if (listener != null)
                    listener.onItemLocalClick(null, getAdapterPosition());
            } else {
                selectedItem = getAdapterPosition();
                notifyItemChanged(selectedItem);
                listener.onItemLocalClick(list.get(getAdapterPosition()), getAdapterPosition());
            }

        }
    }

    public interface OnItemClickLocalListener {
        void onItemLocalClick(LocalVolunteeringViewModel entity, int position);
    }
}
