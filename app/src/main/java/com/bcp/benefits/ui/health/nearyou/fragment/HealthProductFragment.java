package com.bcp.benefits.ui.health.nearyou.fragment;


import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.flexbox.FlexWrap;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.bcp.benefits.R;
import com.bcp.benefits.ui.BaseFragment;
import com.bcp.benefits.ui.health.nearyou.NearYouActivity;
import com.bcp.benefits.ui.health.nearyou.adapter.HealthProductsAdapter;
import com.bcp.benefits.viewmodel.HealthProductViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;


public class HealthProductFragment extends BaseFragment implements HealthProductsAdapter.OnSelectProductListener {

    public static final String DATA_PRODUCTS = "DATA_PRODUCTS";
    public static final String PRODUCT_SELECTED = "PRODUCT_SELECTED";

    @BindView(R.id.text_title_filter)
    TextView titleFilter;
    @BindView(R.id.rv_list_filter_product)
    RecyclerView rvProducts;
    @BindView(R.id.text_btn_filter)
    TextView btnFilter;
    @BindView(R.id.content_filter)
    View contentFilter;

    ArrayList<HealthProductViewModel> productModelList;
    private HealthProductsAdapter healthProductAdapter;
    private HealthProductViewModel selectedProduct;
    private int position = 0;

    public static HealthProductFragment newInstance(String healthProductModelList,
                                                    int positionSelected) {
        HealthProductFragment fragment = new HealthProductFragment();
        Bundle args = new Bundle();
        args.putString(DATA_PRODUCTS, healthProductModelList);
        args.putInt(PRODUCT_SELECTED, positionSelected);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void setUpView() {
        if (getArguments() == null) return;
        loadData();
        titleFilter.setText(getResources().getString(R.string.title_filter_health));
        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(getContext());
        healthProductAdapter = new HealthProductsAdapter(getContext(), this);
        layoutManager.setFlexWrap(FlexWrap.WRAP);
        rvProducts.setLayoutManager(layoutManager);
        rvProducts.setAdapter(healthProductAdapter);
        healthProductAdapter.addList(productModelList);
        selectedProduct = productModelList.get(position);
        healthProductAdapter.setSelectedItem(selectedProduct);
        contentFilter.setOnClickListener(view -> ((NearYouActivity) getActivity()).selectedFilter(selectedProduct, position));
    }

    private void loadData() {
        Gson gson = new Gson();
        productModelList = gson.fromJson(getArguments().getString(DATA_PRODUCTS),
                new TypeToken<List<HealthProductViewModel>>() {
                }.getType());
        this.position = getArguments().getInt(PRODUCT_SELECTED, 0);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public int getLayout() {
        return R.layout.fragment_health_product;
    }


    @Override
    public void selectProduct(HealthProductViewModel model, int position) {
        selectedProduct = model;
        this.position = position;
    }

    @OnClick(R.id.text_btn_filter)
    void onClickFilter() {
        ((NearYouActivity) getActivity()).selectedFilter(selectedProduct, position);
    }
}
