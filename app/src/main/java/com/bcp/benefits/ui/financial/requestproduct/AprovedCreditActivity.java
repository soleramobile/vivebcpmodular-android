package com.bcp.benefits.ui.financial.requestproduct;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bcp.benefits.R;
import com.bcp.benefits.components.PrefixEditText;
import com.bcp.benefits.ui.BaseActivity;
import com.bcp.benefits.ui.financial.ConditionAdapter;
import com.bcp.benefits.ui.financial.ConditionAdapter.OnItemClickListenerCondition;
import com.bcp.benefits.ui.financial.DialogFinanceGeneric;
import com.bcp.benefits.ui.financial.PeriodAdapter;
import com.bcp.benefits.util.AppUtils;
import com.bcp.benefits.viewmodel.CalculateViewModel;
import com.bcp.benefits.viewmodel.ConditionViewModel;
import com.bcp.benefits.viewmodel.PeriodViewModel;
import com.bcp.benefits.viewmodel.ProductFinancialViewModel;

import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

import static com.bcp.benefits.util.Constants.DATE_START;

/**
 * Created by emontesinos on 21/05/19.
 **/

public class AprovedCreditActivity extends BaseActivity implements
        OnItemClickListenerCondition, PeriodAdapter.OnItemClickListenerPeriod,
        AprovedcreditPresenter.AprovedcreditView {
    @Inject
    AprovedcreditPresenter presenter;

    @BindView(R.id.text_title_type_credit)
    TextView ttitleTypeCredit;
    @BindView(R.id.text_list_credit)
    TextView tvCreditType;
    @BindView(R.id.et_amount_required)
    PrefixEditText etAmountRequired;
    @BindView(R.id.et_credit_amount)
    PrefixEditText etCreditAmount;
    @BindView(R.id.txt_title_credit)
    TextView tvTitle;
    @BindView(R.id.recycler_months)
    RecyclerView recyclerMonths;
    @BindView(R.id.et_interest)
    EditText etInterest;
    @BindView(R.id.recycler_conditions)
    RecyclerView recyclerConditions;
    @BindView(R.id.text_mesage_monthly_fee)
    TextView tvDisclaimer;
    @BindView(R.id.text_btn_request)
    TextView btnRequest;
    @BindView(R.id.text_monthly_fee)
    TextView tvAmountCalculate;
    @BindView(R.id.text_interest_rate)
    TextView titleInterest;
    @BindView(R.id.tv_credit_amount)
    TextView tvTitleCreditAmount;
    @BindView(R.id.content_amount_required)
    LinearLayout contentAmountRequired;
    @BindView(R.id.content_credit_amount)
    LinearLayout contentCreditAmount;
    @BindView(R.id.content_list_coditions)
    ConstraintLayout contentCoditions;
    @BindView(R.id.txt_subtitle_credit)
    TextView textSubtitle;

    @BindView(R.id.id_scroll)
    NestedScrollView idScroll;

    private PeriodAdapter adapter;
    private ConditionAdapter conditionAdapter;
    private ProductFinancialViewModel productFinancialViewModel;

    public static void start(Context context, ProductFinancialViewModel entity) {
        Intent starter = new Intent(context, AprovedCreditActivity.class);
        starter.putExtra("entity", entity);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        idScroll.setVisibility(View.VISIBLE);
    }

    @Override
    public void setUpView() {
        this.loadData();
        this.presenter.setView(this);
        getViewGone();
        setUpRecyclerView();
        setupData();
        if (AppUtils.isConnected(this)) {
            this.presenter.preSelected(productFinancialViewModel.getIdProduct(), getIdPeriodSelectedIfExist(),
                    getAmountRequired(), getCreditAmountIfExists(), false,
                    conditionAdapter.getList());
        } else {
            AppUtils.dialogIsConnect(this);
        }

        etAmountRequired.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                AppUtils.hideKeyboard(AprovedCreditActivity.this);
                calculateFinances();
                return true;
            }
            return false;
        });

        AppUtils.updateTextWithFormat(etCreditAmount);
        AppUtils.updateTextWithFormat(etAmountRequired);
    }

    private void setUpRecyclerView() {
        this.adapter = new PeriodAdapter();
        this.adapter.setListener(this);
        DividerItemDecoration itemDecoration = new DividerItemDecoration(this,
                DividerItemDecoration.HORIZONTAL);
        itemDecoration.setDrawable(Objects.requireNonNull(ActivityCompat.getDrawable(this,
                R.drawable.divider_months)));
        recyclerMonths.addItemDecoration(itemDecoration);
        recyclerMonths.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL,
                false));
        this.recyclerMonths.setAdapter(adapter);

        this.conditionAdapter = new ConditionAdapter();
        this.conditionAdapter.setListener(this);
        DividerItemDecoration itemDecoration2 = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        itemDecoration2.setDrawable(Objects.requireNonNull(ActivityCompat.getDrawable(this, R.drawable.divider_months)));
        recyclerConditions.addItemDecoration(itemDecoration2);
        recyclerConditions.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        recyclerConditions.setAdapter(conditionAdapter);

    }

    private void loadData() {
        this.productFinancialViewModel = (ProductFinancialViewModel)
                getIntent().getSerializableExtra("entity");
    }

    private void setupData() {
        this.textSubtitle.setText(getResources().getString(R.string.finance_text_title, productFinancialViewModel.getName()));
        String valueAmount = AppUtils.formatPriceWithoutDecimal(
                Double.parseDouble(productFinancialViewModel.getApprovedAmount() == null
                        ? getString(R.string.amount_default).replace(",", "")
                        : productFinancialViewModel.getApprovedAmount()));

        this.etAmountRequired.setText(valueAmount);
        this.etAmountRequired.setSelection(valueAmount.length());
        this.adapter.setListPeriod(productFinancialViewModel.getPeriodOptions(),
                productFinancialViewModel.getPeriodUnit());
        this.etInterest.setText(getString(R.string.value_interest,
                productFinancialViewModel.getInterest()));
        this.conditionAdapter.setList(productFinancialViewModel.getConditions());
    }

    public void getViewGone() {
        ttitleTypeCredit.setVisibility(View.GONE);
        tvCreditType.setVisibility(View.GONE);
        contentCreditAmount.setVisibility(productFinancialViewModel.isShowAmountField()
                ? View.VISIBLE : View.GONE);
        tvTitleCreditAmount.setVisibility(productFinancialViewModel.isShowAmountField()
                ? View.VISIBLE : View.GONE);
    }

    private void calculateFinances() {
        this.presenter.calculate(productFinancialViewModel.getIdProduct(),
                adapter.getPeriodSelected().getIdPeriod(),
                getAmountRequired(), getCreditAmountIfExists(), false,
                conditionAdapter.getList());
    }

    private Integer getIdPeriodSelectedIfExist() {
        if (recyclerMonths.getVisibility() == View.GONE)
            return null;
        return adapter.getPeriodSelected().getIdPeriod();
    }

    private Double getAmountRequired() {
        if (etAmountRequired.getVisibility() == View.GONE)
            return 0d;
        if (TextUtils.isEmpty(etAmountRequired.getText().toString()
                .replace(",", "")
        )) {
            return 0d;
        }

        String number = etAmountRequired.getText().toString()
                .replace(",", "")
                .trim();
        return Double.parseDouble(number.isEmpty() ? "0" : number);
    }

    private Double getCreditAmountIfExists() {
        if (etCreditAmount.getVisibility() == View.GONE)
            return null;
        if (TextUtils.isEmpty(etCreditAmount.getText())) {
            etCreditAmount.setText(getString(R.string.amount_default));
        }
        String number = etCreditAmount.getText().toString()
                .replace(",", "")
                .trim();
        return Double.parseDouble(number.isEmpty() ? "0" : number);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_new_credit;
    }

    @Override
    public void onItemClickCondition(ConditionViewModel entity, int position) {
        calculateFinances();
    }

    @Override
    public void onItemClickPeriod(PeriodViewModel entity, int previousPosition, int newPosition) {
        adapter.setPositionSelected(newPosition);
        calculateFinances();
    }

    @Override
    public void calculateProduct(CalculateViewModel calculateModel) {
        tvAmountCalculate.setText(AppUtils.formatPrice(calculateModel.getAmount()));
        tvDisclaimer.setText(calculateModel.getTcea());
        etInterest.setText(getString(R.string.value_interest, calculateModel.getInterest()));
    }

    @OnClick(R.id.ivClose)
    public void closeClick() {
        onBackPressed();
    }

    @OnClick(R.id.text_btn_request)
    void onClickRequest() {
        if (validateData() && AppUtils.isConnected(this)) {
            presenter.request(productFinancialViewModel.getIdProduct(), getIdPeriodSelectedIfExist(),
                    getAmountRequired(), getCreditAmountIfExists(), false,
                    conditionAdapter.getList());
        }

    }

    private boolean validateData() {
        if (etAmountRequired.getText().toString()
                .replace(",", "")
                .trim()
                .isEmpty()) {
            Toast.makeText(this, R.string.financial_amount_required, Toast.LENGTH_SHORT).show();
            return false;
        }
        String number = etAmountRequired.getText().toString()
                .replace(",", "")
                .trim();
        double amount = Double.parseDouble(number.isEmpty() ? "0" : number);
        if (amount < 5500) {
            Toast.makeText(this, R.string.financial_atleast_5500, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    @Override
    public void showMessageSuccess() {
        DialogFinanceGeneric dialogFinanceGeneric = DialogFinanceGeneric.newInstance();
        dialogFinanceGeneric.show(getSupportFragmentManager(), DATE_START);
    }


    @OnClick(R.id.content_amount_required)
    void onClickVehicular() {
        AppUtils.showSoftKeyboard(etAmountRequired, this);
        etAmountRequired.setCursorVisible(true);
    }

    @OnClick(R.id.content_credit_amount)
    void onClickCreditAmount() {
        AppUtils.showSoftKeyboard(etCreditAmount, this);
        etCreditAmount.setCursorVisible(true);
    }
}
