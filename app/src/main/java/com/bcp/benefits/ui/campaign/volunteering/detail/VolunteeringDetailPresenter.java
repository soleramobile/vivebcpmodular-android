package com.bcp.benefits.ui.campaign.volunteering.detail;

import androidx.annotation.NonNull;

import com.bcp.benefits.ui.MainView;
import com.bcp.benefits.viewmodel.VolunteeringViewModel;
import javax.inject.Inject;

import pe.solera.benefit.main.entity.Volunteering;
import pe.solera.benefit.main.usecases.executor.Action;
import pe.solera.benefit.main.usecases.usecases.CampaignUseCase;

/**
 * Created by emontesinos on 13/05/19.
 **/

public class VolunteeringDetailPresenter {

    private VolunteeringDetailView view;

    private final CampaignUseCase campaignUseCase;

    @Inject
    public VolunteeringDetailPresenter(CampaignUseCase campaignUseCase) {
        this.campaignUseCase = campaignUseCase;
    }

    public void setView(@NonNull VolunteeringDetailView view) {
        this.view = view;
    }


    void getVolunteeringDetail(int idVolunteering) {
        if (this.view.validateInternet()) {
            this.view.showProgress();
          campaignUseCase.getVolunteeringDetail(String.valueOf(idVolunteering), new Action.Callback<Volunteering>() {
              @Override
              public void onSuccess(Volunteering volunteering) {
                  view.showSuccessVolunteeringDetail(new VolunteeringViewModel(volunteering));
                  view.hideProgress();

              }

              @Override
              public void onError(Throwable throwable) {
                  view.hideProgress();
                  view.showErrorMessage(throwable.getMessage());
              }
          });
        }
    }


    public interface VolunteeringDetailView extends MainView {
        void showSuccessVolunteeringDetail(VolunteeringViewModel response);
    }

}
