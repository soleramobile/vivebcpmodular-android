package com.bcp.benefits.ui.othres.assistanceconfirmation;

import androidx.annotation.NonNull;

import com.bcp.benefits.R;
import com.bcp.benefits.ui.othres.OtherActivityView;
import com.bcp.benefits.ui.othres.assistanceconfirmation.confirmation.ConfirmationVolunteeringView;
import com.bcp.benefits.ui.othres.assistanceconfirmation.listcapaignvolunteeting.ListCampaignVolunteeringView;
import com.bcp.benefits.viewmodel.CampaignVolunteeringViewModel;
import com.bcp.benefits.viewmodel.CollaboratorViewModel;
import com.bcp.benefits.viewmodel.ListCollaboratorViewModel;
import com.bcp.benefits.viewmodel.ListLocalViewModel;
import com.bcp.benefits.viewmodel.ListTypeVolunteerViewModel;
import com.bcp.benefits.viewmodel.LocalVolunteeringViewModel;

import java.util.List;

import javax.inject.Inject;

import pe.solera.benefit.main.entity.CampaignVolunteering;
import pe.solera.benefit.main.entity.Collaborator;
import pe.solera.benefit.main.entity.ListLocal;
import pe.solera.benefit.main.entity.ListType;
import pe.solera.benefit.main.entity.LocalVolunteering;
import pe.solera.benefit.main.usecases.executor.Action;
import pe.solera.benefit.main.usecases.usecases.TicketVolunteeringUseCase;

/**
 * Created by emontesinos on 13/01/20.
 */

public class ConfirmationVolunteeringPresenter {

    private ConfirmationVolunteeringView view;
    private ListCampaignVolunteeringView viewList;
    private OtherActivityView viewOther;
    private final TicketVolunteeringUseCase ticketVolunteeringUseCase;

    @Inject
    public ConfirmationVolunteeringPresenter(TicketVolunteeringUseCase ticketVolunteeringUseCase) {
        this.ticketVolunteeringUseCase = ticketVolunteeringUseCase;

    }

    public void setView(@NonNull ConfirmationVolunteeringView view) {
        this.view = view;
    }

    public void setView(@NonNull ListCampaignVolunteeringView view) {
        this.viewList = view;
    }

    public void setView(@NonNull OtherActivityView view) {
        this.viewOther = view;
    }

    public void getSecondComboData(Integer idVolunteerType) {
        if (this.viewList.validateInternet()) {
            this.viewList.showColor(R.color.confirmation_end);
            //this.viewList.showProgress();
            ticketVolunteeringUseCase.getListCampaignVolunteering(idVolunteerType, new Action.Callback<List<CampaignVolunteering>>() {
                @Override
                public void onSuccess(List<CampaignVolunteering> campaignVolunteerings) {
                    viewList.showListCampaignVolunteering(CampaignVolunteeringViewModel.toCampaignList(campaignVolunteerings));
                    viewList.hideProgress();
                }

                @Override
                public void onError(Throwable throwable) {
                    viewList.showErrorMessage(throwable.getMessage());
                    viewList.hideProgress();
                }
            });

        }

    }

    public void getListTypeVolunteer() {
        if (this.viewList.validateInternet()) {
            ticketVolunteeringUseCase.getListTypeVolunteer(new Action.Callback<List<ListType>>() {
                @Override
                public void onSuccess(List<ListType> listTypes) {
                    viewList.showTypesVolunteer(ListTypeVolunteerViewModel.toListTypeVolunteer(listTypes));
                    viewList.hideProgress();
                }

                @Override
                public void onError(Throwable throwable) {
                    viewList.showErrorMessage(throwable.getMessage());
                    viewList.hideProgress();
                }
            });
        }
    }

    public void getThirdComboData(String idCampaignVolunteering) {
        if (this.viewList.validateInternet()) {
            ticketVolunteeringUseCase.getListLocalVolunteering(idCampaignVolunteering, new Action.Callback<List<LocalVolunteering>>() {
                @Override
                public void onSuccess(List<LocalVolunteering> localVolunteerings) {
                    viewList.showListLocalVolunteering(LocalVolunteeringViewModel.toCampaignListLocal(localVolunteerings));
                    viewList.hideProgress();
                }

                @Override
                public void onError(Throwable throwable) {
                    viewList.showErrorMessage(throwable.getMessage());
                    viewList.hideProgress();
                }
            });
        }
    }

    public void getFourthComboData(Integer idLocalVolunteer) {
        if (this.viewList.validateInternet()) {
            ticketVolunteeringUseCase.getListLocal(idLocalVolunteer, new Action.Callback<List<ListLocal>>() {
                @Override
                public void onSuccess(List<ListLocal> list) {
                    viewList.showList(ListLocalViewModel.toListLocal(list));
                    viewList.hideProgress();
                }

                @Override
                public void onError(Throwable throwable) {
                    viewList.showErrorMessage(throwable.getMessage());
                    viewList.hideProgress();
                }
            });
        }
    }

    public void getListCollaboratorsVolunteering(String idLocal, Integer idVolunteerType, String text, Boolean progress) {
        if (this.view.validateInternet()) {
            if (progress) {
                this.view.showColor(R.color.confirmation_end);
                this.view.showProgress();
            }

            ticketVolunteeringUseCase.getListCollaboratorVolunteering(idLocal, text, idVolunteerType, new Action.Callback<Collaborator>() {
                @Override
                public void onSuccess(Collaborator collaborators) {
                    view.showMessage(collaborators.getConfirmationMessage());
                    view.showListCollaboratorsVolunteering(ListCollaboratorViewModel.toListCollaborator(collaborators.getListCollaborator()));
                    view.hideProgress();
                }

                @Override
                public void onError(Throwable throwable) {
                    view.showErrorMessage(throwable.getMessage());
                    view.hideProgress();
                }
            });

        } else {
            view.showErrorServiceList();
        }
    }

    public void collaboratorsConfirmation(String idVolunteering, String selection, String commentary) {
        if (this.view.validateInternet()) {
            ticketVolunteeringUseCase.confirmationCollaborator(idVolunteering, selection, commentary, new Action.Callback<Boolean>() {
                @Override
                public void onSuccess(Boolean aBoolean) {
                    view.hideProgress();
                    view.showSuccessAccept(aBoolean);
                }

                @Override
                public void onError(Throwable throwable) {
                    view.hideProgress();
                    view.showErrorMessage(throwable.getMessage());
                }
            });

        } else {
            view.showErrorService();
        }
    }

    public void getStatusAssistance(String id, String closeAttendance) {
        if (this.view.validateInternet()) {
            ticketVolunteeringUseCase.getStatusAssistance(id, closeAttendance, new Action.Callback<Boolean>() {
                @Override
                public void onSuccess(Boolean aBoolean) {
                    view.showStatusAssistance(aBoolean);
                }

                @Override
                public void onError(Throwable throwable) {
                    view.showErrorMessage(throwable.getMessage());
                }
            });
        }
    }

    public void getFlag() {
        if (this.viewOther.validateInternet()) {
            this.viewOther.showColor(R.color.confirmation_end);
            this.viewOther.showProgress();
            ticketVolunteeringUseCase.getFlagVolunteering(new Action.Callback<Integer>() {
                @Override
                public void onSuccess(Integer integer) {
                    viewOther.showFlag(integer);
                    viewOther.hideProgress();
                }

                @Override
                public void onError(Throwable throwable) {

                    viewOther.showErrorMessage(throwable.getMessage());
                    viewOther.hideProgress();
                }
            });


        }
    }

}
