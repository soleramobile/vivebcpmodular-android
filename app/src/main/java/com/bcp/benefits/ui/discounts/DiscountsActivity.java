package com.bcp.benefits.ui.discounts;


import android.animation.Animator;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bcp.benefits.analytics.AnalyticsModule;
import com.bcp.benefits.analytics.AnalyticsTags;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;
import com.bcp.benefits.R;
import com.bcp.benefits.components.SearchViewEditText;
import com.bcp.benefits.ui.BaseActivity;
import com.bcp.benefits.ui.discounts.list.DiscountProductFragment;
import com.bcp.benefits.ui.discounts.list.DiscountsListFragment;
import com.bcp.benefits.ui.discounts.nearyoudiscounts.MapNearYouDiscountFragment;
import com.bcp.benefits.util.AppUtils;
import com.bcp.benefits.util.LocationController;
import com.bcp.benefits.viewmodel.DiscountCategoryViewModel;
import com.bcp.benefits.viewmodel.DiscountTypeFilter;
import com.bcp.benefits.viewmodel.DiscountTypeViewModel;
import com.bcp.benefits.viewmodel.StateViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import pe.solera.benefit.main.entity.SortType;

import static com.bcp.benefits.util.Constants.CATEGORY_ID;
import static com.bcp.benefits.util.Constants.DISCOUNT_TYPE_PARAM;
import static com.bcp.benefits.util.Constants.DISTRICT_ID;
import static com.bcp.benefits.util.Constants.FLAG_CAMPAIGNS;
import static com.bcp.benefits.util.Constants.PROVINCE_ID;
import static com.bcp.benefits.viewmodel.DiscountTypeViewModel.EDUTATION;

/**
 * Created by emontesinos on 23/05/19.
 **/

public class DiscountsActivity extends BaseActivity implements
        DiscountsPresenter.DiscountsView, TextWatcher, LocationController.LocationControllerListener {

    @Inject
    DiscountsPresenter presenter;

    @BindView(R.id.toolbar)
    AppBarLayout toolbar;
    @BindView(R.id.img_back)
    ImageButton imgBack;
    @BindView(R.id.title_toolbar)
    TextView titleToolbar;
    @BindView(R.id.tb_types)
    TabLayout tbTypes;
    @BindView(R.id.content_search)
    LinearLayout contentSearch;
    @BindView(R.id.img_search)
    ImageView imgSearch;
    @BindView(R.id.img_list_filter)
    ImageView imgListFilter;
    @BindView(R.id.et_search)
    SearchViewEditText textSearch;
    @BindView(R.id.img_filter_list)
    ImageButton imgFilterList;
    @BindView(R.id.img_close_search)
    ImageView imgCloseSearch;


    private Boolean selection = true;
    private DiscountTypeViewModel discountType;
    private DiscountTypeFilter discountTypeFilter;
    private int typeClear;

    private int categoryId = 0;
    private int provinceId = 0;
    private int districtId = 0;
    private LocationController locationController;
    private String searchText = "";
    private SortType sortType = SortType.VALUE;
    private int positionCategory = -1;
    private int positionProvince = -1;
    private AnalyticsModule analyticsModule = new AnalyticsModule(this);;

    private DiscountCategoryViewModel categoriesSelected;
    private StateViewModel provinceSelected;

    private ArrayList<StateViewModel> provincesFilters;
    private ArrayList<DiscountCategoryViewModel> discountCategoriesFilters;


    public static void start(Context context, DiscountTypeViewModel discountType) {
        Intent starter = new Intent(context, DiscountsActivity.class);
        starter.putExtra(DISCOUNT_TYPE_PARAM, discountType);
        context.startActivity(starter);
    }

    public static void startWithFilters(Context context, DiscountTypeViewModel discountType,
                                        Integer idDiscountCategory, Integer idProvince, Integer idDistrict) {
        Intent starter = new Intent(context, DiscountsActivity.class);
        starter.putExtra(CATEGORY_ID, idDiscountCategory);
        starter.putExtra(PROVINCE_ID, idProvince);
        starter.putExtra(DISTRICT_ID, idDistrict);
        starter.putExtra(DISCOUNT_TYPE_PARAM, discountType);
        starter.putExtra(FLAG_CAMPAIGNS, true);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        this.loadData();
        if (discountType == EDUTATION){
            analyticsModule.sendToAnalytics(AnalyticsTags.Education.Events.educacion, null);
            setTheme(R.style.AppThemeEducation);
        }else {
            analyticsModule.sendToAnalytics(AnalyticsTags.Discounts.Events.descuento, null);
            setTheme(R.style.AppThemeDiscount);
        }
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setUpView() {
        this.presenter.setView(this);
        textSearch.addTextChangedListener(this);
        textSearch.setOnClickListener(view -> {
            textSearch.setEnableEditText(!textSearch.isEnable());
            onClickFilterHide();
        });
        if (AppUtils.isConnected(this)) {
            presenter.doGetDiscountCategories(discountType.getIndex());
            presenter.doGetProvinces();
        } else {
            AppUtils.dialogIsConnect(this);
        }
        configTabs();
        getToolbar();
        changeSkinByDiscountType();

        textSearch.setOnEditorActionListener((v, actionId, keyEvent) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_DONE) {
                searchText = textSearch.getText();
                textSearch.setText(searchText);
                textSearch.setEnableEditText(!textSearch.isEnable());
                typeClear = 2;
                categoryId = -1;
                provinceId = -1;
                if (discountTypeFilter == DiscountTypeFilter.LISTFILTER) {
                    configFragmentList(categoryId, provinceId);
                    selection = !selection;
                    animTap();
                } else {
                    configFragmentMaps(categoryId, provinceId);
                    selection = !selection;
                }
                hideKeyboard();
                return true;
            }
            return false;
        });
    }
    private void loadData() {
        discountType = (DiscountTypeViewModel) getIntent().getSerializableExtra(DISCOUNT_TYPE_PARAM);
        categoryId = getIntent().getIntExtra(CATEGORY_ID, 0);
        provinceId = getIntent().getIntExtra(PROVINCE_ID, 0);
        districtId = getIntent().getIntExtra(DISTRICT_ID, 0);
    }

    private void configFragmentList(Integer idCategory, Integer idProvince) {
        discountTypeFilter = DiscountTypeFilter.LISTFILTER;
        this.categoryId = idCategory;
        this.provinceId = idProvince;
        DiscountsListFragment discountsListFragment = DiscountsListFragment.newInstance(discountType);
        discountsListFragment.setSortType(sortType);
        discountsListFragment.setDiscountType(discountType);
        discountsListFragment.setIdCategory(categoryId);
        discountsListFragment.setSearch((this.categoryId == -1 && this.provinceId == -1) ? textSearch.getText() : "");
        discountsListFragment.setIdState(provinceId);
        discountsListFragment.setIdDistrict(districtId);
        getFragmentSelect(discountsListFragment);
    }

    private void configFragmentListDownAbove(Integer idCategory, Integer idProvince) {
        this.categoryId = idCategory;
        this.provinceId = idProvince;
        DiscountsListFragment discountsListFragment = DiscountsListFragment.newInstance(discountType);
        discountsListFragment.setSortType(sortType);
        discountsListFragment.setDiscountType(discountType);
        discountsListFragment.setIdCategory(categoryId);
        discountsListFragment.setSearch((this.categoryId == -1 && this.provinceId == -1) ? textSearch.getText() : "");
        discountsListFragment.setIdState(provinceId);
        discountsListFragment.setIdDistrict(districtId);
        getFragmentSelectDownAbove(discountsListFragment);
    }

    private void configFragmentMapsDownAbove(Integer idCategory, Integer idProvince) {
        this.categoryId = idCategory;
        this.provinceId = idProvince;
        MapNearYouDiscountFragment mapNearFragment = MapNearYouDiscountFragment.newInstance(discountType);
        mapNearFragment.setSortType(sortType);
        mapNearFragment.setDiscountType(discountType);
        mapNearFragment.setIdCategory(categoryId);
        mapNearFragment.setSearch(searchText);
        mapNearFragment.setIdState(provinceId);
        mapNearFragment.setIdDistrict(districtId);
        getFragmentSelectDownAbove(mapNearFragment);
    }

    private void configFragmentMaps(Integer idCategory, Integer idProvince) {
        this.categoryId = idCategory;
        this.provinceId = idProvince;
        MapNearYouDiscountFragment mapNearFragment = MapNearYouDiscountFragment.newInstance(discountType);
        mapNearFragment.setSortType(sortType);
        mapNearFragment.setDiscountType(discountType);
        mapNearFragment.setIdCategory(categoryId);
        mapNearFragment.setSearch(textSearch.getText());
        mapNearFragment.setIdState(idProvince);
        mapNearFragment.setIdDistrict(provinceId);
        getFragmentSelect(mapNearFragment);
    }

    public void configDiscountList() {
        DiscountsListFragment discountsListFragment = DiscountsListFragment.newInstance(discountType);
        discountsListFragment.setSortType(sortType);
        discountsListFragment.setDiscountType(discountType);
        discountsListFragment.setIdCategory(categoryId);
        discountsListFragment.setSearch(textSearch.getText());
        discountsListFragment.setIdState(provinceId);
        discountsListFragment.setIdDistrict(districtId);
        getFragmentSelectAbodeDown(discountsListFragment);
    }

    public void configTabs() {
        LinearLayout tabLayout = (LinearLayout) ((ViewGroup) tbTypes.getChildAt(0)).getChildAt(0);
        TextView tabTextView = (TextView) tabLayout.getChildAt(1);
        tabTextView.setTextAppearance(this, R.style.FontBold);
        tbTypes.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                LinearLayout tabLayout = (LinearLayout) ((ViewGroup) tbTypes.getChildAt(0)).getChildAt(tab.getPosition());
                TextView tabTextView = (TextView) tabLayout.getChildAt(1);
                tabTextView.setTextAppearance(getApplicationContext(), R.style.FontBold);

                switch (tab.getPosition()) {
                    case 0:
                        sortType = SortType.VALUE;
                        AppUtils.hideKeyboard(DiscountsActivity.this);
                        break;
                    case 1:
                        sortType = SortType.PROVIDER;
                        AppUtils.hideKeyboard(DiscountsActivity.this);
                        break;
                    default:
                        sortType = SortType.DISTANCE;
                        AppUtils.hideKeyboard(DiscountsActivity.this);
                        break;
                }
                configFragmentList(categoryId, provinceId);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                LinearLayout tabLayout = (LinearLayout) ((ViewGroup) tbTypes.getChildAt(0)).getChildAt(tab.getPosition());
                TextView tabTextView = (TextView) tabLayout.getChildAt(1);
                tabTextView.setTextAppearance(getApplicationContext(), R.style.FontRegular);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                //do nothing
            }
        });

    }

    @Override
    public int getLayout() {
        return R.layout.activity_discounts;
    }

    /**
     * Update title for toolbar and add listener to back button
     */
    public void getToolbar() {
        titleToolbar.setText(R.string.my_discounts);
        toolbar.setBackground(getResources().getDrawable(R.drawable.bg_green_discount));
        imgBack.setOnClickListener(v -> {
            onBackPressed();
            overridePendingTransition(R.anim.rigth_in, R.anim.right_out);
        });
    }

    /**
     * this function validate if is education and change colors and icons for this discount type
     */
    public void changeSkinByDiscountType() {
        if (discountType == EDUTATION) {
            titleToolbar.setText(R.string.education);
            toolbar.setBackground(getResources().getDrawable(R.drawable.bg_yellow_discount));
            contentSearch.setBackgroundColor(getResources().getColor(R.color.bg_yellow_end_one));
            tbTypes.setBackgroundColor(getResources().getColor(R.color.bg_yellow_end_one));
            imgSearch.setImageDrawable(getResources().getDrawable(R.drawable.ic_lupa_orange));
            imgListFilter.setImageDrawable(getResources().getDrawable(R.drawable.ic_filtro_orange));
            imgCloseSearch.setImageDrawable(getResources().getDrawable(R.drawable.ic_clear_yellow));
        }
    }

    private void getFragmentSelect(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_fragments, fragment)
                .commit();

    }

    private void getFragmentSelectDownAbove(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.slide_out_up, R.anim.slide_out_down)
                .replace(R.id.content_fragments, fragment)
                .commit();

    }

    private void getFragmentSelectAbodeDown(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.slide_in_up, R.anim.slide_top_down)
                .replace(R.id.content_fragments, fragment)
                .commit();


    }

    public void onClickNearYou() {
        discountTypeFilter = DiscountTypeFilter.MAPSFILTER;
        configFragmentMapsDownAbove(categoryId, provinceId);
        imgFilterList.setVisibility(View.VISIBLE);
        tbTypes.setVisibility(View.GONE);
        imgFilterList.setEnabled(true);

    }

    @OnClick(R.id.img_filter_list)
    public void onClickFilterList() {
        if (discountTypeFilter == DiscountTypeFilter.MAPSFILTER) {
            tbTypes.setVisibility(View.VISIBLE);
            configDiscountList();
            imgFilterList.setVisibility(View.INVISIBLE);
            imgFilterList.setEnabled(false);
            discountTypeFilter = DiscountTypeFilter.LISTFILTER;
        }
    }

    @OnClick(R.id.img_list_filter)
    public void onClickViewFilter() {
        onClickFilterHide();
    }

    @OnClick(R.id.img_close_search)
    public void onCloseSearch() {
        textSearch.setText("");
        textSearch.setEnabled(true);
        if (typeClear == 1) {
            AppUtils.showSoftKeyboard(textSearch, this);
            categoriesSelected = null;
            provinceSelected = null;
            getFragmentSelect(DiscountProductFragment.newInstance(discountType, provincesFilters, discountCategoriesFilters, provinceSelected, categoriesSelected, positionProvince, positionCategory));
        } else {
            this.categoryId = 0;
            this.provinceId = 0;
            categoriesSelected = null;
            provinceSelected = null;
            if (discountTypeFilter == DiscountTypeFilter.LISTFILTER) {
                configFragmentList(categoryId, provinceId);
            } else {
                configFragmentMaps(categoryId, provinceId);
            }

        }
    }

    public void onClickFilterHide() {
        if (selection) {
            typeClear = 1;
            if (discountTypeFilter == DiscountTypeFilter.LISTFILTER) {
                animTap();
            }
            getFragmentSelectAbodeDown(DiscountProductFragment.newInstance(discountType, provincesFilters, discountCategoriesFilters, provinceSelected, categoriesSelected, positionProvince, positionCategory));
            textSearch.setEnableEditText(true);
            selection = !selection;
        } else {
            typeClear = 2;
            if (discountTypeFilter == DiscountTypeFilter.LISTFILTER) {

                if (categoryId == 0) {
                    searchText = textSearch.getText();
                }

                configFragmentListDownAbove(categoryId, provinceId);
                textSearch.setText(textSearch.getText());
                animTap();
                selection = !selection;
            } else {
                if (categoryId == 0) {
                    searchText = textSearch.getText();
                }
                configFragmentMapsDownAbove(categoryId, provinceId);
                selection = !selection;

            }
            textSearch.setEnableEditText(false);
        }
    }

    public void updateDataByFilters(DiscountCategoryViewModel category, StateViewModel province, int positionCategory, int positionProvince) {
        if (category != null) {
            searchText = "";
            this.positionCategory = positionCategory;
            textSearch.setText(category.getCategoryName());
            categoriesSelected = category;
            this.categoryId = category.getIdCategory();
        } else {
            categoriesSelected = null;
            this.positionCategory = -1;
            this.categoryId = -1;
            searchText = textSearch.getText();
        }
        if (province != null) {
            this.positionProvince = positionProvince;
            this.provinceId = province.getIdProvince();
            provinceSelected = province;
        } else {
            provinceSelected = null;
            this.positionProvince = -1;
            this.provinceId = -1;
        }
        typeClear = 2;

        if (discountTypeFilter == DiscountTypeFilter.LISTFILTER) {
            configFragmentListDownAbove(categoryId, provinceId);
            textSearch.setText(textSearch.getText());
            animTap();
            selection = !selection;
        } else {
            configFragmentMapsDownAbove(categoryId, provinceId);
            textSearch.setText(textSearch.getText());
            selection = !selection;

        }
        textSearch.setEnableEditText(false);
    }

    @Override
    public void beforeTextChanged(CharSequence string, int start, int count, int after) {
        //do nothing
    }

    @Override
    public void onTextChanged(CharSequence string, int start, int before, int count) {
        //do nothing
    }

    @Override
    public void afterTextChanged(Editable string) {
        if (string.toString().length() >= 1) {
            imgListFilter.setVisibility(View.GONE);
            imgCloseSearch.setVisibility(View.VISIBLE);

        } else {
            imgListFilter.setVisibility(View.VISIBLE);
            imgCloseSearch.setVisibility(View.GONE);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 450) {
            configFragmentList(categoryId, provinceId);
        }
    }

    private void hideKeyboard() {
        AppUtils.hideKeyboard(this);
    }

    @Override
    public void showProvinces(List<StateViewModel> provinces) {
        this.provincesFilters = (ArrayList<StateViewModel>) provinces;
    }

    @Override
    public void showDiscountCategories(List<DiscountCategoryViewModel> discountCategories) {
        this.discountCategoriesFilters = (ArrayList<DiscountCategoryViewModel>) discountCategories;
    }

    private void animTap() {
        tbTypes.animate()
                .alpha(1)
                .setDuration(500)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        //Do nothing
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        tbTypes.setVisibility(tbTypes.getVisibility()
                                == View.VISIBLE ? View.GONE : View.VISIBLE);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                        //Do nothing
                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {
                        //Do nothing
                    }
                })
                .start();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        locationController.onRequestPermissionsResult(requestCode, grantResults);
    }

    @Override
    protected void onResume() {
        super.onResume();
        locationController = new LocationController(this, 1000, this);
        locationController.connect();
    }

    @Override
    public void onLocationPermissionDenied() {
        //do nothing
    }

    @Override
    public void onGpsUnavailable() {
        //do nothing
    }

    @Override
    public void onGetLocationCompleted(Location location) {
        configFragmentList(categoryId, provinceId);
        //do nothing
    }

    @Override
    public void onLocationUpdate(Location location) {
        //do nothing
    }

    @Override
    public void onError(LocationController.LocationManagerError error) {
        //do nothing
    }
}
