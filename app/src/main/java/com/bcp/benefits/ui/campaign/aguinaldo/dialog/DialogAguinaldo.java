package com.bcp.benefits.ui.campaign.aguinaldo.dialog;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bcp.benefits.R;
import com.bcp.benefits.ui.campaign.aguinaldo.AguinaldoDetailActivity;
import com.bcp.benefits.util.AppUtils;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DialogAguinaldo extends DialogFragment {


    public static DialogAguinaldo newInstance() {
        DialogAguinaldo fragment = new DialogAguinaldo();
        return fragment;
    }

    @BindView(R.id.btn_register_generic)
    TextView btnAcept;
    @BindView(R.id.img_close_generic)
    ImageView imgClose;
    @BindView(R.id.text_title_generic)
    TextView title;
    @BindView(R.id.txt_negative)
    TextView btnNegative;
    @BindView(R.id.content_dialog)
    ConstraintLayout contentDialog;
    @BindView(R.id.linear_selection)
    LinearLayout linearSelection;
    @BindView(R.id.txt_dilog_message)
    TextView dilogMessage;
    @BindView(R.id.btn_register_generic_two)
    TextView btnTwo;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_custom_generic, container, false);
        ButterKnife.bind(this, v);

        dialogSelect();
        return v;
    }

    @NotNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        Objects.requireNonNull(dialog.getWindow()).requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            Drawable backgroundColor = new ColorDrawable(ContextCompat.getColor
                    (Objects.requireNonNull(getContext()), android.R.color.transparent));
            Objects.requireNonNull(dialog.getWindow()).setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            dialog.getWindow().setBackgroundDrawable(backgroundColor);
        }
    }

    @OnClick({R.id.img_close_generic, R.id.txt_negative})
    public void onDialogDismiss() {
        dismiss();
    }

    @OnClick(R.id.btn_register_generic)
    public void onRegister() {
        ((AguinaldoDetailActivity) Objects.requireNonNull(getActivity())).dialogSuccess();
        dismiss();
    }


    private void dialogSelect() {
        title.setText(R.string.message_success_aguinaldo);
        imgClose.setImageResource(R.drawable.ic_close_blue_sky_inside);
        contentDialog.setBackground(getResources().getDrawable(R.drawable.bg_type_custom_sky_blue));
        AppUtils.setGradient(getResources().getColor(R.color.bg_campaign_color), getResources().getColor(R.color.bg_campaign_color),
                AppUtils.dpToPx((int) getResources().getDimension(R.dimen.size_thirteen)), btnAcept);
        btnNegative.setBackground(getResources().getDrawable(R.drawable.bg_sky_blue_corner));

    }
}
