package com.bcp.benefits.ui.othres.volunteering.myTicketsVounteering;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bcp.benefits.R;
import com.bcp.benefits.ui.othres.golden.mytickets.CarouselLinearLayout;
import com.bcp.benefits.viewmodel.TicketVolunteeringViewModel;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Created by emontesinos on 02/05/19.
 **/

public class CarouselPagerVolunteeringAdapter extends FragmentPagerAdapter implements ViewPager.OnPageChangeListener {

    private final static float BIG_SCALE = 1.0f;
    private final static float SMALL_SCALE = 0.7f;
    private final static float DIFF_SCALE = BIG_SCALE - SMALL_SCALE;
    private final MyTicketsVoulunteeringFragment context;
    private final FragmentManager fragmentManager;
    private final List<TicketVolunteeringViewModel> ticketVolunteerings;

    CarouselPagerVolunteeringAdapter(MyTicketsVoulunteeringFragment context, FragmentManager fm, List<TicketVolunteeringViewModel> ticketVolunteerings) {
        super(fm);
        this.fragmentManager = fm;
        this.context = context;
        this.ticketVolunteerings = ticketVolunteerings;
    }

    @NotNull
    @Override
    public Fragment getItem(int position) {
        try {
            position = position % MyTicketsVoulunteeringFragment.count;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return TicketVolunteeringFragment.newInstance(context, ticketVolunteerings.get(position));

    }

    public int getItemPosition(@NotNull Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        int count = 0;
        try {
            count = MyTicketsVoulunteeringFragment.count * MyTicketsVoulunteeringFragment.LOOPS;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return count;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        try {
            if (positionOffset >= 0f && positionOffset <= 1f) {
                CarouselLinearLayout cur = getRootView(position);
                cur.setScaleBoth(BIG_SCALE - DIFF_SCALE * positionOffset);
                if ((position + 1) < this.ticketVolunteerings.size()) {
                    CarouselLinearLayout next = getRootView(position + 1);
                    next.setScaleBoth(SMALL_SCALE + DIFF_SCALE * positionOffset);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @SuppressWarnings("ConstantConditions")
    private CarouselLinearLayout getRootView(int position) {
        return (CarouselLinearLayout) fragmentManager.findFragmentByTag(
                this.getFragmentTag(position)).getView().findViewById(R.id.root_container);
    }

    private String getFragmentTag(int position) {
        return "android:switcher:" + context.pager.getId() + ":" + position;
    }
}
