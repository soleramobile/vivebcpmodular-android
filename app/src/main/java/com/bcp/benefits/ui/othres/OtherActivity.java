package com.bcp.benefits.ui.othres;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.widget.ImageButton;
import android.widget.TextView;
import java.util.ArrayList;
import butterknife.BindView;
import com.bcp.benefits.R;
import com.bcp.benefits.analytics.AnalyticsModule;
import com.bcp.benefits.analytics.AnalyticsTags;
import com.bcp.benefits.ui.BaseActivity;
import com.bcp.benefits.ui.othres.assistanceconfirmation.ConfirmationVolunteeringPresenter;
import com.bcp.benefits.ui.othres.assistanceconfirmation.listcapaignvolunteeting.ListCampaignActivity;
import com.bcp.benefits.ui.othres.buses.BusesActivity;
import com.bcp.benefits.ui.othres.golden.GoldenTicketActivity;
import com.bcp.benefits.ui.othres.licenses.LicencesActivity;
import com.bcp.benefits.ui.othres.volunteering.VolunteeringTicketActivity;
import com.bcp.benefits.util.AppUtils;
import com.bcp.benefits.viewmodel.OtherBenefitViewModel;
import javax.inject.Inject;
import static com.bcp.benefits.util.AppUtils.otherModelFromJson;
import static com.bcp.benefits.util.Constants.BUSES;
import static com.bcp.benefits.util.Constants.CONFIRMATION;
import static com.bcp.benefits.util.Constants.DATE_START;
import static com.bcp.benefits.util.Constants.GOLDEN;
import static com.bcp.benefits.util.Constants.IDENTIFIR_MOTHER_DIALOG;
import static com.bcp.benefits.util.Constants.LICENCE;
import static com.bcp.benefits.util.Constants.MOTHER;
import static com.bcp.benefits.util.Constants.OTHER_JSON;
import static com.bcp.benefits.util.Constants.OTHER_TWO_JSON;
import static com.bcp.benefits.util.Constants.VOLUNTEERING;
public class OtherActivity extends BaseActivity implements OtherAdapter.ListenerViewAdapter,OtherActivityView{

    @Inject
    ConfirmationVolunteeringPresenter presenter;

    @BindView(R.id.rcv_other_items)
    RecyclerView rcvOthersItems;
    @BindView(R.id.img_back)
    ImageButton imgBack;
    @BindView(R.id.title_toolbar)
    TextView titleToobal;

    private Boolean validateClick = false;
    private AnalyticsModule analyticsModule = new AnalyticsModule(this);

    public static void start(Context context) {
        Intent intent = new Intent(context, OtherActivity.class);
        context.startActivity(intent);
    }

    @Override
    public void setUpView() {
        analyticsModule.sendToAnalytics(AnalyticsTags.Others.Events.otros, null);
        this.presenter.setView(this);
        getToolbar();

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (AppUtils.isConnected(this)){
            //presenter.getFlag();
            fillRecyclerWithData(otherModelFromJson(this,OTHER_TWO_JSON));
        }else {
            fillRecyclerWithData(otherModelFromJson(this,OTHER_JSON));
        }
    }

    @Override
    public int getLayout() {
        return R.layout.activity_other;
    }

    public void getToolbar() {
        titleToobal.setText(R.string.others);
        imgBack.setOnClickListener(v -> {
            onBackPressed();
            overridePendingTransition(R.anim.rigth_in, R.anim.right_out);
        });
    }

    public void fillRecyclerWithData(ArrayList<OtherBenefitViewModel> listOthers) {
        rcvOthersItems.setLayoutManager(new LinearLayoutManager(this));
        RecyclerView.Adapter otherAdapter = new OtherAdapter(this);
        ((OtherAdapter) otherAdapter).setListener(this);
        ((OtherAdapter) otherAdapter).addList(listOthers);
        rcvOthersItems.setAdapter(otherAdapter);
    }

    @Override
    public void goView(OtherBenefitViewModel benefit) {
            if (!validateClick) {
                validateClick = true;
                switch (benefit.getId()) {
                    case GOLDEN:
                        GoldenTicketActivity.start(this);
                        overridePendingTransition(R.anim.left_in, R.anim.left_out);
                        break;
                    case LICENCE:
                        LicencesActivity.start(this);
                        overridePendingTransition(R.anim.left_in, R.anim.left_out);
                        break;
                    case MOTHER:
                        DialogCustomGeneric dp = DialogCustomGeneric.newInstance(IDENTIFIR_MOTHER_DIALOG);
                        dp.show(getSupportFragmentManager(), DATE_START);
                        break;
                    case BUSES:
                        BusesActivity.callNew(this);
                        overridePendingTransition(R.anim.left_in, R.anim.left_out);
                        break;
                    case VOLUNTEERING:
                        VolunteeringTicketActivity.start(this);
                        overridePendingTransition(R.anim.left_in, R.anim.left_out);
                        break;
                    case CONFIRMATION:
                        ListCampaignActivity.start(this);
                        overridePendingTransition(R.anim.left_in, R.anim.left_out);
                        break;
                }

        }else {
            showErrorConnect(getResources().getString(R.string.exception_no_internet_error));
        }




    }

    @Override
    protected void onRestart() {
        super.onRestart();
        validateClick = false;
    }


    @Override
    public void showFlag(int response) {
        if (response==1){
            fillRecyclerWithData(otherModelFromJson(this,OTHER_TWO_JSON));
        }else {
            fillRecyclerWithData(otherModelFromJson(this,OTHER_JSON));
        }

    }
}
