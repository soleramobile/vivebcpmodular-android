package com.bcp.benefits.ui.discounts.list;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.flexbox.FlexWrap;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.bcp.benefits.R;
import com.bcp.benefits.ui.BaseFragment;
import com.bcp.benefits.ui.discounts.DiscountsActivity;
import com.bcp.benefits.util.AppUtils;
import com.bcp.benefits.viewmodel.DiscountCategoryViewModel;
import com.bcp.benefits.viewmodel.DiscountTypeViewModel;
import com.bcp.benefits.viewmodel.StateViewModel;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.OnClick;

import static com.bcp.benefits.util.Constants.ID_TYPE;
import static com.bcp.benefits.viewmodel.DiscountTypeViewModel.EDUTATION;

/**
 * Created by emontesinos on 28/05/19.
 **/
public class DiscountProductFragment extends BaseFragment implements
        CategoryAdapter.OnSelectCategoryListener
        , DepartmentDialogFragment.OnSelectDepartmentListener {


    @BindView(R.id.text_title_filter)
    TextView titleFilter;
    @BindView(R.id.rv_list_filter_product)
    RecyclerView rvProducts;
    @BindView(R.id.text_btn_filter)
    TextView btnFilter;
    @BindView(R.id.content_filter_city)
    ConstraintLayout contentFilterCity;
    @BindView(R.id.text_select_department)
    TextView textSelectDepartment;
    @BindView(R.id.text_title_selection)
    TextView textTitleSelection;
    private ArrayList<StateViewModel> provincesFilters;
    private ArrayList<DiscountCategoryViewModel> discountCategoriesFilters;
    private DiscountTypeViewModel discountType;

    private DiscountCategoryViewModel categoriesSelected;
    private StateViewModel provinceSelected;


    private int positionCategory = -1;
    private int positionProvince = -1;

    public static DiscountProductFragment newInstance(DiscountTypeViewModel discountType, ArrayList<StateViewModel> provinces,
                                                      ArrayList<DiscountCategoryViewModel> discountCategories, StateViewModel province,
                                                      DiscountCategoryViewModel discountCategory,
                                                      int provinceSelected, int categorySelected) {
        DiscountProductFragment fragment = new DiscountProductFragment();
        Bundle args = new Bundle();
        args.putSerializable(ID_TYPE, discountType);
        args.putSerializable("provinces", provinces);
        args.putSerializable("discountCategories", discountCategories);
        args.putSerializable("discountCategoriesSelected", categorySelected);
        args.putSerializable("provinceSelected", provinceSelected);
        args.putSerializable("discountCategoriesSelectedModel", discountCategory);
        args.putSerializable("provinceSelectedModel", province);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void setUpView() {
        loadData();
        changeSkinByDiscountType();
        configRecyclerView();
    }

    private void loadData() {
        discountType = (DiscountTypeViewModel) getArguments().getSerializable(ID_TYPE);
        provincesFilters = (ArrayList<StateViewModel>) getArguments().getSerializable("provinces");
        discountCategoriesFilters = (ArrayList<DiscountCategoryViewModel>) getArguments().getSerializable("discountCategories");
        provinceSelected = (StateViewModel) getArguments().getSerializable("provinceSelectedModel");
        categoriesSelected = (DiscountCategoryViewModel) getArguments().getSerializable("discountCategoriesSelectedModel");
        positionCategory = getArguments().getInt("discountCategoriesSelected");
        positionProvince = getArguments().getInt("provinceSelected");
    }

    private void configRecyclerView() {
        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(getContext());
        CategoryAdapter categoryAdapter = new CategoryAdapter(getContext(), discountType, this);
        layoutManager.setFlexWrap(FlexWrap.WRAP);
        rvProducts.setLayoutManager(layoutManager);
        rvProducts.setAdapter(categoryAdapter);
        categoryAdapter.addCategories(discountCategoriesFilters);
        categoryAdapter.setSelectedItem(categoriesSelected);
    }

    @Override
    public int getLayout() {
        return R.layout.fragment_health_product;
    }

    @OnClick(R.id.text_select_department)
    void onClickSelectDepartment() {
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        Fragment dialogFragment = getActivity().getSupportFragmentManager().findFragmentByTag("dialog");
        if (dialogFragment != null) {
            fragmentTransaction.remove(dialogFragment);
        }
        fragmentTransaction.addToBackStack(null);
        DepartmentDialogFragment newFragment = DepartmentDialogFragment.newInstance(provinceSelected, provincesFilters, discountType, this);
        newFragment.show(fragmentTransaction, "dialog");
    }

    /**
     * if selected category is null, we set positionCategory with -1
     *
     * @param category selected category by user
     * @param position selected category position
     */
    @Override
    public void selectCategory(DiscountCategoryViewModel category, int position) {
        this.categoriesSelected = category;
        this.positionCategory = categoriesSelected == null ? -1 : position;
    }

    @OnClick(R.id.text_btn_filter)
    void clickFilter() {
        ((DiscountsActivity) Objects.requireNonNull(getActivity())).updateDataByFilters(
                categoriesSelected, provinceSelected, positionCategory, positionProvince);
    }

    private void changeSkinByDiscountType() {
        contentFilterCity.setVisibility(View.VISIBLE);
        titleFilter.setTextColor(getResources().getColor(discountType == EDUTATION
                ? R.color.orange_common : R.color.green_end));
        btnFilter.setBackground(getResources().getDrawable(discountType == EDUTATION
                ? R.drawable.bg_yellow_gradient_button : R.drawable.bg_button_campaign));
        textTitleSelection.setTextColor(getResources().getColor(discountType == EDUTATION
                ? R.color.orange_common : R.color.green_end));
        textSelectDepartment.setCompoundDrawablesWithIntrinsicBounds(0, 0,
                discountType == EDUTATION ? R.drawable.ico_dropdown_yellow
                        : R.drawable.ico_dropdown_green, 0);
        if (provinceSelected != null) {
            textSelectDepartment.setText(provinceSelected.getName());
            textSelectDepartment.setTextColor(getResources().getColor(R.color.white));
            textSelectDepartment.setBackground(getResources().getDrawable(
                    discountType == EDUTATION ? R.drawable.bg_yellow_gradient_button
                            : R.drawable.bg_button_campaign));
            textSelectDepartment.setCompoundDrawablesWithIntrinsicBounds(0, 0,
                    R.drawable.ico_dropdown_white, 0);
        } else
            configDesign();
    }

    private void configDesign() {
        textSelectDepartment.setBackground(getResources().getDrawable(
                discountType == EDUTATION ? R.drawable.bg_white_corners_yellow_stroke
                        : R.drawable.bg_corners_green_stroke));
    }

    @Override
    public void onSelectDepartment(StateViewModel provinces, int position) {
        if (provinces != null) {
            this.provinceSelected = provinces;
            this.positionProvince = position;
            textSelectDepartment.setText(provinces.getName());
            textSelectDepartment.setTextColor(getResources().getColor(R.color.white));
            textSelectDepartment.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ico_dropdown_white, 0);
            textSelectDepartment.setBackground(getResources().getDrawable(
                    discountType == EDUTATION ? R.drawable.bg_button_discount_yellow : R.drawable.bg_button_campaign));
        } else {
            this.provinceSelected = null;
            this.positionProvince = -1;
            textSelectDepartment.setText(getResources().getString(R.string.select_department));
            if (discountType == DiscountTypeViewModel.DISCOUNT) {
                textTitleSelection.setTextColor(getResources().getColor(R.color.green_end));
                textSelectDepartment.setBackground(getResources().getDrawable(R.drawable.bg_corners_green_stroke));
                textSelectDepartment.setTextColor(getResources().getColor(R.color.gray_800));
            } else if (discountType == EDUTATION) {
                textSelectDepartment.setTextColor(getResources().getColor(R.color.gray_800));
                textSelectDepartment.setBackground(getResources().getDrawable(R.drawable.bg_white_corners_yellow_stroke));
                textSelectDepartment.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ico_dropdown_yellow, 0);
            }
        }
    }

    @OnClick(R.id.content_filter)
    void onClickContent() {
        AppUtils.hideKeyboard(getActivity());
        ((DiscountsActivity) Objects.requireNonNull(getActivity())).onClickFilterHide();
    }
}
