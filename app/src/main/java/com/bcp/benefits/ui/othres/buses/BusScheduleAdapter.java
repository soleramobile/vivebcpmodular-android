package com.bcp.benefits.ui.othres.buses;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bcp.benefits.R;
import com.bcp.benefits.viewmodel.BusScheduleItemViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BusScheduleAdapter extends RecyclerView.Adapter<BusScheduleAdapter.BusScheduleViewHolder> {

    private List<BusScheduleItemViewModel> busScheduleList;
    private BusScheduleListener busScheduleListener;

    BusScheduleAdapter(BusScheduleListener busScheduleListener) {
        busScheduleList = new ArrayList<>();
        this.busScheduleListener = busScheduleListener;
    }

    void setBusScheduleList(List<BusScheduleItemViewModel> busScheduleList) {
        this.busScheduleList = busScheduleList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public BusScheduleViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_buses,
                viewGroup, false);
        return new BusScheduleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BusScheduleViewHolder holder, int position) {
        holder.bind(busScheduleList.get(position));
        holder.textType.setOnClickListener(v -> busScheduleListener.selectedItemSchedule(busScheduleList.get(position)));
    }

    @Override
    public int getItemCount() {
        return busScheduleList.size();
    }

    class BusScheduleViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_type)
        TextView textType;

        BusScheduleViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void bind(BusScheduleItemViewModel item) {
            textType.setTypeface(ResourcesCompat.getFont(itemView.getContext(), R.font.breviaregular));
            textType.setText(item.getArrival());
            textType.setBackgroundResource(item.isSelected() ? R.drawable.bg_buses_selected
                    : R.drawable.bg_buses_unselected);
            textType.setTextColor(itemView.getContext().getResources().getColor(item.isSelected()
                    ? R.color.white
                    : R.color.gray_800));
        }
    }

    public interface BusScheduleListener {
        void selectedItemSchedule(BusScheduleItemViewModel item);
    }
}
