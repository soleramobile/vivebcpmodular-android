package com.bcp.benefits.ui.campaign.dynamic.referredlist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bcp.benefits.R;
import com.bcp.benefits.viewmodel.DynamicFieldValueResponseViewModel;
import com.bcp.benefits.viewmodel.DynamicReferredViewModel;
import java.util.List;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;


public class ReferredAdapter extends RecyclerView.Adapter<ReferredAdapter.ReferredViewHolder> {

    private final Context context;
    private List<DynamicReferredViewModel> dynamicReferredList;

    public ReferredAdapter(Context context, List<DynamicReferredViewModel> dynamicReferredList) {
        this.dynamicReferredList = dynamicReferredList;
        this.context = context;
    }

    @Override
    public ReferredViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_referred_list_item_list, parent, false);
        return new ReferredViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ReferredViewHolder holder, int position) {
        DynamicReferredViewModel dynamicReferred = this.dynamicReferredList.get(position);
        for (DynamicFieldValueResponseViewModel value : dynamicReferred.getFieldValueList()) {
            TextView textView = new TextView(context);
            textView.setTextSize(16);
            textView.setTypeface(ResourcesCompat.getFont(context, R.font.breviaregular));
            textView.setText(String.format("%s: %s", value.getFieldName(), value.getFieldValue()));
            holder.llyFields.addView(textView);
        }
    }

    @Override
    public int getItemCount() {
        return this.dynamicReferredList == null ? 0 : this.dynamicReferredList.size();
    }

    public void setDynamicReferredList(List<DynamicReferredViewModel> dynamicReferredList) {
        this.dynamicReferredList = dynamicReferredList;
        notifyDataSetChanged();
    }

    class ReferredViewHolder extends RecyclerView.ViewHolder {

        LinearLayout llyFields;

        public ReferredViewHolder(View itemView) {
            super(itemView);
            llyFields = itemView.findViewById(R.id.lly_fields);
        }
    }

}
