package com.bcp.benefits.ui.campaign.aguinaldo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bcp.benefits.R;
import com.bcp.benefits.util.AppUtils;
import com.bcp.benefits.viewmodel.CampaignLocationViewModel;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class LocationAdapter extends RecyclerView.Adapter<LocationAdapter.ScheduleVH> {

    private List<CampaignLocationViewModel> locations;
    private OnSelectLocationListener listener;
    private int selectedItem = RecyclerView.NO_POSITION;
    private Context context;
    public boolean isClickable = true;

    public LocationAdapter(Context context, OnSelectLocationListener listener) {
        this.context = context;
        this.listener = listener;
        this.locations = new ArrayList<>();
    }

    public void refreshList(List<CampaignLocationViewModel> buses) {
        clearList();
        this.locations.addAll(buses);
        selectedItem = RecyclerView.NO_POSITION;
        notifyItemChanged(0, buses.size());
    }

    public void clearList() {
        int length = locations.size();
        this.locations.clear();
        notifyItemRangeRemoved(0, length);
    }

    @NotNull
    @Override
    public ScheduleVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_location, parent, false);
        return new ScheduleVH(v);
    }

    @Override
    public void onBindViewHolder(ScheduleVH holder, int position) {
        if (position == RecyclerView.NO_POSITION) return;
        String capitalize = locations.get(position).getDescription();
        holder.tvTypeName.setText(String.format("%s%s", capitalize.substring(0, 1).toUpperCase(), capitalize.substring(1)));
        if (locations.get(position).isSelected()) {
            AppUtils.setGradient(context.getResources().getColor(R.color.bg_campaign_color), context.getResources().getColor(R.color.bg_campaign_color),
                    AppUtils.dpToPx((int) context.getResources().getDimension(R.dimen.size_thirteen)), holder.tvTypeName);
            holder.tvTypeName.setTextColor(ContextCompat.getColor(context, R.color.white));
            holder.tvTypeName.setTextAppearance(holder.itemView.getContext(), R.style.FontBold);
        } else {
            if (selectedItem == position) {
                AppUtils.setGradient(context.getResources().getColor(R.color.bg_campaign_color), context.getResources().getColor(R.color.bg_campaign_color),
                        AppUtils.dpToPx((int) context.getResources().getDimension(R.dimen.size_thirteen)), holder.tvTypeName);
                holder.tvTypeName.setTextColor(ContextCompat.getColor(context, R.color.white));
                holder.tvTypeName.setTextAppearance(holder.itemView.getContext(), R.style.FontBold);
            } else {
                holder.tvTypeName.setBackgroundResource(R.drawable.bg_sky_blue_corner);
                holder.tvTypeName.setTextColor(ContextCompat.getColor(context, R.color.gray_800));
                holder.tvTypeName.setTextAppearance(holder.itemView.getContext(), R.style.FontRegular);
            }
        }
    }

    @Override
    public int getItemCount() {
        return !locations.isEmpty() ? locations.size() : 0;
    }

    class ScheduleVH extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvTypeName;

        ScheduleVH(View itemView) {
            super(itemView);
            tvTypeName = itemView.findViewById(R.id.tv_type);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (getAdapterPosition() == RecyclerView.NO_POSITION)
                return;

            if (listener == null)
                return;

            if (selectedItem != RecyclerView.NO_POSITION) {
                if (isClickable)
                    notifyItemChanged(selectedItem);
            }


            if (selectedItem != getAdapterPosition()) {
                if (!isClickable) {
                    return;
                }
                selectedItem = getAdapterPosition();
                notifyItemChanged(selectedItem);
                listener.selectLocation(locations.get(getAdapterPosition()));

            }
        }
    }

    public interface OnSelectLocationListener {
        void selectLocation(CampaignLocationViewModel campaign);
    }
}
