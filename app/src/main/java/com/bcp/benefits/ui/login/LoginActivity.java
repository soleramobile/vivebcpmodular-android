package com.bcp.benefits.ui.login;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.bcp.benefits.R;
import com.bcp.benefits.analytics.AnalyticsModule;
import com.bcp.benefits.analytics.AnalyticsTags;
import com.bcp.benefits.ui.BaseActivity;
import com.bcp.benefits.ui.home.HomeActivity;
import com.bcp.benefits.util.AppPreferences;
import com.bcp.benefits.util.AppUtils;

import javax.inject.Inject;

import androidx.constraintlayout.widget.ConstraintLayout;

import butterknife.BindView;
import butterknife.OnClick;
import pe.solera.benefit.main.entity.User;

import static com.bcp.benefits.util.Constants.ID_COMMUNICATION;
import static com.bcp.benefits.util.Constants.TYPE;

/**
 * Created by emontesinos.
 **/

public class LoginActivity extends BaseActivity implements LoginPresenter.LoginView, CompoundButton.OnCheckedChangeListener {

    @BindView(R.id.btn_accept)
    Button accept;
    @BindView(R.id.edit_number_doc)
    EditText numberDoc;
    @BindView(R.id.cb_remember)
    CheckBox cbRemember;
    @BindView(R.id.content_login)
    ConstraintLayout contentLogin;
    @BindView(R.id.text_hello)
    TextView textHello;

    private Boolean validateClick = false;

    @Inject
    LoginPresenter presenter;
    private AppPreferences appPreferences;
    private String communicationModel;

    AnalyticsModule analyticsModule = new AnalyticsModule(this);


    public static void start(Context context) {
        Intent intent = new Intent(context, LoginActivity.class);
        context.startActivity(intent);
    }

    public static void startPush(Context context, String communicationModel) {
        Intent starter = new Intent(context, LoginActivity.class);
        starter.putExtra(ID_COMMUNICATION, communicationModel);
        context.startActivity(starter);
    }

    @Override
    public void setUpView() {
        this.presenter.setView(this);
        appPreferences = new AppPreferences(LoginActivity.this);
        communicationModel = getIntent().getStringExtra(ID_COMMUNICATION);
        presenter.loadUser();
        validateType();
        cbRemember.setOnCheckedChangeListener(this);
        numberDoc.setSelection(numberDoc.getText().length());
    }

    @Override
    public int getLayout() {
        return R.layout.activity_login;
    }


    @Override
    public void goHome() {
        sendToAnalytics();
        if (communicationModel != null) {
            HomeActivity.startPush(this, communicationModel);
        } else {
            HomeActivity.start(this);
        }

    }

    private void sendToAnalytics(){
        Bundle bundle = new Bundle();
        bundle.putString(AnalyticsTags.Login.Parameters.login_tipo, AnalyticsTags.Login.Values.login_secundario);
        analyticsModule.sendToAnalytics(AnalyticsTags.Login.Events.login_exitoso, bundle);
    }

    @Override
    public void goValidatedError(String message) {
        AppUtils.showErrorMessage(contentLogin, this, message);
        validateClick = false;
    }

    @OnClick(R.id.btn_accept)
    void onClickAccept() {
        if (validateDoc() && !validateClick) {
            validateClick = true;
            this.presenter.loadDocNumber(numberDoc.getText().toString(), cbRemember.isChecked());
        }
    }

    private Boolean validateDoc() {
        if (numberDoc.getText().toString().isEmpty()) {
            AppUtils.showErrorMessage(contentLogin, this, getResources().getString(R.string.error_doc_empty));
            validateClick = false;
            return false;
        }
        return true;
    }

    private void validateType() {
        this.presenter.doGetTypeIdFromLoggedInUser(appPreferences.getString(TYPE));
        validRemember();
    }

    private void validRemember() {

        if (numberDoc != null && numberDoc.getText().toString().isEmpty()) {
            String docSave = appPreferences.getString("docUser");
            if (!docSave.equals("")) {
                numberDoc.setText(docSave);
                cbRemember.setChecked(true);
            } else {
                numberDoc.setText("");
                cbRemember.setChecked(false);
            }

        } else {
            cbRemember.setChecked(false);
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            appPreferences.savePreference("docUser", numberDoc.getText().toString());
        } else {
            appPreferences.savePreference("docUser", "");
        }
    }

    @Override
    public void showDniHint() {
        cbRemember.setText(R.string.login_remember_dni);
        numberDoc.setHint(R.string.login_dni_hint);
        AppUtils.changeEditTextLength(numberDoc, 8);
        numberDoc.setInputType(InputType.TYPE_CLASS_NUMBER);
    }

    @Override
    public void showCeHint() {
        cbRemember.setText(R.string.login_remember_ce);
        numberDoc.setHint(R.string.login_ce_hint);
        AppUtils.changeEditTextLength(numberDoc, 12);
        numberDoc.setInputType(InputType.TYPE_CLASS_TEXT);
    }

    @Override
    public void loadUser(User user) {
        if (user != null) {
            textHello.setText(getResources().getString(R.string.welcomeComp, user.getName()));
            this.cbRemember.setChecked(user.isRemember());
            if (user.isRemember())
                this.numberDoc.setText(user.getDocumentNumber());
            numberDoc.setSelection(numberDoc.getText().length());
        }
    }

    @Override
    public void activateButton() {
        validateClick = false;
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        validateClick = false;
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }
}