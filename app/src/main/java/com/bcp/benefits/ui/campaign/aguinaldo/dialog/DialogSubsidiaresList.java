package com.bcp.benefits.ui.campaign.aguinaldo.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.bcp.benefits.R;
import com.bcp.benefits.ui.campaign.aguinaldo.adapter.SubsidiaryAdapter;
import com.bcp.benefits.viewmodel.SubsidiaryViewModel;

import java.util.ArrayList;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class DialogSubsidiaresList extends DialogFragment implements SubsidiaryAdapter.OnItemClickListener {


    private OnSelectSubsidiaryListener onSelectSubsidiary;
    private ArrayList<SubsidiaryViewModel> subsidiaryViewModels;
    private SubsidiaryViewModel subsidiaryViewModelSelected;

    public static DialogSubsidiaresList newInstance(ArrayList<SubsidiaryViewModel> types, SubsidiaryViewModel subsidiaryViewModelSelected) {
        DialogSubsidiaresList fragment = new DialogSubsidiaresList();
        Bundle args = new Bundle();
        args.putSerializable("type", types);
        args.putSerializable("selected", subsidiaryViewModelSelected);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (!(context instanceof OnSelectSubsidiaryListener))
            throw new IllegalArgumentException("Activity is not implementing OnSelectIdTypeListener");

        onSelectSubsidiary = (OnSelectSubsidiaryListener) context;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            Drawable backgroundColor = new ColorDrawable(ContextCompat.getColor(
                    Objects.requireNonNull(getContext()), android.R.color.transparent));
            Objects.requireNonNull(dialog.getWindow()).setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            dialog.getWindow().setBackgroundDrawable(backgroundColor);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_identity_types, container, false);
        subsidiaryViewModels = (ArrayList<SubsidiaryViewModel>) getArguments().getSerializable("type");
        subsidiaryViewModelSelected = (SubsidiaryViewModel) getArguments().getSerializable("selected");
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        RecyclerView rvType = view.findViewById(R.id.rv_type);
        ImageView ivClose = view.findViewById(R.id.iv_close);
        ConstraintLayout contentDialog = view.findViewById(R.id.constraintLayout9);
        TextView textGeneric = view.findViewById(R.id.text_title_generic);


        textGeneric.setText(R.string.select_city);
        ivClose.setImageResource(R.drawable.ic_close_blue_sky_inside);
        contentDialog.setBackground(getResources().getDrawable(R.drawable.bg_type_custom_sky_blue));
        ivClose.setOnClickListener(v -> getDialog().dismiss());
        rvType.setLayoutManager(new LinearLayoutManager(getContext()));
        SubsidiaryAdapter subsidiaryAdapter = new SubsidiaryAdapter(getContext(), subsidiaryViewModels);
        DividerItemDecoration itemDecoration = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        rvType.addItemDecoration(itemDecoration);
        rvType.setAdapter(subsidiaryAdapter);
        subsidiaryAdapter.setSelectedItem(subsidiaryViewModelSelected);
        ((SubsidiaryAdapter) subsidiaryAdapter).setListener(this);

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        Objects.requireNonNull(dialog.getWindow()).requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }


    @Override
    public void onItemClick(SubsidiaryViewModel entity, int position) {
        onSelectSubsidiary.onSelectSubsidiary(entity);
        getDialog().dismiss();
    }

    public interface OnSelectSubsidiaryListener {
        void onSelectSubsidiary(SubsidiaryViewModel entity);
    }

}
