package com.bcp.benefits.ui.othres.volunteering.myTicketsVounteering;


import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;

import androidx.viewpager.widget.ViewPager;

import com.bcp.benefits.R;
import com.bcp.benefits.ui.BaseFragment;
import com.bcp.benefits.viewmodel.VolunteeringTicketViewModel;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import butterknife.BindView;

import static com.bcp.benefits.util.Constants.TICKETS_DATA;

/**
 * Created by emontesinos on 02/05/19.
 **/
public class MyTicketsVoulunteeringFragment extends BaseFragment {

    @BindView(R.id.my_view_pager_ticket)
    ViewPager pager;

    public final static int LOOPS = 1;
    private VolunteeringTicketViewModel volunteeringTicketViewModel;
    public static int count = 3;
    private static final int FIRST_PAGE = 0;

    public static MyTicketsVoulunteeringFragment newInstance(VolunteeringTicketViewModel volunteeringTicketViewModel) {
        MyTicketsVoulunteeringFragment fragment = new MyTicketsVoulunteeringFragment();
        Bundle args = new Bundle();
        args.putSerializable(TICKETS_DATA, volunteeringTicketViewModel);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void setUpView() {

    }

    @Override
    public int getLayout() {
        return R.layout.fragment_my_tickets;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       if (getArguments() != null) {
           volunteeringTicketViewModel = (VolunteeringTicketViewModel) getArguments().getSerializable(TICKETS_DATA);
           assert volunteeringTicketViewModel != null;
           count = volunteeringTicketViewModel.getListTicket().size();
        }
    }

    @Override
    public void onViewCreated(@NotNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
       DisplayMetrics metrics = new DisplayMetrics();
        Objects.requireNonNull(getActivity()).getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int pageMargin = ((metrics.widthPixels / 6) * 2);
        pager.setPageMargin(-pageMargin);
        CarouselPagerVolunteeringAdapter adapter = new CarouselPagerVolunteeringAdapter(this, getChildFragmentManager(), volunteeringTicketViewModel.getListTicket());
        pager.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        pager.addOnPageChangeListener(adapter);
        pager.setCurrentItem(FIRST_PAGE);
    }

}