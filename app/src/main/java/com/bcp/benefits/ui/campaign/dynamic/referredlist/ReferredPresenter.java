package com.bcp.benefits.ui.campaign.dynamic.referredlist;

import androidx.annotation.NonNull;

import com.bcp.benefits.ui.MainView;
import com.bcp.benefits.viewmodel.DynamicReferredViewModel;

import java.util.List;

import javax.inject.Inject;

import pe.solera.benefit.main.entity.DynamicReferred;
import pe.solera.benefit.main.usecases.executor.Action;
import pe.solera.benefit.main.usecases.usecases.CampaignUseCase;

public class ReferredPresenter {


    private ReferredView view;

    private final CampaignUseCase campaignUseCase;

    @Inject
    public ReferredPresenter(CampaignUseCase campaignUseCase) {
        this.campaignUseCase = campaignUseCase;
    }

    public void setView(@NonNull ReferredView view) {
        this.view = view;
    }

    void getReferredList(int campaignId) {
        if (this.view.validateInternet()) {
            this.view.showProgress();
            campaignUseCase.getReferredList(campaignId, new Action.Callback<List<DynamicReferred>>() {
                @Override
                public void onSuccess(List<DynamicReferred> dynamicReferreds) {

                    if (dynamicReferreds.size() > 0) {
                        view.showReferredList(DynamicReferredViewModel.toDynamicReferredList(dynamicReferreds));
                        view.hideProgress();
                    } else {
                        view.showReferredListEmpty();
                        view.hideProgress();

                    }

                }

                @Override
                public void onError(Throwable throwable) {
                    view.showErrorMessage(throwable.getMessage());
                    view.hideProgress();

                }
            });
        }
    }

    public interface ReferredView extends MainView {
        void showReferredList(List<DynamicReferredViewModel> campaignModels);

        void showReferredListEmpty();

    }
}
