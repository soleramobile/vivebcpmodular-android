package com.bcp.benefits.ui.othres.buses;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.work.Data;
import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import com.bcp.benefits.analytics.AnalyticsModule;
import com.bcp.benefits.analytics.AnalyticsTags;
import com.google.android.material.appbar.AppBarLayout;
import com.bcp.benefits.R;
import com.bcp.benefits.components.AlarmWorker;
import com.bcp.benefits.ui.BaseActivity;
import com.bcp.benefits.ui.othres.buses.success.SuccessBusesDialog;
import com.bcp.benefits.util.Constants;
import com.bcp.benefits.viewmodel.BusScheduleExit;
import com.bcp.benefits.viewmodel.BusScheduleItemViewModel;
import com.bcp.benefits.viewmodel.BusScheduleViewModel;
import com.bcp.benefits.viewmodel.BusViewModel;

import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import pe.solera.benefit.main.entity.Address;

import static com.bcp.benefits.util.Constants.DATE_START;

public class BusesActivity extends BaseActivity implements BusesPresenter.BusesView,
        BusesAddressDialogFragment.AddressClickListener, BusesTypeAdapter.SelectedTypeBuss,
        BusScheduleAdapter.BusScheduleListener, BusReminderDialogFragment.BusReminderListener,
        BusScheduleExitAdapter.BusScheduleExitListener {

    @Inject
    BusesPresenter presenter;

    @BindView(R.id.recycler_buses)
    RecyclerView recyclerBuses;
    @BindView(R.id.toolbar)
    AppBarLayout toolbar;
    @BindView(R.id.img_back)
    ImageButton imgBack;
    @BindView(R.id.title_toolbar)
    TextView titleToolbar;
    @BindView(R.id.tv_address)
    TextView textAddress;
    @BindView(R.id.tv_arrive_values)
    TextView textArrivalAddress;
    @BindView(R.id.recycler_schedules)
    RecyclerView recyclerSchedules;
    @BindView(R.id.text_start_bus)
    TextView textStartBus;
    @BindView(R.id.tv_select_remember)
    TextView textRemember;
    @BindView(R.id.content_schedule)
    View contentSchedule;
    @BindView(R.id.recyclerBusesScheduleExit)
    RecyclerView recyclerScheduleExit;

    private BusScheduleViewModel busScheduleViewModel;
    private BusViewModel busTypeSelected;
    private BusesTypeAdapter busesTypeAdapter;
    private BusScheduleAdapter busScheduleAdapter;
    private List<BusViewModel> busViewModels;
    private Address address;
    private Address arrivalAddress;
    private BusesAddressDialogFragment busesAddressDialogFragment;
    private BusReminderDialogFragment reminderDialogFragment;
    private BusScheduleItemViewModel scheduleItemSelected;
    private BusScheduleExitAdapter exitAdapter;
    private BusScheduleExit busScheduleExit;

    private String reminder = "";
    private AnalyticsModule analyticsModule = new AnalyticsModule(this);


    public static void callNew(Context context) {
        context.startActivity(new Intent(context, BusesActivity.class));
    }

    @Override
    public int getLayout() {
        return R.layout.activity_buses;
    }

    @Override
    public void setUpView() {
        setUpRecycler();
        configToolbar();
        presenter.setBusesView(this);
        presenter.loadBuses();
        sendToAnalytics();
    }

    private void sendToAnalytics(){
        analyticsModule.sendToAnalytics(AnalyticsTags.Others.Events.horariosAutos, null);
    }

    private void setUpRecycler() {
        busesTypeAdapter = new BusesTypeAdapter(this);
        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
        recyclerBuses.setLayoutManager(layoutManager);
        recyclerBuses.setAdapter(busesTypeAdapter);

        busScheduleAdapter = new BusScheduleAdapter(this);
        recyclerSchedules.setLayoutManager(new GridLayoutManager(this, 3));
        recyclerSchedules.setAdapter(busScheduleAdapter);

        exitAdapter = new BusScheduleExitAdapter(this);
        recyclerScheduleExit.setLayoutManager(new GridLayoutManager(this, 3));
        recyclerScheduleExit.setAdapter(exitAdapter);

    }

    public void configToolbar() {
        titleToolbar.setText(R.string.buses_title);
        toolbar.setBackground(getResources().getDrawable(R.drawable.bg_buses_toolbar));
        imgBack.setOnClickListener(v -> {
            onBackPressed();
            overridePendingTransition(R.anim.rigth_in, R.anim.right_out);
        });
    }

    @OnClick(R.id.tv_address)
    public void clickAddress() {
        presenter.loadAddress(busTypeSelected.getIdBusType());
    }

    @OnClick(R.id.tv_arrive_values)
    public void clickArriveAddress() {
        if (address != null)
            presenter.loadArriveSubsidiaries(address.getIdSubsidiary());
    }

    @OnClick(R.id.tv_select_remember)
    public void clickRemember() {
        presenter.loadReminders();
    }

    @OnClick(R.id.btnReserve)
    public void clickReservate() {
        if (validate()) {
            programAlarm();
        }
    }

    @Override
    public void showBuses(List<BusViewModel> busList) {
        busViewModels = busList;
        for (BusViewModel model : busList) {
            if (model.isSelected()) {
                busTypeSelected = model;
                break;
            }
        }
        busesTypeAdapter.setBusList(busViewModels);
        textStartBus.setText(getString(R.string.schedule_select, busTypeSelected.getDescription()));
        resetValues();
    }

    private void programAlarm() {
        if (busScheduleExit != null) {
            int hours = Integer.parseInt(busScheduleExit.getBusSchedule().split(":")[0]);
            int min = Integer.parseInt(busScheduleExit.getBusSchedule().split(":")[1].split(" ")[0]);
            boolean isMorning = busScheduleExit.getBusSchedule().split(" ")[1].equalsIgnoreCase("pm");
            if (isMorning) {
                hours = hours != 12 ? 0 : hours + 12;
            }
            startWorker(hours, min, getString(R.string.title_alarm, busTypeSelected.getDescription()));

            SuccessBusesDialog dialogFinanceGeneric = SuccessBusesDialog.newInstance();
            dialogFinanceGeneric.show(getSupportFragmentManager(), DATE_START);
        }
    }

    private void startWorker(int hour, int minutes, String title) {
        WorkManager workManager = WorkManager.getInstance();
        long timeAlarm = getTimeAlarm(hour, minutes);
        PeriodicWorkRequest periodicWorkRequest = new PeriodicWorkRequest
                .Builder(AlarmWorker.class, 1, TimeUnit.DAYS, timeAlarm, TimeUnit.MILLISECONDS)
                .setInputData(createDataInput(title))
                .build();
        workManager.enqueueUniquePeriodicWork(Constants.ALARM_WORK_TAG,
                ExistingPeriodicWorkPolicy.REPLACE, periodicWorkRequest);
    }

    private Data createDataInput(String title) {
        return new Data.Builder()
                .putString(Constants.ALARM_TITLE, title)
                .build();
    }

    private long getTimeAlarm(int hour, int minutes) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minutes);
        calendar.set(Calendar.SECOND, 0);

        Calendar calendarNow = Calendar.getInstance();

        if (calendarNow.getTimeInMillis() < calendar.getTimeInMillis()) {
            calendarNow.setTimeInMillis(calendarNow.getTimeInMillis() + TimeUnit.DAYS.toMillis(1));
        }
        long delta = (calendarNow.getTimeInMillis() - calendar.getTimeInMillis());
        return ((delta > PeriodicWorkRequest.MIN_PERIODIC_FLEX_MILLIS) ? delta
                : PeriodicWorkRequest.MIN_PERIODIC_FLEX_MILLIS);
    }


    private boolean validate() {
        if (address == null) {
            Toast.makeText(this, R.string.buses_select_start_bustop_required, Toast.LENGTH_SHORT).show();
            return false;
        }
        if (arrivalAddress == null) {
            Toast.makeText(this, R.string.buses_select_end_bustop_required, Toast.LENGTH_SHORT).show();
            return false;
        }
        if (scheduleItemSelected == null) {
            Toast.makeText(this, R.string.buses_select_start_hour_bustop_required, Toast.LENGTH_SHORT).show();
            return false;
        }

        if (reminder == null) {
            Toast.makeText(this, R.string.buses_remind_empty, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void resetValues() {
        resetAddress();
        resetArrivalAddress();
    }

    private void resetArrivalAddress() {
        arrivalAddress = null;
        textArrivalAddress.setText(getString(R.string.selected_here));
        textArrivalAddress.setTypeface(ResourcesCompat.getFont(this, R.font.breviaregular));
        textArrivalAddress.setTextColor(getResources().getColor(R.color.gray_700));
        contentSchedule.setVisibility(View.GONE);
    }

    private void resetAddress() {
        address = null;
        textAddress.setText(getString(R.string.selected_here));
        textAddress.setTypeface(ResourcesCompat.getFont(this, R.font.breviaregular));
        textAddress.setTextColor(getResources().getColor(R.color.gray_700));
    }

    @Override
    public void showAddress(List<Address> addressList) {
        busesAddressDialogFragment = BusesAddressDialogFragment.newInstance(addressList,
                BusesAddressDialogFragment.AddressType.START, this, address);
        showFragment(busesAddressDialogFragment, "dialog");
    }

    @Override
    public void showArriveAddress(List<Address> addressList) {
        busesAddressDialogFragment = BusesAddressDialogFragment.newInstance(addressList,
                BusesAddressDialogFragment.AddressType.END, this, arrivalAddress);
        showFragment(busesAddressDialogFragment, "dialog");
    }

    private void showFragment(DialogFragment dialogFragment, String tag) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        Fragment fragmentByTag = getSupportFragmentManager().findFragmentByTag(tag);
        if (fragmentByTag != null) {
            fragmentTransaction.remove(fragmentByTag);
        }
        fragmentTransaction.addToBackStack(null);
        dialogFragment.show(fragmentTransaction, tag);
    }

    @Override
    public void showSchedule(BusScheduleViewModel busSchedule) {
        this.busScheduleViewModel = busSchedule;
        this.busScheduleAdapter.setBusScheduleList(busScheduleViewModel.getSchedule());
        exitAdapter.setSchedules(busSchedule.getSchedule().get(0).getStart());
        busScheduleExit = busSchedule.getSchedule().get(0).getStart().get(0);
        scheduleItemSelected = busSchedule.getSchedule().get(0);
    }

    @Override
    public void showReminders(List<String> reminders) {
        reminderDialogFragment = BusReminderDialogFragment.newInstance(reminders, this, reminder);
        showFragment(reminderDialogFragment, "reminderDialog");
    }

    @Override
    public void selectedAddress(Address address) {
        this.address = address;
        this.textAddress.setText(address.getDescription());
        textAddress.setTextColor(getResources().getColor(R.color.gray_800));
        textAddress.setTypeface(ResourcesCompat.getFont(this, R.font.flexo_bold));
        resetArrivalAddress();
    }

    @Override
    public void selectedArrivalAddress(Address address) {
        this.arrivalAddress = address;
        textArrivalAddress.setText(address.getDescription());
        textArrivalAddress.setTextColor(getResources().getColor(R.color.gray_800));
        textArrivalAddress.setTypeface(ResourcesCompat.getFont(this, R.font.flexo_bold));
        this.presenter.loadSchedule(busTypeSelected.getIdBusType(), this.address.getIdSubsidiary(),
                this.arrivalAddress.getIdSubsidiary());
        contentSchedule.setVisibility((this.address != null && arrivalAddress != null) ? View.VISIBLE : View.GONE);
    }

    @Override
    public void selectedType(BusViewModel bus) {
        for (BusViewModel busViewModel : busViewModels) {
            busViewModel.setSelected(busViewModel.getIdBusType() == bus.getIdBusType());
        }
        presenter.updateBuses(busViewModels);
        textStartBus.setText(getString(R.string.schedule_select, bus.getDescription()));
    }

    @Override
    public void selectedItemSchedule(BusScheduleItemViewModel busSchedule) {
        scheduleItemSelected = busSchedule;
        for (BusScheduleItemViewModel item : busScheduleViewModel.getSchedule()) {
            item.setSelected(item.getArrival().equals(busSchedule.getArrival()));
        }
        busScheduleAdapter.setBusScheduleList(busScheduleViewModel.getSchedule());
        exitAdapter.setSchedules(busSchedule.getStart());
        busScheduleExit = busSchedule.getStart().get(0);
    }

    @Override
    public void selectReminder(String reminder) {
        this.reminder = reminder;
        textRemember.setText(reminder);
    }

    @Override
    public void busScheduleExitSelected(BusScheduleExit schedule) {
        busScheduleExit = schedule;
        for (BusScheduleExit item : scheduleItemSelected.getStart()) {
            item.setSelected(item.getBusSchedule().equals(schedule.getBusSchedule()));
        }
        exitAdapter.setSchedules(scheduleItemSelected.getStart());
    }
}
