package com.bcp.benefits.ui.discounts.consume;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

import com.bcp.benefits.R;
import com.bcp.benefits.ui.discounts.rate.RateDiscountActivity;
import com.bcp.benefits.viewmodel.DiscountTypeViewModel;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.bcp.benefits.viewmodel.DiscountTypeViewModel.DISCOUNT;
import static com.bcp.benefits.viewmodel.DiscountTypeViewModel.EDUTATION;


public class SuccessMessageDialogFragment extends DialogFragment {

    public static final String DISCOUNT_TYPE_PARAM = "discount_type";
    private final static String OPERATION_PARAM = "operation";
    private final Integer ANIM_DURATION = 160;

    @BindView(R.id.flContent)
    ConstraintLayout rlRoot;
    @BindView(R.id.img_logo_generic)
    ImageView ivAnimation;
    @BindView(R.id.text_title_generic)
    TextView tvSuccessMessage;
    @BindView(R.id.text_sub_generic_gray)
    TextView tvMessageSub1;
    @BindView(R.id.text_sub_title_generic)
    TextView tvMessageSub2;
    @BindView(R.id.img_close_generic)
    ImageView btnClose;

    public enum Operation {
        RATE, PROPOSE
    }

    private Operation operation;
    private Unbinder unbinder;
    private DiscountTypeViewModel discountType;

    public static SuccessMessageDialogFragment newInstance(Operation operation) {
        Bundle args = new Bundle();
        args.putSerializable(OPERATION_PARAM, operation);
        SuccessMessageDialogFragment fragment = new SuccessMessageDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        loadData();
        setStyle(DialogFragment.STYLE_NO_TITLE,
                discountType == EDUTATION ? R.style.ConsumeEducation : R.style.ConsumeDiscount);

        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            operation = (Operation) getArguments().getSerializable(OPERATION_PARAM);
        }
    }

    private void loadData() {
        discountType = (DiscountTypeViewModel) getActivity().getIntent().getSerializableExtra(DISCOUNT_TYPE_PARAM);
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            Drawable backgroundColor = new ColorDrawable(ContextCompat.getColor(Objects.requireNonNull(getContext()), android.R.color.transparent));
            Objects.requireNonNull(dialog.getWindow()).setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            dialog.getWindow().setBackgroundDrawable(backgroundColor);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_custom_simple, container, false);
        unbinder = ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onViewCreated(@NotNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvMessageSub1.setText((operation == Operation.RATE) ? R.string.dialog_rate_subtitle : R.string.dialog_proposal_subtitle);
        tvMessageSub2.setVisibility(View.GONE);
        btnClose.setImageDrawable(getResources().getDrawable(closeFlv()));
        rlRoot.setBackground(getResources().getDrawable(gradientBgFlv()));
        tvSuccessMessage.setTextColor(ContextCompat.getColor(Objects.requireNonNull(getContext()), startFlv()));
        tvSuccessMessage.setText((operation == Operation.RATE) ? R.string.dialog_calitication_title : R.string.dialog_proposal_title);
        ivAnimation.setImageDrawable(getResources().getDrawable(imageFlv()));

    }

    public int resId(int first, int second) {
        return (discountType == DISCOUNT) ? first : second;
    }

    public int startFlv() {
        return resId(R.color.bg_green_start, R.color.bg_yellow_start);
    }

    public int gradientBgFlv() {
        return resId(R.drawable.bg_consume_discount, R.drawable.bg_yellow_gradient_square);

    }

    public int closeFlv() {
        return resId(R.drawable.ic_close_green, R.drawable.ic_close_orange);

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setOnKeyListener((dialogInterface, keyCode, keyEvent) -> {
            if ((keyCode == KeyEvent.KEYCODE_BACK)) {
                close();
                return true;
            } else
                return false;
        });
        return dialog;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    public int imageFlv() {
        if (operation == Operation.RATE) {
            return resId(R.drawable.ic_star_green, R.drawable.ic_star_yellow);
        } else {
            return resId(R.drawable.ic_letter_green, R.drawable.ic_letter);
        }
    }

    @OnClick(R.id.img_close_generic)
    public void onViewClicked() {
        close();
    }

    private void close() {
        dismiss();
        if (operation == Operation.RATE) {
            ((RateDiscountActivity) getActivity()).onResultDialog();
        }
        getActivity().finish();
    }


}
