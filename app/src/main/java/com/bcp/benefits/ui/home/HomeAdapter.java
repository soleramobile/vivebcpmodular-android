package com.bcp.benefits.ui.home;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bcp.benefits.R;
import com.bcp.benefits.viewmodel.HomeViewModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.bcp.benefits.util.AppUtils.applyBackgroundColor;
import static com.bcp.benefits.util.AppUtils.colorFromStrings;
import static com.bcp.benefits.util.AppUtils.imgDrawable;
import static com.bcp.benefits.util.AppUtils.textFromStrings;
import static com.bcp.benefits.util.Constants.BORDER_COLOR;
import static com.bcp.benefits.util.Constants.END_COLOR;
import static com.bcp.benefits.util.Constants.START_COLOR;

/**
 * Created by ernestogaspard on 23/04/19.
 **/

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.ItemViewHolder> {

    private HomeInterface listener;
    private ArrayList<HomeViewModel> items;

    HomeAdapter(HomeInterface listener, ArrayList<HomeViewModel> items) {
        this.listener = listener;
        this.items = items;
    }

    public interface HomeInterface {
        void clickItem(HomeViewModel type);
    }

    @Override
    public int getItemCount() {
        return (items != null) ? items.size() : 0;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_home, viewGroup, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        holder.bindData(items.get(position));

    }


    class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txv_item_title)
        TextView txvItemTitle;
        @BindView(R.id.img_item_icon)
        ImageView imgItemIcon;
        @BindView(R.id.ctr_home_item)
        ConstraintLayout ctrHomeItem;

        ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bindData(final HomeViewModel item) {
            txvItemTitle.setText(textFromStrings(itemView.getContext(), item.getTitle()));
            txvItemTitle.setTextColor(colorFromStrings(itemView.getContext(), item.getTitleColor()));
            imgItemIcon.setImageDrawable(imgDrawable(itemView.getContext(), item.getIcon()));
            String stColor = (String) item.getColors().get(START_COLOR);
            String endColor = (String) item.getColors().get(END_COLOR);
            String bdColor = (String) item.getColors().get(BORDER_COLOR);
            applyBackgroundColor(ctrHomeItem, itemView.getContext(), stColor, endColor, bdColor, 36f);
            ctrHomeItem.setOnClickListener(v -> listener.clickItem(item));
        }
    }
}
