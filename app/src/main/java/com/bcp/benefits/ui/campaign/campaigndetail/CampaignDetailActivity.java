package com.bcp.benefits.ui.campaign.campaigndetail;



import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.widget.NestedScrollView;

import com.bcp.benefits.R;
import com.bcp.benefits.ui.BaseActivity;
import com.bcp.benefits.viewmodel.DetailInfoViewModel;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.OnClick;

import static com.bcp.benefits.util.Constants.DETAIL_PARAM;

/**
 * Created by emontesinos on 16/05/19.
 **/

public class CampaignDetailActivity extends BaseActivity {

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_subtitle)
    TextView tvSubtitle;
    @BindView(R.id.iv_image)
    ImageView ivImage;
    @BindView(R.id.tv_title_two)
    TextView titleTwo;
    @BindView(R.id.tv_description)
    TextView tvDescription;
    @BindView(R.id.content_info)
    NestedScrollView contentInfo;


    public static void start(Context context, DetailInfoViewModel detailInfo) {
        Intent starter = new Intent(context, CampaignDetailActivity.class);
        starter.putExtra(DETAIL_PARAM, detailInfo);
        context.startActivity(starter);
    }


    @Override
    public void setUpView() {
        Animation animations= AnimationUtils.loadAnimation(this,R.anim.animation_in);
        contentInfo.startAnimation(animations);
         getDataView();


    }

    public void getDataView(){
        if (getIntent().getSerializableExtra(DETAIL_PARAM) != null) {
            DetailInfoViewModel detailInfo = (DetailInfoViewModel) getIntent().getSerializableExtra(DETAIL_PARAM);

            if (detailInfo.getSubTitle() != null && !detailInfo.getSubTitle().isEmpty()) {
                tvSubtitle.setText(detailInfo.getSubTitle());
            } else {
                tvSubtitle.setVisibility(View.GONE);
            }

            if (detailInfo.getTitle() != null && !detailInfo.getTitle().isEmpty()) {
                titleTwo.setText(detailInfo.getTitle());
            } else {
                titleTwo.setVisibility(View.GONE);
            }

            if (detailInfo.getImage() != null && !detailInfo.getImage().isEmpty()) {
                Picasso.get().load(detailInfo.getImage()).placeholder(R.drawable.ic_place_campam).into(ivImage);
            }
            if (detailInfo.getCaption() != null && !detailInfo.getCaption().isEmpty()) {
                tvDescription.setText(detailInfo.getCaption());
            } else {
                tvDescription.setVisibility(View.GONE);
            }
     }
    }

    @Override
    public int getLayout() {
        return R.layout.activity_campaign_detail;
    }


    @OnClick(R.id.img_close)
    public void onViewClicked() {
        finish();
    }
}
