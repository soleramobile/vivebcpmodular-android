package com.bcp.benefits.ui.othres.assistanceconfirmation.listcapaignvolunteeting;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.bcp.benefits.R;
import com.bcp.benefits.ui.BaseActivity;
import com.bcp.benefits.ui.othres.assistanceconfirmation.ConfirmationVolunteeringPresenter;
import com.bcp.benefits.ui.othres.assistanceconfirmation.confirmation.ConfirmationVolunteeringActivity;
import com.bcp.benefits.ui.othres.assistanceconfirmation.dialog.CampaignVolunteeringDialogFragment;
import com.bcp.benefits.ui.othres.assistanceconfirmation.dialog.ListLocalDialogFragment;
import com.bcp.benefits.ui.othres.assistanceconfirmation.dialog.LocalVolunteeringDialogFragment;
import com.bcp.benefits.ui.othres.assistanceconfirmation.dialog.TypeVolunteerDialogFragment;
import com.bcp.benefits.util.AppUtils;
import com.bcp.benefits.viewmodel.CampaignVolunteeringViewModel;
import com.bcp.benefits.viewmodel.ListLocalViewModel;
import com.bcp.benefits.viewmodel.ListTypeVolunteerViewModel;
import com.bcp.benefits.viewmodel.LocalVolunteeringViewModel;
import com.google.android.material.appbar.AppBarLayout;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

import static com.bcp.benefits.util.Constants.DATE_START;

/**
 * Created by emontesinos on 13/01/20.
 */


public class ListCampaignActivity extends BaseActivity implements CampaignVolunteeringDialogFragment.OnSelectCampaignListener,
        LocalVolunteeringDialogFragment.OnSelectLocalListener, ListCampaignVolunteeringView, TypeVolunteerDialogFragment.OnSelectTypeVolunteerListener, ListLocalDialogFragment.OnSelectLocalListener {


    @Inject
    ConfirmationVolunteeringPresenter presenter;

    @BindView(R.id.img_back)
    ImageButton imgBack;
    @BindView(R.id.title_toolbar)
    TextView titleToolbar;
    @BindView(R.id.toolbar)
    AppBarLayout toolbar;
    @BindView(R.id.btn_Send_Filter)
    TextView btnSendFilter;
    @BindView(R.id.firstComboBox)
    TextView firstComboBox;
    @BindView(R.id.secondComboBox)
    TextView secondComboBox;
    @BindView(R.id.thirdComboBox)
    TextView thirdComboBox;
    @BindView(R.id.textTitleLocalVolunteering)
    TextView textTitleLocalVolunteering;
    @BindView(R.id.containerOne)
    ConstraintLayout containerOne;
    @BindView(R.id.tvChanger)
    TextView tvChanger;
    @BindView(R.id.containerTwo)
    ConstraintLayout containerTwo;
    @BindView(R.id.fourthComboBox)
    TextView fourthComboBox;
    @BindView(R.id.containerCero)
    ConstraintLayout containerCero;

    @BindView(R.id.contentListCa)
    ConstraintLayout contentListCa;


    private Boolean validClick = true;


    private ArrayList<CampaignVolunteeringViewModel> listCampaignVolunteering = new ArrayList<>();
    private CampaignVolunteeringViewModel campaignVolunteeringSelected;
    private CampaignVolunteeringViewModel campaignVolunteeringSelectedUpdate;
    private ArrayList<LocalVolunteeringViewModel> listLocalVolunteering = new ArrayList<>();
    private LocalVolunteeringViewModel localVolunteeringSelected;
    private LocalVolunteeringViewModel localVolunteeringSelectedUpdate;
    private ArrayList<ListTypeVolunteerViewModel> listTypeVolunteers = new ArrayList<>();
    private ListTypeVolunteerViewModel typeVolunteerSelected;
    private ListTypeVolunteerViewModel typeVolunteerSelectedUpdate;
    private ArrayList<ListLocalViewModel> listLocals = new ArrayList<>();
    private ListLocalViewModel localSelected;
    private ListLocalViewModel localSelectedUpdate;
    private String closeAttendance;
    private Integer idVolunteerType;


    public static void start(Context context) {
        Intent intent = new Intent(context, ListCampaignActivity.class);
        context.startActivity(intent);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_list_campaign;
    }

    @Override
    public void setUpView() {
        getToolbar();

    }

    public void getToolbar() {
        this.presenter.setView(this);
        titleToolbar.setText(R.string.confirmation);
        unableFilter();
        toolbar.setBackground(getResources().getDrawable(R.drawable.bg_pink_confirmation));
        imgBack.setOnClickListener(v -> {
            onBackPressed();
            overridePendingTransition(R.anim.rigth_in, R.anim.right_out);
        });
        // goneLocalSelected();
        if (AppUtils.isConnected(this)) {
            presenter.getListTypeVolunteer(); //send id selected
        } else {
            showErrorConnect(getResources().getString(R.string.exception_no_internet_error));
        }

    }


    @OnClick(R.id.firstComboBox)
    public void onClickVolunteerType() {
        if (AppUtils.isConnected(this)) {
            if (listTypeVolunteers.size() > 0) {
                TypeVolunteerDialogFragment df = TypeVolunteerDialogFragment.newInstance(typeVolunteerSelected, listTypeVolunteers, this);
                df.show(getSupportFragmentManager(), "TypeVolunteer");
            } else {
                AppUtils.showErrorMessage(contentListCa, this, "No hay tipos de voluntariados disponibles");
            }
        } else {
            showErrorConnect(getResources().getString(R.string.exception_no_internet_error));
        }

    }

    @OnClick(R.id.secondComboBox)
    public void onClickSelectCampaignVolunteering() {
        unSelectedBoxCustom(thirdComboBox);
        unSelectedBoxCustom(fourthComboBox);
        unSelectedBoxCustom(secondComboBox);
        campaignVolunteeringSelected = null;
        fourthComboBox.setText(null);
        thirdComboBox.setText(null);
        containerOne.setVisibility(View.GONE);
        containerTwo.setVisibility(View.GONE);
        if (listCampaignVolunteering.size() > 0) {
            CampaignVolunteeringDialogFragment dp = CampaignVolunteeringDialogFragment.newInstance(campaignVolunteeringSelected, listCampaignVolunteering, this);
            dp.show(getSupportFragmentManager(), DATE_START);
        } else {
            AppUtils.showErrorMessage(contentListCa, this, getResources().getString(R.string.error_list_ca));
        }
    }

    @OnClick(R.id.thirdComboBox)
    public void onClickSelectLocal() {
        unSelectedBoxCustom(fourthComboBox);
        fourthComboBox.setText(null);
        unSelectedBoxCustom(thirdComboBox);
        thirdComboBox.setText(null);
        localVolunteeringSelected = null;
        if (AppUtils.isConnected(this)) {
            if (campaignVolunteeringSelected != null) {
                if (listLocalVolunteering.size() > 0) {
                    LocalVolunteeringDialogFragment dpLocal = LocalVolunteeringDialogFragment.newInstance(localVolunteeringSelected, listLocalVolunteering, this);
                    dpLocal.show(getSupportFragmentManager(), DATE_START);
                } else {
                    AppUtils.showErrorMessage(contentListCa, this, getResources().getString(R.string.error_list_lo));
                }
            } else {
                AppUtils.showErrorMessage(contentListCa, this, getResources().getString(R.string.error_selected));
            }
        } else {
            showErrorConnect(getResources().getString(R.string.exception_no_internet_error));
        }
    }

    @OnClick(R.id.fourthComboBox)
    public void onClickLocal() {
        localSelected = null;
        unSelectedBoxCustom(fourthComboBox);
        fourthComboBox.setText(null);
        if (AppUtils.isConnected(this)) {
        if (listLocals.size() > 0) {
            ListLocalDialogFragment df = ListLocalDialogFragment.newInstance(localSelected, listLocals, this);
            df.show(getSupportFragmentManager(), "TypeVolunteer");
        } else {
            AppUtils.showErrorMessage(contentListCa, this, "No hay locales disponibles");
        }
        } else {
            showErrorConnect(getResources().getString(R.string.exception_no_internet_error));
        }
    }

    @OnClick(R.id.btn_Send_Filter)
    public void onClickSendFilter() {
        if (localVolunteeringSelectedUpdate != null) {
            if (validClick) {
                validClick = false;
                ConfirmationVolunteeringActivity.start(this, localVolunteeringSelectedUpdate, idVolunteerType, campaignVolunteeringSelectedUpdate, closeAttendance);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        validClick = true;
    }

    private void validation(CampaignVolunteeringViewModel entity) {
        containerOne.setVisibility(View.VISIBLE);
        if (entity.getEnableLocal() == 1) {
            //SECOND COMBO
            tvChanger.setText("Selecciona la charla");
            thirdComboBox.setHint("Charla");
            presenter.getThirdComboData(String.valueOf(entity.getId()));
            containerTwo.setVisibility(View.VISIBLE);
        } else {
            tvChanger.setText("Selecciona el local");
            thirdComboBox.setHint("Local");
            presenter.getThirdComboData(String.valueOf(entity.getId()));
        }
    }

    private void selectedBoxCustom(TextView box) {
        box.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ico_dropdown_white, 0);
        box.setTextColor(getResources().getColor(R.color.white));
        AppUtils.setGradient(getResources().getColor(R.color.confirmation_end), getResources().getColor(R.color.confirmation_end),
                AppUtils.dpToPx((int) getResources().getDimension(R.dimen.size_thirteen)), box);
    }

    private void unSelectedBoxCustom(TextView box) {
        box.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ico_dropdown_green, 0);
        box.setTextColor(getResources().getColor(R.color.gray_volun));
        box.setBackground(getResources().getDrawable(R.drawable.bg_white_green_corners_stroke));
        unableFilter();
    }

    private void ableFilter() {
        btnSendFilter.setEnabled(true);
        AppUtils.setGradient(getResources().getColor(R.color.confirmation_end), getResources().getColor(R.color.confirmation_end),
                AppUtils.dpToPx((int) getResources().getDimension(R.dimen.size_thirteen)), btnSendFilter);
    }

    private void unableFilter() {
        btnSendFilter.setEnabled(false);
        AppUtils.setGradient(getResources().getColor(R.color.gray), getResources().getColor(R.color.gray),
                AppUtils.dpToPx((int) getResources().getDimension(R.dimen.size_thirteen)), btnSendFilter);
    }


    private void unSelectedLocal() {
        containerOne.setVisibility(View.GONE);
        containerTwo.setVisibility(View.GONE);
        campaignVolunteeringSelected = null;
        campaignVolunteeringSelectedUpdate = null;
        unSelectedBoxCustom(secondComboBox);
        secondComboBox.setText("Campañas");
        unableFilter();
    }

    @Override
    public void showTypesVolunteer(List<ListTypeVolunteerViewModel> listTypeVolunteerViewModel) {
        listTypeVolunteers = (ArrayList<ListTypeVolunteerViewModel>) listTypeVolunteerViewModel;
    }

    @Override
    public void showListCampaignVolunteering(List<CampaignVolunteeringViewModel> campaignVolunteeringViewModels) {
        listCampaignVolunteering = (ArrayList<CampaignVolunteeringViewModel>) campaignVolunteeringViewModels;
    }

    @Override
    public void showListLocalVolunteering(List<LocalVolunteeringViewModel> localVolunteeringViewModels) {
        listLocalVolunteering = (ArrayList<LocalVolunteeringViewModel>) localVolunteeringViewModels;
    }

    @Override
    public void showList(List<ListLocalViewModel> listLocalViewModels) {
        listLocals = (ArrayList<ListLocalViewModel>) listLocalViewModels;
    }

    @Override
    public void firstCombo(ListTypeVolunteerViewModel entity, int position) {
        if (entity != null) {
            typeVolunteerSelected = entity;
            idVolunteerType = entity.getIdVolunteerType();
            typeVolunteerSelectedUpdate = entity;
            firstComboBox.setText(entity.getName());
            selectedBoxCustom(firstComboBox);
            presenter.getSecondComboData(entity.getIdVolunteerType());
            unSelectedLocal();
            containerCero.setVisibility(View.VISIBLE);
        } else {
            reset();
        }
    }

    @Override
    public void secondCombo(CampaignVolunteeringViewModel entity, int position) {
        if (entity != null) {
            campaignVolunteeringSelected = entity;
            campaignVolunteeringSelectedUpdate = entity;
            secondComboBox.setText(entity.getName());
            selectedBoxCustom(secondComboBox);
            validation(entity);
            localVolunteeringSelected = null;
            localVolunteeringSelected = null;
            localSelected = null;
            localSelectedUpdate = null;
        }
    }

    @Override
    public void thirdCombo(LocalVolunteeringViewModel entity, int position) {
        if (entity != null) {
            localVolunteeringSelected = entity;
            localVolunteeringSelectedUpdate = entity;
            thirdComboBox.setText(entity.getName());
            if (entity.getCloseAttendance() != null)
                this.closeAttendance = entity.getCloseAttendance();
            presenter.getFourthComboData(entity.getId());
            selectedBoxCustom(thirdComboBox);
            if (localVolunteeringSelected != null && tvChanger.getText().equals("Selecciona el local")) {
                ableFilter();
            }
        }

    }

    @Override
    public void fourthCombo(ListLocalViewModel entity, int position) {
        if (entity != null) {
            localSelected = entity;
            localSelectedUpdate = entity;
            fourthComboBox.setText(entity.getLocal());
            selectedBoxCustom(fourthComboBox);
            if (typeVolunteerSelected != null && campaignVolunteeringSelected != null && localVolunteeringSelected != null && localSelected != null) {
                ableFilter();
            }
        }
    }

    public void reset() {
        unSelectedLocal();
        containerOne.setVisibility(View.GONE);
        containerTwo.setVisibility(View.GONE);
        typeVolunteerSelected = null;
        listCampaignVolunteering.clear();
        unSelectedBoxCustom(firstComboBox);
        typeVolunteerSelectedUpdate = null;
        firstComboBox.setText(getResources().getString(R.string.by_selected_vo));
    }


}
