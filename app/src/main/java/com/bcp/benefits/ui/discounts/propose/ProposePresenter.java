package com.bcp.benefits.ui.discounts.propose;

import com.bcp.benefits.R;
import com.bcp.benefits.ui.MainView;
import com.bcp.benefits.viewmodel.DiscountCategoryViewModel;
import com.bcp.benefits.viewmodel.DiscountTypeViewModel;

import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import pe.solera.benefit.main.entity.DiscountCategory;
import pe.solera.benefit.main.usecases.executor.Action;
import pe.solera.benefit.main.usecases.usecases.DiscountUseCases;

public class ProposePresenter {

    private final DiscountUseCases discountUseCases;

    private ProposeView view;

    @Inject
    public ProposePresenter(final DiscountUseCases discountUseCases) {
        this.discountUseCases = discountUseCases;

    }

    public void setView(@NonNull ProposeView view) {
        this.view = view;
    }

    public void doProposeDiscount(Integer categoryId, String comment) {
        if(!validate(categoryId, comment))
            return;
        if (this.view.validateInternet()) {
            this.view.showProgress();
            discountUseCases.proposeDiscount(categoryId, comment, new Action.Callback<Boolean>() {
                @Override
                public void onSuccess(Boolean aBoolean) {
                    view.showProposeSuccess(categoryId, comment);
                    view.hideProgress();
                }

                @Override
                public void onError(Throwable throwable) {
                    view.showErrorMessage(throwable.getMessage());
                    view.hideProgress();

                }
            });
        }

    }

    public void goGetCategories(DiscountTypeViewModel type) {
        if (this.view.validateInternet()) {
            this.view.showProgress();
            discountUseCases.getDiscountCategories(type.getIndex(), new Action.Callback<List<DiscountCategory>>() {
                @Override
                public void onSuccess(List<DiscountCategory> discountCategories) {

                    view.hideCategoryProgress(DiscountCategoryViewModel.toDiscountCategoryViewModelList(discountCategories));
                    view.hideProgress();

                }

                @Override
                public void onError(Throwable throwable) {

                    view.showErrorMessage(throwable.getMessage());
                    view.hideProgress();

                }
            });
        }

    }

    private boolean validate(Integer categoryId, String comment){
        if(categoryId==null){
            view.showErrorMessage(R.string.propose_discount_category_empty);
            return false;
        }

        if(comment==null || comment.isEmpty()){
            view.showErrorMessage(R.string.propose_discount_comment_empty);
            return false;
        }
        return true;
    }

    public interface ProposeView extends MainView {

        void hideCategoryProgress(List<DiscountCategoryViewModel> response);
        void showProposeSuccess(Integer categoryId, String comment);


    }
}
