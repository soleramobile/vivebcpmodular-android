package com.bcp.benefits.ui.health.detailproduct;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.bcp.benefits.R;
import com.bcp.benefits.viewmodel.HealthProductItemViewModel;

public class HealthProductDetailRowAdapter extends RecyclerView.Adapter<HealthProductDetailRowAdapter.HealthProductDetailVH>{

    private List<HealthProductItemViewModel> list;

    public HealthProductDetailRowAdapter(List<HealthProductItemViewModel> list){
        this.list = list;
    }

    @NotNull
    @Override public HealthProductDetailVH onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_health_product_detail, parent, false);
        return new HealthProductDetailVH(view);
    }

    @Override public void onBindViewHolder(@NotNull HealthProductDetailVH holder, int position) {
        holder.bind(list.get(position));
    }

    @Override public int getItemCount() {
        return list==null?0:list.size();
    }

    class HealthProductDetailVH extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_product_name)
        TextView tvProductName;
        @BindView(R.id.tv_extra) TextView tvExtra;
        @BindView(R.id.tv_coverage) TextView tvCoverage;
        @BindView(R.id.tv_amount) TextView tvAmount;
        @BindView(R.id.tv_disclaimer) TextView tvDisclaimer;
        @BindView(R.id.ll_product_name)
        LinearLayout llProductName;

        public HealthProductDetailVH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(HealthProductItemViewModel item){
           if(item.getDescription() != null){
                tvProductName.setText(item.getDescription());
                llProductName.setVisibility(View.VISIBLE);
            }else{
                llProductName.setVisibility(View.GONE);
            }

            if(item.getExtras() != null){
                tvExtra.setText(item.getExtras());
                tvExtra.setVisibility(View.VISIBLE);
            }else{
                tvExtra.setVisibility(View.GONE);
            }

            if(item.getCoverage() != null){
                tvCoverage.setText(item.getCoverage());
                tvCoverage.setVisibility(View.VISIBLE);
            }else{
                tvCoverage.setVisibility(View.GONE);
            }

            if(item.getAmount() != null){
                tvAmount.setText(item.getAmount());
                tvAmount.setVisibility(View.VISIBLE);
            }else{
                tvAmount.setVisibility(View.GONE);
            }

            if(item.getDisclaimer() != null){
                tvDisclaimer.setText(item.getDisclaimer());
                tvDisclaimer.setVisibility(View.VISIBLE);
            }else{
                tvDisclaimer.setVisibility(View.GONE);
            }
        }
    }


}
