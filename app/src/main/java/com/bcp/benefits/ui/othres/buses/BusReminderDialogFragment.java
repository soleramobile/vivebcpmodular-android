package com.bcp.benefits.ui.othres.buses;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bcp.benefits.R;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BusReminderDialogFragment extends DialogFragment implements ReminderAdapter.ReminderListener {
    private List<String> reminders;
    private BusReminderListener busReminderListener;

    @BindView(R.id.constraintLayout9)
    View contentHeader;
    @BindView(R.id.text_title_generic)
    TextView textTitle;
    @BindView(R.id.iv_close)
    ImageView imageClose;
    @BindView(R.id.rv_type)
    RecyclerView recyclerAddress;

    private ReminderAdapter reminderAdapter;
    private String reminderSelected;

    BusReminderDialogFragment(List<String> reminders, BusReminderListener busReminderListener,
                              String reminderSelected) {
        this.reminders = reminders;
        this.busReminderListener = busReminderListener;
        this.reminderSelected = reminderSelected;
    }

    public static BusReminderDialogFragment newInstance(List<String> reminders,
                                                        BusReminderListener listener, String reminderSelected) {
        return new BusReminderDialogFragment(reminders, listener, reminderSelected);
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            Drawable backgroundColor = new ColorDrawable(ContextCompat.getColor(
                    Objects.requireNonNull(getContext()), android.R.color.transparent));
            Objects.requireNonNull(dialog.getWindow()).setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            dialog.getWindow().setBackgroundDrawable(backgroundColor);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_identy_simple, container, false);
        ButterKnife.bind(this, view);
        this.setUpView();
        return view;
    }

    private void setUpView() {
        contentHeader.setBackgroundResource(R.drawable.bg_buses_dialog_blue);
        textTitle.setText(R.string.select_reminder);
        imageClose.setImageDrawable(this.getContext().getDrawable(R.drawable.ic_close_buses_blue));


        reminderAdapter = new ReminderAdapter(this);
        recyclerAddress.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerAddress.setAdapter(reminderAdapter);
        reminderAdapter.setReminders(reminders, reminders.indexOf(reminderSelected));
    }

    @OnClick(R.id.iv_close)
    public void clickClose() {
        dismiss();
    }

    @Override
    public void itemSelected(String item) {
        busReminderListener.selectReminder(item);
        dismiss();
    }

    public interface BusReminderListener {
        void selectReminder(String reminder);
    }
}
