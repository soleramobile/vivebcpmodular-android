package com.bcp.benefits.ui.splash;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.bcp.benefits.BuildConfig;
import com.bcp.benefits.R;
import com.bcp.benefits.ui.BaseActivity;
import com.bcp.benefits.ui.login.LoginActivity;
import com.bcp.benefits.ui.tutorial.TutorialActivity;
import com.bcp.benefits.ui.validate.ValidateActivity;
import com.bcp.benefits.util.AppUtils;
import com.scottyab.rootbeer.RootBeer;

import javax.inject.Inject;

import static com.bcp.benefits.util.Constants.ID_COMMUNICATION;

/**
 * Created by emontesinos.
 **/

public class SplashActivity extends BaseActivity implements SplashPresenter.SplashView {


    @Inject
    SplashPresenter presenter;

    private String communicationModel;

    public static void start(Context context) {
        Intent intent = new Intent(context, SplashActivity.class);
        context.startActivity(intent);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_splash;
    }

    @Override
    public void setUpView() {

        this.presenter.setView(this);
        this.communicationModel = getIntent().getStringExtra(ID_COMMUNICATION);
        this.presenter.configApp();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void goToLogin() {
        if (communicationModel != null && !communicationModel.isEmpty()) {
            LoginActivity.startPush(this, communicationModel);
        } else {
            LoginActivity.start(this);
        }
        finish();
    }

    @Override
    public void goBanner() {
        TutorialActivity.start(this);
    }

    @Override
    public void goToValidate() {
        ValidateActivity.start(this);
    }


    @Override
    public void openUrl(String url) {
        AppUtils.openUrl(this, url);
        finish();
    }
}
