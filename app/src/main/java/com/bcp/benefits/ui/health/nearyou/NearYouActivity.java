package com.bcp.benefits.ui.health.nearyou;


import android.animation.Animator;
import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.AppBarLayout;
import com.google.gson.Gson;
import com.bcp.benefits.R;
import com.bcp.benefits.components.SearchViewEditText;
import com.bcp.benefits.ui.BaseActivity;
import com.bcp.benefits.ui.health.nearyou.adapter.AttentionTypeAdapter;
import com.bcp.benefits.ui.health.nearyou.fragment.HealthProductFragment;
import com.bcp.benefits.ui.health.nearyou.fragment.MapNearFragment;
import com.bcp.benefits.util.AppUtils;
import com.bcp.benefits.viewmodel.HealthAttentionTypeViewModel;
import com.bcp.benefits.viewmodel.HealthProductViewModel;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by emontesinos on 22/05/19.
 **/
public class NearYouActivity extends BaseActivity implements AttentionTypeAdapter.OnSelectAttentionTypeListener,
        NearYouPresenter.NearYouView, TextWatcher {
    @Inject
    NearYouPresenter presenter;

    @BindView(R.id.toolbar)
    AppBarLayout toolbar;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.title_toolbar)
    TextView titleToobal;
    @BindView(R.id.rv_attention_types)
    RecyclerView rvAttentionTypes;
    @BindView(R.id.et_search)
    SearchViewEditText textSearch;
    @BindView(R.id.content_not_found)
    View contentNotFound;
    @BindView(R.id.img_list_filter)
    ImageView imgListFilter;
    @BindView(R.id.img_close_search)
    ImageView imgClose;

    private AttentionTypeAdapter attentionTypeAdapter;
    private HealthProductViewModel filteredProduct;
    private List<HealthProductViewModel> filteredProductsData;
    private HealthAttentionTypeViewModel selectedHealthAttentionType;
    private Boolean isMapShow = true;
    private int selectFilterPosition = 0;
    private String searchText = "";

    public static void start(Context context) {
        Intent starter = new Intent(context, NearYouActivity.class);
        context.startActivity(starter);
    }

    @Override
    public void setUpView() {
        this.presenter.setView(this);
        getToolbar();
        textSearch.setHint(getString(R.string.search_health));
        textSearch.setOnEditorActionListener((textView, actionId, keyEvent) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                searchText = textSearch.getText();
                textSearch.setText(searchText);
                textSearch.setEnableEditText(!textSearch.isEnable());
                if (isMapShow) {
                    updateFragments();
                }
                hideKeyboard();
                return true;
            }
            return false;
        });
        textSearch.setOnClickListener(view -> {
            if (!isMapShow) {
                textSearch.setEnableEditText(!textSearch.isEnable());
                updateFragments();
            }
        });
        textSearch.addTextChangedListener(this);
        attentionTypeAdapter = new AttentionTypeAdapter(this, this);
        rvAttentionTypes.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        rvAttentionTypes.setAdapter(attentionTypeAdapter);
        updateHealthProducts();
    }

    private void hideKeyboard() {
        AppUtils.hideKeyboard(this);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_near_you;
    }

    private void getToolbar() {
        titleToobal.setText(R.string.health_two);
        toolbar.setBackground(getResources().getDrawable(R.drawable.bg_health_blue));
        imgBack.setOnClickListener(v -> {
            onBackPressed();
            overridePendingTransition(R.anim.rigth_in, R.anim.right_out);
        });
    }

    public void selectedFilter(HealthProductViewModel selectedProduct, int position) {
        filteredProduct = selectedProduct;
        this.selectFilterPosition = position;
        textSearch.setText(textSearch.getText());
        textSearch.setEnableEditText(false);
        showHealthAttentionTypes(filteredProduct.getAttentionTypes());
        hideKeyboard();
    }

    public void updateHealthProducts() {
        this.presenter.doGetHealthProducts();
    }

    @Override
    public void showHealthProducts(List<HealthProductViewModel> list) {
        if (!list.isEmpty()) {
            filteredProduct = filteredProduct == null ? list.get(0) : filteredProduct;
        }
        this.filteredProductsData = list;
    }

    @Override
    public void showHealthAttentionTypes(List<HealthAttentionTypeViewModel> list) {
        if (list.isEmpty())
            return;
        attentionTypeAdapter.addList(list);
        selectedHealthAttentionType = list.get(0);
        updateFragments();
    }

    @OnClick({R.id.linear_search, R.id.et_search})
    public void onClickViewFilter() {
        updateFragments();
    }

    @Override
    public void showNotFoundData() {
        contentNotFound.setVisibility(View.VISIBLE);
    }

    private void updateFragments() {
        if (filteredProduct == null || selectedHealthAttentionType == null)
            return;
        if (isMapShow) {
            getFragmentSelectDownAbove(MapNearFragment.newInstance(textSearch.getText(), filteredProduct,
                    selectedHealthAttentionType));
        } else {
            Gson gson = new Gson();
            getFragmentSelectAbodeDown(HealthProductFragment.newInstance(gson.toJson(filteredProductsData),
                    selectFilterPosition));
        }
        animRecycler();
        isMapShow = !isMapShow;
    }

    private void animRecycler() {
        rvAttentionTypes.animate()
                .alpha(isMapShow ? 1 : 0)
                .setDuration(500)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        //Do nothing
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        rvAttentionTypes.setVisibility(rvAttentionTypes.getVisibility()
                                == View.VISIBLE ? View.GONE : View.VISIBLE);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                        //Do nothing
                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {
                        //Do nothing
                    }
                })
                .start();
    }


    private void getFragmentSelectDownAbove(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.slide_out_up, R.anim.slide_out_down)
                .replace(R.id.conten_dinamic, fragment)
                .commit();
    }

    private void getFragmentSelectAbodeDown(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.slide_in_up, R.anim.slide_top_down)
                .replace(R.id.conten_dinamic, fragment)
                .commit();
    }

    @Override
    public void selectAttentionType(HealthAttentionTypeViewModel type) {
        selectedHealthAttentionType = type;
        getFragmentSelectDownAbove(MapNearFragment.newInstance(textSearch.getText(), filteredProduct, selectedHealthAttentionType));
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        //do nothing
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        //do nothing
    }

    @Override
    public void afterTextChanged(Editable string) {
        if (string.toString().length() >= 1) {
            imgListFilter.setVisibility(View.GONE);
            imgClose.setVisibility(View.VISIBLE);

        } else {
            imgListFilter.setVisibility(View.VISIBLE);
            imgClose.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.img_close_search)
    public void onCloseSearch() {
        textSearch.setText("");
        textSearch.setEnableEditText(isMapShow);
        textSearch.setEnabled(isMapShow);
        AppUtils.hideKeyboard(this);
    }
}
