package com.bcp.benefits.ui.campaign.aguinaldo;


import android.animation.Animator;
import android.app.Activity;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bcp.benefits.R;
import com.bcp.benefits.components.AdjustImage;
import com.bcp.benefits.components.EditTextComponent;
import com.bcp.benefits.ui.BaseActivity;
import com.bcp.benefits.ui.campaign.aguinaldo.adapter.LocationAdapter;
import com.bcp.benefits.ui.campaign.aguinaldo.dialog.DialogAguinaldo;
import com.bcp.benefits.ui.campaign.aguinaldo.dialog.DialogSubsidiaresList;
import com.bcp.benefits.ui.campaign.productDetail.DialogProductDetail;
import com.bcp.benefits.util.AnimationUtils;
import com.bcp.benefits.util.AppUtils;
import com.bcp.benefits.viewmodel.CampaignLocationViewModel;
import com.bcp.benefits.viewmodel.CampaignProductViewModel;
import com.bcp.benefits.viewmodel.DetailInfoViewModel;
import com.bcp.benefits.viewmodel.LocationDetailViewModel;
import com.bcp.benefits.viewmodel.SubsidiaryViewModel;
import com.bcp.benefits.viewmodel.UsageDetailViewModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

import static com.bcp.benefits.util.Constants.BUTTON_1;
import static com.bcp.benefits.util.Constants.BUTTON_2;
import static com.bcp.benefits.util.Constants.DATE_START;
import static com.bcp.benefits.util.Constants.DETAIL_PARAM;
import static com.bcp.benefits.util.Constants.ID_CAMPAIGN_PARAM;
import static com.bcp.benefits.util.Constants.LOCATION_DETAIL_PARAM;
import static com.bcp.benefits.util.Constants.PRODUCTS_PARAM;
import static com.bcp.benefits.util.Constants.SELECT_PRODUCTS;
import static com.bcp.benefits.util.Constants.SHOW_LOCATION_PARAM;
import static com.bcp.benefits.util.Constants.USAGE_DETAIL_PARAM;
import static com.bcp.benefits.util.Constants.USED_PARAM;

/**
 * Created by emontesinos on 15/05/19.
 **/
public class AguinaldoDetailActivity extends BaseActivity implements AguinaldoAdapter.ListenerViewAdapterDetail,
        AguinaldoPresenter.AguinaldoView, LocationAdapter.OnSelectLocationListener,
        DialogSubsidiaresList.OnSelectSubsidiaryListener, SearchAdaper.OnItemClickListener, SearchPresenter.SearchView {

    @Inject
    AguinaldoPresenter presenter;

    @Inject
    SearchPresenter searchPresenter;
    @BindView(R.id.txt_disclaimer)
    TextView txtDisclaimer;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_title_2)
    TextView txTitle2;
    @BindView(R.id.tv_subtitle)
    TextView tvSubtitle;
    @BindView(R.id.recycler)
    RecyclerView recyclerProducts;
    @BindView(R.id.recycler_locations)
    RecyclerView recyclerViewLocation;
    @BindView(R.id.btn_enviar)
    Button btnEnviar;
    @BindView(R.id.txt_title_location)
    TextView txtLocation2;
    @BindView(R.id.search_subsidiaries)
    EditTextComponent searchSubsidiaries;
    @BindView(R.id.content_search_province)
    ConstraintLayout contentSearch;
    @BindView(R.id.iv_product_image)
    AdjustImage imgProdSelected;
    @BindView(R.id.etName_location_list)
    EditText cbLocation;
    @BindView(R.id.content_dinamic)
    ConstraintLayout contentDinamic;
    @BindView(R.id.content_aguinaldo_detail)
    ConstraintLayout contentAguinaldoDetail;
    @BindView(R.id.content_location)
    ConstraintLayout contentLocation;
    @BindView(R.id.content_location_general)
    ConstraintLayout contentLocationGeneral;
    @BindView(R.id.subsidiary_address_selected)
    TextView txtSubAddressSelected;
    @BindView(R.id.subsidiary_code_selected)
    TextView txtSubCodeSelected;
    @BindView(R.id.content_selected_city)
    ConstraintLayout contentSelectedCity;
    @BindView(R.id.txt_product_title)
    TextView txtProdSelectedTitle;
    @BindView(R.id.tv_title_from_location)
    TextView titleFromLocation;
    @BindView(R.id.rv_list_location)
    RecyclerView recyclerLocations;

    private CampaignLocationViewModel locationSelect;

    private CampaignProductViewModel campaignProductViewModel;

    private DetailInfoViewModel detailInfo;
    private ArrayList<CampaignProductViewModel> products;
    private LocationDetailViewModel locationDetail;
    private int idCampaign, idSubsidiary = -1;
    private ArrayList<Integer> productIds;
    private boolean used, showLocation;
    private UsageDetailViewModel usageDetail;
    private boolean button1, button2;
    private LocationAdapter locationAdapter;
    private boolean selectProducts;
    private List<SubsidiaryViewModel> subsidiaryList;
    private SubsidiaryViewModel subsidiaryViewModelSelected;
    private int id;

    private SearchAdaper subsidiaryAdapter;


    public static void start(Activity context, DetailInfoViewModel detailInfo, ArrayList<CampaignProductViewModel> products,
                             LocationDetailViewModel locationDetail, int idCampaign, UsageDetailViewModel usageDetail,
                             boolean usado, boolean showLocation, boolean selectProducts, boolean button1, boolean button2) {
        Intent starter = new Intent(context, AguinaldoDetailActivity.class);
        starter.putExtra(DETAIL_PARAM, detailInfo);
        starter.putExtra(PRODUCTS_PARAM, products);
        starter.putExtra(LOCATION_DETAIL_PARAM, locationDetail);
        starter.putExtra(ID_CAMPAIGN_PARAM, idCampaign);
        starter.putExtra(USED_PARAM, usado);
        starter.putExtra(SHOW_LOCATION_PARAM, showLocation);
        starter.putExtra(BUTTON_1, button1);
        starter.putExtra(BUTTON_2, button2);
        starter.putExtra(SELECT_PRODUCTS, selectProducts);
        starter.putExtra(USAGE_DETAIL_PARAM, usageDetail);
        context.startActivityForResult(starter, 100);
    }

    @Override
    public void setUpView() {
        presenter.setView(this);
        searchPresenter.setView(this);
        detailInfo = (DetailInfoViewModel) getIntent().getSerializableExtra(DETAIL_PARAM);
        products = (ArrayList<CampaignProductViewModel>) getIntent().getSerializableExtra(PRODUCTS_PARAM);
        locationDetail = (LocationDetailViewModel) getIntent().getSerializableExtra(LOCATION_DETAIL_PARAM);
        idCampaign = getIntent().getIntExtra(ID_CAMPAIGN_PARAM, 0);
        id = idCampaign;
        used = getIntent().getBooleanExtra(USED_PARAM, false);
        usageDetail = (UsageDetailViewModel) getIntent().getSerializableExtra(USAGE_DETAIL_PARAM);
        showLocation = getIntent().getBooleanExtra(SHOW_LOCATION_PARAM, false);
        selectProducts = getIntent().getBooleanExtra(SELECT_PRODUCTS, false);
        button1 = getIntent().getBooleanExtra(BUTTON_1, false);
        button2 = getIntent().getBooleanExtra(BUTTON_2, false);

        subsidiaryAdapter = new SearchAdaper(this);
        subsidiaryAdapter.setListener(this);
        recyclerLocations.setLayoutManager(new LinearLayoutManager(this));
        recyclerLocations.setAdapter(subsidiaryAdapter);

        getValidateTitles();
        getValidateListProduct();
        getInitRecyclerViewProducts();
        getSelectProduct();
        getLocation();
        getValidateSelectedCity();
        listenSearchProvinces();
        searchSubsidiaries.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus)
                onClickSubsidiarySearch();
        });
    }

    private void listenSearchProvinces() {
        searchSubsidiaries.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (recyclerLocations.getVisibility() == View.GONE)
                    scaleDownSearcher();
            }

            @Override
            public void onTextChanged(CharSequence text, int start, int before, int count) {
                if (text.toString().length() >= 2) {
                    searchPresenter.requestSuggestions(idCampaign, locationSelect.getIdLocation(), text.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void getSelectProduct() {
        if (selectProducts) {
            if (showLocation) {
                btnEnviar.setEnabled(false);
                btnEnviar.setAlpha(0);
                contentLocationGeneral.setVisibility(View.VISIBLE);
            } else {
                contentLocationGeneral.setVisibility(View.GONE);
                if (products.isEmpty()) {
                    btnEnviar.setEnabled(false);
                    btnEnviar.setAlpha(0);
                }
            }
        }
    }

    public void getLocation() {
        if (showLocation) {
            locationAdapter = new LocationAdapter(this, this);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
            recyclerViewLocation.setLayoutManager(linearLayoutManager);
            recyclerViewLocation.setLayoutManager(linearLayoutManager);
            recyclerViewLocation.setAdapter(locationAdapter);
            if (locationDetail.getLocations() != null) {
                locationAdapter.refreshList(locationDetail.getLocations());
                if (!selectProducts) {
                    locationAdapter.isClickable = false;
                } else {
                    locationAdapter.isClickable = !button2;
                }
            }
        } else {
            contentLocationGeneral.setVisibility(View.GONE);
        }
    }

    public void getInitRecyclerViewProducts() {
        AguinaldoAdapter productAdapter = new AguinaldoAdapter(this, products, this, selectProducts);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        DividerItemDecoration itemDecoration = new DividerItemDecoration(this, DividerItemDecoration.HORIZONTAL);
        itemDecoration.setDrawable(Objects.requireNonNull(ActivityCompat.getDrawable(this, R.drawable.divider_horizontal)));
        recyclerProducts.addItemDecoration(itemDecoration);
        recyclerProducts.setLayoutManager(linearLayoutManager);
        recyclerProducts.setAdapter(productAdapter);
    }

    public void getValidateTitles() {
        titleFromLocation.setText(locationDetail.getTitle());
        if (detailInfo.getTitle() != null && !detailInfo.getTitle().isEmpty()) {
            tvTitle.setText(detailInfo.getTitle());
        } else {
            tvTitle.setVisibility(View.GONE);
        }

        if (detailInfo.getSubTitle() != null && !detailInfo.getSubTitle().isEmpty()) {
            tvSubtitle.setText(detailInfo.getSubTitle());
        } else {
            tvSubtitle.setVisibility(View.GONE);
        }

    }

    public void getValidateSelectedCity() {
        if (button2) {
            if (usageDetail.getSubsidiary() == null) {
                if (showLocation) {
                    locationAdapter.isClickable = true;
                } else {
                    contentLocationGeneral.setVisibility(View.GONE);
                    btnEnviar.setEnabled(false);
                    btnEnviar.setAlpha(0);
                }

            } else {
                txTitle2.setVisibility(View.GONE);
                contentSelectedCity.setVisibility(View.VISIBLE);

                tvTitle.setVisibility(View.INVISIBLE);
                titleFromLocation.setVisibility(View.GONE);
                if (!usageDetail.getSubsidiary().getSubsidiaryDetail().isEmpty()) {
                    txtSubCodeSelected.setText(usageDetail.getSubsidiary().getSubsidiaryDetail());
                } else {
                    txtSubCodeSelected.setVisibility(View.GONE);
                }
                btnEnviar.setEnabled(false);
                btnEnviar.setAlpha(0);
                contentLocation.setVisibility(View.GONE);
                txtDisclaimer.setVisibility(View.GONE);
                txtLocation2.setVisibility(View.GONE);
                txtSubAddressSelected.setText(usageDetail.getSubsidiary().getAddress());
                assert usageDetail.getSubsidiary().getAddress() != null;
                txTitle2.setVisibility(!usageDetail.getSubsidiary().getAddress().isEmpty() ? View.GONE : View.VISIBLE);
            }
        }
    }

    public void getValidateListProduct() {
        if (button1) {
            loadSelection();
            contentDinamic.setVisibility(View.VISIBLE);
            tvSubtitle.setVisibility(View.GONE);
            recyclerProducts.setVisibility(View.GONE);
            btnEnviar.setText(getResources().getString(R.string.submit));
        } else {
            recyclerProducts.setVisibility(View.VISIBLE);
            contentDinamic.setVisibility(View.GONE);
        }
    }

    @Override
    public int getLayout() {
        return R.layout.activity_aguinaldo;
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        finish();
    }

    @Override
    public void goDialogDetail(CampaignProductViewModel entity) {
        DialogProductDetail dp = DialogProductDetail.newInstance(entity);
        dp.show(getSupportFragmentManager(), DATE_START);
    }

    @Override
    public void selectProduct(CampaignProductViewModel selectEntity) {
        this.productIds = new ArrayList<>();
        if (selectEntity != null) {
            productIds.add(selectEntity.getIdCampaignProduct());
            btnEnviar.setEnabled(true);
            btnEnviar.setAlpha(1);
        } else {
            btnEnviar.setEnabled(false);
            btnEnviar.setAlpha(0);
            productIds.clear();
        }
    }

    @OnClick(R.id.textMore)
    void clickMore() {
        DialogProductDetail dp = DialogProductDetail.newInstance(campaignProductViewModel);
        dp.show(getSupportFragmentManager(), DATE_START);
    }

    public void dialogSuccess() {
        presenter.request(idCampaign, idSubsidiary, productIds);
    }

    @Override
    public void selectLocation(CampaignLocationViewModel campaignLocation) {
        if (recyclerLocations.getVisibility() == View.VISIBLE)
            scaleUpSearcher();
        txtSubAddressSelected.setVisibility(View.GONE);
        contentSelectedCity.setVisibility(View.GONE);
        subsidiaryList = new ArrayList<>();
        locationSelect = campaignLocation;
        btnEnviar.setEnabled(true);
        btnEnviar.setAlpha(1);
        contentLocation.setVisibility(View.VISIBLE);
        txtDisclaimer.setVisibility(View.VISIBLE);
        txtDisclaimer.setText(campaignLocation.getTextCollect());
        idSubsidiary = -1;
        if (campaignLocation.isCollect()) {
            contentSearch.setVisibility(View.GONE);
            if (campaignLocation.getSubsidiares() != null) {
                subsidiaryList = campaignLocation.getSubsidiares();
            }
            cbLocation.setVisibility(View.VISIBLE);
            searchSubsidiaries.setVisibility(View.GONE);
            recyclerLocations.setVisibility(View.GONE);
            searchSubsidiaries.setText("");
        } else {
            contentSearch.setVisibility(View.VISIBLE);
            searchSubsidiaries.setVisibility(View.VISIBLE);
            recyclerLocations.setVisibility(View.VISIBLE);
            cbLocation.setVisibility(View.GONE);
            cbLocation.setText("");
            subsidiaryViewModelSelected = null;
            cbLocation.setCompoundDrawablesRelativeWithIntrinsicBounds(getResources()
                    .getDrawable(R.drawable.ic_check_location), null, null, null);
        }
        txtDisclaimer.setVisibility(View.VISIBLE);
        txtLocation2.setVisibility(View.VISIBLE);
        txtDisclaimer.setText(locationDetail.getDisclaimer());
        txtLocation2.setText(campaignLocation.getTextCollect());
    }

    private void loadSelection() {
        if (usageDetail != null) {
            if (usageDetail.getProducts() != null) {
                for (int i = 0; i < products.size(); i++) {
                    if (usageDetail.getProducts().size() > 0) {
                        int pr1 = products.get(i).getIdCampaignProduct();
                        int pr2 = usageDetail.getProducts().get(0);
                        if (pr1 == pr2) {
                            campaignProductViewModel = products.get(i);
                            products.get(i).Selected = true;
                            productIds = new ArrayList<>();
                            productIds.add(products.get(i).getIdCampaignProduct());
                            txtProdSelectedTitle.setText(products.get(i).getTitle());
                            txTitle2.setText(products.get(i).getDescriptions().get(0));
                            String cad = "";
                            for (int k = 0; k < products.get(i).getDescriptions().size(); k++) {
                                if (k == products.get(i).getDescriptions().size() - 1) {
                                    cad = cad + products.get(i).getDescriptions().get(k);
                                } else {
                                    cad = cad + products.get(i).getDescriptions().get(k) + ", ";
                                }

                            }
                            Picasso.get().load(products.get(i).getImage())
                                    .placeholder(R.drawable.ic_place_campam).into(imgProdSelected);

                        }
                    }

                }
            }
        }

    }

    @OnClick(R.id.btn_enviar)
    public void onSend() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment dialogFragment = getSupportFragmentManager().findFragmentByTag("dialog");
        if (dialogFragment != null) {
            ft.remove(dialogFragment);
        }
        ft.addToBackStack(null);
        if (productIds == null) {
            AppUtils.showErrorMessage(contentAguinaldoDetail, this, getResources().getString(R.string.message_error_aguinaldo_detail));
            return;
        }
        if (productIds.size() == 0) {
            AppUtils.showErrorMessage(contentAguinaldoDetail, this, getResources().getString(R.string.message_error_aguinaldo_detail));
            return;
        }
        if (button1) {
            if (idSubsidiary == -1) {
                AppUtils.showErrorMessage(contentAguinaldoDetail, this, getResources().getString(R.string.message_error_aguinaldo_detail_sede));
                return;
            }
            if (AppUtils.isConnected(this)) {
                DialogAguinaldo newFragment = DialogAguinaldo.newInstance();
                newFragment.show(ft, "dialog");
            } else {
                AppUtils.dialogIsConnect(this);
            }

        } else {
            if (AppUtils.isConnected(this)) {
                idSubsidiary = -1;
                DialogAguinaldo newFragment = DialogAguinaldo.newInstance();
                newFragment.show(ft, "dialog");
            } else {
                AppUtils.dialogIsConnect(this);
            }
        }
    }

    @Override
    public void showMessageSuccess() {


        Intent returnIntent = new Intent();
        returnIntent.putExtra("idCampania",id);
        setResult(RESULT_OK,returnIntent);
        finish();

    }

    @OnClick(R.id.img_close)
    public void onClickClose() {
        setResult(RESULT_CANCELED);
        finish();
    }

    @OnClick(R.id.etName_location_list)
    public void onClickSubsidiary() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment dialogFragment = getSupportFragmentManager().findFragmentByTag("dialog");
        if (dialogFragment != null) {
            ft.remove(dialogFragment);
        }
        ft.addToBackStack(null);
        DialogSubsidiaresList newFragment = DialogSubsidiaresList
                .newInstance((ArrayList<SubsidiaryViewModel>) subsidiaryList, subsidiaryViewModelSelected);
        newFragment.show(ft, "dialog");
        if (recyclerLocations.getVisibility() == View.VISIBLE)
            scaleUpSearcher();
    }

    private void onClickSubsidiarySearch() {

       /* Intent starter = new Intent(this, SearchActivity.class);
        starter.putExtra(ID_CAMPAIGN_LOCATION, idCampaign);
        starter.putExtra(ID_LOCATION, locationSelect.getIdLocation());
        starter.putExtra(PROVINCE_TITLE, locationSelect.getTextCollect());
        startActivityForResult(starter, 30);
        overridePendingTransition(R.anim.slide_out_up, R.anim.slide_out_down);*/
        if (recyclerLocations.getVisibility() == View.GONE)
            scaleDownSearcher();
        else
            scaleUpSearcher();
    }

    private void scaleDownSearcher() {
        recyclerLocations.setVisibility(View.VISIBLE);
        AnimationUtils.extendHeightView(searchSubsidiaries.getBottom(), AppUtils.convertDpToPx(this, 270), contentSearch,
                300, new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {
                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {

                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {
                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {
                    }
                }).start();
    }

    private void scaleUpSearcher() {
        recyclerLocations.setVisibility(View.GONE);
        subsidiaryAdapter.addList(null);
        AnimationUtils.extendHeightView(contentSearch.getTop(), searchSubsidiaries.getBottom(), contentSearch,
                400, new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {
                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {

                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {
                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {
                    }
                }).start();
    }

    @Override
    public void onSelectSubsidiary(SubsidiaryViewModel entitySub) {
        if (entitySub != null && entitySub.getIdSubsidiary() != null) {
            subsidiaryViewModelSelected = entitySub;
            idSubsidiary = Integer.parseInt(entitySub.getIdSubsidiary());
            cbLocation.setText(entitySub.getName());
            cbLocation.setCompoundDrawablesRelativeWithIntrinsicBounds(getResources()
                    .getDrawable(R.drawable.ic_checked_green_item), null, null, null);
        } else {
            subsidiaryViewModelSelected = null;
            cbLocation.setText("");
            cbLocation.setCompoundDrawablesRelativeWithIntrinsicBounds(getResources()
                    .getDrawable(R.drawable.ic_check_location), null, null, null);
            idSubsidiary = -1;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onItemClickSelect(SubsidiaryViewModel selectedSubsidiary) {
        idSubsidiary = selectedSubsidiary.getIdSubsidiaryCampaign();
        //String[] nameCode = selectedSubsidiary.getAgency().split("-");
        searchSubsidiaries.setText(selectedSubsidiary.getAgency());
        txtSubAddressSelected.setText(selectedSubsidiary.getAddress());
        txtSubAddressSelected.setVisibility(View.VISIBLE);
        contentSelectedCity.setVisibility(View.VISIBLE);
        scaleUpSearcher();
    }

    @Override
    public void showRefreshSubsidiariesList(List<SubsidiaryViewModel> subsidiaryViewModels) {
        subsidiaryAdapter.addList((ArrayList<SubsidiaryViewModel>) subsidiaryViewModels);
    }
}
