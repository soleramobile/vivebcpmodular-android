package com.bcp.benefits.ui.discounts.nearyoudiscounts;

import android.content.Context;

import com.bcp.benefits.ui.MainFragment;
import com.bcp.benefits.viewmodel.DiscountTypeViewModel;
import com.bcp.benefits.viewmodel.DiscountViewModel;

import java.util.List;

import javax.inject.Inject;

import pe.solera.benefit.main.entity.DiscountList;
import pe.solera.benefit.main.entity.SortType;
import pe.solera.benefit.main.usecases.executor.Action;
import pe.solera.benefit.main.usecases.usecases.DiscountUseCases;

public class MapNearYouDiscountPresenter {

    private MapNearYouDiscountView view;
    private final Context context;
    private final DiscountUseCases discountUseCases;

    @Inject
    public MapNearYouDiscountPresenter(final DiscountUseCases discountUseCases, Context context) {
        this.discountUseCases = discountUseCases;
        this.context = context;
    }

    public void setView(MapNearYouDiscountView view) {
        this.view = view;
    }

    void doFilterDiscountsForMap(DiscountTypeViewModel type, String search, Integer idCategory, Integer idState,
                                 Integer idDistrict, String latitude, String longitude) {


        this.view.showProgress();
        discountUseCases.getDiscountByType(DiscountTypeViewModel.getDiscountType(type), SortType.DISTANCE, search, idCategory, idState,
                idDistrict, latitude, longitude, 0, 1, new Action.Callback<DiscountList>() {
                    @Override
                    public void onSuccess(DiscountList discounts) {
                        view.showDiscountsForMap(DiscountViewModel.toDiscountListViewModels(discounts, type));
                        view.hideProgress();
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        view.showErrorMessage(throwable.getMessage());
                        view.hideProgress();

                    }
                });
    }

    interface MapNearYouDiscountView extends MainFragment {

        void showDiscountsForMap(List<DiscountViewModel> response);
    }
}
