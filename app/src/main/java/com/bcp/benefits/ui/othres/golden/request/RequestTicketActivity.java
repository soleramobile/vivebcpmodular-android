package com.bcp.benefits.ui.othres.golden.request;

import android.animation.Animator;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bcp.benefits.R;
import com.bcp.benefits.components.EditTextComponent;
import com.bcp.benefits.ui.BaseActivity;
import com.bcp.benefits.ui.othres.golden.DialogTicket;
import com.bcp.benefits.util.AnimationUtils;
import com.bcp.benefits.util.AppUtils;
import com.bcp.benefits.viewmodel.BossViewModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import timber.log.Timber;

import static com.bcp.benefits.util.Constants.DATE_START;
import static com.bcp.benefits.util.Constants.HEADNAME;
import static com.bcp.benefits.util.Constants.IDENTIFIER_TICKETS_FORCE_APROVAL_DIALOG;
import static com.bcp.benefits.util.Constants.IDTICKET;
import static com.bcp.benefits.util.Constants.SHOWRADIO;

/**
 * Created by emontesinos on 06/05/19.
 **/
public class RequestTicketActivity extends BaseActivity implements DatePickerDialog.OnDateSetListener,
        BossAdapter.OnSelectBossItemListener, RequestTicketPresenter.RequestTicketView {

    @BindView(R.id.et_birthday)
    EditText tvBirthday;
    @BindView(R.id.etHeadName)
    EditTextComponent etHeadName;
    @BindView(R.id.radioGroup)
    RadioGroup mRadioGroup;
    @BindView(R.id.ll_search)
    LinearLayout llSearcher;
    @BindView(R.id.llDurationContainer)
    LinearLayout llDurationContainer;
    @BindView(R.id.rv_bosses)
    RecyclerView rvBosses;

    @BindView(R.id.id_scroll)
    NestedScrollView contentScroll;


    @Inject
    RequestTicketPresenter presenter;

    private DatePickerDialog dpdBirtday;
    private int idTicket;
    private boolean isShowRadioGroup;
    private Integer idDurationDetail;
    private StringBuffer usageDate;
    private BossViewModel selectedBoss;
    private BossAdapter bossAdapter;
    private boolean searcherIsShown = false;


    @Override
    public void setUpView() {
        this.presenter.setView(this);
        llDurationContainer.setVisibility(View.VISIBLE);
        getInitCalendar();
        AppUtils.hideSoftKeyboard(this);
        if (getIntent().getStringExtra(HEADNAME)!=null)
            etHeadName.setText(getIntent().getStringExtra(HEADNAME));
            isShowRadioGroup = getIntent().getBooleanExtra(SHOWRADIO, false);
            idTicket = getIntent().getIntExtra(IDTICKET, 0);

        etHeadName.setSelection(etHeadName.getText().length());
        initList();
        onClickDrawableSearch();
        searchCharacter();
        funCheckBock();
    }

    @Override
    public int getLayout() {
        return R.layout.activity_request_ticket;
    }


    @OnClick(R.id.et_birthday)
    public void onSelectBirthday(){
        dpdBirtday.show();
    }


    public void onClickDrawableSearch(){
        etHeadName.setOnClickListener(v -> {
                if(searcherIsShown)
                    scaleUpSearcher();
                else
                    scaleDownSearcher();

        });
    }

    @OnClick(R.id.ivClose)
    public void onBackFromClose(){
        onBackPressed();
    }

    @OnClick(R.id.btnAccept)
    public void onAccept(){
        if(usageDate == null){
            AppUtils.showErrorMessage(contentScroll,this,getResources().getString(R.string.request_golden_ticket_date_empty));
            return;
        }
        if(etHeadName.getText().toString().isEmpty()){
            AppUtils.showErrorMessage(contentScroll,this,getResources().getString(R.string.request_golden_ticket_boss_empty));
            return;
        }

        Integer idUserDepartmentHead = null;
        if(Objects.requireNonNull(etHeadName.getText()).toString().isEmpty()){
            selectedBoss = null;
        }else{
            if(selectedBoss!=null)
                idUserDepartmentHead = selectedBoss.idUser;
        }

        String date = usageDate.toString();
        presenter.doRequest(idTicket, idDurationDetail, date, idUserDepartmentHead);

    }

    public void getInitCalendar(){
        Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        funDate(mYear,mMonth,mDay);
        dpdBirtday = new DatePickerDialog(this,R.style.datepicker, this, mYear, mMonth, mDay);
    }

    private void funCheckBock(){
        if (!isShowRadioGroup) {
            idDurationDetail = null;
            llDurationContainer.setVisibility(View.GONE);
        } else {
            llDurationContainer.setVisibility(View.VISIBLE);
            mRadioGroup.setOnCheckedChangeListener((group, checkedId) -> {
                RadioButton rb = findViewById(checkedId);
                if (rb.getText().equals(getString(R.string.mornig))) {
                    idDurationDetail = 1;
                } else if (rb.getText().equals(getString(R.string.afternoon))) {
                    idDurationDetail = 2;
                }
            });
            mRadioGroup.check(mRadioGroup.getChildAt(0).getId());
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        view.updateDate(year, month, dayOfMonth);
        StringBuilder sbu = new StringBuilder();
        usageDate = new StringBuffer();
        sbu.append( (dayOfMonth<10)?"0"+dayOfMonth:dayOfMonth);
        sbu.append("/");
        sbu.append((month+1)<10?"0"+(month+1):month+1);
        sbu.append("/");
        sbu.append(year);
        usageDate.append(year);
        usageDate.append("-");
        usageDate.append((month+1)<10?"0"+(month+1):month+1);
        usageDate.append("-");
        usageDate.append( (dayOfMonth<10)?"0"+dayOfMonth:dayOfMonth);
        tvBirthday.setText(sbu.toString());
    }

    private void scaleUpSearcher(){
        rvBosses.setVisibility(View.GONE);
        bossAdapter.addList(null);
        AnimationUtils.extendHeightView(llSearcher.getTop(), etHeadName.getBottom(), llSearcher,
                400, new Animator.AnimatorListener() {
                    @Override public void onAnimationStart(Animator animator) {}
                    @Override public void onAnimationEnd(Animator animator) {
                        searcherIsShown = false;
                        if(selectedBoss!=null)
                            etHeadName.setText(selectedBoss.name);

                    }
                    @Override public void onAnimationCancel(Animator animator) {}
                    @Override public void onAnimationRepeat(Animator animator) {}
                }).start();
    }

    private void scaleDownSearcher() {
        AnimationUtils.extendHeightView(etHeadName.getBottom(), AppUtils.convertDpToPx(this, 170), llSearcher,
                300, new Animator.AnimatorListener() {
                    @Override public void onAnimationStart(Animator animator) {}
                    @Override public void onAnimationEnd(Animator animator) {
                        searcherIsShown = true;
                        etHeadName.setSelection(Objects.requireNonNull(etHeadName.getText()).toString().length());
                        rvBosses.setVisibility(View.VISIBLE);
                        presenter.doGetBosses(etHeadName.getText().toString());
                    }
                    @Override public void onAnimationCancel(Animator animator) {}
                    @Override public void onAnimationRepeat(Animator animator) {}
                }).start();
    }

    @Override
    public void selectBossItem(BossViewModel boss) {
        selectedBoss = boss;
        etHeadName.setText(boss.name);
        etHeadName.setSelection(etHeadName.getText().toString().length());
        scaleUpSearcher();
    }


   public void searchCharacter(){
       etHeadName.addTextChangedListener(new TextWatcher() {
           @Override public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

           }

           @Override public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

           }

           @Override public void afterTextChanged(Editable editable) {
               Timber.v(editable.toString());
               presenter.doGetBosses(editable.toString());
               etHeadName.setSelection(etHeadName.getText().length());
           }
       });
   }

    public void initList() {
        bossAdapter = new BossAdapter(this);
        rvBosses.setLayoutManager(new LinearLayoutManager(this));
        rvBosses.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        rvBosses.setAdapter(bossAdapter);


    }

    @Override
    public void successRequest(String message) {
        DialogTicket dp = DialogTicket.newInstance(IDENTIFIER_TICKETS_FORCE_APROVAL_DIALOG,message);
        dp.show(getSupportFragmentManager(), DATE_START);
    }

    @Override
    public void showRefreshBossesList(List<BossViewModel> viewModels) {
        this.bossAdapter.addList((ArrayList<BossViewModel>) viewModels);


    }

    public void successRequestDialog() {
        setResult(Activity.RESULT_OK);
                    finish();

    }


    private void funDate(int mYear, int mMonth, int mDay){
        StringBuilder sbu = new StringBuilder();
        usageDate = new StringBuffer();
        sbu.append( (mDay<10)?"0"+mDay:mDay);
        sbu.append("/");
        sbu.append((mMonth+1)<10?"0"+(mMonth+1):mMonth+1);
        sbu.append("/");
        sbu.append(mYear);
        usageDate.append(mYear);
        usageDate.append("-");
        usageDate.append((mMonth+1)<10?"0"+(mMonth+1):mMonth+1);
        usageDate.append("-");
        usageDate.append( (mDay<10)?"0"+mDay:mDay);
        tvBirthday.setText(sbu.toString());
    }
}
