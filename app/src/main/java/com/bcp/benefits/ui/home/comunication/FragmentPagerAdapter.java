package com.bcp.benefits.ui.home.comunication;

import com.bcp.benefits.viewmodel.CommunicationViewModel;

import java.util.ArrayList;
import java.util.List;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
/**
 * Created by emontesinos on 12/07/19.
 **/

public class FragmentPagerAdapter extends FragmentStatePagerAdapter {

    private List<CommunicationViewModel> communicationViewModels;

    FragmentPagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
        this.communicationViewModels = new ArrayList<>();
    }

    void setTutorialViewModels(List<CommunicationViewModel> communicationViewModels) {
        this.communicationViewModels = communicationViewModels;
        notifyDataSetChanged();
    }

    @Override
    public Fragment getItem(int position) {
        return CommunicationFragment.newInstance(communicationViewModels.get(position));
    }

    @Override
    public int getCount() {
        return communicationViewModels.size();
    }

}
