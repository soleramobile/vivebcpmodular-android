package com.bcp.benefits.ui.financial;

import androidx.annotation.NonNull;

import com.bcp.benefits.R;
import com.bcp.benefits.ui.MainView;
import com.bcp.benefits.viewmodel.BannerViewModel;
import com.bcp.benefits.viewmodel.ProductFinancialViewModel;

import java.util.List;

import javax.inject.Inject;

import pe.solera.benefit.main.entity.Banner;
import pe.solera.benefit.main.entity.FinancialProducts;
import pe.solera.benefit.main.usecases.executor.Action;
import pe.solera.benefit.main.usecases.usecases.FinancialUseCase;

/**
 * Created by emontesinos on 20/05/19.
 **/
public class FinancialPresenter {
    private FinancialView view;

    private final FinancialUseCase financialUseCase;


    @Inject
    public FinancialPresenter(FinancialUseCase financialUseCase) {
        this.financialUseCase = financialUseCase;
    }

    public void setView(@NonNull FinancialView view) {
        this.view = view;
    }

    void loadCredits() {
        if (this.view.validateInternet()) {
            this.view.showColor(R.color.home_finance_start);
            this.view.showProgress();
            this.financialUseCase.getProducts(new Action.Callback<FinancialProducts>() {
                @Override
                public void onSuccess(FinancialProducts financialProducts) {
                    view.showAllFinancialList(ProductFinancialViewModel.toList(financialProducts.getAll()));
                    if (!financialProducts.getOwned().isEmpty()) {
                        view.showOwnedListFinancial(ProductFinancialViewModel.toList(financialProducts.getOwned()));
                        view.hideProgress();
                    } else {
                        view.showEmptyList();
                        view.hideProgress();
                    }

                }

                @Override
                public void onError(Throwable throwable) {
                    view.showErrorMessage(throwable.getMessage());
                    view.hideProgress();
                }
            });
        }
    }

    void goListBanner() {
        if (this.view.validateInternet()) {
            this.view.showColor(R.color.home_finance_start);
            this.view.showProgress();
            this.financialUseCase.getBannerList(new Action.Callback<List<Banner>>() {
                @Override
                public void onSuccess(List<Banner> bannerList) {
                    view.showListBanner(BannerViewModel.bannerModels(bannerList));
                    view.hideProgress();
                }

                @Override
                public void onError(Throwable throwable) {
                    view.hideProgress();
                    view.showErrorBanner(throwable.getMessage());
                }
            });
        }
    }

    void getFinancialPhone() {
        if (this.view.validateInternet()) {
            this.financialUseCase.getPhone(new Action.Callback<String>() {
                @Override
                public void onSuccess(String phone) {
                    view.showFinancialPhone(phone);
                }

                @Override
                public void onError(Throwable throwable) {
                    view.showErrorMessage(throwable.getMessage());
                    view.hideProgress();
                }
            });
        }
    }


    public interface FinancialView extends MainView {
        void showOwnedListFinancial(List<ProductFinancialViewModel> list);

        void showAllFinancialList(List<ProductFinancialViewModel> list);

        void showListBanner(List<BannerViewModel> bannerModels);

        void showEmptyList();

        void showFinancialPhone(String phone);

        void showErrorBanner(String error);
    }
}
