package com.bcp.benefits.ui.financial;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bcp.benefits.R;
import com.bcp.benefits.util.AppUtils;
import com.bcp.benefits.viewmodel.ProductFinancialViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by emontesinos on 20/05/19.
 **/
public class ProductFinancialAdapter extends RecyclerView.Adapter<ProductFinancialAdapter.ProductViewHolder> {

    private List<ProductFinancialViewModel> list;
    private OnItemClickListener listener;

    ProductFinancialAdapter() {
        list = new ArrayList<>();
    }

    public void setList(List<ProductFinancialViewModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public ProductFinancialAdapter.ProductViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_pre_aprobado, viewGroup, false);
        return new ProductFinancialAdapter.ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductFinancialAdapter.ProductViewHolder holder, int position) {
        holder.bind(list.get(position));
        holder.btnRequest.setOnClickListener(view -> {
            if (listener != null) {
                listener.onItemClick(list.get(position), position);
            }
        });
    }

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    class ProductViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_row_credit)
        TextView textCredit;
        @BindView(R.id.text_row_quantity)
        TextView textQuantity;
        @BindView(R.id.text_row_interest)
        TextView textInterest;
        @BindView(R.id.txt_btn_request)
        TextView btnRequest;

        ProductViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(ProductFinancialViewModel entity) {
            textCredit.setText(entity.getName());
            if (entity.getApprovedAmount() != null && !entity.getApprovedAmount().isEmpty()) {
                try {
                    Double amount = Double.parseDouble(entity.getApprovedAmount());
                    textQuantity.setVisibility(View.VISIBLE);
                    textQuantity.setText(itemView.getContext().getResources().getString(R.string.simbole, AppUtils.formatPriceWithoutDecimal(amount)));
                } catch (Exception ex) {
                    textQuantity.setVisibility(View.GONE);
                }
            } else
                textQuantity.setVisibility(View.GONE);

            try {
                textInterest.setText("Tasa " + entity.getInterest() + " %");
            } catch (Exception e) {
                textInterest.setText("0 %");
            }
        }
    }


    public interface OnItemClickListener {

        void onItemClick(ProductFinancialViewModel entity, int position);
    }
}
