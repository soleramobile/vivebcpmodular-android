package com.bcp.benefits.ui.othres.buses;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bcp.benefits.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReminderAdapter extends RecyclerView.Adapter<ReminderAdapter.ReminderViewHolder> {
    private List<String> reminders;
    private ReminderListener reminderListener;
    private int positionSelected;

    ReminderAdapter(ReminderListener reminderListener) {
        this.reminders = new ArrayList<>();
        this.reminderListener = reminderListener;
    }

    void setReminders(List<String> reminders, int positionSelected) {
        this.reminders = reminders;
        this.positionSelected = positionSelected;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ReminderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_type_custom, parent, false);
        return new ReminderViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ReminderViewHolder holder, int position) {
        holder.bind(reminders.get(position));
        holder.textAddress.setOnClickListener(v ->
                reminderListener.itemSelected(reminders.get(position)));
        holder.imageCircle.setVisibility(position == positionSelected ? View.VISIBLE : View.GONE);
    }

    @Override
    public int getItemCount() {
        return reminders.size();
    }

    class ReminderViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_type_texto)
        TextView textAddress;
        @BindView(R.id.img_circle)
        ImageView imageCircle;

        ReminderViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            imageCircle.setImageDrawable(itemView.getResources().getDrawable(R.drawable.bg_circle_blue));
        }

        public void bind(String item) {
            textAddress.setText(item);
        }
    }

    public interface ReminderListener {
        void itemSelected(String item);
    }
}
