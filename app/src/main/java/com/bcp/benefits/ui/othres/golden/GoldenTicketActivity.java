package com.bcp.benefits.ui.othres.golden;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import android.os.Bundle;
import android.widget.ImageButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;

import com.bcp.benefits.analytics.AnalyticsModule;
import com.bcp.benefits.analytics.AnalyticsTags;
import com.google.android.material.appbar.AppBarLayout;
import com.bcp.benefits.R;
import com.bcp.benefits.ui.BaseActivity;
import com.bcp.benefits.ui.othres.GenericPagerAdapter;
import com.bcp.benefits.ui.tutorial.NonSwipeableViewPager;
import com.bcp.benefits.viewmodel.GoldenTicketViewModel;

import static com.bcp.benefits.util.Constants.DATE_START;
import static com.bcp.benefits.util.Constants.GOLDEN_TICKETS;
import static com.bcp.benefits.util.Constants.IDENTIFIER_TICKET;
import static com.bcp.benefits.util.Constants.IDENTIFIER_TICKETS_APROVAL_DIALOG;
import static com.bcp.benefits.util.Constants.IDENTIFIER_TICKETS_REFUSE_DIALOG;
import static com.bcp.benefits.util.Constants.RESULT_TICKET;
import static com.bcp.benefits.util.Constants.TICKETS_TO_APPROVED;

/**
 * Created by emontesinos on 30/04/19.
 **/

public class GoldenTicketActivity extends BaseActivity implements GoldenTicketPresenter.GoldenTicketView {

    @Inject
    GoldenTicketPresenter presenter;

    @BindView(R.id.toolbar)
    AppBarLayout toolbar;
    @BindView(R.id.img_back)
    ImageButton imgBack;
    @BindView(R.id.title_toolbar)
    TextView titleToobal;
    @BindView(R.id.radioGroup)
    RadioGroup mRadioGroup;
    @BindView(R.id.viewPager_ticket)
    NonSwipeableViewPager vpGoldenTicketsItems;
    private int selector;

    private AnalyticsModule analyticsModule = new AnalyticsModule(this);

    public static void start(Context context) {
        Intent intent = new Intent(context, GoldenTicketActivity.class);
        context.startActivity(intent);
    }

    @Override
    public void setUpView() {
        this.presenter.setView(this);
        sendToAnalytics();
        setToolbar();
        setUpPager();
        getGoldenTickets();
    }

    @Override
    public int getLayout() {
        return R.layout.activity_golden_ticket;
    }

    private void sendToAnalytics(){
        analyticsModule.sendToAnalytics(AnalyticsTags.Others.Events.ticketDorado, null);
    }

    private void setToolbar() {
        titleToobal.setText(R.string.golde_ticket);
        toolbar.setBackground(getResources().getDrawable(R.drawable.bg_golden_ticket));
        imgBack.setOnClickListener(v -> {
            onBackPressed();
            overridePendingTransition(R.anim.rigth_in, R.anim.right_out);
        });
    }

    private void getGoldenTickets() {
        this.presenter.doGetTickets();
    }

    private void setUpPager() {
        vpGoldenTicketsItems.setPagingEnabled(false);
        mRadioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            if (checkedId == R.id.rbMyTickets) {
                vpGoldenTicketsItems.setCurrentItem(GOLDEN_TICKETS);
                selector = GOLDEN_TICKETS;
            } else if (checkedId == R.id.rbForApproved) {
                vpGoldenTicketsItems.setCurrentItem(TICKETS_TO_APPROVED);
                selector = TICKETS_TO_APPROVED;
            }
        });
    }

    @Override
    public void showTickets(GoldenTicketViewModel goldenTicketModel) {
        GenericPagerAdapter genericPagerAdapter = new GenericPagerAdapter(getSupportFragmentManager(), IDENTIFIER_TICKET);
        vpGoldenTicketsItems.setAdapter(genericPagerAdapter);
        genericPagerAdapter.addGoldenTicketResponse(goldenTicketModel);
        if (selector == GOLDEN_TICKETS) {
            mRadioGroup.check(R.id.rbMyTickets);
            vpGoldenTicketsItems.setCurrentItem(GOLDEN_TICKETS);
        } else {
            mRadioGroup.check(R.id.rbForApproved);
            vpGoldenTicketsItems.setCurrentItem(TICKETS_TO_APPROVED);
        }
        mRadioGroup.getChildAt(1).setEnabled(goldenTicketModel.isDepartmentHead());
    }

    @Override
    public void successApprove(String message) {
        getGoldenTickets();
    }

    @Override
    public void successCancel(String message) {
        getGoldenTickets();
    }

    public void approveTicket(final int idTicket) {
        DialogTicket dp = DialogTicket.newInstance(IDENTIFIER_TICKETS_APROVAL_DIALOG, idTicket);
        dp.show(getSupportFragmentManager(), DATE_START);
    }

    public void cancelTicket(final int idTicket) {
        DialogTicket dp = DialogTicket.newInstance(IDENTIFIER_TICKETS_REFUSE_DIALOG, idTicket);
        dp.show(getSupportFragmentManager(), DATE_START);
    }

    public void approveDialogTicket(final int idTicket) {

        this.presenter.doApproveTicket(idTicket, false);
    }

    public void cancelDialogTicket(final int idTicket) {

        this.presenter.doCancelTicket(idTicket);
    }

    public void releaseTicket(final int idTicket) {
        presenter.doReleaseTicket(idTicket);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_TICKET) {
            if (resultCode == Activity.RESULT_OK || resultCode == Activity.RESULT_CANCELED) {
                getGoldenTickets();
            }
        }
    }
}
