package com.bcp.benefits.ui.discounts.detail;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.AppBarLayout;
import com.bcp.benefits.R;
import com.bcp.benefits.ui.BaseActivity;
import com.bcp.benefits.viewmodel.DiscountTypeViewModel;
import com.bcp.benefits.viewmodel.SubsidiaryViewModel;

import java.util.ArrayList;

import butterknife.BindView;

import static com.bcp.benefits.viewmodel.DiscountTypeViewModel.DISCOUNT;
import static com.bcp.benefits.viewmodel.DiscountTypeViewModel.EDUTATION;

public class SubsidiariesActivity extends BaseActivity implements SubsidiariesAdapter.OnSelectSubsidiaryListener {

    public static final String SUBSIDIARY_OPTION = "subsidiary_option";
    public static final String SUBSIDIARIES = "subsidiaries";
    public static final String DISCOUNT_TYPE_PARAM = "discount_type";
    public static final Integer PHONE_PERMISSION_REQUEST = 3000;
    public static final String ID_WAZE = "com.waze";
    public static final String ID_MAPS = "com.google.android.apps.maps";

    enum SubsidiaryOption {
        HOW_TO_GET, CALL_US
    }

    @BindView(R.id.toolbar)
    AppBarLayout toolbar;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.title_toolbar)
    TextView titleToobar;
    @BindView(R.id.rv_subsidiaries)
    RecyclerView rvSubsidiaries;

    private DiscountTypeViewModel discountType;
    private SubsidiaryOption subsidiaryOption;
    private SubsidiaryViewModel selectedSubsidiary;

    public static void start(Context context, DiscountTypeViewModel discountType, SubsidiaryOption subsidiaryOption, ArrayList<SubsidiaryViewModel> subsidiaries) {
        Intent starter = new Intent(context, SubsidiariesActivity.class);
        starter.putExtra(SUBSIDIARY_OPTION, subsidiaryOption);
        starter.putExtra(SUBSIDIARIES, subsidiaries);
        starter.putExtra(DISCOUNT_TYPE_PARAM, discountType);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        discountType = (DiscountTypeViewModel) getIntent().getSerializableExtra(DISCOUNT_TYPE_PARAM);
        setTheme(discountType == EDUTATION ? R.style.AppThemeEducation : R.style.AppThemeDiscount);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setUpView() {
        rvSubsidiaries.setLayoutManager(new LinearLayoutManager(this));
        ArrayList<SubsidiaryViewModel> subsidiaries = (ArrayList<SubsidiaryViewModel>) getIntent().getSerializableExtra(SUBSIDIARIES);
        subsidiaryOption = (SubsidiaryOption) getIntent().getSerializableExtra(SUBSIDIARY_OPTION);
        setToolbar();
        SubsidiariesAdapter adapter = new SubsidiariesAdapter(subsidiaryOption, discountType);
        adapter.addList(subsidiaries);
        adapter.addListener(this);

        rvSubsidiaries.setAdapter(adapter);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_subsidiaries;
    }

    @Override
    public void onSelectSubsidiary(SubsidiaryViewModel subsidiary) {

        this.selectedSubsidiary = subsidiary;
        if (subsidiaryOption == SubsidiaryOption.HOW_TO_GET) {
            selectNavigationApp();
        } else {
            checkForCalling();
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }


    public int resId(int first, int second) {
        return (discountType == DISCOUNT) ? first : second;
    }


    private void selectWaze() {
        String intentUriWaze = "waze://?ll=" + selectedSubsidiary.getLatitude() + ", " + selectedSubsidiary.getLongitude() + "&navigate=yes";
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(intentUriWaze));

        if (mapIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(mapIntent);
        } else {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + ID_WAZE)));
        }
    }

    private void selectGoogleMaps() {
        String position = selectedSubsidiary.getLatitude() + "," + selectedSubsidiary.getLongitude();
        Uri gmmIntentUri = Uri.parse("google.navigation:q=" + position + "&mode=d");
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");

        if (mapIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(mapIntent);
        } else {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + ID_MAPS)));
        }
    }

    private void selectNavigationApp() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.subsidiaries_select_nav_app_subtitle)
                .setItems(R.array.navigation_apps, (dialog, which) -> {
                    dialog.dismiss();
                    if (which == 0) {
                        selectGoogleMaps();
                    } else if (which == 1) {
                        selectWaze();
                    }

                });

        builder.create();

        builder.show();
    }

    private void checkForCalling() {
        if (selectedSubsidiary == null) return;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(SubsidiariesActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(SubsidiariesActivity.this, new String[]{Manifest.permission.CALL_PHONE}, PHONE_PERMISSION_REQUEST);
            } else
                call();
        } else
            call();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PHONE_PERMISSION_REQUEST)
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                call();
    }

    private void call() throws SecurityException {
        if (selectedSubsidiary == null) return;

        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + selectedSubsidiary.getPhone()));
        startActivity(intent);
    }

    public int squareFlv() {
        return resId(R.drawable.bg_green_discount, R.drawable.bg_orange_square);

    }


    public void setToolbar() {
        titleToobar.setText(subsidiaryOption == SubsidiaryOption.HOW_TO_GET ? R.string.discount_detail_how_to_get_title : R.string.discount_detail_call_us_title);


        toolbar.setBackground(getResources().getDrawable(squareFlv()));
        imgBack.setOnClickListener(v -> {
            onBackPressed();
            overridePendingTransition(R.anim.rigth_in, R.anim.right_out);
        });
    }
}
