package com.bcp.benefits.ui.campaign.aguinaldo;

import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;

import com.bcp.benefits.R;
import com.bcp.benefits.ui.BaseActivity;
import com.bcp.benefits.viewmodel.SubsidiaryViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.OnClick;

import static com.bcp.benefits.util.Constants.DATA_LOCATION_RESULT;
import static com.bcp.benefits.util.Constants.ID_CAMPAIGN_LOCATION;
import static com.bcp.benefits.util.Constants.ID_LOCATION;
import static com.bcp.benefits.util.Constants.PROVINCE_TITLE;

public class SearchActivity extends BaseActivity implements SearchAdaper.OnItemClickListener,
        SearchPresenter.SearchView {

    @Inject
    SearchPresenter presenter;
    private int idCampaign;
    private int idLocation;
    @BindView(R.id.edit_search)
    EditText editSearch;
    @BindView(R.id.rv_list_location)
    RecyclerView rvListLocation;

    @BindView(R.id.textView5)
    TextView textTitle;

    SearchAdaper subsidiaryAdapter;

    String title = "";

    @Override
    public void setUpView() {
        this.presenter.setView(this);
        idCampaign = Objects.requireNonNull(getIntent().getExtras()).getInt(ID_CAMPAIGN_LOCATION);
        idLocation = getIntent().getExtras().getInt(ID_LOCATION);
        title = getIntent().getExtras().getString(PROVINCE_TITLE);
        textTitle.setText(title);

        rvListLocation.setLayoutManager(new LinearLayoutManager(this));
        subsidiaryAdapter = new SearchAdaper(this);
        subsidiaryAdapter.setListener(this);
        rvListLocation.setAdapter(subsidiaryAdapter);
        editSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().length() >= 2) {
                    presenter.requestSuggestions(idCampaign, idLocation, editSearch.getText().toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    @Override
    public int getLayout() {
        return R.layout.dialog_search_adress;
    }

    @Override
    public void onItemClickSelect(SubsidiaryViewModel entity) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(DATA_LOCATION_RESULT, entity);
        setResult(RESULT_OK, returnIntent);
        finish();
        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_top_down);
    }

    @Override
    public void showRefreshSubsidiariesList(List<SubsidiaryViewModel> subsidiaryViewModels) {
        subsidiaryAdapter.addList((ArrayList<SubsidiaryViewModel>) subsidiaryViewModels);
    }

    @OnClick(R.id.img_close_search)
    public void onClickClose() {
        setResult(RESULT_CANCELED);
        finish();
        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_top_down);
    }

}
