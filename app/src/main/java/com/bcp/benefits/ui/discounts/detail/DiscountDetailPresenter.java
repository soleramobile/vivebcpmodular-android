package com.bcp.benefits.ui.discounts.detail;

import androidx.annotation.NonNull;

import com.bcp.benefits.R;
import com.bcp.benefits.ui.MainView;
import com.bcp.benefits.viewmodel.DiscountTypeViewModel;
import com.bcp.benefits.viewmodel.DiscountViewModel;

import javax.inject.Inject;

import pe.solera.benefit.main.entity.Discount;
import pe.solera.benefit.main.usecases.executor.Action;
import pe.solera.benefit.main.usecases.usecases.DiscountUseCases;

public class DiscountDetailPresenter {

    private final DiscountUseCases discountUseCases;
    private DiscountDetailView view;

    @Inject
    public DiscountDetailPresenter(final DiscountUseCases discountUseCases) {
        this.discountUseCases = discountUseCases;
    }

    public void setView(@NonNull DiscountDetailView view) {
        this.view = view;
    }

    void doGetDiscountDetail(DiscountTypeViewModel discountTypeViewModel, Integer discountId, Integer subsidiaryId) {
        if (this.view.validateInternet()) {
            this.view.showColor(discountTypeViewModel == DiscountTypeViewModel.DISCOUNT ? R.color.green_start : R.color.bg_yellow_start);
            this.view.showProgress();
            discountUseCases.getDiscountById(discountId, subsidiaryId, new Action.Callback<Discount>() {
                @Override
                public void onSuccess(Discount discount) {
                    view.showDetail(new DiscountViewModel(discount));
                    view.hideProgress();
                }

                @Override
                public void onError(Throwable throwable) {
                    view.showErrorMessage(throwable.getMessage());
                    view.hideProgress();
                    view.errorDiscountNotFound();
                }
            });
        }
    }

    public interface DiscountDetailView extends MainView {
        void showDetail(DiscountViewModel discountViewModel);
        void errorDiscountNotFound();
    }
}
