package com.bcp.benefits.ui.discounts.nearyoudiscounts;

import android.animation.Animator;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.TextAppearanceSpan;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import com.bcp.benefits.viewmodel.DiscountEnterType;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.bcp.benefits.R;
import com.bcp.benefits.ui.BaseFragment;
import com.bcp.benefits.ui.discounts.detail.DiscountDetailActivity;
import com.bcp.benefits.util.AppPreferences;
import com.bcp.benefits.util.AppUtils;
import com.bcp.benefits.util.CallController;
import com.bcp.benefits.util.Constants;
import com.bcp.benefits.util.LocationController;
import com.bcp.benefits.viewmodel.DiscountTypeViewModel;
import com.bcp.benefits.viewmodel.DiscountViewModel;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import pe.solera.benefit.main.entity.SortType;

import static com.bcp.benefits.util.Constants.DISCOUNT_TYPE_PARAM;
import static com.bcp.benefits.util.Constants.ID_MAPS;
import static com.bcp.benefits.util.Constants.ID_WAZE;
import static com.bcp.benefits.viewmodel.DiscountTypeViewModel.DISCOUNT;

/**
 * Created by emontesinos on 23/05/19.
 **/
public class MapNearYouDiscountFragment extends BaseFragment implements OnMapReadyCallback
        , GoogleMap.InfoWindowAdapter, GoogleMap.OnInfoWindowClickListener
        , MapNearYouDiscountPresenter.MapNearYouDiscountView, LocationController.LocationControllerListener {

    @Inject
    MapNearYouDiscountPresenter presenter;

    @BindView(R.id.content_marker_custom)
    ConstraintLayout contentMarkerCustom;
    @BindView(R.id.content_map)
    ConstraintLayout contentMap;
    @BindView(R.id.content_info)
    ConstraintLayout contentInfo;
    @BindView(R.id.tv_title)
    TextView title;
    @BindView(R.id.tv_sub_title_1)
    TextView subTitle;
    @BindView(R.id.tv_sub_title_2)
    TextView subTitleTwo;
    @BindView(R.id.tv_callus)
    TextView btnCallUs;
    @BindView(R.id.tv_how_to_get)
    TextView btnHowToGet;

    private SortType sortType = SortType.VALUE;
    private String search = "";
    private int idCategory = 0;
    private int idState = 0;
    private int idDistrict = 0;
    DiscountTypeViewModel discountType;
    private int colorSkin;
    private CallController callController;
    private GoogleMap mMap;
    private MapView mMapView;
    DiscountViewModel discountProduct;
    private Location queryLocation;
    private LocationController locationController;
    private HashMap<Marker, DiscountViewModel> markerMap = new HashMap<>();
    AppPreferences appPreferences;

    public static MapNearYouDiscountFragment newInstance(DiscountTypeViewModel discountType) {
        MapNearYouDiscountFragment fragment = new MapNearYouDiscountFragment();
        Bundle args = new Bundle();
        args.putSerializable(DISCOUNT_TYPE_PARAM, discountType);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void setUpView() {
        this.presenter.setView(this);
        locationController = new LocationController(getContext(), 1000, this);
        callController = new CallController(getContext(), null);
        colorSkin = ContextCompat.getColor(getContext(), colorSkinFlv());
        btnCallUs.setCompoundDrawablesWithIntrinsicBounds(phoneFlv(), 0, 0, 0);
        btnHowToGet.setCompoundDrawablesWithIntrinsicBounds(markerFlv(), 0, 0, 0);
        btnHowToGet.setBackground(fetchDrawable(bgFlv()));
        btnCallUs.setBackground(fetchDrawable(bgFlv()));
        btnHowToGet.setTextColor(colorSkin);
        btnCallUs.setTextColor(colorSkin);
        if (getArguments() != null) {
            discountType = (DiscountTypeViewModel) getArguments().getSerializable(DISCOUNT_TYPE_PARAM);
        }
    }

    @Override
    public int getLayout() {
        return R.layout.fragment_map;
    }

    @Override
    public void onViewCreated(@NonNull View rootView, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(rootView, savedInstanceState);
        mMapView = rootView.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume(); // needed to get the map to display immediately
        try {
            MapsInitializer.initialize(Objects.requireNonNull(getContext()));
        } catch (Exception e) {
            //do nothing
        }
        mMapView.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) throws SecurityException {
        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.setOnMyLocationButtonClickListener(() -> false);
        mMap.setInfoWindowAdapter(this);
        mMap.setOnInfoWindowClickListener(this);
        LatLng peru = new LatLng(-12.084155, -76.975721);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(peru, 5.0f));

    }

    @Override
    public void onResume() {
        super.onResume();
        if (locationController != null)
            locationController.connect();
        mMapView.onResume();
        contentMarkerCustom.setVisibility(View.GONE);
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mMap != null)
            mMap.clear();
        if (mMapView != null)
            mMapView.onDestroy();
        mMapView = null;
        super.onDestroyView();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }


    @Override
    public View getInfoWindow(Marker marker) {
        Animation animations = AnimationUtils.loadAnimation(getContext(), R.anim.traslate_in);
        Animation animationsTwo = AnimationUtils.loadAnimation(getContext(), R.anim.traslate_on);
        discountProduct = markerMap.get(marker);
        contentMarkerCustom.animate()
                .alpha(1)
                .setDuration(100)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        //Do nothing
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        if (contentMarkerCustom.getVisibility() == View.VISIBLE) {
                            contentMarkerCustom.setVisibility(View.GONE);
                            contentInfo.setVisibility(View.VISIBLE);
                            subTitleTwo.setVisibility(View.GONE);
                            contentMarkerCustom.startAnimation(animationsTwo);
                        } else {
                            if (discountProduct.getSubsidiaries() != null && discountProduct.getSubsidiaries().size() > 0) {
                                btnCallUs.setVisibility(View.VISIBLE);
                            } else {
                                btnCallUs.setVisibility(View.INVISIBLE);
                            }
                            contentMarkerCustom.startAnimation(animations);
                            contentMarkerCustom.setVisibility(View.VISIBLE);
                            contentInfo.setVisibility(View.VISIBLE);
                            subTitleTwo.setVisibility(View.GONE);

                            title.setText(discountProduct.getProvider());
                            if (discountProduct.getCanBeReviewed() != null && discountProduct.getCanBeReviewed()) {
                                StringBuilder stCaption = new StringBuilder();
                                if (discountProduct.getValue() != null)
                                    stCaption.append(discountProduct.getValue());
                                if (discountProduct.getCaption() != null) {
                                    if (discountProduct.getValue() != null)
                                        stCaption.append(Constants.SPACE);
                                    stCaption.append(discountProduct.getCaption());
                                }
                                SpannableString spannableString = new SpannableString(stCaption.toString());
                                spannableString.setSpan(new TextAppearanceSpan(getContext(), R.style.FontBold), 0, discountProduct.getValue().length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
                                spannableString.setSpan(new RelativeSizeSpan(1.25f), 0, discountProduct.getValue().length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
                                int color = colorSkinFlv();
                                spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getContext(), color)), 0, discountProduct.getValue().length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
                                subTitle.setText(spannableString);
                            } else {
                                subTitle.setText(discountProduct.getGroup());
                            }
                        }

                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                        //Do nothing
                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {
                        //Do nothing
                    }
                })
                .start();
        return null;

    }

    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }

    @Override
    public void showDiscountsForMap(List<DiscountViewModel> discountViewModels) {
        LatLng closestLocation = null;
        Float distance = null;
        int drawable = drawableForMarker();

        for (DiscountViewModel discount : discountViewModels) {
            try {
                if (discount.getLatitude() != 0.0 && discount.getLongitude() != 0.0) {
                    LatLng latLng = new LatLng(discount.getLatitude(), discount.getLongitude());
                    MarkerOptions markerOptions = new MarkerOptions();
                    markerOptions.position(latLng);
                    markerOptions.icon(AppUtils.bitmapDescriptorFromVector(getContext(), drawable));

                    Marker marker = mMap.addMarker(markerOptions);
                    markerMap.put(marker, discount);

                    if (closestLocation != null) {
                        float results[] = new float[1];
                        Location.distanceBetween(queryLocation.getLatitude(), queryLocation.getLongitude(),
                                latLng.latitude, latLng.longitude, results);
                        if (results[0] < distance) {
                            distance = results[0];
                            closestLocation = latLng;
                        }
                    } else {
                        float results[] = new float[1];
                        Location.distanceBetween(queryLocation.getLatitude(), queryLocation.getLongitude(),
                                latLng.latitude, latLng.longitude, results);
                        distance = results[0];
                        closestLocation = latLng;
                    }
                }
            } catch (Exception ex) {/*Do nothing*/}
        }

        if (closestLocation != null) {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            builder.include(new LatLng(queryLocation.getLatitude(), queryLocation.getLongitude()));
            builder.include(closestLocation);
            LatLngBounds bounds = builder.build();

            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 150);
            mMap.animateCamera(cu);
        }

    }


    private int drawableForMarker() {
        if (discountType == DiscountTypeViewModel.DISCOUNT) {
            return R.drawable.ic_marker_green;
        } else {
            return R.drawable.ic_marker_orange;
        }
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        DiscountViewModel discount = markerMap.get(marker);
        DiscountDetailActivity.start(getActivity(), discountType, discount.getIdDiscount(), discount.getIdSubsidiary(), DiscountEnterType.MAPA);
    }


    private void getDiscount(DiscountTypeViewModel type, String searchText, Integer categoryId, Integer state, Integer district) {
        if (mMap != null)
            mMap.clear();

        markerMap.clear();

        String latitude = null, longitude = null;
        if (queryLocation != null) {
            latitude = String.valueOf(queryLocation.getLatitude());
            longitude = String.valueOf(queryLocation.getLongitude());
        }
        if (AppUtils.isConnected(getContext())) {
            presenter.doFilterDiscountsForMap(type, searchText, categoryId, state, district, latitude, longitude);
        }

    }

    @Override
    public void onGetLocationCompleted(Location location) throws SecurityException {

        queryLocation = location;
        mMap.setMyLocationEnabled(true);
        LatLng currentLocation = new LatLng(location.getLatitude(), location.getLongitude());
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 16.0f));
        locationController.disconnect();
        getDiscount(discountType, search, idCategory, idState, idDistrict);

    }


    public void setSortType(SortType sortType) {
        this.sortType = sortType;
    }

    public void setDiscountType(DiscountTypeViewModel discountType) {
        this.discountType = discountType;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public void setIdCategory(int idCategory) {
        this.idCategory = idCategory;
    }

    public void setIdState(int idState) {
        this.idState = idState;
    }

    public void setIdDistrict(int idDistrict) {
        this.idDistrict = idDistrict;
    }


    @OnClick(R.id.tv_how_to_get)
    public void onTvHowToGetClicked() {
        if (discountProduct != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle(R.string.subsidiaries_select_nav_app_subtitle)
                    .setItems(R.array.navigation_apps, (dialog, which) -> {
                        dialog.dismiss();
                        if (which == 0) {
                            selectGoogleMaps();
                        } else if (which == 1) {
                            selectWaze();
                        }

                    });

            builder.create();

            builder.show();
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        callController.onRequestPermissionsResult(requestCode, grantResults);
    }


    @OnClick(R.id.tv_callus)
    public void onTvCallusClicked() {
        if (discountProduct != null) {
            if (discountProduct.getSubsidiaries() != null && discountProduct.getSubsidiaries().size() > 0) {
                callController.doCall(discountProduct.getSubsidiaries().get(0).getPhone());
            } else {
                AppUtils.showErrorMessage(contentMap, getContext(), getResources().getString(R.string.number_empty));
            }
        }

    }

    @OnClick(R.id.content_marker_custom)
    void onClickDetailClinic() {
        if (discountProduct != null) {
            DiscountDetailActivity.start(getActivity(), discountType, discountProduct.getIdDiscount(), discountProduct.getIdSubsidiary(), DiscountEnterType.MAPA);

        }
    }

    @Override
    public void onLocationPermissionDenied() {
        //do nothing
    }

    @Override
    public void onGpsUnavailable() {
        //do nothing
    }

    @Override
    public void onLocationUpdate(Location location) {
        //do nothing
    }

    @Override
    public void onError(LocationController.LocationManagerError error) {
        //do nothing
    }

    public int resId(int first, int second) {
        return (discountType == DISCOUNT) ? first : second;
    }

    int phoneFlv() {
        return resId(R.drawable.ic_call_green, R.drawable.ic_call_yellow_two);

    }

    int markerFlv() {
        return resId(R.drawable.ic_location_green, R.drawable.ic_location_yellow);
    }

    public int colorSkinFlv() {
        return resId(R.color.bg_green_start, R.color.bg_yellow_start);
    }

    public int bgFlv() {
        return resId(R.drawable.bg_white_green_corners_stroke_detail, R.drawable.bg_white_corners_yellow_stroke_detail);

    }

    public Drawable fetchDrawable(int id) {
        return getResources().getDrawable(id);
    }


    private void selectWaze() {
        String intentUriWaze = "waze://?ll=" + discountProduct.getLatitude() + ", " + discountProduct.getLongitude() + "&navigate=yes";
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(intentUriWaze));

        if (mapIntent.resolveActivity(getContext().getPackageManager()) != null) {
            startActivity(mapIntent);
        } else {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + ID_WAZE)));
        }
    }

    private void selectGoogleMaps() {
        String position = discountProduct.getLatitude() + "," + discountProduct.getLongitude();
        Uri gmmIntentUri = Uri.parse("google.navigation:q=" + position + "&mode=d");
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");

        if (mapIntent.resolveActivity(getContext().getPackageManager()) != null) {
            startActivity(mapIntent);
        } else {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + ID_MAPS)));
        }
    }


}
