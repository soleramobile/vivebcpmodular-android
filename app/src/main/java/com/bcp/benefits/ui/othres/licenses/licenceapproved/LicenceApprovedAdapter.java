package com.bcp.benefits.ui.othres.licenses.licenceapproved;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.bcp.benefits.R;
import com.bcp.benefits.ui.othres.licenses.LicencesActivity;
import com.bcp.benefits.viewmodel.ForApprovalViewModel;

/**
 * Created by emontesinos on 08/05/19.
 **/

public class LicenceApprovedAdapter extends RecyclerView.Adapter<LicenceApprovedAdapter.LicenceApprovedViewHolder> {

    private ArrayList<ForApprovalViewModel> forApprovalModels;
    private final Context context;

    void addList(ArrayList<ForApprovalViewModel> forApprovalModels){
        this.forApprovalModels = forApprovalModels;
        notifyDataSetChanged();
    }

    LicenceApprovedAdapter(Context context){
        this.context = context;
    }

    @NonNull
    @Override
    public LicenceApprovedAdapter.LicenceApprovedViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_list_approved, viewGroup, false);
        return new LicenceApprovedAdapter.LicenceApprovedViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull LicenceApprovedAdapter.LicenceApprovedViewHolder holder, int position) {
        ForApprovalViewModel approvalModel = forApprovalModels.get(position);
      /*  holder.tvName.setText(approvalModel.getUser().getName());
        holder.tvDate.setText(approvalModel.getUsageDate());

        holder.tvDurationDetail.setText(approvalModel.getDurationDetailText());
        holder.tvDurationDetail2.setText(approvalModel.getDurationText());

        holder.tvPosition.setText(approvalModel.getUser().getPosition());*/
        holder.tvAccept.setOnClickListener(view -> ((LicencesActivity)context).approveTicket(approvalModel.getIdTicket()));
        holder.tvCancel.setOnClickListener(view -> ((LicencesActivity)context).cancelTicket(approvalModel.getIdTicket()));

    }

    @Override
    public int getItemCount() {
        return forApprovalModels.size();
    }

    class LicenceApprovedViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvDurationDetailprincipal)
        TextView tvDurationDetail;
        @BindView(R.id.tvName)
        TextView tvName;
        @BindView(R.id.tvPosition)
        TextView tvPosition;
        @BindView(R.id.tvDate)
        TextView tvDate;
        @BindView(R.id.tvDuration_detail)
        TextView tvDurationDetail2;
        @BindView(R.id.tvAccept)
        TextView tvAccept;
        @BindView(R.id.tvCancel)
        TextView tvCancel;
        LicenceApprovedViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }
}
