package com.bcp.benefits.ui.tutorial;

import android.content.Context;
import android.content.Intent;
import android.widget.Button;
import android.widget.ImageView;

import androidx.viewpager.widget.ViewPager;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

import com.bcp.benefits.R;
import com.bcp.benefits.ui.BaseActivity;
import com.bcp.benefits.ui.validate.ValidateActivity;
import com.bcp.benefits.util.AppPreferences;
import com.bcp.benefits.viewmodel.TutorialViewModel;

import static com.bcp.benefits.util.Constants.VALIDATED_GO;
import static com.bcp.benefits.util.Constants.VALIDATE_VIEW;

/**
 * Created by emontesinos.
 **/

public class TutorialActivity extends BaseActivity implements TutorialPresenter.TutorialView {

    @BindView(R.id.viewpager)
    NonSwipeableViewPager mViewPager;
    @BindView(R.id.btn_close)
    ImageView btnClose;
    @BindView(R.id.btn_next)
    Button btnNext;

    @Inject
    TutorialPresenter presenter;

    private TutorialPagerAdapter adapter;
    private ArrayList<TutorialViewModel> listTutorial = new ArrayList<>();

    public static void start(Context context) {
        Intent intent = new Intent(context, TutorialActivity.class);
        context.startActivity(intent);
    }

    @Override
    public void setUpView() {
        this.setUpViewPager();
        this.presenter.setView(this);
        this.presenter.loadTutorial();
    }

    private void setUpViewPager() {
        AppPreferences appPreferences = new AppPreferences(TutorialActivity.this);
        appPreferences.savePreference(VALIDATE_VIEW, VALIDATED_GO);
        adapter = new TutorialPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(adapter);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (position == 0){
                    btnNext.setBackgroundResource(R.drawable.bg_button_banner_type_1);
                }else {
                    btnNext.setBackgroundResource(R.drawable.bg_button_banner_type_2);
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public int getLayout() {
        return R.layout.activity_tutorial;
    }

    @Override
    public void showTutorialList(List<TutorialViewModel> tutorialViewModels) {
        this.adapter.setTutorialViewModels(tutorialViewModels);
        listTutorial = (ArrayList<TutorialViewModel>) tutorialViewModels;
        this.setColorNext();
    }

    private void setColorNext() {
        //btnNext.setImage(AppUtils.getId(this,
        //      listTutorial.get(mViewPager.getCurrentItem()).getColorButton()));
    }

    @OnClick(R.id.btn_close)
    void onClickClose() {
        Intent intent = new Intent(this, ValidateActivity.class);
        startActivity(intent);
    }

    private void nextButton() {
        if (mViewPager.getCurrentItem() == adapter.getCount() - 1) {
            Intent intent = new Intent(this, ValidateActivity.class);
            startActivity(intent);
        } else {
            mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1, true);
            setColorNext();
        }
        if (adapter.getCount() == listTutorial.size()) {

        }
    }

    @OnClick(R.id.btn_next)
    void OnClickNext() {
        nextButton();
    }
}
