package com.bcp.benefits.ui.othres.buses;

import com.bcp.benefits.ui.MainView;
import com.bcp.benefits.viewmodel.BusScheduleViewModel;
import com.bcp.benefits.viewmodel.BusViewModel;

import java.util.List;

import javax.inject.Inject;

import pe.solera.benefit.main.entity.Address;
import pe.solera.benefit.main.entity.Bus;
import pe.solera.benefit.main.entity.BusSchedule;
import pe.solera.benefit.main.usecases.executor.Action;
import pe.solera.benefit.main.usecases.usecases.AppUseCase;
import pe.solera.benefit.main.usecases.usecases.BusesUseCase;

public class BusesPresenter {
    private final AppUseCase appUseCase;
    private final BusesUseCase busesUseCase;
    private BusesView view;

    @Inject
    public BusesPresenter(AppUseCase appUseCase, BusesUseCase busesUseCase) {
        this.appUseCase = appUseCase;
        this.busesUseCase = busesUseCase;
    }

    void setBusesView(BusesView busesView) {
        this.view = busesView;
    }

    void loadBuses() {
        if (this.view.validateInternet()) {
            this.view.showProgress();
            appUseCase.getBuses(new Action.Callback<List<Bus>>() {
                @Override
                public void onSuccess(List<Bus> busList) {
                    view.showBuses(BusViewModel.toList(busList));
                    view.hideProgress();
                }

                @Override
                public void onError(Throwable throwable) {
                    view.showErrorMessage(throwable.getMessage());
                    view.hideProgress();
                }
            });
        }
    }

    void updateBuses(List<BusViewModel> busViewModels) {
        view.showBuses(busViewModels);
    }

    void loadAddress(Integer type) {
        if (this.view.validateInternet()) {
            this.view.showProgress();
            busesUseCase.getAddress(type, new Action.Callback<List<Address>>() {
                @Override
                public void onSuccess(List<Address> addressList) {
                    view.showAddress(addressList);
                    view.hideProgress();
                }

                @Override
                public void onError(Throwable throwable) {
                    view.showErrorMessage(throwable.getMessage());
                    view.hideProgress();
                }
            });
        }
    }

    void loadArriveSubsidiaries(Integer addressId) {
        if (this.view.validateInternet()) {
            this.view.showProgress();
            busesUseCase.getArrivalAddress(addressId, new Action.Callback<List<Address>>() {
                @Override
                public void onSuccess(List<Address> addressList) {
                    view.showArriveAddress(addressList);
                    view.hideProgress();
                }

                @Override
                public void onError(Throwable throwable) {
                    view.showErrorMessage(throwable.getMessage());
                    view.hideProgress();
                }
            });
        }
    }

    void loadSchedule(Integer type, Integer addressId, Integer arrivalAddressId) {
        if (this.view.validateInternet()) {
            this.view.showProgress();
            busesUseCase.getBusSchedule(type, addressId, arrivalAddressId, new Action.Callback<BusSchedule>() {
                @Override
                public void onSuccess(BusSchedule busSchedule) {
                    view.showSchedule(new BusScheduleViewModel(busSchedule));
                    view.hideProgress();
                }

                @Override
                public void onError(Throwable throwable) {
                    view.showErrorMessage(throwable.getMessage());
                    view.hideProgress();
                }
            });
        }
    }

    void loadReminders() {
        busesUseCase.getReminders(new Action.Callback<List<String>>() {
            @Override
            public void onSuccess(List<String> reminders) {
                view.showReminders(reminders);
                view.hideProgress();
            }

            @Override
            public void onError(Throwable throwable) {
                view.showErrorMessage(throwable.getMessage());
                view.hideProgress();
            }
        });
    }

    public interface BusesView extends MainView {
        void showBuses(List<BusViewModel> busList);

        void showAddress(List<Address> addressList);

        void showArriveAddress(List<Address> addressList);

        void showSchedule(BusScheduleViewModel busSchedule);

        void showReminders(List<String> reminders);
    }
}
