package com.bcp.benefits.ui.othres.licenses.requestlicence;

import android.Manifest;
import android.animation.Animator;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bcp.benefits.R;
import com.bcp.benefits.ui.BaseActivity;
import com.bcp.benefits.ui.othres.golden.request.BossAdapter;
import com.bcp.benefits.ui.othres.licenses.DialogLicence;
import com.bcp.benefits.util.AnimationUtils;
import com.bcp.benefits.util.AppPreferences;
import com.bcp.benefits.util.AppUtils;
import com.bcp.benefits.viewmodel.BossViewModel;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

import static com.bcp.benefits.util.Constants.DATE_START;
import static com.bcp.benefits.util.Constants.HEADNAME;
import static com.bcp.benefits.util.Constants.IDENTIFIER_PERMISSION_DIALOG;
import static com.bcp.benefits.util.Constants.IDTICKET;
import static com.bcp.benefits.util.Constants.TYPE_BIRDAY;
import static com.bcp.benefits.util.Constants.TYPE_KILL;
import static com.bcp.benefits.util.Constants.TYPE_MATERNITY;
import static com.bcp.benefits.util.Constants.TYPE_MEDIC;

/**
 * Created by emontesinos on 08/05/19.
 **/
public class LicenceRequestActivity extends BaseActivity implements DatePickerDialog.OnDateSetListener,
        BossAdapter.OnSelectBossItemListener, LicenceRequestPresenter.LicenceRequestView {

    @BindView(R.id.et_birthday)
    EditText tvBirthday;
    @BindView(R.id.etHeadName)
    EditText etHeadName;
    @BindView(R.id.ll_search)
    LinearLayout llSearcher;
    @BindView(R.id.llDurationContainer)
    LinearLayout llDurationContainer;
    @BindView(R.id.rv_bosses)
    RecyclerView rvBosses;
    @BindView(R.id.content_permission)
    ConstraintLayout contentPermissionView;
    @BindView(R.id.text_message_photo)
    TextView messageCamera;
    @BindView(R.id.text_img_camera)
    TextView imgCamera;
    @BindView(R.id.id_scroll)
    NestedScrollView scroll;

    private final static int MY_PERMISSION_REQUEST_CAMERA = 2000;
    Calendar c;
    private String pathImage;
    private String identifierPathImage;
    private DatePickerDialog dpdBirtday;
    private int idTicket;
    private StringBuffer usageDate;
    private BossViewModel selectedBoss;
    private BossAdapter bossAdapter;
    private boolean searcherIsShown = false;
    private String departament;
    private AppPreferences appPreferences;


    @Inject
    LicenceRequestPresenter presenter;

    @Override
    public void setUpView() {
        this.presenter.setView(this);
        appPreferences = new AppPreferences(this);
        c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        funDate(mYear, mMonth, mDay);
        AppUtils.hideSoftKeyboard(this);
        dpdBirtday = new DatePickerDialog(this, R.style.datepicker, this, mYear, mMonth, mDay);
        etHeadName.setText(R.string.test_sub_title);
        idTicket = getIntent().getIntExtra(IDTICKET, 0);
        departament = getIntent().getStringExtra(HEADNAME);
        getView();
        bossAdapter = new BossAdapter(this);
        rvBosses.setLayoutManager(new LinearLayoutManager(this));
        rvBosses.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        rvBosses.setAdapter(bossAdapter);

    }

    @Override
    public int getLayout() {
        return R.layout.activity_request_ticket;
    }

    @Override
    public void showBoss(@NonNull ArrayList<BossViewModel> bossModelList) {
        this.bossAdapter.addList(bossModelList);
    }

    @Override
    public void successRequest(String message) {
        DialogLicence dp = DialogLicence.newInstance(IDENTIFIER_PERMISSION_DIALOG, message);
        dp.show(getSupportFragmentManager(), DATE_START);
    }

    public void requestSuccess() {
        setResult(Activity.RESULT_OK);
        appPreferences.savePreference("identifierPathImage", "");
        finish();
    }

    @OnClick(R.id.et_birthday)
    public void onSelectBirthday() {
        dpdBirtday.show();
    }

    @OnClick(R.id.etHeadName)
    public void onClickName() {
        if (searcherIsShown) {
            scaleUpSearcher();
        } else {
            scaleDownSearcher();
        }
    }

    @OnClick(R.id.ivClose)
    public void onBackFromClose() {
        onBackPressed();
    }

    @OnClick(R.id.btnAccept)
    public void onAccept() {
        if (usageDate == null) {
            AppUtils.showErrorMessage(scroll, this, getResources().getString(R.string.request_golden_ticket_date_empty));
            return;
        }

        Integer idUserDepartmentHead = null;
        if (Objects.requireNonNull(etHeadName.getText()).toString().isEmpty()) {
            selectedBoss = null;
        } else {
            if (selectedBoss != null)
                idUserDepartmentHead = selectedBoss.idUser;
        }
        if (identifierPathImage.isEmpty()) {
            AppUtils.showErrorMessage(scroll, this, getResources().getString(R.string.request_photo_empty));
            return;
        }

        String date = usageDate.toString();
        presenter.doRequest(idTicket, 1, date, idUserDepartmentHead);

    }

    private void scaleUpSearcher() {
        rvBosses.setVisibility(View.GONE);
        bossAdapter.addList(null);
        AnimationUtils.extendHeightView(llSearcher.getTop(), etHeadName.getBottom(), llSearcher,
                400, new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {
                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {
                        searcherIsShown = false;
                        if (selectedBoss != null)
                            etHeadName.setText(selectedBoss.name);
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {
                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {
                    }
                }).start();
    }

    private void scaleDownSearcher() {
        AnimationUtils.extendHeightView(etHeadName.getBottom(), AppUtils.convertDpToPx(this, 170), llSearcher,
                300, new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {
                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {
                        searcherIsShown = true;
                        etHeadName.setSelection(Objects.requireNonNull(etHeadName.getText()).toString().length());
                        rvBosses.setVisibility(View.VISIBLE);
                        presenter.loadBoss();
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {
                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {
                    }
                }).start();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        view.updateDate(year, month, dayOfMonth);
        StringBuilder sbu = new StringBuilder();
        usageDate = new StringBuffer();
        sbu.append((dayOfMonth < 10) ? "0" + dayOfMonth : dayOfMonth);
        sbu.append("/");
        sbu.append((month + 1) < 10 ? "0" + (month + 1) : month + 1);
        sbu.append("/");
        sbu.append(year);
        usageDate.append(year);
        usageDate.append("-");
        usageDate.append((month + 1) < 10 ? "0" + (month + 1) : month + 1);
        usageDate.append("-");
        usageDate.append((dayOfMonth < 10) ? "0" + dayOfMonth : dayOfMonth);
        tvBirthday.setText(sbu.toString());
    }


    private void funDate(int mYear, int mMonth, int mDay) {
        StringBuilder sbu = new StringBuilder();
        usageDate = new StringBuffer();
        sbu.append((mDay < 10) ? "0" + mDay : mDay);
        sbu.append("/");
        sbu.append((mMonth + 1) < 10 ? "0" + (mMonth + 1) : mMonth + 1);
        sbu.append("/");
        sbu.append(mYear);
        usageDate.append(mYear);
        usageDate.append("-");
        usageDate.append(((mMonth + 1) >= 10) ? mMonth + 1 : "0" + (mMonth + 1));
        usageDate.append("-");
        usageDate.append((mDay < 10) ? "0" + mDay : mDay);
        tvBirthday.setText(sbu.toString());
    }

    @Override
    public void selectBossItem(BossViewModel boss) {
        selectedBoss = boss;
        etHeadName.setText(boss.name);
        scaleUpSearcher();
    }

    public void getView() {
        switch (departament) {
            case TYPE_MEDIC:
                contentPermissionView.setVisibility(View.VISIBLE);
                messageCamera.setText(AppUtils.getStyleText(this, 49, getResources().getString(R.string.txt_medic_certificate), 71));
                messageCamera.setMovementMethod(LinkMovementMethod.getInstance());
                break;
            case TYPE_BIRDAY:
                contentPermissionView.setVisibility(View.GONE);
                break;
            case TYPE_KILL:
                contentPermissionView.setVisibility(View.VISIBLE);
                messageCamera.setText(AppUtils.getStyleText(this, 56, getResources().getString(R.string.txt_death_certificate), 79));
                messageCamera.setMovementMethod(LinkMovementMethod.getInstance());
                break;
            case TYPE_MATERNITY:
                contentPermissionView.setVisibility(View.VISIBLE);
                messageCamera.setText(AppUtils.getStyleText(this, 58, getResources().getString(R.string.txt_birth_certificate), 80));
                messageCamera.setMovementMethod(LinkMovementMethod.getInstance());
                break;
        }
    }

    @OnClick(R.id.text_img_camera)
    public void OnClickPhoto() {
        checkCameraAndWriteStoragePermissions();
    }

    private void checkCameraAndWriteStoragePermissions() {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP_MR1) {
            chooseCameraOptions(this, MY_PERMISSION_REQUEST_CAMERA, "imagenes");
            return;
        }

        String[] permissions = getNeededPermissions();

        if (permissions.length == 0) {
            chooseCameraOptions(this, MY_PERMISSION_REQUEST_CAMERA, "imagenes");
            return;
        }
        ActivityCompat.requestPermissions(this, permissions, MY_PERMISSION_REQUEST_CAMERA);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NotNull String permissions[], @NotNull int[] grantResults) {
        if (requestCode == MY_PERMISSION_REQUEST_CAMERA) {
            if (grantResults.length == 2 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                    grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                chooseCameraOptions(this, MY_PERMISSION_REQUEST_CAMERA, "imagenes");
                return;
            }

            if (grantResults.length == 1 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                chooseCameraOptions(this, MY_PERMISSION_REQUEST_CAMERA, "imagenes");
                return;
            }
            Toast.makeText(this, getString(R.string.permissions_needed), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        assert data != null;
        File dirImages = new File(getFilesDir(), "personalcetificate");
        dirImages.deleteOnExit();
        File picture = new File(getFilesDir() + "/personalcetificate");
        picture.mkdir();
        pathImage = guardarImagen(picture, (Bitmap) Objects.requireNonNull(data.getExtras()).get("data"));
        appPreferences.savePreference("identifierPathImage", pathImage);

    }

    private String[] getNeededPermissions() {
        ArrayList<String> permissions = new ArrayList<>();

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
            permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
            permissions.add(Manifest.permission.CAMERA);

        return permissions.toArray(new String[0]);
    }

    public void chooseCameraOptions(Activity context, int requestCode, String title) {
        ArrayList<Intent> cameraIntents = new ArrayList<>();
        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        PackageManager packageManager = context.getPackageManager();
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);

        for (ResolveInfo res : listCam) {
            String packageName = res.activityInfo.packageName;
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(packageName);
            cameraIntents.add(intent);
        }

        Intent galleryIntent = new Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        );

        Intent chooserIntent = Intent.createChooser(galleryIntent, title);
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[0]));
        context.startActivityForResult(chooserIntent, requestCode);
    }

    private void UnableToSave() {
        AppUtils.showErrorMessage(scroll, this, getResources().getString(R.string.request_photo_error));
    }

    private void AbleToSave() {
        AppUtils.showErrorMessageSucces(scroll, this, getResources().getString(R.string.request_photo_succes));
    }

    public String guardarImagen(File picture, Bitmap imagen) {
        File myPath = new File(picture, "" + c.getTimeInMillis() + ".png");
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(myPath);
            imagen.compress(Bitmap.CompressFormat.JPEG, 10, fos);
            fos.flush();
            AbleToSave();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
            UnableToSave();
        } catch (IOException ex) {
            ex.printStackTrace();
            UnableToSave();
        }
        return myPath.getAbsolutePath();
    }

    @Override
    protected void onResume() {
        super.onResume();
        identifierPathImage = appPreferences.getString("identifierPathImage");
        if (!identifierPathImage.isEmpty()) {
            imgCamera.setBackground(getResources().getDrawable(R.drawable.bg_white_corner_error));
            pathImage = identifierPathImage;
        } else {
            imgCamera.setBackground(getResources().getDrawable(R.drawable.bg_corners_green_stroke));
        }
    }
}
