package com.bcp.benefits.ui.campaign.volunteering;


import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bcp.benefits.R;
import com.bcp.benefits.analytics.AnalyticsModule;
import com.bcp.benefits.analytics.AnalyticsTags;
import com.bcp.benefits.ui.BaseActivity;
import com.bcp.benefits.ui.campaign.volunteering.adapter.VolunteeringAdapter;
import com.bcp.benefits.ui.campaign.volunteering.dialogs.OptionsDialogFragment;
import com.bcp.benefits.ui.campaign.volunteering.dialogs.adapter.ListOptionsAdapter;
import com.bcp.benefits.util.AppUtils;
import com.bcp.benefits.util.Constants;
import com.bcp.benefits.viewmodel.ListRoleViewModel;
import com.bcp.benefits.viewmodel.VolunteeringViewModel;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import timber.log.Timber;

import static com.bcp.benefits.util.Constants.DATA_DESCRIPTION_CAMPAIGN_VOLUNTEERING;
import static com.bcp.benefits.util.Constants.DATA_TITLE_CAMPAIGN_VOLUNTEERING;
import static com.bcp.benefits.util.Constants.ENABLE_LOCAL;
import static com.bcp.benefits.util.Constants.ENABLE_ROLE;
import static com.bcp.benefits.util.Constants.ID_CAMPAIGN_PARAM;

public class VolunteeringActivity extends BaseActivity implements VolunteeringPresenter.VolunteeringView, VolunteeringAdapter.ListenerViewVolunteeringAdapter, OptionsDialogFragment.SelectedIdListener {


    @Inject
    VolunteeringPresenter presenter;

    @BindView(R.id.rvVolunteering)
    RecyclerView rvVolunteering;
    @BindView(R.id.btnEnviarVolunteering)
    Button btnEnviarVolunteering;
    @BindView(R.id.img_close_volunteering)
    ImageView imgBack;

    @BindView(R.id.titleVolunteering)
    TextView titleVolunteering;
    @BindView(R.id.descriptionVolunteering)
    TextView descriptionVolunteering;


    @BindView(R.id.content_volunteering)
    ConstraintLayout contentVolunteering;

    private VolunteeringAdapter volunteeringAdapter;
    private String idVolunteering = "";
    private String tittle = "";
    private String description = "";
    private Integer enableRole = 0;
    private Integer enableLocal = 0;
    private Integer idRoleVolunteer = null;
    private Integer idLocalVolunteer = null;
    private String nameLocal;
    private String nameRole;
    private String type;
    private int position;
    private int id;

    public static void start(Activity context, int idCampaign, String title, String description, Integer enableRole, Integer enableLocal) {
        Intent starter = new Intent(context, VolunteeringActivity.class);
        starter.putExtra(ID_CAMPAIGN_PARAM, idCampaign);
        starter.putExtra(DATA_TITLE_CAMPAIGN_VOLUNTEERING, title);
        starter.putExtra(DATA_DESCRIPTION_CAMPAIGN_VOLUNTEERING, description);
        starter.putExtra(ENABLE_ROLE, enableRole);
        starter.putExtra(ENABLE_LOCAL, enableLocal);
        context.startActivityForResult(starter, 100);
    }

    @Override
    public void setUpView() {
        this.presenter.setView(this);
        int campaignId = getIntent().getIntExtra(ID_CAMPAIGN_PARAM, 0);
        id = campaignId;
        tittle = getIntent().getStringExtra(DATA_TITLE_CAMPAIGN_VOLUNTEERING);
        description = getIntent().getStringExtra(DATA_DESCRIPTION_CAMPAIGN_VOLUNTEERING);
        this.enableRole = getIntent().getIntExtra(ENABLE_ROLE, 0);
        this.enableLocal = getIntent().getIntExtra(ENABLE_LOCAL, 0);
        this.presenter.getListVolunteering(String.valueOf(campaignId));
        getToolbar();
        volunteeringAdapter = new VolunteeringAdapter(this, this, this.enableRole, this.enableLocal);
        rvVolunteering.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        rvVolunteering.setAdapter(volunteeringAdapter);
    }

    private void getToolbar() {

        imgBack.setOnClickListener(v -> onBackPressed());
        btnEnviarVolunteering.setEnabled(false);
        titleVolunteering.setText(tittle);
        descriptionVolunteering.setText(getResources().getString(R.string.description_volun));
    }

    @Override
    public int getLayout() {
        return R.layout.activity_volunteering;
    }


    @Override
    public void showListVolunteering(List<VolunteeringViewModel> volunteeringViewModels) {
        volunteeringAdapter.addList(volunteeringViewModels);
    }


    @OnClick(R.id.btnEnviarVolunteering)
    public void onClickEnviarSolicitud() {
        if (!idVolunteering.isEmpty()) {
            if (enableRole == 1 && idRoleVolunteer == null){
                Toast.makeText(this,"Selecciona un rol", Toast.LENGTH_SHORT).show();
                return;
            }
            if (enableLocal == 1 && idLocalVolunteer == null){
                Toast.makeText(this,"Selecciona un local", Toast.LENGTH_SHORT).show();
                return;
            }

            if (enableRole == 0)
                presenter.requestVolunteering(idVolunteering, enableLocal, enableRole, null, idLocalVolunteer);
            else {
                if (enableLocal == 0)
                    presenter.requestVolunteering(idVolunteering, enableLocal, enableRole, idRoleVolunteer, null);
                else {
                    presenter.requestVolunteering(idVolunteering, enableLocal, enableRole, idRoleVolunteer, idLocalVolunteer);
                }
            }

            Log.d("SOLICITUD ENVIADA", "idVolunteering -> " + idVolunteering +
                    "\nenableLocal -> " + enableLocal +
                    "\nenableRole -> " + enableRole +
                    "\nidRoleVolunteer -> " + idRoleVolunteer +
                    "\nidLocalVolunteer -> " + idLocalVolunteer);
        } else {
            AppUtils.showErrorMessage(contentVolunteering, this, getResources().getString(R.string.txt_unselected_volunteering));
        }
    }

    @Override
    public void showSuccessVolunteering(Boolean response) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("idCampania", id);
        setResult(RESULT_OK, returnIntent);
        finish();
    }

    @Override
    public void showEmptyList() {

    }

    @Override
    public void selectProduct(VolunteeringViewModel select) {
        idVolunteering = String.valueOf(select.getIdVolunteer());
        if (!idVolunteering.isEmpty()) {
            ableButton();
        }

    }

    public void ableButton(){
        btnEnviarVolunteering.setEnabled(true);
        btnEnviarVolunteering.setBackground(getResources().getDrawable(R.drawable.sl_enter_campaign_button));
    }

    @Override
    public void notifyClickRole(VolunteeringViewModel volunteeringViewModel, Boolean click, String type, int position) {
        if (click) {
            Log.d("CLICK DETECTED: ", "Hizo click en escoger rol");
            this.position = position;
            if (volunteeringViewModel.getListRole() != null && volunteeringViewModel.getListRole().size() != 0) {
                DialogFragment df = OptionsDialogFragment.newInstance(getResources().getString(R.string.select_rol), volunteeringViewModel.getListRole(), type);
                df.show(getSupportFragmentManager().beginTransaction(), "ROL OPTIONS DIALOG");
            } else {
                Toast.makeText(this, "Aún no cargamos eventos, intenta nuevamente más tarde", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void notifyClickPlace(VolunteeringViewModel volunteeringViewModel, Boolean click, String type, int position) {
        if (click) {
            Log.d("CLICK DETECTED: ", "Hizo click en escoger local");
            this.position = position;
            if (volunteeringViewModel.getListLocal() != null && volunteeringViewModel.getListLocal().size() != 0) {
                DialogFragment df = OptionsDialogFragment.newInstance(getResources().getString(R.string.select_place), volunteeringViewModel.getListLocal(), type);
                df.show(getSupportFragmentManager().beginTransaction(), "PLACE OPTIONS DIALOG");
            } else {
                Toast.makeText(this, "Aún no cargamos eventos, intenta nuevamente más tarde", Toast.LENGTH_SHORT).show();
            }

        }
    }

    @Override
    public void selectedId(int id, String name, String type) {
        if (type.equals(Constants.TYPE_ROL)) {
            this.idRoleVolunteer = id;
            this.nameRole = name;
            volunteeringAdapter.changeTextRol(position, nameRole);
        }
        else {
            this.idLocalVolunteer = id;
            this.nameLocal = name;
            volunteeringAdapter.changeTextLocal(position, nameLocal);
        }

        this.type = type;
        Log.d("CLICK DETECTED ", "Id de lo seleccionado -> " + id);
    }
}
