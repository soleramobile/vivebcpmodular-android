package com.bcp.benefits.ui.campaign.aguinaldo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bcp.benefits.R;
import com.bcp.benefits.viewmodel.SubsidiaryViewModel;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by emontesinos on 11/06/19.
 */

public class SubsidiaryAdapter extends RecyclerView.Adapter<SubsidiaryAdapter.SubsidiaryViewHolder> {

    private ArrayList<SubsidiaryViewModel> list;
    private OnItemClickListener listener;
    private int selectedItem = -1;

    private Context context;

    public SubsidiaryAdapter(Context context, ArrayList<SubsidiaryViewModel> list) {
        this.list = list;
        this.context = context;
    }

    public void setSelectedItem(SubsidiaryViewModel subsidiaryViewModel) {
        if (subsidiaryViewModel == null) return;
        int index = list.indexOf(subsidiaryViewModel);
        if (selectedItem != index) {
            selectedItem = index;
            notifyDataSetChanged();
        }
    }

    @NotNull
    @Override
    public SubsidiaryAdapter.SubsidiaryViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_type_custom, parent, false);
        return new SubsidiaryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NotNull SubsidiaryViewHolder holder, int position) {

        SubsidiaryViewModel subsidiaryViewModel = list.get(position);
        holder.text.setText(subsidiaryViewModel.getName());
        if (selectedItem == position) {
            holder.imgCircle.setBackground(context.getResources().getDrawable(R.drawable.bg_circle_sky_blue));
        } else {
            holder.imgCircle.setVisibility(View.INVISIBLE);
        }

    }

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class SubsidiaryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.txt_type_texto)
        TextView text;
        @BindView(R.id.img_circle)
        ImageView imgCircle;


        public SubsidiaryViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (listener == null)
                return;

            if (selectedItem != -1)
                notifyItemChanged(selectedItem);


            if (selectedItem == getAdapterPosition()) {
                selectedItem = -1;
                notifyItemChanged(getAdapterPosition());
                if (listener != null)
                    listener.onItemClick(null, getAdapterPosition());
            } else {
                selectedItem = getAdapterPosition();
                notifyItemChanged(selectedItem);
                listener.onItemClick(list.get(getAdapterPosition()), getAdapterPosition());
            }

        }

    }


    public interface OnItemClickListener {
        void onItemClick(SubsidiaryViewModel entity, int position);
    }
}
