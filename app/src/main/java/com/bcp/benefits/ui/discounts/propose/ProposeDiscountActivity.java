package com.bcp.benefits.ui.discounts.propose;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.bcp.benefits.analytics.AnalyticsModule;
import com.bcp.benefits.analytics.AnalyticsTags;
import com.google.android.flexbox.FlexWrap;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;
import com.bcp.benefits.R;
import com.bcp.benefits.ui.BaseActivity;
import com.bcp.benefits.ui.discounts.consume.SuccessMessageDialogFragment;
import com.bcp.benefits.ui.discounts.list.CategoryAdapter;
import com.bcp.benefits.util.AppUtils;
import com.bcp.benefits.viewmodel.DiscountCategoryViewModel;
import com.bcp.benefits.viewmodel.DiscountTypeViewModel;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

import static com.bcp.benefits.util.Constants.DISCOUNT_TYPE_PARAM;
import static com.bcp.benefits.viewmodel.DiscountTypeViewModel.DISCOUNT;
import static com.bcp.benefits.viewmodel.DiscountTypeViewModel.EDUTATION;

public class ProposeDiscountActivity extends BaseActivity implements CategoryAdapter.OnSelectCategoryListener
        , ProposePresenter.ProposeView {

    @Inject
    ProposePresenter presenter;

    @BindView(R.id.rv_categories)
    RecyclerView rvCategories;
    @BindView(R.id.et_comment)
    EditText etComment;
    @BindView(R.id.tv_title_1)
    TextView tvTitle1;
    @BindView(R.id.tv_title_2)
    TextView tvTitle2;
    @BindView(R.id.tv_discount_type)
    TextView tvDiscountType;
    @BindView(R.id.tv_business)
    TextView tvBusiness;
    @BindView(R.id.btn_send)
    TextView btnSend;
    @BindView(R.id.iv_close)
    ImageView icClose;
    @BindView(R.id.content_propose)
    ScrollView contentPropose;

    private DiscountCategoryViewModel selectedCategory;

    private CategoryAdapter categoryAdapter;
    private DiscountTypeViewModel discountType;
    private AnalyticsModule analyticsModule = new AnalyticsModule(this);


    public static void start(Context context, DiscountTypeViewModel discountType) {
        Intent starter = new Intent(context, ProposeDiscountActivity.class);
        starter.putExtra(DISCOUNT_TYPE_PARAM, discountType);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        loadData();
        setTheme(discountType == EDUTATION ? R.style.AppThemeEducation : R.style.AppThemeDiscount);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setUpView() {
        this.presenter.setView(this);

        sendToAnalytics();

        tvTitle1.setTextColor(ContextCompat.getColor(this, startFlv()));
        tvTitle2.setTextColor(ContextCompat.getColor(this, startFlv()));
        tvDiscountType.setTextColor(ContextCompat.getColor(this, startFlv()));
        tvBusiness.setTextColor(ContextCompat.getColor(this, startFlv()));
        etComment.setBackground(getResources().getDrawable(borderFlv()));
        etComment.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE
                | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);

        if (discountType == DiscountTypeViewModel.DISCOUNT) {
            AppUtils.setGradient(getResources().getColor(R.color.green_start), getResources().getColor(R.color.green_end),
                    AppUtils.dpToPx((int) getResources().getDimension(R.dimen.size_thirteen)), btnSend);
        } else if (discountType == DiscountTypeViewModel.EDUTATION) {
            btnSend.setBackground(getResources().getDrawable(R.drawable.bg_button_discount_yellow));
        }


        categoryAdapter = new CategoryAdapter(this, discountType, this);
        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(this);
        layoutManager.setFlexWrap(FlexWrap.WRAP);
        layoutManager.setJustifyContent(JustifyContent.FLEX_START);
        rvCategories.setLayoutManager(layoutManager);
        rvCategories.setAdapter(categoryAdapter);

        presenter.goGetCategories(discountType);
        this.icClose.setImageResource(discountType.equals(DiscountTypeViewModel.DISCOUNT)
                ? R.drawable.ic_close_green : R.drawable.ic_close_orange);

    }

    private void sendToAnalytics(){
        Bundle bundle = new Bundle();
        bundle.putString(AnalyticsTags.ProposeDiscounts.Parameters.categoria_nombre, discountType.getValue());
        analyticsModule.sendToAnalytics(AnalyticsTags.ProposeDiscounts.Events.proponerDescuento, bundle);
    }

    private void sendToAnalytics(String comment){
        Bundle bundle = new Bundle();
        bundle.putString(AnalyticsTags.ProposeDiscounts.Parameters.categoria_nombre, discountType.getValue());
        bundle.putString(AnalyticsTags.ProposeDiscounts.Parameters.beneficios_categoria, selectedCategory.getCategoryName());
        bundle.putString(AnalyticsTags.ProposeDiscounts.Parameters.beneficios_mensaje, AppUtils.getFirstNCharacters(comment));
        analyticsModule.sendToAnalytics(AnalyticsTags.ProposeDiscounts.Events.proponerDescuento_exitoso, bundle);
    }


    private void loadData() {
        discountType = (DiscountTypeViewModel) getIntent().getSerializableExtra(DISCOUNT_TYPE_PARAM);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_propose_discount;
    }

    public int resId(int first, int second) {
        return (discountType == DISCOUNT) ? first : second;
    }

    public int startFlv() {
        return resId(R.color.green_end, R.color.bg_yellow_start);

    }

    public int borderFlv() {
        return resId(R.drawable.bg_white_green_corners_stroke, R.drawable.bg_white_corners_yellow_stroke);

    }

    @Override
    public void selectCategory(DiscountCategoryViewModel categoryDiscount, int position) {
        selectedCategory = categoryDiscount;
    }

    @Override
    public void hideCategoryProgress(List<DiscountCategoryViewModel> categories) {
        categoryAdapter.addCategories(categories);
    }

    @Override
    public void showProposeSuccess(Integer categoryId, String comment) {
        sendToAnalytics(comment);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment dialogFragment = getSupportFragmentManager().findFragmentByTag("dialog_propose_message");
        if (dialogFragment != null) {
            ft.remove(dialogFragment);
        }
        ft.addToBackStack(null);
        SuccessMessageDialogFragment newFragment =
                SuccessMessageDialogFragment.newInstance(SuccessMessageDialogFragment.Operation.PROPOSE);
        newFragment.show(ft, "dialog_propose_message");

    }

    @OnClick(R.id.iv_close)
    public void onIvCloseClicked() {
        onBackPressed();
        overridePendingTransition(R.anim.rigth_in, R.anim.right_out);
    }

    @OnClick(R.id.btn_send)
    public void onBtnSendClicked() {
        Integer selectedCategoryId = null;

        if (selectedCategory != null)
            selectedCategoryId = selectedCategory.getIdCategory();

        if (selectedCategoryId != null) {
            if (!etComment.getText().toString().equals("")) {
                presenter.doProposeDiscount(selectedCategoryId, etComment.getText().toString());
            } else {
                AppUtils.showErrorMessage(contentPropose, this, getResources().getString(R.string.propose_discount_no_comment));
            }
        } else {
            AppUtils.showErrorMessage(contentPropose, this, getResources().getString(R.string.propose_discount_no_category));}

    }


    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.rigth_in, R.anim.right_out);
    }
}
