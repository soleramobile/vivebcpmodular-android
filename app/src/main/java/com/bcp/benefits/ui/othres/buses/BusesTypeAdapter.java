package com.bcp.benefits.ui.othres.buses;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bcp.benefits.R;
import com.bcp.benefits.viewmodel.BusViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BusesTypeAdapter extends RecyclerView.Adapter<BusesTypeAdapter.BusesTypeViewHolder> {
    private List<BusViewModel> busList;
    private SelectedTypeBuss selectedTypeBuss;

    BusesTypeAdapter(SelectedTypeBuss selectedTypeBuss) {
        busList = new ArrayList<>();
        this.selectedTypeBuss = selectedTypeBuss;
    }

    void setBusList(List<BusViewModel> busList) {
        this.busList = busList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public BusesTypeViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_buses,
                viewGroup, false);
        return new BusesTypeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BusesTypeViewHolder holder, int position) {
        holder.bind(busList.get(position));
        holder.textBusType.setOnClickListener(v -> selectedTypeBuss.selectedType(busList.get(position)));
    }


    @Override
    public int getItemCount() {
        return busList.size();
    }

    class BusesTypeViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_type)
        TextView textBusType;

        BusesTypeViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(BusViewModel bus) {
            textBusType.setText(bus.getDescription());
            textBusType.setBackgroundResource(bus.isSelected() ? R.drawable.bg_buses_selected
                    : R.drawable.bg_buses_unselected);
            textBusType.setTextColor(itemView.getContext().getResources().getColor(bus.isSelected()
                    ? R.color.white
                    : R.color.buses_color_blue));
        }
    }

    public interface SelectedTypeBuss {
        void selectedType(BusViewModel bus);
    }
}
