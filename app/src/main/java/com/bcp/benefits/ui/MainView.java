package com.bcp.benefits.ui;

import androidx.annotation.StringRes;

public interface MainView {
    void showProgress();
    void hideProgress();
    void showErrorMessage(@StringRes int message);
    void showErrorMessage(String message);
    void showColor(int resource);
    void showProgressColorhealth();
    Boolean validateInternet();
    void showInfoMessage(@StringRes int message);
    void showInfoMessage(String message);
    void showSuccessMessage(@StringRes int message);
    void showSuccessMessage(String message);
    void showWarningMessage(@StringRes int message);
    void showWarningMessage(String message);
}
