package com.bcp.benefits.ui.campaign.dynamic;


import android.content.Context;
import android.content.Intent;
import android.text.SpannableStringBuilder;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.widget.NestedScrollView;

import com.bcp.benefits.R;
import com.bcp.benefits.components.DynamicFormView;
import com.bcp.benefits.ui.BaseActivity;
import com.bcp.benefits.ui.campaign.dynamic.referredlist.ReferredActivity;
import com.bcp.benefits.util.AppUtils;
import com.bcp.benefits.viewmodel.CampaignDynamicViewModel;
import com.bcp.benefits.viewmodel.DynamicFieldValueViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

import static com.bcp.benefits.util.Constants.ID_CAMPAIGN_PARAM;

/**
 * Created by emontesinos on 16/05/19.
 **/

public class DynamicCampaignActivity extends BaseActivity implements DynamicCampaignPresenter.DinamicCampaignView {

    @Inject
    DynamicCampaignPresenter presenter;

    @BindView(R.id.txv_title)
    protected TextView txvTitle;
    @BindView(R.id.txv_sub_title)
    protected TextView txvSubTitle;
    @BindView(R.id.btnAccept_dinamic)
    TextView btnAction;
    @BindView(R.id.rv_compaign_dinamic)
    DynamicFormView dynamicFormView;
    @BindView(R.id.content_dynamic)
    NestedScrollView content_dynamic;

    @BindView(R.id.linean_content_check)
    LinearLayout lineanContentCheck;

    @BindView(R.id.textView7)
    TextView textView7;
    @BindView(R.id.content)
    View content;

    private Boolean validateClick = false;


    private int campaignId;

    public static void start(Context context, int idCampaign) {
        Intent starter = new Intent(context, DynamicCampaignActivity.class);
        starter.putExtra(ID_CAMPAIGN_PARAM, idCampaign);
        context.startActivity(starter);
    }


    @Override
    public void setUpView() {

        this.presenter.setView(this);
        campaignId = getIntent().getIntExtra(ID_CAMPAIGN_PARAM, 0);

        this.presenter.getCompaign(campaignId);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_dynamic_campaign;
    }


    @Override
    public void showForm(CampaignDynamicViewModel dynamicCampaign) {
        Animation animations = AnimationUtils.loadAnimation(this, R.anim.animation_in);
        animations.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                //Do nothing
                content_dynamic.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                //Do nothing
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                //Do nothing
            }
        });
        content_dynamic.startAnimation(animations);

        btnAction.setVisibility(View.VISIBLE);
        txvTitle.setText(dynamicCampaign.getTitle());
        if (dynamicCampaign.getSubTitle() != null) {
            txvSubTitle.setText(dynamicCampaign.getSubTitle());
        } else {
            txvSubTitle.setVisibility(View.GONE);
        }
        txvTitle.setText(dynamicCampaign.getTitle());
        txvSubTitle.setText(dynamicCampaign.getSubTitle());
        btnAction.setText(dynamicCampaign.getButtonTextSend());
        lineanContentCheck.setVisibility(dynamicCampaign.isViewReferees() ? View.VISIBLE : View.GONE);
        SpannableStringBuilder ssb = new SpannableStringBuilder(getResources().getString(R.string.text_list));
        ssb.setSpan(new UnderlineSpan(), 0, getResources().getString(R.string.text_list).length(), 0);
        textView7.setText(ssb);
        dynamicFormView.setModel(dynamicCampaign.getDynamicFieldModelList());


    }

    @Override
    public void onReferSuccess() {
        finish();

    }

    @OnClick(R.id.btnAccept_dinamic)
    public void onClickSubmit() {
        submit();
    }

    public void submit() {
        List<DynamicFieldValueViewModel> allValueList = dynamicFormView.getValues();

        List<DynamicFieldValueViewModel> validValueList = new ArrayList<>();
        List<DynamicFieldValueViewModel> invalidValueList = new ArrayList<>();

        for (DynamicFieldValueViewModel model : allValueList) {
            if (model.isValid() && !model.getValue().isEmpty()) {
                validValueList.add(model);
            } else {
                invalidValueList.add(model);
            }
        }

        if (validValueList.size() != dynamicFormView.getModelList().size()) {
            AppUtils.showErrorMessage(content_dynamic, this, getResources().getString(R.string.message_error_label_dynamic));
        } else {
            presenter.refer(campaignId, validValueList);
        }
    }


    @OnClick(R.id.img_close_dynamic)
    public void onClickClose() {
        finish();
    }

    @OnClick(R.id.linean_content_check)
    public void onClickContentCheck() {
        if (!validateClick) {
            validateClick = true;
            ReferredActivity.start(this, campaignId);
            overridePendingTransition(R.anim.left_in, R.anim.left_out);
        }
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        validateClick = false;
    }
}
