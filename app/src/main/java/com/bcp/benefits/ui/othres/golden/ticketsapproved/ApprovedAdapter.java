package com.bcp.benefits.ui.othres.golden.ticketsapproved;


import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.bcp.benefits.R;
import com.bcp.benefits.ui.othres.golden.GoldenTicketActivity;
import com.bcp.benefits.viewmodel.ForApprovalViewModel;

/**
 * Created by emontesinos on 03/05/19.
 **/
public class ApprovedAdapter extends RecyclerView.Adapter<ApprovedAdapter.ApprovedViewHolder> {


    private ArrayList<ForApprovalViewModel> forApprovalModels;
    private final Context context;

    void addList(ArrayList<ForApprovalViewModel> forApprovalModels) {
        this.forApprovalModels = forApprovalModels;
        notifyDataSetChanged();
    }

    ApprovedAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public ApprovedViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_list_approved, viewGroup, false);
        return new ApprovedViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ApprovedViewHolder holder, int position) {
        ForApprovalViewModel approvalModel = forApprovalModels.get(position);
        holder.tvName.setText(approvalModel.getUser().getName());
        holder.tvDate.setText(approvalModel.getUsageDate());
        holder.tvDurationDetail.setText(approvalModel.getDurationText());
        holder.tvDurationDetail2.setText(approvalModel.getDurationText());
        holder.tvPosition.setText(approvalModel.getUser().getPosition());
        holder.tvAccept.setOnClickListener(view -> ((GoldenTicketActivity) context).approveTicket(approvalModel.getIdTicket()));
        holder.tvCancel.setOnClickListener(view -> ((GoldenTicketActivity) context).cancelTicket(approvalModel.getIdTicket()));

    }

    @Override
    public int getItemCount() {
        return forApprovalModels.size();
    }

    class ApprovedViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvDurationDetailprincipal)
        TextView tvDurationDetail;
        @BindView(R.id.tvName)
        TextView tvName;
        @BindView(R.id.tvPosition)
        TextView tvPosition;
        @BindView(R.id.tvDate)
        TextView tvDate;
        @BindView(R.id.tvDuration_detail)
        TextView tvDurationDetail2;
        @BindView(R.id.tvAccept)
        TextView tvAccept;
        @BindView(R.id.tvCancel)
        TextView tvCancel;

        ApprovedViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }
}
