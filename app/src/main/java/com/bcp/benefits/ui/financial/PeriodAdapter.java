package com.bcp.benefits.ui.financial;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bcp.benefits.R;
import com.bcp.benefits.viewmodel.PeriodViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by emontesinos on 21/05/19.
 **/

public class PeriodAdapter extends RecyclerView.Adapter<PeriodAdapter.PeriodViewHolder> {

    private List<PeriodViewModel> listPeriod;
    private String periodUnit;
    private int positionSelected;
    private OnItemClickListenerPeriod listener;

    public PeriodAdapter() {
        this.listPeriod = new ArrayList<>();
    }

    public void setListPeriod(List<PeriodViewModel> listPeriod, String periodUnit) {
        this.listPeriod = listPeriod;
        this.periodUnit = periodUnit;
        positionSelected = 0;
        notifyDataSetChanged();
    }

    public PeriodViewModel getPeriodSelected() {
        if (listPeriod != null && !listPeriod.isEmpty())
            return listPeriod.get(positionSelected);
        else
            return null;
    }

    public void setPositionSelected(int positionSelected) {
        this.positionSelected = positionSelected;
    }

    public int getPositionSelected() {
        return positionSelected;
    }

    public void setListener(OnItemClickListenerPeriod listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public PeriodViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_periodo, viewGroup, false);
        return new PeriodAdapter.PeriodViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PeriodViewHolder holder, int position) {
        holder.bind(listPeriod.get(position));
    }

    @Override
    public int getItemCount() {
        return listPeriod.size();
    }

    class PeriodViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_bg)
        ImageView imgBg;
        @BindView(R.id.tv_quantity)
        TextView tvQuantity;
        @BindView(R.id.tv_periodUnit)
        TextView tvPeriodUnit;

        public PeriodViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(PeriodViewModel entity) {
            tvPeriodUnit.setText(periodUnit);

            imgBg.setImageResource(selectorFlv(getAdapterPosition()));
            tvQuantity.setTextColor(ContextCompat.getColor(itemView.getContext(), textColorFlv(getAdapterPosition())));
            tvPeriodUnit.setVisibility((positionSelected == getAdapterPosition()) ? View.VISIBLE : View.INVISIBLE);

            tvQuantity.setText(entity.getDescription());
            itemView.setOnClickListener(view -> {

                if (listener != null) {
                    listener.onItemClickPeriod(entity, positionSelected, getAdapterPosition());
                }

                positionSelected = getAdapterPosition();
                notifyDataSetChanged();

            });

        }
    }

    public int resId(int first, int second, int position) {
        return (positionSelected == position) ? first : second;
    }

    public int selectorFlv(int position) {
        return resId(R.drawable.bg_month_financial_normal_orange, R.drawable.ic_circle_orange_finance, position);
    }

    public int textColorFlv(int position) {
        return resId(R.color.white, R.color.gray_800, position);
    }

    public interface OnItemClickListenerPeriod {
        void onItemClickPeriod(PeriodViewModel entity, int previousPosition, int newPosition);
    }
}
