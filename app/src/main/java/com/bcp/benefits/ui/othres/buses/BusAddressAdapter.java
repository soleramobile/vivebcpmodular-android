package com.bcp.benefits.ui.othres.buses;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bcp.benefits.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pe.solera.benefit.main.entity.Address;

public class BusAddressAdapter extends RecyclerView.Adapter<BusAddressAdapter.BusAddressViewHolder> {
    private List<Address> addressList;
    private AddressSelected addressSelected;
    private int positionSelected;

    BusAddressAdapter(AddressSelected addressSelected) {
        this.addressList = new ArrayList<>();
        this.addressSelected = addressSelected;
    }

    void setAddressList(List<Address> addressList, int positionSelected) {
        this.addressList = addressList;
        this.positionSelected = positionSelected;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public BusAddressViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_type_custom, parent, false);
        return new BusAddressViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BusAddressViewHolder holder, int position) {
        holder.bind(addressList.get(position));
        holder.textAddress.setOnClickListener(v ->
                addressSelected.selectedAddress(addressList.get(position)));
        holder.imageCircle.setVisibility(position == positionSelected ? View.VISIBLE : View.GONE);
    }

    @Override
    public int getItemCount() {
        return addressList.size();
    }

    class BusAddressViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_type_texto)
        TextView textAddress;
        @BindView(R.id.img_circle)
        ImageView imageCircle;

        BusAddressViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            imageCircle.setImageDrawable(itemView.getResources().getDrawable(R.drawable.bg_circle_blue));
        }

        private void bind(Address address) {
            textAddress.setText(address.getDescription());
        }
    }

    public interface AddressSelected {
        void selectedAddress(Address address);
    }
}
