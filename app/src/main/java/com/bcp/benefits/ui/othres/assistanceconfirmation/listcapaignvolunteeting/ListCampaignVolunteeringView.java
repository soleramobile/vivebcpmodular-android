package com.bcp.benefits.ui.othres.assistanceconfirmation.listcapaignvolunteeting;

import com.bcp.benefits.ui.MainView;
import com.bcp.benefits.viewmodel.CampaignVolunteeringViewModel;
import com.bcp.benefits.viewmodel.ListLocalViewModel;
import com.bcp.benefits.viewmodel.ListRoleViewModel;
import com.bcp.benefits.viewmodel.ListTypeVolunteerViewModel;
import com.bcp.benefits.viewmodel.LocalVolunteeringViewModel;

import java.util.List;

/**
 * Created by emontesinos on 13/01/20.
 */

public interface ListCampaignVolunteeringView extends MainView {
    void showTypesVolunteer(List<ListTypeVolunteerViewModel> listTypeVolunteers);
    void showListCampaignVolunteering(List<CampaignVolunteeringViewModel> campaignVolunteeringViewModels);
    void showListLocalVolunteering(List<LocalVolunteeringViewModel> localVolunteeringViewModels);
    void showList(List<ListLocalViewModel> listLocalViewModels);
}