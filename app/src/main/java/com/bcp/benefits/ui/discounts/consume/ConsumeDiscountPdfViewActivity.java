package com.bcp.benefits.ui.discounts.consume;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.github.barteksc.pdfviewer.PDFView;
import com.google.android.material.appbar.AppBarLayout;
import com.bcp.benefits.R;
import com.bcp.benefits.components.MiBancoProgressBar;
import com.bcp.benefits.ui.BaseActivity;
import com.bcp.benefits.viewmodel.DiscountTypeViewModel;

import java.io.IOException;
import java.io.InputStream;

import butterknife.BindView;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.bcp.benefits.viewmodel.DiscountTypeViewModel.DISCOUNT;

public class ConsumeDiscountPdfViewActivity extends BaseActivity {

    public static final String DISCOUNT_TYPE_PARAM = "discount_type";
    public static final String URL_PARAM = "url";
    public static final String PROVIDER_PARAM = "provider";

    @BindView(R.id.img_back)
    ImageButton imgBack;
    @BindView(R.id.title_toolbar)
    TextView titleToobal;


    TextView mTvTitle;
    @BindView(R.id.pdfView)
    PDFView mPdfView;
    @BindView(R.id.toolbar)
    AppBarLayout toolbar;


    private DiscountTypeViewModel discountType;

    private MiBancoProgressBar bcpProgressBar;

    public static void start(Context context, DiscountTypeViewModel discountType, String url, String provider) {
        Intent starter = new Intent(context, ConsumeDiscountPdfViewActivity.class);
        starter.putExtra(DISCOUNT_TYPE_PARAM, discountType);
        starter.putExtra(URL_PARAM, url);
        starter.putExtra(PROVIDER_PARAM, provider);
        context.startActivity(starter);
    }

    @Override
    public void setUpView() {
        String url = getIntent().getStringExtra(URL_PARAM);
        String provider = getIntent().getStringExtra(PROVIDER_PARAM);
        discountType = (DiscountTypeViewModel) getIntent().getSerializableExtra(DISCOUNT_TYPE_PARAM);
        titleToobal.setText(provider);
        settingToolbar();
        bcpProgressBar = new MiBancoProgressBar(this);
        Toast errorToast = Toast.makeText(this, getResources().getString(R.string.error_pdf), Toast.LENGTH_SHORT);
        new GetPdfAsyncTask(bcpProgressBar, mPdfView, errorToast, url).execute();
    }

    @Override
    public int getLayout() {
        return R.layout.activity_consume_discount_pdf_view;
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void settingToolbar() {
        imgBack.setOnClickListener(v -> {
            onBackPressed();
            overridePendingTransition(R.anim.rigth_in, R.anim.right_out);
        });

        toolbar.setBackground(getResources().getDrawable(gradientBgFlv()));
    }

    public int resId(int first, int second) {
        return (discountType == DISCOUNT) ? first : second;
    }

    public int gradientBgFlv() {
        return resId(R.drawable.bg_green_discount, R.drawable.bg_yellow_gradient_square);

    }

    static class GetPdfAsyncTask extends AsyncTask<Void, Void, InputStream> {

        private MiBancoProgressBar bcpProgressBar;
        private PDFView mPdfView;
        private String pdfUrl;
        private Toast errorToast;

        public GetPdfAsyncTask(MiBancoProgressBar bcpProgressBar, PDFView mPdfView, Toast errorToast, String pdfUrl) {
            this.bcpProgressBar = bcpProgressBar;
            this.mPdfView = mPdfView;
            this.pdfUrl = pdfUrl;
            this.errorToast = errorToast;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            bcpProgressBar.show();
        }

        @Override
        protected InputStream doInBackground(Void... voids) {
            try {
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder().url(pdfUrl).build();
                Response response = client.newCall(request).execute();
                return response.body().byteStream();
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(InputStream inputStream) {
            if (inputStream != null) {
                mPdfView.fromStream(inputStream)
                        .defaultPage(0)
                        .enableAnnotationRendering(false)
                        .swipeHorizontal(false)
                        .enableDoubletap(true)
                        .enableSwipe(true)
                        .scrollHandle(null)
                        .spacing(5)
                        .load();
            } else {
                errorToast.show();
            }
            bcpProgressBar.dismiss();
        }
    }
}
