package com.bcp.benefits.ui.health.clinicdetail;


import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bcp.benefits.R;
import com.bcp.benefits.ui.BaseActivity;
import com.bcp.benefits.util.AppUtils;
import com.bcp.benefits.util.CallController;
import com.bcp.benefits.viewmodel.ClinicViewModel;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

import static com.bcp.benefits.util.Constants.CLINIC_ID_PARAM;
import static com.bcp.benefits.util.Constants.ID_MAPS;
import static com.bcp.benefits.util.Constants.ID_WAZE;
import static com.bcp.benefits.util.Constants.PRODUCT_ID_PARAM;

/**
 * Created by emontesinos on 23/05/19.
 **/

public class ClinicDetailActivity extends BaseActivity implements ClinicDetailPresenter.ClinicDetailView {

    @Inject
    ClinicDetailPresenter presenter;

    @BindView(R.id.txt_title_name_clinic)
    TextView tvTitle;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.tv_web_page)
    TextView tvWebPage;
    @BindView(R.id.rv_attention_types)
    RecyclerView rvAttentionTypes;
    private CallController callController;
    private ClinicViewModel clinic;

    public static void start(Context context, Integer clinicId, Integer productId) {
        Intent starter = new Intent(context, ClinicDetailActivity.class);
        starter.putExtra(CLINIC_ID_PARAM, clinicId);
        starter.putExtra(PRODUCT_ID_PARAM, productId);
        context.startActivity(starter);
    }


    @Override
    public void setUpView() {
        this.presenter.setView(this);
        Integer clinicId = getIntent().getIntExtra(CLINIC_ID_PARAM, 0);
        Integer productId = getIntent().getIntExtra(PRODUCT_ID_PARAM, 0);
        callController = new CallController(this, null);
        this.presenter.doGetClinic(clinicId, productId);


    }

    @Override
    public int getLayout() {
        return R.layout.activity_clinic_detail;
    }

    @Override
    public void showClinic(ClinicViewModel clinic) {
        this.clinic = clinic;
        tvTitle.setText(clinic.getName());
        tvAddress.setText(clinic.getAddress());
        tvWebPage.setText(clinic.getWebpage());
        rvAttentionTypes.setLayoutManager(new LinearLayoutManager(this));

        DividerItemDecoration dv = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        Drawable mDivider = ContextCompat.getDrawable(this, R.drawable.row_divider_line);
        assert mDivider != null;
        dv.setDrawable(mDivider);
        rvAttentionTypes.addItemDecoration(dv);
        ClinicAttentionTypeAdapter adapter = new ClinicAttentionTypeAdapter(clinic.getAttentionTypes());
        rvAttentionTypes.setAdapter(adapter);

    }

    @OnClick(R.id.tv_how_to_get)
    public void onTvHowToGetClicked() {
        if (clinic == null || clinic.getLatitude() == null || clinic.getLongitude() == null) return;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.subsidiaries_select_nav_app_subtitle)
                .setItems(R.array.navigation_apps, (dialog, which) -> {
                    dialog.dismiss();
                    if (which == 0) {
                        selectGoogleMaps();
                    } else if (which == 1) {
                        selectWaze();
                    }

                });

        builder.create();

        builder.show();

    }

    @OnClick(R.id.tv_callus)
    public void onTvCallusClicked() {
        if (clinic == null || clinic.getPhone() == null) return;
        callController.doCall(clinic.getPhone());
    }
//en caso de ser aqui seria el reject
    @OnClick(R.id.img_close_blue)
    public void onIvCloseClicked() {
        onBackPressed();
    }

    @OnClick(R.id.btn_appointment)
    public void onBtnAppointmentClicked() {
        if (clinic == null || clinic.getPhone() == null) return;
        callController.doCall(clinic.getPhone());
    }

    @OnClick(R.id.tv_web_page)
    void onclickWeb() {
        AppUtils.openBrowser(this, tvWebPage.getText().toString());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        callController.onRequestPermissionsResult(requestCode, grantResults);
    }

    private void selectGoogleMaps() {
        String position = clinic.getLatitude() + "," + clinic.getLongitude();
        Uri gmmIntentUri = Uri.parse("google.navigation:q=" + position + "&mode=d");
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");

        if (mapIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(mapIntent);
        } else {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + ID_MAPS)));
        }
    }

    private void selectWaze() {
        String intentUriWaze = "waze://?ll=" + clinic.getLatitude() + ", " + clinic.getLongitude() + "&navigate=yes";
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(intentUriWaze));

        if (mapIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(mapIntent);
        } else {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + ID_WAZE)));
        }
    }


}
