package com.bcp.benefits.ui.othres.licenses.mylicences;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import org.jetbrains.annotations.NotNull;
import java.util.Objects;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.bcp.benefits.R;
import com.bcp.benefits.ui.othres.golden.mytickets.CarouselLinearLayout;
import com.bcp.benefits.ui.othres.licenses.LicencesActivity;
import com.bcp.benefits.ui.othres.licenses.requestlicence.LicenceRequestActivity;
import com.bcp.benefits.util.AppUtils;
import com.bcp.benefits.viewmodel.OwnedViewModel;
import static com.bcp.benefits.util.Constants.HEADNAME;
import static com.bcp.benefits.util.Constants.IDTICKET;
import static com.bcp.benefits.util.Constants.OWNED;
import static com.bcp.benefits.util.Constants.RESULT_TICKET;
import static com.bcp.benefits.util.Constants.SCALE;
import static com.bcp.benefits.util.Constants.SHOWRADIO;
import static com.bcp.benefits.util.Constants.STATUS_TICKET_APPROVED;
import static com.bcp.benefits.util.Constants.STATUS_TICKET_PENDING;
import static com.bcp.benefits.util.Constants.TYPE_BIRDAY;
import static com.bcp.benefits.util.Constants.TYPE_KILL;
import static com.bcp.benefits.util.Constants.TYPE_MATERNITY;
import static com.bcp.benefits.util.Constants.TYPE_MEDIC;
/**
 * Created by emontesinos on 08/05/19.
 **/

public class LicenceFragment extends Fragment {

    @BindView(R.id.txt_state_ticket)
    TextView stateTicket;
    @BindView(R.id.ivImg)
    ImageView imgTicket;
    @BindView(R.id.tvDurationText)
    TextView durationTicket;
    @BindView(R.id.line_golden)
    View lineGolden;
    @BindView(R.id.line_horizontal)
    View lineHorizontal;
    @BindView(R.id.rlv_activate)
    RelativeLayout rlvActivate;
    @BindView(R.id.txt_message)
    TextView txtMessage;
    @BindView(R.id.txt_title_model)
    TextView titleModel;
    @BindView(R.id.rlv_content_view)
    RelativeLayout rlvContentView;

    private OwnedViewModel ownedModel;

    public static Fragment newInstance(MysLicencesFragment context, OwnedViewModel ownedModel) {
        Bundle b = new Bundle();
        b.putSerializable(OWNED, ownedModel);
        return Fragment.instantiate(context.getContext(), LicenceFragment.class.getName(), b);
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        assert this.getArguments() != null;
        float scale = this.getArguments().getFloat(SCALE);
        ownedModel = (OwnedViewModel) this.getArguments().getSerializable(OWNED);
        LinearLayout linearLayout = (LinearLayout) inflater.inflate(R.layout.fragment_image, container, false);
        ButterKnife.bind(this, linearLayout);
        CarouselLinearLayout root = linearLayout.findViewById(R.id.root_container);
        root.setScaleBoth(scale);
            if(ownedModel.getShowButton()){
               switch(ownedModel.getDepartmentHeadName()){
                  case TYPE_MEDIC :
                        titleModel.setText(R.string.txt_medic);
                        imgTicket.setImageDrawable(getResources().getDrawable(R.drawable.ic_medic));
                        break;
                    case TYPE_BIRDAY :
                        titleModel.setText(R.string.txt_birday);
                        imgTicket.setImageDrawable(getResources().getDrawable(R.drawable.ic_present));
                        break;
                    case TYPE_KILL :
                        titleModel.setText(R.string.txt_kill);
                        imgTicket.setImageDrawable(getResources().getDrawable(R.drawable.ic_cruz));
                        break;
                    case TYPE_MATERNITY :
                        titleModel.setText(R.string.txt_maternity);
                        imgTicket.setImageDrawable(getResources().getDrawable(R.drawable.ic_oso_yellow));
                        break;
                }

            }else {
                    rlvActivate.setVisibility(View.INVISIBLE);
                    lineGolden.setVisibility(View.GONE);
                    txtMessage.setVisibility(View.GONE);
                    lineHorizontal.setVisibility(View.GONE);
                    stateTicket.setVisibility(View.VISIBLE);
                   switch(ownedModel.getStatusText()){
                        case STATUS_TICKET_APPROVED :
                            stateTicket.setText(R.string.tickets_approved);
                            stateTicket.setTextColor(getResources().getColor(R.color.golden_text));
                            imgTicket.setImageDrawable(getResources().getDrawable(R.drawable.ic_carita_approved));
                            break;
                        case STATUS_TICKET_PENDING :
                            stateTicket.setText(R.string.tickets_pending_text);
                            imgTicket.setImageDrawable(getResources().getDrawable(R.drawable.ic_carita_pending));
                            break;
                    }
                    durationTicket.setText(AppUtils.getStyleText(getContext(),7,ownedModel.getUsageDateFormatted(),
                            ownedModel.getUsageDateFormatted().length()));
                    durationTicket.setMovementMethod(LinkMovementMethod.getInstance());
                }
        return linearLayout;
    }


    @OnClick({R.id.rlv_content_title,R.id.rlv_content_view})
    public void onClickLicence(){
        if (ownedModel.getShowButton()) {
                     getIntent();
        }else {
            if (ownedModel.isReleaseable()) {
                ((LicencesActivity) Objects.requireNonNull(getActivity())).releaseLicence();
            }
        }
    }

    @OnClick(R.id.rlv_activate)
    public void onClickActivate(){
        if (ownedModel.getShowButton()) {
           getIntent();
        }
    }

    private void getIntent(){
        Intent starter = new Intent(getContext(), LicenceRequestActivity.class);
        starter.putExtra(HEADNAME, ownedModel.getDepartmentHeadName());
        starter.putExtra(IDTICKET, ownedModel.getIdTicket());
        starter.putExtra(SHOWRADIO, ownedModel.getShowDurationDetail());
        Objects.requireNonNull(getActivity()).startActivityForResult(starter, RESULT_TICKET);
    }

}
