package com.bcp.benefits.ui.discounts.consume;

import androidx.annotation.NonNull;

import com.bcp.benefits.ui.MainView;
import com.bcp.benefits.viewmodel.ConsumedDiscountViewModel;
import com.bcp.benefits.viewmodel.UserViewModel;

import javax.inject.Inject;

import pe.solera.benefit.main.entity.ConsumeDiscount;
import pe.solera.benefit.main.entity.User;
import pe.solera.benefit.main.usecases.executor.Action;
import pe.solera.benefit.main.usecases.usecases.DiscountUseCases;
import pe.solera.benefit.main.usecases.usecases.UserUseCase;

public class ConsumePresenter {

    private final DiscountUseCases discountUseCases;
    private final UserUseCase userUseCase;
    private ConsumeView view;

    @Inject
    public ConsumePresenter(final DiscountUseCases discountUseCases, UserUseCase userUseCase) {
        this.discountUseCases = discountUseCases;
        this.userUseCase = userUseCase;
    }

    public void setView(@NonNull ConsumeView view) {
        this.view = view;
    }

    public void doGetUserInfo() {
        if (this.view.validateInternet()) {
            this.view.showProgress();
            userUseCase.getUser(new Action.Callback<User>() {
                @Override
                public void onSuccess(User user) {
                    view.showUserInfo(new UserViewModel(user));
                    view.hideProgress();
                }

                @Override
                public void onError(Throwable throwable) {
                    view.showErrorMessage(throwable.getMessage());
                    view.hideProgress();

                }
            });
        }
    }

    void doConsumeDiscount(Integer discountId, Integer subsidiaryId) {
        if (this.view.validateInternet()) {
            this.view.showProgress();
            discountUseCases.redeemDiscount(discountId, subsidiaryId, new Action.Callback<ConsumeDiscount>() {
                @Override
                public void onSuccess(ConsumeDiscount consumeDiscount) {
                    view.showConsumeSuccess(new ConsumedDiscountViewModel(consumeDiscount));
                    view.hideProgress();
                }

                @Override
                public void onError(Throwable throwable) {
                    view.showErrorMessage(throwable.getMessage());
                    view.hideProgress();

                }
            });
        }

    }

    public interface ConsumeView extends MainView {
        void showUserInfo(UserViewModel userViewModel);

        void showConsumeSuccess(ConsumedDiscountViewModel consumedDiscountViewModel);
    }
}
