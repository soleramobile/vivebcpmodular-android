package com.bcp.benefits.ui.validate;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.DialogFragment;

import com.bcp.benefits.R;
import com.bcp.benefits.util.Constants;

import java.util.Calendar;
import java.util.Date;

import static com.bcp.benefits.util.Constants.ANDROID;
import static com.bcp.benefits.util.Constants.DATE;
import static com.bcp.benefits.util.Constants.DAY;
import static com.bcp.benefits.util.Constants.FUTURE;
import static com.bcp.benefits.util.Constants.ID_HOME;
import static com.bcp.benefits.util.Constants.MODE;
import static com.bcp.benefits.util.Constants.MONTH;
import static com.bcp.benefits.util.Constants.OLD;


/**
 * Created by emontesinos on 29/04/19.
 **/

public class DatePickerDialogCustom extends DialogFragment{

    private EditNameDialogListener listener ;
    private Date elegDate;
    private final Boolean anual= false;
    private int modo = 0;

    public static DatePickerDialogCustom newInstance(int dateId, Date dateOld, int futuro, int modo){
        DatePickerDialogCustom frag = new DatePickerDialogCustom() ;
        Bundle args = new Bundle();
        args.putInt(DATE, dateId);
        args.putLong(OLD, dateOld.getTime());
        args.putInt(FUTURE, futuro);
        args.putInt(MODE, modo);
        frag.setArguments(args);
        return frag;
    }

    public void setListener(EditNameDialogListener listener) {
        this.listener = listener;
    }

    public interface EditNameDialogListener {
        void onFinishEditDialog(Date date, int type, Boolean anual, int modo);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable((Color.TRANSPARENT)));
        return inflater.inflate(R.layout.dialog_date_picker, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        DatePicker dailyDatepicker =  view.findViewById(R.id.daily_datepicker);
        Button buttonAccept =  view.findViewById(R.id.button_aceptar);
        Button buttonCancel =  view.findViewById(R.id.button_cancelar);
        CardView cardContainer = view.findViewById(R.id.cardContainer);
        assert getArguments() != null;
        int dateId = getArguments().getInt(DATE, 0);
        long old = getArguments().getLong(OLD, 0L);
        int future = getArguments().getInt(FUTURE, 0);
        modo = getArguments().getInt(MODE, Constants.MODO_SIN_DIA);

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.YEAR, future);

        Calendar calendar = Calendar.getInstance();
        elegDate = calendar.getTime();

        Calendar calendarFuture = Calendar.getInstance();
        calendarFuture.add(Calendar.YEAR, future);



        switch (modo){
            case Constants.MODO_SIN_DIA_MES :
                dailyDatepicker.findViewById(getResources().getIdentifier
                        (DAY, ID_HOME,ANDROID)).setVisibility(View.GONE);
                dailyDatepicker.findViewById(getResources().getIdentifier
                        (MONTH, ID_HOME,ANDROID)).setVisibility(View.GONE);
                break;
            case Constants.MODO_SIN_DIA :
                dailyDatepicker.findViewById(getResources().getIdentifier
                        (DAY, ID_HOME,ANDROID)).setVisibility(View.GONE);
                break;
        }

        if (old != 0L) {
            Calendar ce = Calendar.getInstance();
            ce.setTime(new Date(old));
            elegDate = ce.getTime();
            calendar = ce;
        }

        dailyDatepicker.init(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH),
                (view1, year1, monthOfYear, dayOfMonth) -> {
                    Calendar eleg = Calendar.getInstance();
                    eleg.set(Calendar.YEAR, year1);
                    eleg.set(Calendar.MONTH, monthOfYear);
                    eleg.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    elegDate = eleg.getTime();

                });

        buttonAccept.setOnClickListener(v -> {
                listener.onFinishEditDialog(elegDate, dateId, anual, modo);
            dismiss();
        });

        cardContainer.setBackgroundResource(R.drawable.bg_date_picker);


        buttonCancel.setOnClickListener(v -> dismiss()); {

        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow()
                .getAttributes().windowAnimations = R.style.DialogTheme;
    }
}
