package com.bcp.benefits.ui.health.nearyou;

import androidx.annotation.NonNull;

import com.bcp.benefits.ui.MainView;
import com.bcp.benefits.viewmodel.HealthAttentionTypeViewModel;
import com.bcp.benefits.viewmodel.HealthProductViewModel;

import java.util.List;

import javax.inject.Inject;

import pe.solera.benefit.main.entity.HealthProduct;
import pe.solera.benefit.main.usecases.executor.Action;
import pe.solera.benefit.main.usecases.usecases.HealthUseCase;

public class NearYouPresenter {

    private NearYouView view;
    private final HealthUseCase healthUseCase;

    @Inject
    public NearYouPresenter(HealthUseCase healthUseCase) {
        this.healthUseCase = healthUseCase;
    }

    public void setView(@NonNull NearYouView view) {
        this.view = view;
    }

    void doGetHealthProducts() {
        if (this.view.validateInternet()) {
            this.view.showProgressColorhealth();
            view.showProgress();
            healthUseCase.getClinicsProducts(new Action.Callback<List<HealthProduct>>() {
                @Override
                public void onSuccess(List<HealthProduct> healthProducts) {
                    List<HealthProductViewModel> modelList = HealthProductViewModel.toHealthProductViewModel(healthProducts);
                    view.showHealthProducts(modelList);
                    if (!modelList.isEmpty() && !modelList.get(0).getAttentionTypes().isEmpty())
                        view.showHealthAttentionTypes(modelList.get(0).getAttentionTypes());
                    else
                        view.showNotFoundData();
                    view.hideProgress();
                }

                @Override
                public void onError(Throwable throwable) {
                    view.showErrorMessage(throwable.getMessage());
                    view.hideProgress();
                }
            });
        }
    }

    public interface NearYouView extends MainView {
        void showHealthProducts(List<HealthProductViewModel> list);

        void showHealthAttentionTypes(List<HealthAttentionTypeViewModel> list);

        void showNotFoundData();
    }
}
