package com.bcp.benefits.ui.financial;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.bcp.benefits.R;
import com.bcp.benefits.viewmodel.ConditionViewModel;

/**
 * Created by emontesinos on 21/05/19.
 **/

public class ConditionAdapter extends RecyclerView.Adapter<ConditionAdapter.ConditionViewHolder> {

    private List<ConditionViewModel> listCondition;
    private OnItemClickListenerCondition listener;

    public ConditionAdapter() {
        this.listCondition = new ArrayList<>();
    }

    public void setList(List<ConditionViewModel> listCondition) {
        this.listCondition = listCondition;
        notifyDataSetChanged();
    }

    public List<ConditionViewModel> getList() {
        return listCondition;
    }

    public void setListener(OnItemClickListenerCondition listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public ConditionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_codition, parent, false);
        return new ConditionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ConditionViewHolder holder, int position) {
        holder.bind(listCondition.get(position));
    }

    @Override
    public int getItemCount() {
        return listCondition.size();
    }

    public void setItem(int position, boolean isChecked) {
        listCondition.get(position).setChecked(isChecked);
    }

    class ConditionViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_description)
        TextView tvDescription;
        @BindView(R.id.rb_condition)
        ImageView rbCondition;


        public ConditionViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(ConditionViewModel entity) {

            tvDescription.setText(entity.getDescription());

            rbCondition.setImageResource(selectorFlv(entity.isChecked()));

            rbCondition.setOnClickListener(view -> {
                entity.setChecked(!entity.isChecked());
                if (listener != null) {
                    listener.onItemClickCondition(entity, getAdapterPosition());
                }
                notifyDataSetChanged();
            });

        }
    }

    public int resId(int first, int second, boolean isChecked) {
        return (isChecked) ? first : second;
    }

    public int selectorFlv(boolean isChecked) {
        return resId(R.drawable.ic_checked_orange, R.drawable.ic_checked_orange_unselect, isChecked);
    }

    public interface OnItemClickListenerCondition {
        void onItemClickCondition(ConditionViewModel entity, int position);
    }
}
