package com.bcp.benefits.ui;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.bcp.benefits.R;
import com.bcp.benefits.util.AppUtils;

import butterknife.ButterKnife;
import dagger.android.support.DaggerFragment;

public abstract class BaseFragment extends DaggerFragment implements MainFragment {

    public abstract void setUpView();

    public abstract int getLayout();

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(getLayout(), container, false);
        ButterKnife.bind(this, view);
        this.setUpView();
        return view;
    }

    @Override
    public Boolean validateInternet() {
        if (AppUtils.isConnected(getContext())) {
            return true;
        }
        showErrorConnect();
        return false;
    }

    void showErrorConnect() {
        new AlertDialog.Builder(getContext())
                .setTitle(R.string.app_name)
                .setCancelable(false)
                .setMessage(R.string.exception_no_internet_error_view)
                .setPositiveButton(R.string.dialog_accept, (dialogInterface, i) -> {
                    dialogInterface.dismiss();

                })
                .show();
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void showErrorMessage(int message) {

    }

    @Override
    public void showErrorMessage(String message) {

    }

}
