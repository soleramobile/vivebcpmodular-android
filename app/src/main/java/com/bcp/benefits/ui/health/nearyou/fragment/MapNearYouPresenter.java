package com.bcp.benefits.ui.health.nearyou.fragment;

import android.content.Context;

import com.bcp.benefits.ui.MainFragment;
import com.bcp.benefits.viewmodel.ClinicViewModel;

import java.util.List;

import androidx.annotation.NonNull;

import javax.inject.Inject;

import pe.solera.benefit.main.entity.Clinic;
import pe.solera.benefit.main.usecases.executor.Action;
import pe.solera.benefit.main.usecases.usecases.HealthUseCase;

public class MapNearYouPresenter {

    private MapNearYouView view;
    private final HealthUseCase healthUseCase;
    private Context context;

    @Inject
    public MapNearYouPresenter(HealthUseCase healthUseCase, Context context) {
        this.healthUseCase = healthUseCase;
        this.context= context;
    }

    public void setView(@NonNull MapNearYouView view) {
        this.view = view;
    }

    public void doGetClinics(String searchText, Integer healthProductId, Integer attentionTypeId,
                             String latitude, String longitude) {
        if (this.view.validateInternet()) {
            view.showProgress();
            this.healthUseCase.getClinics(attentionTypeId, healthProductId, latitude, longitude, searchText,
                    new Action.Callback<List<Clinic>>() {
                        @Override
                        public void onSuccess(List<Clinic> clinics) {
                            view.showClinics(ClinicViewModel.toClinicViewModels(clinics));
                            view.hideProgress();
                        }

                        @Override
                        public void onError(Throwable throwable) {
                            view.showErrorMessage(throwable.getMessage());
                            view.hideProgress();
                        }
                    });
        }

    }

    public interface MapNearYouView extends MainFragment {
        void showClinics(List<ClinicViewModel> clinics);
    }
}
