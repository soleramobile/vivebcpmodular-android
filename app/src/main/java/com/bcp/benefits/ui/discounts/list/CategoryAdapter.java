package com.bcp.benefits.ui.discounts.list;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bcp.benefits.R;
import com.bcp.benefits.viewmodel.DiscountCategoryViewModel;
import com.bcp.benefits.viewmodel.DiscountTypeViewModel;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.bcp.benefits.viewmodel.DiscountTypeViewModel.DISCOUNT;

/**
 * Created by emontesinos on 28/05/19.
 **/

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryVH> {

    private List<DiscountCategoryViewModel> categories;
    private OnSelectCategoryListener onSelectCategoryListener;
    private int selectedItem = -1;
    private Context context;
    private DiscountTypeViewModel discountType;


    public CategoryAdapter(Context context, DiscountTypeViewModel discountType,
                           OnSelectCategoryListener onSelectCategoryListener) {
        this.context = context;
        this.onSelectCategoryListener = onSelectCategoryListener;
        this.discountType = discountType;
        this.categories = new ArrayList<>();
    }

    public void addCategories(List<DiscountCategoryViewModel> categories) {
        this.categories.clear();
        this.categories = categories;
        notifyDataSetChanged();
    }

    void setSelectedItem(DiscountCategoryViewModel categorySelected) {
        if (categories == null) return;
        int index = categories.indexOf(categorySelected);
        if (selectedItem != index) {
            selectedItem = index;
            notifyDataSetChanged();
        }
    }

    @NotNull
    @Override
    public CategoryVH onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_health_products_select, parent, false);
        return new CategoryVH(v);
    }

    @Override
    public void onBindViewHolder(@NotNull CategoryVH holder, int position) {
        holder.tvCategoryName.setText(categories.get(position).getCategoryName());
        if (selectedItem == position) {
            holder.tvCategoryName.setTextColor(ContextCompat.getColor(context, R.color.white));
            holder.tvCategoryName.setTextAppearance(holder.itemView.getContext(), R.style.FontBold);
            holder.tvCategoryName.setBackgroundResource(resId(R.drawable.bg_button_campaign, R.drawable.bg_button_discount_yellow));

        } else {
            holder.tvCategoryName.setTextColor(ContextCompat.getColor(context, R.color.gray_800));
            holder.tvCategoryName.setTextAppearance(holder.itemView.getContext(), R.style.FontRegular);
            holder.tvCategoryName.setBackgroundResource(resId(R.drawable.bg_white_green_corners_stroke, R.drawable.bg_white_corners_yellow_stroke));
        }
    }

    private int resId(int first, int second) {
        return (discountType == DISCOUNT) ? first : second;
    }


    @Override
    public int getItemCount() {
        return categories != null ? categories.size() : 0;
    }

    class CategoryVH extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.tv_item)
        TextView tvCategoryName;

        CategoryVH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            tvCategoryName.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (onSelectCategoryListener == null)
                return;

            if (selectedItem != -1)
                notifyItemChanged(selectedItem);


            if (selectedItem == getAdapterPosition()) {
                selectedItem = -1;
                notifyItemChanged(getAdapterPosition());
                if (onSelectCategoryListener != null)
                    onSelectCategoryListener.selectCategory(null, -1);
            } else {
                selectedItem = getAdapterPosition();
                notifyItemChanged(selectedItem);
                onSelectCategoryListener.selectCategory(categories.get(getAdapterPosition()), getAdapterPosition());
            }

        }
    }

    public interface OnSelectCategoryListener {
        void selectCategory(DiscountCategoryViewModel category, int position);
    }
}
