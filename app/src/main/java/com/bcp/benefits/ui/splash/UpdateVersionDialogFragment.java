package com.bcp.benefits.ui.splash;
/* 
   Created by: Flavia Figueroa
               18/02/2020
         Solera Mobile
*/


import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.bcp.benefits.R;
import com.bcp.benefits.util.AppUtils;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class UpdateVersionDialogFragment extends DialogFragment {
    private  GoToPlayStore listener;

    public static UpdateVersionDialogFragment newInstance(Activity activity) {

        Bundle args = new Bundle();

        UpdateVersionDialogFragment fragment = new UpdateVersionDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.layout_update_version, container, false);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable((Color.TRANSPARENT)));
        ButterKnife.bind(this, v);
        return v;
    }

    @OnClick(R.id.btnGo)
    public void clickGo(){
        dismiss();
        listener.versionUpdate(true);
    }

    public void setListener(GoToPlayStore listener) {
        this.listener = listener;
    }

    public interface GoToPlayStore{
        void versionUpdate(Boolean click);
    }
}
