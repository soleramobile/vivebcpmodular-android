package com.bcp.benefits.ui.validate;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bcp.benefits.R;
import com.bcp.benefits.viewmodel.TypeDocViewModel;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by emontesinos.
 **/

public class TypesDialogFragment extends DialogFragment implements TypeAdapter.ListenerAdapter {

    public static final Integer DNI = 1;
    private static final Integer CE = 2;

    private OnSelectIdTypeListener onSelectIdType;
    private ArrayList<TypeDocViewModel> listType;
    private int docViewModel;

    public static TypesDialogFragment newInstance(ArrayList<TypeDocViewModel> types, Integer docSelected) {
        TypesDialogFragment fragment = new TypesDialogFragment();
        fragment.docViewModel = docSelected;
        Bundle args = new Bundle();
        args.putSerializable("type", types);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (!(context instanceof OnSelectIdTypeListener))
            throw new IllegalArgumentException("Activity is not implementing OnSelectIdTypeListener");

        onSelectIdType = (OnSelectIdTypeListener) context;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            Drawable backgroundColor = new ColorDrawable(ContextCompat.getColor(
                    Objects.requireNonNull(getContext()), android.R.color.transparent));
            Objects.requireNonNull(dialog.getWindow()).setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            dialog.getWindow().setBackgroundDrawable(backgroundColor);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_identy_simple, container, false);
        listType = (ArrayList<TypeDocViewModel>) getArguments().getSerializable("type");
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow()
                .getAttributes().windowAnimations = R.style.DialogTheme;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        RecyclerView rvType = view.findViewById(R.id.rv_type);
        ImageView ivClose = view.findViewById(R.id.iv_close);
        View headerView = view.findViewById(R.id.constraintLayout9);
        headerView.setBackgroundResource(R.drawable.bg_type_custom_blue);
        ivClose.setImageResource(R.drawable.ic_close);
        ivClose.setOnClickListener(v -> getDialog().dismiss());
        rvType.setLayoutManager(new LinearLayoutManager(getContext()));
        RecyclerView.Adapter typeAdapter = new TypeAdapter(listType, docViewModel);
        DividerItemDecoration itemDecoration = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        rvType.addItemDecoration(itemDecoration);
        rvType.setAdapter(typeAdapter);
        ((TypeAdapter) typeAdapter).setListener(this);

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        Objects.requireNonNull(dialog.getWindow()).requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void selectType(TypeDocViewModel valor) {
        onSelectIdType.onSelectIdType(valor.getValor());
        getDialog().dismiss();
    }

    public interface OnSelectIdTypeListener {
        void onSelectIdType(int idType);
    }
}
