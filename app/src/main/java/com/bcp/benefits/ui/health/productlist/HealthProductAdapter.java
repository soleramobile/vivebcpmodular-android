package com.bcp.benefits.ui.health.productlist;


import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.bcp.benefits.R;
import com.bcp.benefits.viewmodel.HealthProductViewModel;

/**
 * Created by emontesinos on 22/05/19.
 **/
public class HealthProductAdapter extends RecyclerView.Adapter<HealthProductAdapter.HealthProductViewHolder> {

    private List<HealthProductViewModel> products;
    private HealthProductItemClickListener healthProductItemClickListener;

    public HealthProductAdapter(HealthProductItemClickListener healthProductItemClickListener) {

        this.products = new ArrayList<>();
        this.healthProductItemClickListener = healthProductItemClickListener;
    }

    public void setProducts(List<HealthProductViewModel> products) {
        this.products = products;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public HealthProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_health_products, parent, false);
        return new HealthProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HealthProductViewHolder holder, int position) {
        holder.bind(products.get(position));
    }

    @Override
    public int getItemCount() {
        return products != null ? products.size() : 0;
    }

    public class HealthProductViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_product_type)
        TextView tvProductType;
        @BindView(R.id.tv_product_name)
        TextView tvProductName;
        @BindView(R.id.tv_status)
        TextView tvProductStatus;
        @BindView(R.id.rl_root)
        RelativeLayout rlRoot;

        public HealthProductViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(HealthProductViewModel productModel) {
            tvProductType.setText(productModel.getProductType());
            tvProductName.setText(productModel.getProductName());

            if (productModel.getInUse()) {
                itemView.setBackground(itemView.getContext().getResources().getDrawable(R.drawable.bg_item_health_skyblue));
                tvProductStatus.setText(R.string.health_product_actived);

                tvProductName.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.white));
                tvProductType.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.white));
                tvProductStatus.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.white));

                tvProductType.setBackground(null);
                tvProductStatus.setBackground(null);
            } else {
                itemView.setBackground(itemView.getContext().getResources().getDrawable(R.drawable.bg_item_health_white));
                tvProductStatus.setText(R.string.health_product_more_info);
                tvProductName.setTextColor(ContextCompat.getColorStateList(itemView.getContext(), R.color.gray_health_use));
                tvProductType.setTextColor(ContextCompat.getColorStateList(itemView.getContext(), R.color.home_health_start));
                tvProductStatus.setTextColor(ContextCompat.getColorStateList(itemView.getContext(), R.color.gray_850));
            }

            if (healthProductItemClickListener != null)
                itemView.setOnClickListener(view -> healthProductItemClickListener.onHealthProductItemClick(productModel));
        }
    }

    interface HealthProductItemClickListener {
        void onHealthProductItemClick(HealthProductViewModel productModel);
    }
}
