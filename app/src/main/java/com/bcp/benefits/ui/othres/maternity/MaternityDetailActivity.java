package com.bcp.benefits.ui.othres.maternity;


import android.content.Context;
import android.content.Intent;
import androidx.appcompat.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnClick;
import com.bcp.benefits.R;
import com.bcp.benefits.ui.BaseActivity;

/**
 * Created by emontesinos on 17/05/19.
 **/
public class MaternityDetailActivity extends BaseActivity {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.title_toolbar)
    TextView titleToobal;
    @BindView(R.id.img_close_detail_maternity)
    ImageView imgCloseDetail;


    public static void start(Context context) {
        Intent intent = new Intent(context, MaternityDetailActivity.class);
        context.startActivity(intent);
    }


    @Override
    public void setUpView() {
        getToolbar();

    }

    @Override
    public int getLayout() {
        return R.layout.activity_maternity_detail;
    }

    public void getToolbar() {
        titleToobal.setText(R.string.maternity);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(v -> {
            onBackPressed();
            overridePendingTransition(R.anim.rigth_in, R.anim.right_out);
        });
    }

    @OnClick(R.id.img_close_detail_maternity)
    public void OnClickCloseDetail(){
        finish();
    }
}
