package com.bcp.benefits.ui.tutorial;

import androidx.annotation.NonNull;

import com.bcp.benefits.R;
import com.bcp.benefits.ui.MainView;
import com.bcp.benefits.viewmodel.TutorialViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import pe.solera.benefit.main.usecases.executor.Action;
import pe.solera.benefit.main.usecases.usecases.AppUseCase;

/**
 * Created by emontesinos.
 **/

public class TutorialPresenter {

    private TutorialView view;
    private final AppUseCase appUseCase;

    @Inject
    public TutorialPresenter(AppUseCase appUseCase) {
        this.appUseCase = appUseCase;
    }

    public void setView(@NonNull TutorialView view) {
        this.view = view;
    }

    void loadTutorial() {
        List<TutorialViewModel> tutorialViewModels = new ArrayList<>();
        tutorialViewModels.add(new TutorialViewModel("1", "Accede a todos tus beneficios en un solo lugar...",
                "", String.valueOf(R.drawable.tutorial_1), String.valueOf(R.drawable.bg_tutorial_1)));
        tutorialViewModels.add(new TutorialViewModel("2", "Sorpréndete con los súper descuentos que tenemos para ti…",
                "", String.valueOf(R.drawable.tutorial_2), String.valueOf(R.drawable.bg_tutorial_2)));
        tutorialViewModels.add(new TutorialViewModel("3", "¡Y disfruta de muchos beneficios más!",
                "", String.valueOf(R.drawable.tutorial_3), String.valueOf(R.drawable.bg_tutorial_3)));
        view.showTutorialList(tutorialViewModels);
        updateWatchBanner();
        /*if (this.view.validateInternet()) {
            this.view.showProgress();
            this.appUseCase.getBanners(new Action.Callback<List<Banner>>() {
                @Override
                public void onSuccess(List<Banner> bannerList) {
                    view.showTutorialList(TutorialViewModel.tutorialViewModelList(bannerList));
                    updateWatchBanner();
                    view.hideProgress();
                }

                @Override
                public void onError(Throwable throwable) {
                    view.showErrorMessage(throwable.getMessage());
                    view.hideProgress();
                }
            });

        }*/

    }

    private void updateWatchBanner() {
        if (this.view.validateInternet()) {
            this.appUseCase.setBannerWatching(new Action.Callback<Boolean>() {
                @Override
                public void onSuccess(Boolean aBoolean) {
                    //DoNothing
                }

                @Override
                public void onError(Throwable throwable) {
                    view.showErrorMessage(throwable.getMessage());
                }
            });
        }
    }


    public interface TutorialView extends MainView {
        void showTutorialList(List<TutorialViewModel> tutorialViewModels);
    }
}
