package com.bcp.benefits.ui.financial.newcredit;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bcp.benefits.R;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by emontesinos on 5/06/19.
 **/
public class DialogTypeCredit extends DialogFragment implements TypeCreditAdapter.ListenerAdapter {

    private OnSelectIdTypeCreditListener onSelectIdType;
    private ArrayList<String> listType;
    private String type;

    public static DialogTypeCredit newInstance(ArrayList<String> typesDoc,String type) {
        DialogTypeCredit fragment = new DialogTypeCredit();
        Bundle args = new Bundle();
        args.putStringArrayList("typeDoc", typesDoc);
        args.putString("typeSelected",type);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (!(context instanceof OnSelectIdTypeCreditListener))
            throw new IllegalArgumentException("Activity is not implementing OnSelectIdTypeListener");

        onSelectIdType = (OnSelectIdTypeCreditListener) context;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            Drawable backgroundColor = new ColorDrawable(ContextCompat.getColor(
                    Objects.requireNonNull(getContext()), android.R.color.transparent));
            Objects.requireNonNull(dialog.getWindow()).setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            dialog.getWindow().setBackgroundDrawable(backgroundColor);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_identy_simple, container, false);
        listType = getArguments().getStringArrayList("typeDoc");
        type = getArguments().getString("typeSelected");
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow()
                .getAttributes().windowAnimations = R.style.DialogTheme;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        RecyclerView rvType = view.findViewById(R.id.rv_type);
        ImageView ivClose = view.findViewById(R.id.iv_close);
        TextView titulo = view.findViewById(R.id.text_title_generic);
        titulo.setText(getResources().getString(R.string.financial_select_credit_type));
        ivClose.setOnClickListener(v -> getDialog().dismiss());
        rvType.setLayoutManager(new LinearLayoutManager(getContext()));
        DividerItemDecoration itemDecoration = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        rvType.addItemDecoration(itemDecoration);
        TypeCreditAdapter typeAdapter = new TypeCreditAdapter(listType);
        rvType.setAdapter(typeAdapter);
        typeAdapter.setSelectedItem(type);
        typeAdapter.setListener(this);

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        Objects.requireNonNull(dialog.getWindow()).requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }


    @Override
    public void selectType(String type, int position) {
        onSelectIdType.onSelectTypeCredit(type, position);
        getDialog().dismiss();

    }

    public interface OnSelectIdTypeCreditListener {
        void onSelectTypeCredit(String type, int position);
    }
}
