package com.bcp.benefits.ui.othres.buses;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bcp.benefits.R;
import com.bcp.benefits.viewmodel.BusScheduleExit;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BusScheduleExitAdapter
        extends RecyclerView.Adapter<BusScheduleExitAdapter.BusScheduleExitViewHolder> {

    private List<BusScheduleExit> schedules;

    private BusScheduleExitListener busScheduleListener;

    BusScheduleExitAdapter(BusScheduleExitListener busScheduleListener) {
        this.schedules = new ArrayList<>();
        this.busScheduleListener = busScheduleListener;
    }

    void setSchedules(List<BusScheduleExit> schedules) {
        this.schedules = schedules;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public BusScheduleExitViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_buses,
                viewGroup, false);
        return new BusScheduleExitViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BusScheduleExitViewHolder holder, int position) {
        holder.bind(schedules.get(position));
        holder.textType.setOnClickListener(v -> busScheduleListener.busScheduleExitSelected(schedules.get(position)));
    }

    @Override
    public int getItemCount() {
        return schedules.size();
    }

    class BusScheduleExitViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_type)
        TextView textType;

        BusScheduleExitViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void bind(BusScheduleExit item) {
            textType.setTypeface(ResourcesCompat.getFont(itemView.getContext(), R.font.breviaregular));
            textType.setText(item.getBusSchedule());
            textType.setBackgroundResource(item.isSelected() ? R.drawable.bg_buses_selected
                    : R.drawable.bg_buses_unselected);
            textType.setTextColor(itemView.getContext().getResources().getColor(item.isSelected()
                    ? R.color.white
                    : R.color.gray_800));
        }
    }

    interface BusScheduleExitListener {
        void busScheduleExitSelected(BusScheduleExit schedule);
    }
}
