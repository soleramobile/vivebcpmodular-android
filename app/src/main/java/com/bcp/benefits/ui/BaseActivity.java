package com.bcp.benefits.ui;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.bcp.benefits.BuildConfig;
import com.google.firebase.messaging.FirebaseMessaging;
import com.bcp.benefits.R;
import com.bcp.benefits.components.MiBancoProgressBar;
import com.bcp.benefits.ui.home.HomeActivity;
import com.bcp.benefits.util.AppUtils;
import com.bcp.benefits.util.fcm.Config;
import com.bcp.benefits.util.fcm.NotificationUtils;
import com.scottyab.rootbeer.RootBeer;

import butterknife.ButterKnife;
import dagger.android.support.DaggerAppCompatActivity;

public abstract class BaseActivity extends DaggerAppCompatActivity implements MainView {

    private MiBancoProgressBar miBancoProgressBar;
    protected String communicationBase;
    protected BroadcastReceiver mRegistrationBroadcastBase;

    public abstract void setUpView();

    public abstract int getLayout();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());
        miBancoProgressBar = new MiBancoProgressBar(this);
        ButterKnife.bind(this);
        if (!validateAntiRoot()) finishAffinity();
        this.setUpView();
        mRegistrationBroadcastBase = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);
                } else {
                    if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                        communicationBase = intent.getStringExtra("idCommunication");
                        Intent intHome = new Intent(getApplicationContext(), HomeActivity.class);
                        intHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intHome.putExtra("idCommunication", communicationBase);
                        startActivity(intHome);
                    } else {
                        if (intent.getAction().equals(Config.PUSH_MESSAGE)) {
                            NotificationUtils notificationUtils;
                            notificationUtils = new NotificationUtils(context);
                            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            notificationUtils.showNotificationMessage(intent.getStringExtra("title"), intent.getStringExtra("message"), "0", intent);
                        }
                    }
                }
            }
        };

    }

    private Boolean validateAntiRoot(){
        if (new RootBeer(this).isRooted()) return rootEnd();

        if (AppUtils.checkRunningProcesses()) return rootEnd();

        if (BuildConfig.DEBUG) return rootEnd();

        return true;
    }

    private Boolean rootEnd(){
        Toast.makeText(this, R.string.rooted_device, Toast.LENGTH_SHORT).show();
        finishAffinity();
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void showColor(int resource) {
        miBancoProgressBar.setColor(getResources().getColor(resource));
    }

    @Override
    public void showProgressColorhealth() {
        miBancoProgressBar.setColor(getResources().getColor(R.color.home_health_end));
    }

    @Override
    public void showProgress() {
        if (miBancoProgressBar != null)
            miBancoProgressBar.show();
    }

    @Override
    public void hideProgress() {
        if (miBancoProgressBar != null)
            miBancoProgressBar.dismiss();
    }

    @Override

    public void showErrorMessage(int message) {
        showErrorConnect(getResources().getString(message));
    }

    @Override
    public Boolean validateInternet() {
        if (AppUtils.isConnected(this)) {
            return true;
        }
        showErrorConnect(getResources().getString(R.string.exception_no_internet_error_view));
        return false;
    }

    @Override
    public void showErrorMessage(String message) {
        final ViewGroup viewGroup = (ViewGroup) ((ViewGroup) this
                .findViewById(android.R.id.content)).getChildAt(0);
        if (message != null && !message.isEmpty()) {
            AppUtils.showErrorMessage(viewGroup, this, message);
        } else {
            showErrorConnect(getResources().getString(R.string.exception_no_internet_error_service));
        }
    }

    public void showErrorConnect(String messageEror) {
        new AlertDialog.Builder(this)
                .setTitle(R.string.app_name)
                .setCancelable(false)
                .setMessage(messageEror)
                .setPositiveButton(R.string.dialog_accept, (dialogInterface, i) -> dialogInterface.dismiss())
                .show();
    }

    @Override
    public void showInfoMessage(int message) {
        //Do nothing
    }

    @Override
    public void showInfoMessage(String message) {
        //Do nothing
    }

    @Override
    public void showSuccessMessage(int message) {
        //Do nothing
    }

    @Override
    public void showSuccessMessage(String message) {
        //Do nothing
    }

    @Override
    public void showWarningMessage(int message) {
        //Do nothing
    }

    @Override
    public void showWarningMessage(String message) {
        //Do nothing
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!validateAntiRoot()) finishAffinity();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastBase,
                new IntentFilter(Config.REGISTRATION_COMPLETE));
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastBase,
                new IntentFilter(Config.PUSH_NOTIFICATION));
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastBase,
                new IntentFilter(Config.PUSH_MESSAGE));
        NotificationUtils.clearNotifications(getApplicationContext());
    }
}
