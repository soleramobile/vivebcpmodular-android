package com.bcp.benefits.ui.health.clinicdetail;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.bcp.benefits.R;
import com.bcp.benefits.util.AppUtils;
import com.bcp.benefits.viewmodel.HealthAttentionTypeViewModel;
/**
 * Created by emontesinos on 23/05/19.
 **/

public class ClinicAttentionTypeAdapter extends RecyclerView.Adapter<ClinicAttentionTypeAdapter.ClinicAttentionTypeViewHolder> {

    public ClinicAttentionTypeAdapter(List<HealthAttentionTypeViewModel> list){
        this.list = list;
    }

    @NonNull
    @Override
    public ClinicAttentionTypeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_clinic_detail, parent, false);
        return new ClinicAttentionTypeViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ClinicAttentionTypeViewHolder holder, int i) {
        holder.bind(list.get(i));

    }

    @Override public int getItemCount() {
        return list==null?0:list.size();
    }




    private List<HealthAttentionTypeViewModel> list;

    class ClinicAttentionTypeViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.tv_amount)
        TextView tvAmount;
        @BindView(R.id.tv_coverage)
        TextView tvCoverage;

        public ClinicAttentionTypeViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(HealthAttentionTypeViewModel attention){
           tvName.setText(attention.getName());
           String text = "S/ "+attention.getAmount();
           String textStyle = itemView.getContext().getString(R.string.co_pago_two,text);
           tvAmount.setText(AppUtils.getStyleText(itemView.getContext(),textStyle,text));
            tvCoverage.setText(itemView.getContext().getString(R.string.text_percentage,attention.getCoverage()));

      }
    }
}
