package com.bcp.benefits.ui.discounts.list;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bcp.benefits.R;
import com.bcp.benefits.viewmodel.DiscountTypeViewModel;
import com.bcp.benefits.viewmodel.StateViewModel;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by emontesinos on 11/06/19.
 */

public class DepartmentAdapter extends RecyclerView.Adapter<DepartmentAdapter.SubsidiaryViewHolder> {

    private ArrayList<StateViewModel> list;
    private OnItemClickDepartmentListener listener;
    private DiscountTypeViewModel type;
    private int selectedItem = -1;
    private Context context;

    public DepartmentAdapter(Context context,DiscountTypeViewModel type) {
        this.context = context;
        this.type = type;
    }

    public void addCategories(ArrayList<StateViewModel> StateViewModels) {
        this.list = StateViewModels;
        notifyDataSetChanged();
    }

    public void setSelectedItem(StateViewModel stateViewModel) {
        if (stateViewModel == null) return;
        int index = list.indexOf(stateViewModel);
        if (selectedItem != index) {
            selectedItem = index;
            notifyDataSetChanged();
        }
    }

    @NotNull
    @Override
    public DepartmentAdapter.SubsidiaryViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_type_custom, parent, false);
        return new SubsidiaryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NotNull SubsidiaryViewHolder holder, int position) {
        holder.text.setText( list.get(position).getName());


        if (selectedItem == position) {
            if (type == DiscountTypeViewModel.DISCOUNT){
                holder.imgCircle.setBackground(context.getResources().getDrawable(R.drawable.bg_cicle_green));
            }else {
                holder.imgCircle.setBackground(context.getResources().getDrawable(R.drawable.bg_cicle_yellow));
            }
             holder.imgCircle.setVisibility(View.VISIBLE);

        } else {
            holder.imgCircle.setVisibility(View.INVISIBLE);
        }





       // holder.itemView.setOnClickListener(v -> listener.onItemDepartmentClick(subsidiary,position));

    }

    public void setListener(OnItemClickDepartmentListener listener) {
        this.listener = listener;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class SubsidiaryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        @BindView(R.id.txt_type_texto)
        TextView text;
        @BindView(R.id.img_circle)
        ImageView imgCircle;


        public SubsidiaryViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            text.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (listener == null)
                return;

            if (selectedItem != -1)
                notifyItemChanged(selectedItem);


            if (selectedItem == getAdapterPosition()) {
                selectedItem = -1;
                notifyItemChanged(getAdapterPosition());
                if (listener != null)
                    listener.onItemDepartmentClick(null, getAdapterPosition());
            } else {
                selectedItem = getAdapterPosition();
                notifyItemChanged(selectedItem);
                listener.onItemDepartmentClick(list.get(getAdapterPosition()), getAdapterPosition());
            }

        }
    }

    public interface OnItemClickDepartmentListener {
        void onItemDepartmentClick(StateViewModel entity, int position);
    }
}
