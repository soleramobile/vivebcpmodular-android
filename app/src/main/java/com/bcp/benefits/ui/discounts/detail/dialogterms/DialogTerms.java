package com.bcp.benefits.ui.discounts.detail.dialogterms;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

import com.bcp.benefits.R;
import com.bcp.benefits.ui.discounts.detail.DiscountDetailActivity;
import com.bcp.benefits.util.AppUtils;
import com.bcp.benefits.viewmodel.DiscountTypeViewModel;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.bcp.benefits.util.Constants.IDENTIFIER_DIALOG_TERMS;
import static com.bcp.benefits.util.Constants.TEXT_TERMS;

/**
 * Created by emontesinos on 13/05/19.
 **/

public class DialogTerms extends DialogFragment {

    public static DialogTerms newInstance(DiscountTypeViewModel dialogIdentifier, String terms) {
        DialogTerms fragment = new DialogTerms();
        Bundle args = new Bundle();
        args.putSerializable(IDENTIFIER_DIALOG_TERMS, dialogIdentifier);
        args.putString(TEXT_TERMS, terms);
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.btn_action_accept)
    TextView btnAccept;
    @BindView(R.id.content_dialog_terms)
    ConstraintLayout contentDialog;
    @BindView(R.id.txt_dialog_term)
    TextView txtDialogText;

    private DiscountTypeViewModel identifierDialog;
    private String terms;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_terms, container, false);
        ButterKnife.bind(this, v);
        assert getArguments() != null;
        identifierDialog = (DiscountTypeViewModel) getArguments().getSerializable(IDENTIFIER_DIALOG_TERMS);
        terms = getArguments().getString(TEXT_TERMS);
        txtDialogText.setMovementMethod(new ScrollingMovementMethod());
        dialogSelect();
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow()
                .getAttributes().windowAnimations = R.style.DialogTheme;
    }

    @NotNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        Objects.requireNonNull(dialog.getWindow()).requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            Drawable backgroundColor = new ColorDrawable(ContextCompat.getColor
                    (Objects.requireNonNull(getContext()), android.R.color.transparent));
            Objects.requireNonNull(dialog.getWindow()).setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            dialog.getWindow().setBackgroundDrawable(backgroundColor);
        }
    }

    @OnClick(R.id.btn_action_accept)
    public void onRegister() {
        dismiss();
        ((DiscountDetailActivity) Objects.requireNonNull(getActivity())).updateFlag();
    }

    private void dialogSelect() {
        switch (identifierDialog) {
            case DISCOUNT:
                contentDialog.setBackground(getResources().getDrawable(R.drawable.bg_type_custom_green));
                txtDialogText.setText(terms);
                AppUtils.setGradient(getResources().getColor(R.color.green_end), getResources().getColor(R.color.green_end),
                        AppUtils.dpToPx((int) getResources().getDimension(R.dimen.size_thirteen)), btnAccept);

                break;
            case EDUTATION:
                contentDialog.setBackground(getResources().getDrawable(R.drawable.bg_type_custom_yellow));
                txtDialogText.setText(terms);
                btnAccept.setBackground(getResources().getDrawable(R.drawable.bg_button_discount_yellow));
                break;
        }

    }
}
