package com.bcp.benefits.ui.othres.licenses;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import javax.inject.Inject;

import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.BindView;
import com.bcp.benefits.R;
import com.bcp.benefits.ui.BaseActivity;
import com.bcp.benefits.ui.othres.GenericPagerAdapter;
import com.bcp.benefits.ui.tutorial.NonSwipeableViewPager;
import com.bcp.benefits.viewmodel.GoldenTicketViewModel;

import static com.bcp.benefits.util.Constants.DATE_START;
import static com.bcp.benefits.util.Constants.GOLDEN_TICKETS;
import static com.bcp.benefits.util.Constants.IDENTIFIER_LICENCE;
import static com.bcp.benefits.util.Constants.IDENTIFIER_LICENCE_APROVAL_DIALOG;
import static com.bcp.benefits.util.Constants.IDENTIFIER_LICENCE_REFUSE_DIALOG;
import static com.bcp.benefits.util.Constants.RESULT_PERMISSION;
import static com.bcp.benefits.util.Constants.TICKETS_TO_APPROVED;
/**
 * Created by emontesinos on 07/05/19.
 **/

public class LicencesActivity extends BaseActivity implements LicencesPresenter.LicencesView {

    @Inject
    LicencesPresenter presenter;

    @BindView(R.id.toolbar)
    ConstraintLayout toolbar;
    @BindView(R.id.img_back)
    ImageButton imgBack;
    @BindView(R.id.title_toolbar)
    TextView titleToobal;
    @BindView(R.id.radioGroup)
    RadioGroup mRadioGroup;
    @BindView(R.id.viewPager_ticket)
    NonSwipeableViewPager vpGoldenTicketsItems;
    private int selector;

    @BindView(R.id.rbMyTickets)
    RadioButton rbMyTickets;
    @BindView(R.id.rbForApproved)
    RadioButton rbForApproved;

    public static void start(Context context) {
        Intent intent = new Intent(context, LicencesActivity.class);
        context.startActivity(intent);
    }

    @Override
    public void setUpView() {
        this.presenter.setView(this);
        setToolbar();
        setUpPager();
        getLicences();


    }

    @Override
    public int getLayout() {
        return R.layout.activity_golden_ticket;
    }

    private void setToolbar() {
        titleToobal.setText(R.string.permission);
        imgBack.setOnClickListener(v -> {
            onBackPressed();
            overridePendingTransition(R.anim.rigth_in, R.anim.right_out);
        });
    }

    private void getLicences() {
        this.presenter.goLicences();
    }

    private void setUpPager() {
        rbMyTickets.setText(R.string.mypermission);
        rbForApproved.setText(R.string.mypermission_pending);
        vpGoldenTicketsItems.setPagingEnabled(false);
        mRadioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            switch (checkedId) {
                case R.id.rbMyTickets:
                    vpGoldenTicketsItems.setCurrentItem(GOLDEN_TICKETS);
                    selector = GOLDEN_TICKETS;
                    break;
                case R.id.rbForApproved:
                    vpGoldenTicketsItems.setCurrentItem(TICKETS_TO_APPROVED);
                    selector = TICKETS_TO_APPROVED;
                    break;
            }
        });
    }

    @Override
    public void showLicences(GoldenTicketViewModel goldenTicketModel) {
        GenericPagerAdapter genericPagerAdapter = new GenericPagerAdapter(getSupportFragmentManager(),IDENTIFIER_LICENCE);
        vpGoldenTicketsItems.setAdapter(genericPagerAdapter);
        genericPagerAdapter.addGoldenTicketResponse(goldenTicketModel);
        if (selector == GOLDEN_TICKETS) {
            mRadioGroup.check(R.id.rbMyTickets);
            vpGoldenTicketsItems.setCurrentItem(GOLDEN_TICKETS);
        } else {
            mRadioGroup.check(R.id.rbForApproved);
            vpGoldenTicketsItems.setCurrentItem(TICKETS_TO_APPROVED);
        }
        mRadioGroup.getChildAt(1).setEnabled(goldenTicketModel.isDepartmentHead());

    }

    @Override
    public void successApprove(String message) {
      getLicences();
    }

    @Override
    public void successCancel(String message) {
      getLicences();
    }

    public void approveTicket(final int idTicket) {
        DialogLicence dp = DialogLicence.newInstance(IDENTIFIER_LICENCE_APROVAL_DIALOG,idTicket);
        dp.show(getSupportFragmentManager(), DATE_START);
    }

    public void cancelTicket(final int idTicket) {
        DialogLicence dp = DialogLicence.newInstance(IDENTIFIER_LICENCE_REFUSE_DIALOG, idTicket);
        dp.show(getSupportFragmentManager(), DATE_START);
    }

    public void approveDialogLicence(final int idTicket) {

        this.presenter.doApproveTicket(idTicket,false);

    }

    public void cancelDialogLicence(final int idTicket) {

        this.presenter.doCancelTicket(idTicket);

    }

    @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_PERMISSION) {
            if(resultCode == Activity.RESULT_OK || resultCode == Activity.RESULT_CANCELED){
               getLicences();
            }
        }
    }

    public void releaseLicence(){
        //presenter.goGoldenTickets();
        Toast.makeText(this,"actulizar lista ticket",Toast.LENGTH_LONG).show();
    }
}
