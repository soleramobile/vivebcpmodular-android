package com.bcp.benefits.ui.campaign.volunteering.adapter;


import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.Html;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bcp.benefits.R;
import com.bcp.benefits.ui.campaign.volunteering.VolunteeringActivity;
import com.bcp.benefits.ui.campaign.volunteering.dialogs.OptionsDialogFragment;
import com.bcp.benefits.ui.campaign.volunteering.dialogs.adapter.ListOptionsAdapter;
import com.bcp.benefits.util.Constants;
import com.bcp.benefits.viewmodel.ListLocalViewModel;
import com.bcp.benefits.viewmodel.ListRoleViewModel;
import com.bcp.benefits.viewmodel.VolunteeringViewModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by emontesinos on 15/05/19.
 **/

public class VolunteeringAdapter extends RecyclerView.Adapter<VolunteeringAdapter.ViewVolunteeringViewHolder> {

    private List<VolunteeringViewModel> list;
    private ListenerViewVolunteeringAdapter listener;
    private Context context;
    private int selectedItem = -1;
    private Integer enableRole = 0;
    private Integer enableLocal = 0;
    private String text = "";
    private String nameLocal;
    private String nameRole;

    public void addList(List<VolunteeringViewModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public void changeTextLocal(int position, String name){
        list.get(position).setOptionLocal(name);
        notifyDataSetChanged();
    }

    public void changeTextRol(int position, String name){
        list.get(position).setOptionRole(name);
        notifyDataSetChanged();
    }

    public VolunteeringAdapter(Context context, ListenerViewVolunteeringAdapter listener, Integer enableRole, Integer enableLocal) {
        this.list = new ArrayList<>();
        this.context = context;
        this.listener = listener;
        this.enableRole = enableRole;
        this.enableLocal = enableLocal;
    }

    @NonNull
    @Override
    public ViewVolunteeringViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_volunteering, viewGroup, false);
        return new ViewVolunteeringViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewVolunteeringViewHolder holder, int position) {
        VolunteeringViewModel volunteering = list.get(position);
        holder.title.setText(volunteering.getNameVolunteer());

        holder.txtDateVolunteering.setText(volunteering.getDate());
        holder.txtCapacity.setText(context.getResources().getString(R.string.txt_con_capacity, String.valueOf(volunteering.getCapacity())));
        holder.txtStock.setText(volunteering.getTime() + "-" + volunteering.getNameVolunteerTime());
        holder.txtNameEncargado.setText((volunteering.getAvailableVolunteer()) + "/" + (volunteering.getCapacity()));
        if (!volunteering.getImageLocal().isEmpty()) {
            Picasso.get().load(volunteering.getImageLocal()).placeholder(R.drawable.ic_place_campam).into(holder.imgAddress);
        }

        if (this.enableRole == 1) {
            holder.tvSelectRol.setVisibility(View.VISIBLE);
            holder.tvSelectRol.setText(volunteering.getOptionRole());
            holder.tvSelectRol.setOnClickListener(view -> {
                listener.notifyClickRole(volunteering, true, Constants.TYPE_ROL, position);
            });
        }

        if (this.enableLocal == 1) {
            Typeface typeface = ResourcesCompat.getFont(context, R.font.flexo_bold);
            holder.txtAddress.setTypeface(typeface);
            holder.txtAddress.setText(volunteering.getOptionLocal());
            holder.txtAddress.setTextColor(ContextCompat.getColor(context, R.color.color_green_volunteering));
            holder.txtAddress.setOnClickListener(view -> {
                listener.notifyClickPlace(volunteering, true, Constants.TYPE_LOCAL, position);
            });
        }else {
            holder.txtAddress.setText(volunteering.getAddress());
        }

        if (position == selectedItem) {
            listener.selectProduct(volunteering);
            holder.imgSelected.setVisibility(View.VISIBLE);
        } else {
            holder.imgSelected.setVisibility(View.GONE);
        }
        holder.itemView.setOnClickListener(v -> {
            if (listener == null)
                return;

            if (selectedItem != -1)
                notifyItemChanged(selectedItem);


            if (selectedItem == position) {
              /*  selectedItem = -1;
                notifyItemChanged(position);
                if (listener != null){
                    //listener.selectProduct(null, -1);
                }*/
            } else {
                selectedItem = position;
                notifyItemChanged(selectedItem);
                listener.selectProduct(volunteering);
            }

        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    class ViewVolunteeringViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_title_volunteering)
        TextView title;
        @BindView(R.id.txt_address)
        TextView txtAddress;
        @BindView(R.id.txt_date_volunteering)
        TextView txtDateVolunteering;
        @BindView(R.id.txt_capacity)
        TextView txtCapacity;
        @BindView(R.id.txt_stock)
        TextView txtStock;
        @BindView(R.id.txt_name_encargado)
        TextView txtNameEncargado;
        @BindView(R.id.img_selected)
        ImageView imgSelected;
        @BindView(R.id.img_address)
        ImageView imgAddress;
        @BindView(R.id.tvSelectRol)
        TextView tvSelectRol;

        ViewVolunteeringViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface ListenerViewVolunteeringAdapter {
        void selectProduct(VolunteeringViewModel select);

        void notifyClickRole(VolunteeringViewModel select, Boolean click, String type, int position);

        void notifyClickPlace(VolunteeringViewModel select, Boolean click, String type, int position);
    }

}
