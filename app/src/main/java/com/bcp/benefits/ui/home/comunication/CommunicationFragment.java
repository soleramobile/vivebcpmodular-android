package com.bcp.benefits.ui.home.comunication;


import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.bcp.benefits.R;
import com.bcp.benefits.ui.BaseFragment;
import com.bcp.benefits.ui.campaign.CampaignActivity;
import com.bcp.benefits.util.AppPreferences;
import com.bcp.benefits.util.AppUtils;
import com.bcp.benefits.viewmodel.CommunicationViewModel;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by emontesinos on 12/07/19.
 **/

public class CommunicationFragment extends BaseFragment implements CompoundButton.OnCheckedChangeListener {


    @BindView(R.id.check_hide)
    CheckBox checkHide;
    @BindView(R.id.iv_image)
    ImageView ivImage;
    @BindView(R.id.btn_action)
    TextView btnAction;

    private static final String COMMUNICATION_PARAMF = "communicationFragment";
    private static final String CAMPAIGN_TYPE = "campaign";
    private AppPreferences sharePreferences;
    private CommunicationViewModel communication;


    public static CommunicationFragment newInstance(CommunicationViewModel communicationModel) {
        CommunicationFragment fragment = new CommunicationFragment();
        Bundle args = new Bundle();
        args.putSerializable(COMMUNICATION_PARAMF, communicationModel);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void setUpView() {
        checkHide.setOnCheckedChangeListener(this);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharePreferences = new AppPreferences(getContext());
        if (getArguments() != null) {
            communication = (CommunicationViewModel) getArguments().getSerializable(COMMUNICATION_PARAMF);
        }


    }

    @Override
    public void onViewCreated(@NotNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (communication.getImage() != null && !communication.getImage().isEmpty()) {
            Picasso.get().load(communication.getImage()).placeholder(R.drawable.ic_place_campam).into(ivImage);
        } else {
            ivImage.setImageDrawable(getResources().getDrawable(R.drawable.ic_place_campam));
        }

        if (!communication.isButton) {
            btnAction.setVisibility(View.INVISIBLE);
        } else {

            btnAction.setText(communication.buttonText);
            int color = Color.parseColor(communication.buttonColor);
            GradientDrawable bgShape = (GradientDrawable) btnAction.getBackground();
            bgShape.setColor(color);
        }

    }

    @Override
    public int getLayout() {
        return R.layout.fragment_communication;
    }

    @OnClick(R.id.btn_action)
    public void onViewClicked() {
        if (communication.action.equals(CAMPAIGN_TYPE)) {

            if (communication.campaignContent != null) {
                if (communication.campaignContent.getIdCampaign() != null && !communication.campaignContent.getIdCampaign().isEmpty()) {
                    CampaignActivity.startSpecific(this.getContext(), communication.campaignContent.getIdCampaign());
                } else {
                    Toast.makeText(getContext(), "No tienes esa campaña", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getContext(), "No tienes esa campaña", Toast.LENGTH_SHORT).show();
            }

        } else {
            AppUtils.openBrowser(getContext(), communication.content);
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        switch (compoundButton.getId()) {
            case R.id.check_hide:
                if (b) {
                    sharePreferences.savePreference("USER" + communication.idCommunication + "", 1);
                } else {
                    sharePreferences.savePreference("USER" + communication.idCommunication + "", 0);
                }


                break;
        }
    }


}
