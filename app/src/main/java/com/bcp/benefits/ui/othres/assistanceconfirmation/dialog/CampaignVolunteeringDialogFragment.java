package com.bcp.benefits.ui.othres.assistanceconfirmation.dialog;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.bcp.benefits.R;
import com.bcp.benefits.viewmodel.CampaignVolunteeringViewModel;
import java.util.ArrayList;
import java.util.Objects;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CampaignVolunteeringDialogFragment extends DialogFragment implements CampaignVolunteeringAdapter.OnItemClickCampaignListener {


    @BindView(R.id.rv_type)
    RecyclerView rvType;
    @BindView(R.id.iv_close)
    ImageView ivClose;
    @BindView(R.id.constraintLayout9)
    ConstraintLayout contentDialog;
    @BindView(R.id.text_title_generic)
    TextView textGeneric;

    private OnSelectCampaignListener onSelectDepartment;
    private ArrayList<CampaignVolunteeringViewModel> listCampaignVolunteering = new ArrayList<>();
    private CampaignVolunteeringViewModel campaignVolunteeringSelected;

    public static CampaignVolunteeringDialogFragment newInstance(CampaignVolunteeringViewModel stateSelected, ArrayList<CampaignVolunteeringViewModel> types
            , OnSelectCampaignListener onSelectDepartment
    ) {
        CampaignVolunteeringDialogFragment fragment = new CampaignVolunteeringDialogFragment();
        Bundle args = new Bundle();
        fragment.onSelectDepartment = onSelectDepartment;
        args.putSerializable("dataCampaignVolunteering", types);
        args.putSerializable("volunteeringSelected", stateSelected);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow()
                .getAttributes().windowAnimations = R.style.DialogTheme;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            Drawable backgroundColor = new ColorDrawable(ContextCompat.getColor(
                    Objects.requireNonNull(getContext()), android.R.color.transparent));
            Objects.requireNonNull(dialog.getWindow()).setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            dialog.getWindow().setBackgroundDrawable(backgroundColor);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_identity_types, container, false);
        ButterKnife.bind(this, v);
        listCampaignVolunteering = (ArrayList<CampaignVolunteeringViewModel>) getArguments().getSerializable("dataCampaignVolunteering");
        campaignVolunteeringSelected = (CampaignVolunteeringViewModel) getArguments().getSerializable("volunteeringSelected");
        return v;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        textGeneric.setText(R.string.by_selected_ch_vo);
        ivClose.setImageResource(R.drawable.ic_close_white);
        contentDialog.setBackground(getResources().getDrawable(R.drawable.bg_type_custom_green));
        ivClose.setOnClickListener(v -> getDialog().dismiss());
        rvType.setLayoutManager(new LinearLayoutManager(getContext()));
        CampaignVolunteeringAdapter campaignVolunteeringAdapter = new CampaignVolunteeringAdapter(getContext());
        DividerItemDecoration itemDecoration = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        rvType.addItemDecoration(itemDecoration);
        rvType.setAdapter(campaignVolunteeringAdapter);
        campaignVolunteeringAdapter.addCategories(listCampaignVolunteering);
        campaignVolunteeringAdapter.setSelectedItem(campaignVolunteeringSelected);
        campaignVolunteeringAdapter.setListener(this);

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        Objects.requireNonNull(dialog.getWindow()).requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onItemCampaignClick(CampaignVolunteeringViewModel entity, int position) {
        onSelectDepartment.secondCombo(entity, position);
        dismiss();

    }

    public interface OnSelectCampaignListener {
        void secondCombo(CampaignVolunteeringViewModel entity, int position);
    }
}
