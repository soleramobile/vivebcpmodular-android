package com.bcp.benefits.ui.login;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;

import com.bcp.benefits.R;
import com.bcp.benefits.analytics.AnalyticsModule;
import com.bcp.benefits.analytics.AnalyticsTags;
import com.bcp.benefits.ui.MainView;
import com.bcp.benefits.util.Constants;

import javax.inject.Inject;

import pe.solera.benefit.main.entity.User;
import pe.solera.benefit.main.usecases.executor.Action;
import pe.solera.benefit.main.usecases.usecases.UserUseCase;

/**
 * Created by emontesinos.
 **/

public class LoginPresenter {

    private LoginView view;
    private final UserUseCase userUseCase;
    private final Context context;

    @Inject
    public LoginPresenter(UserUseCase userUseCase, Context context) {
        this.userUseCase = userUseCase;
        this.context = context;
    }

    public void setView(@NonNull LoginView view) {
        this.view = view;
    }

    void doGetTypeIdFromLoggedInUser(String type) {
        if (type.equals(Constants.TYPE_ID_DNI))
            view.showDniHint();
        else
            view.showCeHint();
    }

    void loadDocNumber(String number, boolean saveDocument) {
        if (this.view.validateInternet()) {
            this.view.showProgress();
            this.userUseCase.signIn(number, saveDocument, new Action.Callback<Boolean>() {
                @Override
                public void onSuccess(Boolean aBoolean) {
                    if (aBoolean) {
                        view.goHome();
                    } else {
                        view.goValidatedError(context.getString(R.string.document_not_found));
                        view.hideProgress();
                    }
                }

                @Override
                public void onError(Throwable throwable) {
                    view.showErrorMessage(throwable.getMessage());
                    view.hideProgress();
                    view.activateButton();
                }
            });
        }
    }

    void loadUser() {
        if (this.view.validateInternet()) {
            this.view.showProgress();
            this.userUseCase.getUser(new Action.Callback<User>() {
                @Override
                public void onSuccess(User user) {
                    view.loadUser(user);
                    view.hideProgress();
                }

                @Override
                public void onError(Throwable throwable) {
                    view.showErrorMessage(throwable.getMessage());
                    view.hideProgress();
                }
            });
        }
    }

    public interface LoginView extends MainView {
        void goHome();

        void goValidatedError(String message);

        void showDniHint();

        void showCeHint();

        void loadUser(User user);

        void activateButton();
    }
}
