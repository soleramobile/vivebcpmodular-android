package com.bcp.benefits.ui.othres.assistanceconfirmation.confirmation.dialog;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

import com.bcp.benefits.R;
import com.bcp.benefits.ui.othres.assistanceconfirmation.confirmation.ConfirmationVolunteeringActivity;
import com.bcp.benefits.util.AppUtils;
import com.bcp.benefits.viewmodel.CollaboratorViewModel;
import com.bcp.benefits.viewmodel.ListCollaboratorViewModel;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class DialogTCommentary extends DialogFragment {

    public static DialogTCommentary newInstance(ListCollaboratorViewModel collaboratorViewModel) {
        DialogTCommentary fragment = new DialogTCommentary();
        Bundle args = new Bundle();
        args.putSerializable("dataCollaborator", collaboratorViewModel);

        fragment.setArguments(args);
        return fragment;
    }


    @BindView(R.id.text_name_collaborator)
    TextView textNameCollaborator;
    @BindView(R.id.ed_commentary)
    AppCompatEditText edCommentary;
    @BindView(R.id.btn_commentary)
    TextView btnCommentary;


    private String commentary = "";

    private ListCollaboratorViewModel collaboratorViewModel;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_commentary, container, false);
        ButterKnife.bind(this, v);
        assert getArguments() != null;
        collaboratorViewModel = (ListCollaboratorViewModel) getArguments().getSerializable("dataCollaborator");
        edCommentary.setText(collaboratorViewModel.getCommentary());
        commentary = collaboratorViewModel.getCommentary();
        textNameCollaborator.setText(collaboratorViewModel.getCollaboratorName());
        if (collaboratorViewModel.getCommentary().isEmpty()){
            AppUtils.setGradient(getResources().getColor(R.color.gray), getResources().getColor(R.color.gray),
                    AppUtils.dpToPx((int) getResources().getDimension(R.dimen.size_thirteen)), btnCommentary);
        }else {
            AppUtils.setGradient(getResources().getColor(R.color.confirmation_end), getResources().getColor(R.color.confirmation_end),
                    AppUtils.dpToPx((int) getResources().getDimension(R.dimen.size_thirteen)), btnCommentary);
        }

        edCommentary.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                commentary = s.toString();
                if (s.toString().length() > 0 && !edCommentary.getText().toString().trim().isEmpty()) {
                    AppUtils.setGradient(getResources().getColor(R.color.confirmation_end), getResources().getColor(R.color.confirmation_end),
                            AppUtils.dpToPx((int) getResources().getDimension(R.dimen.size_thirteen)), btnCommentary);

                } else {
                    AppUtils.setGradient(getResources().getColor(R.color.gray), getResources().getColor(R.color.gray),
                            AppUtils.dpToPx((int) getResources().getDimension(R.dimen.size_thirteen)), btnCommentary);
                }

            }
        });
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow()
                .getAttributes().windowAnimations = R.style.DialogTheme;
    }

    @NotNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        Objects.requireNonNull(dialog.getWindow()).requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            Drawable backgroundColor = new ColorDrawable(ContextCompat.getColor
                    (Objects.requireNonNull(getContext()), android.R.color.transparent));
            Objects.requireNonNull(dialog.getWindow()).setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            dialog.getWindow().setBackgroundDrawable(backgroundColor);
        }
    }

    @OnClick(R.id.img_close_commentary)
    public void onDialogDismiss() {
        dismiss();
    }

    @OnClick(R.id.btn_commentary)
    public void onComment() {
        if (!commentary.trim().isEmpty()) {
            ((ConfirmationVolunteeringActivity)getActivity()).sendComment(collaboratorViewModel,commentary);
            dismiss();
        }
    }
}
