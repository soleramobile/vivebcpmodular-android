package com.bcp.benefits.application;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;

import com.bcp.benefits.di.component.ApplicationComponent;
import com.bcp.benefits.di.component.DaggerApplicationComponent;

public class CredicorpApplication extends DaggerApplication {


    final ApplicationComponent applicationComponent = DaggerApplicationComponent
            .builder()
            .application(this)
            .build();

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return this.applicationComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        applicationComponent.inject(this);


    }

}
