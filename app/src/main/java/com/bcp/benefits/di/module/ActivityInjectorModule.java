package com.bcp.benefits.di.module;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

import com.bcp.benefits.di.scope.MainScope;
import com.bcp.benefits.ui.campaign.CampaignActivity;
import com.bcp.benefits.ui.campaign.aguinaldo.AguinaldoDetailActivity;
import com.bcp.benefits.ui.campaign.aguinaldo.SearchActivity;
import com.bcp.benefits.ui.campaign.campaigndetail.CampaignDetailActivity;
import com.bcp.benefits.ui.campaign.dynamic.DynamicCampaignActivity;
import com.bcp.benefits.ui.campaign.dynamic.referredlist.ReferredActivity;
import com.bcp.benefits.ui.campaign.volunteering.VolunteeringActivity;
import com.bcp.benefits.ui.campaign.volunteering.detail.DetailVolunteeringActivity;
import com.bcp.benefits.ui.discounts.DiscountsActivity;
import com.bcp.benefits.ui.discounts.consume.ConsumeDiscountActivity;
import com.bcp.benefits.ui.discounts.consume.ConsumeDiscountPdfViewActivity;
import com.bcp.benefits.ui.discounts.detail.DiscountDetailActivity;
import com.bcp.benefits.ui.discounts.detail.SubsidiariesActivity;
import com.bcp.benefits.ui.discounts.propose.ProposeDiscountActivity;
import com.bcp.benefits.ui.discounts.rate.RateDiscountActivity;
import com.bcp.benefits.ui.financial.FinancialActivity;
import com.bcp.benefits.ui.financial.newcredit.NewCreditActivity;
import com.bcp.benefits.ui.financial.requestproduct.AprovedCreditActivity;
import com.bcp.benefits.ui.health.clinicdetail.ClinicDetailActivity;
import com.bcp.benefits.ui.health.nearyou.NearYouActivity;
import com.bcp.benefits.ui.health.productlist.HealthProductsActivity;
import com.bcp.benefits.ui.health.detailproduct.HealthProductDetailActivity;
import com.bcp.benefits.ui.home.HomeActivity;
import com.bcp.benefits.ui.home.comunication.CommunicationsActivity;
import com.bcp.benefits.ui.login.LoginActivity;
import com.bcp.benefits.ui.othres.OtherActivity;
import com.bcp.benefits.ui.othres.assistanceconfirmation.confirmation.ConfirmationVolunteeringActivity;
import com.bcp.benefits.ui.othres.assistanceconfirmation.listcapaignvolunteeting.ListCampaignActivity;
import com.bcp.benefits.ui.othres.buses.BusesActivity;
import com.bcp.benefits.ui.othres.golden.GoldenTicketActivity;
import com.bcp.benefits.ui.othres.golden.request.RequestTicketActivity;
import com.bcp.benefits.ui.othres.licenses.LicencesActivity;
import com.bcp.benefits.ui.othres.licenses.requestlicence.LicenceRequestActivity;
import com.bcp.benefits.ui.othres.maternity.MaternityActivity;
import com.bcp.benefits.ui.othres.maternity.MaternityDetailActivity;
import com.bcp.benefits.ui.othres.volunteering.VolunteeringTicketActivity;
import com.bcp.benefits.ui.validate.ValidateActivity;
import com.bcp.benefits.ui.splash.SplashActivity;
import com.bcp.benefits.ui.tutorial.TutorialActivity;

@Module(includes = AndroidSupportInjectionModule.class)
public abstract class ActivityInjectorModule {
    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract SplashActivity splashActivityInjector();

    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract TutorialActivity tutorialActivityInjector();

    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract ValidateActivity registerActivityInjector();

    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract LoginActivity loginActivityInjector();

    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract HomeActivity homeActivityInjector();

    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract OtherActivity otherActivityInjector();

    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract MaternityActivity maternityActivityInjector();

    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract GoldenTicketActivity goldenTicketActivityInjector();

    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract RequestTicketActivity requestTicketActivityInjector();

    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract LicencesActivity licencesActivityInjector();

    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract LicenceRequestActivity licenceRequestInjector();

    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract CampaignActivity campaignInjector();

    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract AguinaldoDetailActivity aguinaldoDetailInjector();

    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract DynamicCampaignActivity dynamicCampaignInjector();

    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract CampaignDetailActivity campaignDetailInjector();

    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract MaternityDetailActivity maternityDetailInjector();

    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract FinancialActivity finalcialDetailInjector();

    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract NewCreditActivity newCreditInjector();

    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract AprovedCreditActivity aprovedCreditInjector();

    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract HealthProductsActivity healthProductsInjector();

    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract HealthProductDetailActivity healthProductDetailInjector();

    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract ClinicDetailActivity clinicDetailInjector();

    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract DiscountsActivity DiscountsInjector();

    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract NearYouActivity nearYouInjector();

    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract DiscountDetailActivity discountDetailInjector();

    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract SearchActivity searchDialogFragmentInjector();

    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract ConsumeDiscountActivity ConsumeDiscountActivityInjector();

    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract ProposeDiscountActivity ProposeDiscountActivityInjector();

    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract SubsidiariesActivity subsidiariesActivityInjector();

    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract ReferredActivity ReferredActivityInjector();

    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract RateDiscountActivity rateDiscountActivityInjector();

    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract ConsumeDiscountPdfViewActivity consumeDiscountPdfViewActivityInjector();


    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract CommunicationsActivity communicationsActivityInjector();

    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract BusesActivity busesActivityInjector();

    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract VolunteeringTicketActivity VolunteeringTicketActivityInjector();


    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract VolunteeringActivity volunteetingActivityInjector();

    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract DetailVolunteeringActivity detailVolunteeringActivityInjector();


    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract ListCampaignActivity listCampaignActivityInjector();

    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract ConfirmationVolunteeringActivity confirmationVolunteeringInjector();
}
