package com.bcp.benefits.di.module;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

import com.bcp.benefits.di.scope.MainScope;
import com.bcp.benefits.ui.discounts.list.DiscountProductFragment;
import com.bcp.benefits.ui.discounts.list.DiscountsListFragment;
import com.bcp.benefits.ui.discounts.nearyoudiscounts.MapNearYouDiscountFragment;
import com.bcp.benefits.ui.health.nearyou.fragment.HealthProductFragment;
import com.bcp.benefits.ui.health.nearyou.fragment.MapNearFragment;
import com.bcp.benefits.ui.home.comunication.CommunicationFragment;
import com.bcp.benefits.ui.othres.golden.mytickets.MyTicketsFragment;
import com.bcp.benefits.ui.othres.golden.ticketsapproved.TicketsApprovedFragment;
import com.bcp.benefits.ui.othres.licenses.licenceapproved.LicenceApprovedFragment;
import com.bcp.benefits.ui.othres.licenses.mylicences.MysLicencesFragment;
import com.bcp.benefits.ui.othres.volunteering.myTicketsVounteering.MyTicketsVoulunteeringFragment;
import com.bcp.benefits.ui.othres.volunteering.ticketapprovedvolunteering.TicketsApprovedVolunteeringFragment;
import com.bcp.benefits.ui.tutorial.TutorialFragment;

@Module
public abstract class FragmentInjectorModule {
    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract TutorialFragment tutorialFragmentInjector();

    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract MyTicketsFragment myTicketsFragmentInjector();

    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract MyTicketsVoulunteeringFragment myTicketsVoulunteeringFragmentInjector();

    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract TicketsApprovedFragment ticketsApprovedFragmentInjector();

    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract TicketsApprovedVolunteeringFragment ticketsApprovedVolunteeringFragmentInjector();

    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract MysLicencesFragment myLicenceFragmentInjector();

    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract LicenceApprovedFragment licenceApprovedFragmentInjector();

    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract MapNearFragment mapNearFragmentInjector();

    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract HealthProductFragment healthProductFragmentInjector();

    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract DiscountsListFragment discountsFragmentInjector();

    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract MapNearYouDiscountFragment nearYouDiscountFragmentInjector();

    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract DiscountProductFragment discountProductFragmentInjector();

    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract CommunicationFragment communicationFragmentInjector();



}
