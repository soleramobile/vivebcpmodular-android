package com.bcp.benefits.di.module;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import pe.solera.benefit.main.entity.Device;

import com.bcp.benefits.BuildConfig;
import com.bcp.benefits.application.CredicorpApplication;
import com.bcp.benefits.util.AppUtils;

@Module
public class ApplicationModule {
    @Provides
    @Singleton
    Context providerApplicationContext(CredicorpApplication credicorpApplication) {
        return credicorpApplication.getApplicationContext();
    }

    @Provides
    @Singleton
    Device providerDeviceModel(Context context) {
        return new Device(BuildConfig.BASE_URL, AppUtils.getDensity(context),
                AppUtils.getWidth(context), AppUtils.getHeight(context), BuildConfig.VERSION_NAME,
                String.valueOf(BuildConfig.VERSION_CODE), BuildConfig.DEBUG);
    }
}
