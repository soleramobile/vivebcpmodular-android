package com.bcp.benefits.di.module;

import dagger.Binds;
import dagger.Module;

import com.bcp.benefits.ui.BaseActivity;
import com.bcp.benefits.ui.BaseFragment;
import com.bcp.benefits.ui.MainFragment;
import com.bcp.benefits.ui.MainView;

@Module
public abstract class MainModule {
    @Binds
    abstract MainView bindBaseActivity(BaseActivity baseActivity);

    @Binds
    abstract MainFragment bindBaseFragment(BaseFragment baseFragment);

}
