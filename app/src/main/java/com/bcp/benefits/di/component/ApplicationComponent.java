package com.bcp.benefits.di.component;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import pe.solera.benefit.main.local.database.di.RoomModule;
import pe.solera.benefit.main.local.file.di.FileModule;
import pe.solera.benefit.main.local.preferences.di.PreferencesModule;
import pe.solera.benefit.main.remote.network.di.NetworkModule;
import pe.solera.benefit.main.usecases.executor.di.ExecutorModule;

import com.bcp.benefits.application.CredicorpApplication;
import com.bcp.benefits.di.module.ActivityInjectorModule;
import com.bcp.benefits.di.module.ApplicationModule;
import com.bcp.benefits.di.module.FragmentInjectorModule;

@Singleton
@Component(modules = {
        ApplicationModule.class,
        ActivityInjectorModule.class,
        FragmentInjectorModule.class,
        ExecutorModule.class,
        RoomModule.class,
        PreferencesModule.class,
        NetworkModule.class,
        FileModule.class
})
public interface ApplicationComponent extends AndroidInjector<CredicorpApplication> {
    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(CredicorpApplication credicorpApplication);

        ApplicationComponent build();
    }

}
