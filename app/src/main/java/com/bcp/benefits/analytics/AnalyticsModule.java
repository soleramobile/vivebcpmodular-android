package com.bcp.benefits.analytics;

/*
    Created by: Flavia Figueroa
    Email: ffigueroa@solera.pe
    
    11/30/20 - 14:38
    Solera Mobile
*/

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.google.firebase.analytics.FirebaseAnalytics;

public class AnalyticsModule {
    private FirebaseAnalytics mFirebaseAnalytics;

    public AnalyticsModule(Context context) {
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
    }

    public void sendToAnalytics(String event, @Nullable  Bundle bundle) {
        this.mFirebaseAnalytics.logEvent(event, bundle);
    }
}
