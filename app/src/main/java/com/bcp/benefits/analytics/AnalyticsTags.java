package com.bcp.benefits.analytics;

/*
    Created by: Flavia Figueroa
    Email: ffigueroa@solera.pe
    
    11/27/20 - 14:59
    Solera Mobile
*/

public class AnalyticsTags {
    public static class Login {
        public static class Events {
            public static String login_exitoso = "login_exitoso";
        }

        public static class Parameters {
            public static String login_tipo = "login_tipo";
        }

        public static class Values {
            public static String login_principal = "login principal";
            public static String login_secundario = "login secundario";
        }
    }

    public static class Discounts {
        public static class Events {
            public static String descuento = "descuento";
            public static String descuento_detalle = "descuento_detalle";
            public static String descuento_exitoso = "descuento_exitoso";
        }

        public static class Parameters {
            public static String beneficios_id = "beneficios_id";
            public static String beneficios_nombre = "beneficios_nombre";
            public static String beneficios_empresa_id = "beneficios_empresa_id";
            public static String beneficios_empresa = "beneficios_empresa";
            public static String beneficios_tipo = "beneficios_tipo";
            public static String pantalla_tipo = "pantalla_tipo";
        }
    }

    public static class Education {
        public static class Events {
            public static String educacion = "educacion";
            public static String educacion_detalle = "educacion_detalle";
            public static String educacion_exitoso = "educacion_exitoso";
        }

        public static class Parameters {
            public static String beneficios_id = "beneficios_id";
            public static String beneficios_nombre = "beneficios_nombre";
            public static String beneficios_empresa_id = "beneficios_empresa_id";
            public static String beneficios_empresa = "beneficios_empresa";
            public static String beneficios_tipo = "beneficios_tipo";
            public static String pantalla_tipo = "pantalla_tipo";
        }
    }

    public static class Health {
        public static class Events {
            public static String salud = "salud";
        }
    }

    public static class Finance {
        public static class Events {
            public static String financiero = "financiero";
        }
    }

    public static class Others {
        public static class Events {
            public static String otros = "otros";
            public static String ticketDorado = "ticketDorado";
            public static String ticketVoluntariado = "ticketVoluntariado";
            public static String horariosAutos = "horariosAutos";
        }
    }

    public static class Campaigns {
        public static class Events {
            public static String campana = "campana";
            public static String campana_detalle = "campana_detalle";
        }

        public static class Parameters {
            public static String campana_clase_id = "campana_clase_id";
            public static String campana_clase = "campana_clase";
            public static String campana_id = "campana_id";
            public static String campana_nombre = "campana_nombre";
        }
    }

    public static class ProposeDiscounts {
        public static class Events {
            public static String proponerDescuento = "proponerDescuento";
            public static String proponerDescuento_exitoso = "proponerDescuento_exitoso";
        }

        public static class Parameters {
            public static String categoria_nombre ="categoria_nombre";
            public static String beneficios_categoria = "beneficios_categoria";
            public static String beneficios_mensaje = "beneficios_mensaje";
        }
    }

    public static class Rate {
        public static class Events {
            public static String calificacion = "calificacion";
            public static String calificacion_exitoso = "calificacion_exitoso";
        }

        public static class Parameters {
            public static String categoria_nombre = "categoria_nombre";
            public static String beneficios_id = "beneficios_id";
            public static String beneficios_nombre = "beneficios_nombre";
            public static String calificacion_categoria = "calificacion_categoria";
            public static String beneficios_empresa = "beneficios_empresa";
            public static String contador_estrellas = "contador_estrellas";
        }
    }

    public static class TransversalElements {
        public static class Events {
            public static String btn_interactuar = "btn_interactuar";
        }

        public static class Parameters {
            public static String categoria_nombre = "categoria_nombre";
            public static String boton_accion = "boton_accion";
            public static String boton_nombre = "boton_nombre";
            public static String pantalla_tipo = "pantalla_tipo";
            public static String beneficios_id = "beneficios_id";
            public static String beneficios_nombre = "beneficios_nombre";
            public static String beneficios_empresa_id = "beneficios_empresa_id";
            public static String beneficios_empresa = "beneficios_empresa";
        }
    }

    public static class Common{
        public static class Values{
            public static String lista = "lista";
            public static String mapa = "mapa";
            public static String campana = "campaña";
        }
    }
}

