package com.bcp.benefits.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;

import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.AppCompatTextView;

import android.util.AttributeSet;

import com.bcp.benefits.R;


/**
 * Created by emontesinos.
 **/


public class TextViewWithDrawable extends AppCompatTextView {
    public TextViewWithDrawable(Context context) {
        super(context);
    }

    public TextViewWithDrawable(Context context, AttributeSet attrs) {
        super(context, attrs);
        initAttrs(context, attrs);
    }

    public TextViewWithDrawable(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initAttrs(context, attrs);
    }

    private void initAttrs(Context context, AttributeSet attrs) {

        setFocusableInTouchMode(false);

        if (attrs != null) {
            TypedArray attributeArray = context.obtainStyledAttributes(
                    attrs,
                    R.styleable.TextViewWithDrawable);

            Drawable drawableLeft = null;
            Drawable drawableRight = null;
            Drawable drawableBottom = null;
            Drawable drawableTop = null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                drawableLeft = attributeArray.getDrawable(
                        R.styleable.TextViewWithDrawable_drawableLeftCompat);
                drawableRight = attributeArray.getDrawable(
                        R.styleable.TextViewWithDrawable_drawableRightCompat);
                drawableBottom = attributeArray.getDrawable(
                        R.styleable.TextViewWithDrawable_drawableBottomCompat);
                drawableTop = attributeArray.getDrawable(
                        R.styleable.TextViewWithDrawable_drawableTopCompat);
            } else {
                final int drawableLeftId = attributeArray.getResourceId(
                        R.styleable.TextViewWithDrawable_drawableLeftCompat, -1);
                final int drawableRightId = attributeArray.getResourceId(
                        R.styleable.TextViewWithDrawable_drawableRightCompat, -1);
                final int drawableBottomId = attributeArray.getResourceId(
                        R.styleable.TextViewWithDrawable_drawableBottomCompat, -1);
                final int drawableTopId = attributeArray.getResourceId(
                        R.styleable.TextViewWithDrawable_drawableTopCompat, -1);

                if (drawableLeftId != -1)
                    drawableLeft = AppCompatResources.getDrawable(context, drawableLeftId);
                if (drawableRightId != -1)
                    drawableRight = AppCompatResources.getDrawable(context, drawableRightId);
                if (drawableBottomId != -1)
                    drawableBottom = AppCompatResources.getDrawable(context, drawableBottomId);
                if (drawableTopId != -1)
                    drawableTop = AppCompatResources.getDrawable(context, drawableTopId);
            }
            setCompoundDrawablesWithIntrinsicBounds(drawableLeft, drawableTop, drawableRight,
                    drawableBottom);
            attributeArray.recycle();
        }
    }
}
