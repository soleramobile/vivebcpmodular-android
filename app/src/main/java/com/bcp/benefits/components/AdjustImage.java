package com.bcp.benefits.components;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatImageView;

public class AdjustImage extends AppCompatImageView {
    public AdjustImage(Context context) {
        super(context);
    }

    public AdjustImage(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AdjustImage(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        Drawable drawable = getDrawable();
        if (drawable != null) {
            int width = MeasureSpec.getSize(widthMeasureSpec);

            int height = (int) Math.ceil((double) (width * (float) drawable.getIntrinsicHeight() / (float) drawable.getIntrinsicWidth()));

            setMeasuredDimension(width, height);
        } else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }
}
