package com.bcp.benefits.components;

/**
 * Created by emontesinos.
 **/

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

public class DateTextWatcher implements TextWatcher {

    private final EditText editText;

    public DateTextWatcher(EditText editText) {
        this.editText = editText;
        this.editText.addTextChangedListener(this);
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        String working = s.toString();
        int length = working.length();

        if (length == 2 && before == 0) {
            working += "/";
            editText.setText(working);
            editText.setSelection(working.length());
        }

        if (length == 5 && before == 0) {
            working += "/";
            editText.setText(working);
            editText.setSelection(working.length());
        }
    }

    @Override
    public void afterTextChanged(Editable s) {
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

}

