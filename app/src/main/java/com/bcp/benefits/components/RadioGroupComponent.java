package com.bcp.benefits.components;

import android.content.Context;

import androidx.annotation.DrawableRes;

import android.util.AttributeSet;
import android.widget.RadioGroup;

import com.bcp.benefits.R;

/**
 * Created by emontesinos on 17/05/19.
 **/
public class RadioGroupComponent extends RadioGroup {
    private Context context;
    @DrawableRes
    private int selectColor;
    @DrawableRes
    private int unSelectColor;

    public RadioGroupComponent(Context context) {
        super(context);
        this.context = context;
        init();
    }

    private void init() {
        this.selectColor = R.drawable.ic_indicator_green;
        this.unSelectColor = R.drawable.ic_indicator_unselect;
    }

    public RadioGroupComponent(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    public void setColor(@DrawableRes int selectColor, @DrawableRes int unSelectColor) {
        this.selectColor = selectColor;
        this.unSelectColor = unSelectColor;
    }

    public void setChecked(int position) {
        this.changeColorButtons();
        this.getChildAt(position).setBackground(context.getDrawable(selectColor));
        this.check(position);
    }

    private void changeColorButtons() {
        int countButtons = getChildCount();
        for (int i = 0; i < countButtons; i++) {
            getChildAt(i).setBackground(context.getDrawable(unSelectColor));
        }
    }
}
