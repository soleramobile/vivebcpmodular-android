package com.bcp.benefits.components;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.bcp.benefits.util.ViewIdGenerator;
import com.bcp.benefits.viewmodel.DynamicFieldValueViewModel;
import com.bcp.benefits.viewmodel.DynamicFieldViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by emontesinos on 10/06/19.
 **/

public class DynamicFormView extends LinearLayout {

    private List<DynamicFieldViewModel> modelList;

    public DynamicFormView(Context context) {
        super(context);
        initView();
    }

    public DynamicFormView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public DynamicFormView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        if (this.getId() == -1) {
            this.setId(ViewIdGenerator.generateViewId());
        }
    }

    public void setModel(List<DynamicFieldViewModel> modelList) {
        this.modelList = modelList;

        if (modelList != null) {
            for (DynamicFieldViewModel model : modelList) {
                DynamicFieldView dynamicFieldView = new DynamicFieldView(getContext());
                dynamicFieldView.setModel(model);
                this.addView(dynamicFieldView);
            }
        }
    }

    public List<DynamicFieldValueViewModel> getValues() {
        List<DynamicFieldValueViewModel> allValueList = new ArrayList<>();
        for (int i = 0; i < this.getChildCount(); i++) {
            DynamicFieldView dfv = (DynamicFieldView) this.getChildAt(i);
            allValueList.addAll(dfv.getValues());
        }
        return allValueList;
    }

    public List<DynamicFieldViewModel> getModelList() {
        return modelList;
    }
}
