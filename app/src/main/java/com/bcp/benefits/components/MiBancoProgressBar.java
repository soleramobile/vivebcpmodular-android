package com.bcp.benefits.components;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;

import com.bcp.benefits.R;
import com.wang.avi.AVLoadingIndicatorView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;

/**
 * Created by emontesinos on 12/06/19.
 */

public class MiBancoProgressBar extends AlertDialog {


    private int color;

    private void init() {
        color = getContext().getResources().getColor(R.color.colorPrimary);
    }

    public MiBancoProgressBar(@NonNull Context context) {
        super(context, android.R.style.Theme_Holo_Light_NoActionBar_Fullscreen);
        init();
        setView(context);

    }

    public MiBancoProgressBar(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        setView(context);
        init();
    }

    protected MiBancoProgressBar(@NonNull Context context, boolean cancelable, @Nullable DialogInterface.OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        setView(context);
        init();
    }


    private void setView(Context context) {
        getWindow().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(context, R.color.transparency_white)));
    }

    public void setColor(int colorIndicator) {
        color = colorIndicator;
    }

    public void show() {
        super.show();
        this.setContentView(R.layout.dialog_progressbar);
        AVLoadingIndicatorView progress = findViewById(R.id.pg_loading);
        progress.setIndicatorColor(color);
        this.setCancelable(false);
    }


}
