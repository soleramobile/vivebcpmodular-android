package com.bcp.benefits.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.bcp.benefits.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchViewEditText extends ConstraintLayout {
    @BindView(R.id.edtSearchEdit)
    EditText edtSearch;
    @BindView(R.id.textSearch)
    AppCompatTextView textSearch;
    private boolean isEnable = false;
    private String textHint = "";

    public SearchViewEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        View view = inflate(getContext(), R.layout.componet_search_view, this);
        ButterKnife.bind(this, view);
        TypedArray attributeArray = getContext().obtainStyledAttributes(
                attrs,
                R.styleable.SearchViewEditText);
        textHint = attributeArray.getString(
                R.styleable.SearchViewEditText_hint);
        attributeArray.recycle();
        edtSearch.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
    }

    public void addTextChangedListener(TextWatcher textWatcher) {
        edtSearch.addTextChangedListener(textWatcher);
    }

    public void setOnEditorActionListener(TextView.OnEditorActionListener onEditorActionListener) {
        edtSearch.setOnEditorActionListener(onEditorActionListener);
    }

    public String getText() {
        return edtSearch.getText().toString();
    }

    public void setText(String text) {
        edtSearch.setText(text);
        textSearch.setText(text.trim().isEmpty() ? textHint : text);
        textSearch.setTextColor(text.trim().isEmpty()
                ? getContext().getResources().getColor(R.color.gray_350)
                : getContext().getResources().getColor(R.color.gray_800));
        edtSearch.setSelection(edtSearch.length());
    }

    public void setHint(String text) {
        edtSearch.setHint(text);
        textSearch.setText(text);
    }

    public void setEnableEditText(boolean isEnable) {
        this.isEnable = isEnable;
        this.edtSearch.setVisibility(isEnable ? VISIBLE : GONE);
        this.textSearch.setVisibility(isEnable ? GONE : VISIBLE);
    }

    public boolean isEnable() {
        return isEnable;
    }

    public void setOnClickListener(OnClickListener onClickListener) {
        this.textSearch.setOnClickListener(onClickListener);
    }
}
