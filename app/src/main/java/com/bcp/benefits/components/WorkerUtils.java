package com.bcp.benefits.components;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.bcp.benefits.R;
import com.bcp.benefits.util.Constants;

public class WorkerUtils {
    public static void makeStatusNotification(String message, Context context) {
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(Constants.CHANNEL_ID,
                    Constants.VERBOSE_NOTIFICATION_CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH);
            channel.setDescription(Constants.VERBOSE_NOTIFICATION_CHANNEL_DESCRIPTION);
            NotificationManager notificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(channel);
        }
        long[] vibrates = {0L};
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, Constants.CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(Constants.NOTIFICATION_TITLE)
                .setContentText(message)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setVibrate(vibrates);

        NotificationManagerCompat.from(context).notify(Constants.NOTIFICATION_ID, builder.build());
    }
}
