package com.bcp.benefits.components;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.bcp.benefits.util.Constants;

public class AlarmWorker extends Worker {

    public AlarmWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
        String titleAlarm = getInputData().getString(Constants.ALARM_TITLE);
        WorkerUtils.makeStatusNotification(titleAlarm, getApplicationContext());
        return Result.success();
    }
}
