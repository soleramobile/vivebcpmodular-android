package com.bcp.benefits.components;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import com.bcp.benefits.R;
import com.bcp.benefits.util.AppUtils;
import com.bcp.benefits.util.ViewIdGenerator;
import com.bcp.benefits.viewmodel.DynamicFieldValueViewModel;
import com.bcp.benefits.viewmodel.DynamicFieldViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by emontesinos on 10/06/19.
 **/

public class DynamicFieldView extends ConstraintLayout {
    private static final int TAG_FIELD_ID = 1 + 2 << 24;
    private static final String NUMERIC = "numeric";
    private static final String ALPHANUMERIC = "alphanumeric";
    private static final String SELECTION = "selection";

    private TextView lblLabel;
    private EditText edtInput;
    private DynamicFieldViewModel model;
    private RadioGroup radioGroup;
    private LinearLayout llyNestedFields;

    public DynamicFieldView(Context context) {
        super(context);
        initView(context);
    }

    public DynamicFieldView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public DynamicFieldView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        inflate(context, R.layout.dynamic_view_field, this);
        if (this.getId() == -1) {
            this.setId(ViewIdGenerator.generateViewId());
        }
        lblLabel = findViewById(R.id.lbl_label);
        edtInput = findViewById(R.id.edt_input);
        radioGroup = findViewById(R.id.radio_group);
        llyNestedFields = findViewById(R.id.lly_nested_fields);
    }

    public void setModel(DynamicFieldViewModel model) {
        this.model = model;

        if (model != null) {
            lblLabel.setText(model.getLabel());

            switch (model.getType()) {
                case DynamicFieldViewModel.TYPE_SWITCH:
                    edtInput.setVisibility(View.GONE);
                    radioGroup.setVisibility(View.VISIBLE);

                    if (model.getValues().size() > 2) {
                        for (int i = model.getValues().size() - 1; i > 1; i--) {
                            model.getValues().remove(i);
                        }
                    }
                    buildNestedFields(model.getValues(), model.getDynamicFieldModelList());

                    break;
                case DynamicFieldViewModel.TYPE_STRING:
                    edtInput.setVisibility(View.VISIBLE);
                    edtInput.setFilters(new InputFilter[]{new InputFilter.LengthFilter(model.getMax())});
                    assignKeyboard(model.getKeyboard(), edtInput);
                    radioGroup.setVisibility(View.GONE);
                    break;
                case DynamicFieldViewModel.TYPE_NUMBER:
                    edtInput.setVisibility(View.VISIBLE);
                    assignKeyboard(model.getKeyboard(), edtInput);
                    radioGroup.setVisibility(View.GONE);
                    break;
                default:
                    throw new RuntimeException("Field Type not implemented");
            }

            if (model.getValidationType() == DynamicFieldViewModel.VALIDATION_TYPE_LENGTH) {
                edtInput.setFilters(new InputFilter[]{new InputFilter.LengthFilter(model.getMax())});
                edtInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        isValid();
                    }
                });
            } else if (model.getValidationType() == DynamicFieldViewModel.VALIDATION_TYPE_VALUE) {
                if (model.getLabel().equals("edad") || model.getLabel().equals("EDAD")) {
                    InputFilter[] filterArray = new InputFilter[1];
                    filterArray[0] = new InputFilter.LengthFilter(10);
                    edtInput.setFilters(filterArray);
                }
                edtInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        isValid();
                    }
                });
            }


        }
    }

    private void assignKeyboard(String keyboard, EditText eText) {
        if (keyboard.equals(NUMERIC)) {
            eText.setInputType(InputType.TYPE_CLASS_NUMBER);
        } else if (keyboard.equals(ALPHANUMERIC)) {
            eText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        } else if (keyboard.equals(SELECTION)) {
            return;
        }
    }

    private void buildNestedFields(List<String> valueList, List<DynamicFieldViewModel> fieldModelList) {
        for (int i = 0; i < valueList.size(); i++) {

            RadioButton rb = new RadioButton(getContext());
            rb.setText(valueList.get(i));
            rb.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.bg_sky_blue_corner));
            rb.setButtonDrawable(null);
            rb.setTypeface(Typeface.DEFAULT);
            rb.setGravity(Gravity.CENTER_HORIZONTAL);
            rb.setTextColor(ContextCompat.getColorStateList(getContext(), R.color.bg_check_subsidiary_text));
            rb.setPadding(
                    AppUtils.convertDpToPx(getContext(), 16),
                    AppUtils.convertDpToPx(getContext(), 8),
                    AppUtils.convertDpToPx(getContext(), 16),
                    AppUtils.convertDpToPx(getContext(), 8)
            );
            RadioGroup.LayoutParams params = new RadioGroup.LayoutParams(getContext(), null);
            params.width = LayoutParams.MATCH_PARENT;
            params.weight = 1f / valueList.size();
            params.setMargins(
                    AppUtils.convertDpToPx(getContext(), 8 * (i != 0 ? 1 : 0)),
                    AppUtils.convertDpToPx(getContext(), 0),
                    AppUtils.convertDpToPx(getContext(), 8 * (i != valueList.size() - 1 ? 1 : 0)),
                    AppUtils.convertDpToPx(getContext(), 0));
            rb.setLayoutParams(params);
            radioGroup.addView(rb);

            if (i < fieldModelList.size()) {
                DynamicFieldViewModel fieldModel = fieldModelList.get(i);
                DynamicFieldView fieldView = new DynamicFieldView(getContext());
                fieldView.setModel(fieldModel);
                fieldView.setVisibility(View.GONE);
                llyNestedFields.addView(fieldView);
                rb.setTag(TAG_FIELD_ID, fieldView.getId());
            }
        }

        radioGroup.setOnCheckedChangeListener((radioGroup, i) -> {
            RadioButton checkedRadioButton = radioGroup.findViewById(radioGroup.getCheckedRadioButtonId());
            Integer fieldId = (Integer) checkedRadioButton.getTag(TAG_FIELD_ID);
            if (fieldId != null) {
                hideAllNestedFields();
                llyNestedFields.findViewById(fieldId).setVisibility(VISIBLE);
            }
        });

        if (radioGroup.getChildCount() > 0) {
            RadioButton firstRadioButton = (RadioButton) radioGroup.getChildAt(0);

            Integer fieldId = (Integer) firstRadioButton.getTag(TAG_FIELD_ID);
            if (fieldId != null) {
                llyNestedFields.findViewById(fieldId).setVisibility(VISIBLE);
            }
        }

    }

    private void hideAllNestedFields() {
        for (int i = 0; i < llyNestedFields.getChildCount(); i++) {
            llyNestedFields.getChildAt(i).setVisibility(GONE);
        }
    }

    public List<DynamicFieldValueViewModel> getValues() {
        List<DynamicFieldValueViewModel> allValueList = new ArrayList<>();

        switch (model.getType()) {
            case DynamicFieldViewModel.TYPE_SWITCH:
                RadioButton rb = radioGroup.findViewById(radioGroup.getCheckedRadioButtonId());
                if (rb != null) {
                    allValueList.add(new DynamicFieldValueViewModel(model.getId(), rb.getText().toString()));

                    for (int i = 0; i < llyNestedFields.getChildCount(); i++) {
                        DynamicFieldView dfv = (DynamicFieldView) llyNestedFields.getChildAt(i);
                        if (dfv.getVisibility() == VISIBLE) {
                            allValueList.addAll(dfv.getValues());
                        }
                    }
                }
                break;
            case DynamicFieldViewModel.TYPE_STRING:
                String stringValue = edtInput.getText().toString();
                if (!stringValue.trim().isEmpty()) {
                    allValueList.add(new DynamicFieldValueViewModel(model.getId(), stringValue, isValid()));
                }
                break;
            case DynamicFieldViewModel.TYPE_NUMBER:
                String numberValue = edtInput.getText().toString();
                if (!numberValue.trim().isEmpty()) {
                    allValueList.add(new DynamicFieldValueViewModel(model.getId(), numberValue, isValid()));
                }
                break;
            default:
                throw new RuntimeException("Field Type not implemented");
        }
        return allValueList;
    }

    private boolean isValidValue(String name) {
        boolean isValid = false;
        Long value = 0L;
        if (edtInput.getText().length() > 0) {
            try {
                value = Long.parseLong(edtInput.getText().toString());
            } catch (Exception ignored) {
            }
            if (value < model.getMin()) {
                edtInput.setError(String.format("%s debe ser mayor a %s", name, model.getMin()));
            } else if (value > model.getMax()) {
                edtInput.setError(String.format("%s debe ser menor a %s", name, model.getMax()));
            } else {
                edtInput.setError(null);
                isValid = true;
            }
        }
        return isValid;
    }

    private boolean isValidLength(String name) {
        boolean isValid = false;
        if (edtInput.getText().length() > 0) {
            if (edtInput.getText().length() < model.getMin()) {
                edtInput.setError(String.format("%s debe tener mínimo %s caracteres", name, model.getMin()));
            } else if (edtInput.getText().length() > model.getMax()) {
                edtInput.setError(String.format("%s debe tener máximo %s caracteres", name, model.getMax()));
            } else {
                edtInput.setError(null);
                isValid = true;
            }
        }
        return isValid;
    }

    private boolean isValid() {
        String firstLetter = model.getLabel().substring(0, 1).toUpperCase();
        String restName = model.getLabel().substring(1).toLowerCase();
        if (model.getValidationType() == DynamicFieldViewModel.VALIDATION_TYPE_LENGTH) {
            return isValidValue(firstLetter + restName);
        } else if (model.getValidationType() == DynamicFieldViewModel.VALIDATION_TYPE_VALUE) {
            return isValidLength(firstLetter + restName);
        } else {
            return false;
        }
    }
}
