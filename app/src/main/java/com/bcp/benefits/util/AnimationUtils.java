package com.bcp.benefits.util;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by emontesinos on 02/05/19.
 **/

public class AnimationUtils {

    public static ValueAnimator extendHeightView(int start, int end, final View view, int duration, Animator.AnimatorListener animatorListener) {
        ValueAnimator animator = ValueAnimator.ofInt(start, end);
        animator.addUpdateListener(valueAnimator -> {
            int val = (Integer) valueAnimator.getAnimatedValue();
            ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
            layoutParams.height = val;
            view.setLayoutParams(layoutParams);
        });
        animator.setDuration(duration);
        if(animatorListener!=null)
            animator.addListener(animatorListener);
        return animator;
    }

}
