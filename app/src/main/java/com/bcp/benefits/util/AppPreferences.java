package com.bcp.benefits.util;


import android.content.Context;
import android.content.SharedPreferences;

public class AppPreferences {
    private final SharedPreferences sharedPreferences;


    public AppPreferences(Context context) {
        String SHARE_PREFERENCES_NAME = "MiBanco";
        this.sharedPreferences = context.getSharedPreferences(SHARE_PREFERENCES_NAME, Context.MODE_PRIVATE);

    }

    public String getString(String strPrefKey) {
        return sharedPreferences.getString(strPrefKey, "");
    }

    public int getInt(String strPrefKey) {
        return sharedPreferences.getInt(strPrefKey, 0);
    }

    public void savePreference(final String strPrefKey, final Object objPrefValue) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if (objPrefValue instanceof String) {
            editor.putString(strPrefKey, (String) objPrefValue);
        } else if (objPrefValue instanceof Boolean) {
            editor.putBoolean(strPrefKey, (Boolean) objPrefValue);
        } else if (objPrefValue instanceof Integer) {
            editor.putInt(strPrefKey, (Integer) objPrefValue);
        } else if (objPrefValue instanceof Long) {
            editor.putLong(strPrefKey, (Long) objPrefValue);
        } else if (objPrefValue instanceof Float) {
            editor.putFloat(strPrefKey, (Float) objPrefValue);
        }
        editor.apply();
    }
}
