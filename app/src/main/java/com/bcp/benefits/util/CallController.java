package com.bcp.benefits.util;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

/**
 * Created by emontesinos on 22/05/19.
 **/
public class CallController {

    public static final Integer PHONE_PERMISSION_REQUEST = 3000;

    private Context context;
    private String phone;
    private CallControllerListener listener;

    public CallController(Context context, CallControllerListener listener){
        this.context = context;
        this.listener = listener;
    }

    public void doCall(@NonNull String phone){
        this.phone = phone;
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions((Activity)context, new String[]{Manifest.permission.CALL_PHONE},PHONE_PERMISSION_REQUEST);
            } else
                call();

    }

    public void onRequestPermissionsResult(int requestCode, @NonNull int[] grantResults) {
        if(requestCode == PHONE_PERMISSION_REQUEST){
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                call();
            else
                if(listener!=null) listener.onCallError(ErrorCall.PERMISSION_DENIED);
        }
    }

    private void call() throws SecurityException {
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:"+phone));
        context.startActivity(intent);
    }

    public enum ErrorCall {
        PERMISSION_DENIED
    }

    public interface CallControllerListener{
        void onCallError(ErrorCall errorCall);
    }
}
