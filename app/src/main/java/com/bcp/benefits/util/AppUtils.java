package com.bcp.benefits.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.InputFilter;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.style.TextAppearanceSpan;
import android.text.style.UnderlineSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.ColorRes;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.material.snackbar.Snackbar;
import com.bcp.benefits.R;
import com.bcp.benefits.components.CustomTypefaceSpan;
import com.bcp.benefits.components.RadioGroupComponent;
import com.bcp.benefits.viewmodel.CommunicationViewModel;
import com.bcp.benefits.viewmodel.HomeViewModel;
import com.bcp.benefits.viewmodel.OtherBenefitViewModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import timber.log.Timber;

import static com.bcp.benefits.util.Constants.BORDER_COLOR;
import static com.bcp.benefits.util.Constants.COLORS;
import static com.bcp.benefits.util.Constants.CONTENT;
import static com.bcp.benefits.util.Constants.END_COLOR;
import static com.bcp.benefits.util.Constants.HOME_JSON;
import static com.bcp.benefits.util.Constants.ICON;
import static com.bcp.benefits.util.Constants.ID_HOME;
import static com.bcp.benefits.util.Constants.NONE;
import static com.bcp.benefits.util.Constants.OTHER_JSON;
import static com.bcp.benefits.util.Constants.OTHER_TWO_JSON;
import static com.bcp.benefits.util.Constants.START_COLOR;
import static com.bcp.benefits.util.Constants.TITLE;
import static com.bcp.benefits.util.Constants.TITLE_COLOR;

public class AppUtils {

    private static String COLOR = "color";

    //Read json data
    private static String readFromFile(Context context, String fileName) {
        String ret = "";
        try {
            InputStream inputStream = context.getAssets().open(fileName);

            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String receiveString;
            StringBuilder stringBuilder = new StringBuilder();

            while ((receiveString = bufferedReader.readLine()) != null) {
                stringBuilder.append(receiveString);
            }

            inputStream.close();
            ret = stringBuilder.toString();
        } catch (FileNotFoundException e) {
            Log.e("File Ex - ", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("IO Ex - ", "Can not read file: " + e.toString());
        }

        return ret;
    }

    //Applies gradient background
    public static void applyBackgroundColor(View view, Context context, String startColor,
                                            String endColor, String borderColor, Float radius) {
        Resources res = context.getResources();
        Context appCtx = context.getApplicationContext();
        try {
            if (startColor.equals(NONE) && endColor.equals(NONE)) {
                int bColorId = res.getIdentifier(borderColor, COLOR, appCtx.getPackageName());
                GradientDrawable gd = new GradientDrawable();
                gd.setColor(res.getColor(R.color.white));
                gd.setCornerRadius(radius);
                gd.setStroke(2, res.getColor(bColorId));
                view.setBackground(gd);
            } else if (endColor.equals(NONE) && borderColor.equals(NONE)) {
                int sColorId = res.getIdentifier(startColor, COLOR, appCtx.getPackageName());
                view.setBackground(appCtx.getDrawable(sColorId));
            } else {
                int[] colors = {colorFromStrings(appCtx, startColor), colorFromStrings(appCtx, endColor)};
                GradientDrawable gd = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, colors);
                //Consider using "36f" as default value for 'radius'
                gd.setCornerRadius(radius);
                view.setBackground(gd);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void applyBackgroundColorOther(View view, Context context, String startColor,
                                                 String endColor, String borderColor, Float radius) {
        Resources res = context.getResources();
        Context appCtx = context.getApplicationContext();
        try {
            if (startColor.equals(NONE) && endColor.equals(NONE)) {
                int bColorId = res.getIdentifier(borderColor, COLOR, appCtx.getPackageName());
                GradientDrawable gd = new GradientDrawable();
                gd.setColor(res.getColor(R.color.white));
                gd.setCornerRadius(radius);
                gd.setStroke(2, res.getColor(bColorId));
                view.setBackground(gd);
            } else if (endColor.equals(NONE) && borderColor.equals(NONE)) {
                int sColorId = res.getIdentifier(startColor, COLOR, appCtx.getPackageName());
                view.setBackground(appCtx.getDrawable(sColorId));
            } else {
                int[] colors = {colorFromStrings(appCtx, startColor), colorFromStrings(appCtx, endColor)};
                GradientDrawable gd = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, colors);
                //Consider using "36f" as default value for 'radius'
                gd.setCornerRadius(radius);
                view.setBackground(gd);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Return specific image from drawable
    public static Drawable imgDrawable(Context context, String icoName) {
        Resources res = context.getResources();
        Context appCtx = context.getApplicationContext();
        int resID = 0;
        try {
            String DRAWABLE = "drawable";
            resID = res.getIdentifier(icoName, DRAWABLE, appCtx.getPackageName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res.getDrawable(resID);
    }

    //Return specific string from strings
    public static String textFromStrings(Context context, String stringName) {
        Resources res = context.getResources();
        Context appCtx = context.getApplicationContext();
        int resID = 0;
        try {
            String STRING = "string";
            resID = res.getIdentifier(stringName, STRING, appCtx.getPackageName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res.getString(resID);
    }

    //Return specific color from strings
    public static int colorFromStrings(Context context, String stringName) {
        Resources res = context.getResources();
        Context appCtx = context.getApplicationContext();
        int resID = 0;
        try {
            resID = res.getIdentifier(stringName, COLOR, appCtx.getPackageName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res.getColor(resID);
    }

    //Home functions
    private static HashMap fetchColors(JSONObject colors) {
        HashMap<String, String> colorsMap = new HashMap<>();
        try {
            colorsMap.put(START_COLOR, colors.getString(START_COLOR));
            if (colors.getString(END_COLOR) != null) {
                colorsMap.put(END_COLOR, colors.getString(END_COLOR));
            }
            if (!colors.getString(BORDER_COLOR).equals(NONE)) {
                colorsMap.put(BORDER_COLOR, colors.getString(BORDER_COLOR));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return colorsMap;
    }

    public static ArrayList<HomeViewModel> homeModelFromJson(Context context) {
        ArrayList<HomeViewModel> home = new ArrayList<>();
        String jsonString = AppUtils.readFromFile(context, HOME_JSON);
        try {
            JSONObject data = new JSONObject(jsonString);
            JSONArray content = data.getJSONArray(CONTENT);
            for (int i = 0; i < content.length(); i++) {

                home.add(new HomeViewModel(content.getJSONObject(i).getString(ID_HOME)
                        , content.getJSONObject(i).getString(TITLE)
                        , content.getJSONObject(i).getString(ICON)
                        , content.getJSONObject(i).getString(TITLE_COLOR)
                        , fetchColors(content.getJSONObject(i).getJSONObject(COLORS))));
            }

        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        return home;
    }

    public static ArrayList<OtherBenefitViewModel> otherModelFromJson(Context context, String dataJson) {
        ArrayList<OtherBenefitViewModel> other = new ArrayList<>();
        String jsonString = AppUtils.readFromFile(context, dataJson);
        try {
            JSONObject data = new JSONObject(jsonString);
            JSONArray content = data.getJSONArray(CONTENT);
            for (int i = 0; i < content.length(); i++) {
                other.add(new OtherBenefitViewModel(content.getJSONObject(i).getString(ID_HOME),
                        content.getJSONObject(i).getString(TITLE), content.getJSONObject(i).getString(ICON),
                        fetchColors(content.getJSONObject(i).getJSONObject(COLORS))));
            }

        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        return other;
    }


    public static Drawable getImage(Context c, String imageName) {
        int id = c.getResources().getIdentifier(imageName, "drawable", c.getPackageName());
        if (id != 0) {
            return ContextCompat.getDrawable(c, id);
        }
        return null;
    }

    public static void getImage(String link, ImageView imageView) {
        try {
            URL url = new URL(link);
            Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
            imageView.setImageBitmap(bmp);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void changeEditTextLength(EditText et, int length) {
        et.setFilters(new InputFilter[]{new InputFilter.LengthFilter(length)});
    }

    public static String getRealPathFromURI(Uri contentURI, Activity context) {
        String[] projection = {MediaStore.Images.Media.DATA};
        @SuppressWarnings("deprecation")
        Cursor cursor = context.managedQuery(contentURI, projection, null,
                null, null);
        if (cursor == null)
            return null;
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        if (cursor.moveToFirst()) {
            String s = cursor.getString(column_index);
            cursor.close();
            return s;
        }
        cursor.close();
        return null;
    }

    public static void showErrorMessage(View view, Context context, String message) {
        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(context, R.color.red));
        snackbar.show();
    }

    public static void showErrorMessageSucces(View view, Context context, String message) {
        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(context, R.color.green_text_buuton));
        snackbar.show();
    }

    public static void hideSoftKeyboard(Activity activity) {
        if (activity.getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            assert inputMethodManager != null;
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }
    }

    public static void showSoftKeyboard(View view, Activity activity) {
        if (view != null)
            view.requestFocus();

        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);

        if (inputMethodManager != null)
            inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
    }

    public static TextWatcher hideSoftKeyboardLimit(final Activity activity, EditText editText, final int limit, TextWatcher textWatcherOld) {
        if (textWatcherOld != null) {
            editText.removeTextChangedListener(textWatcherOld);
        }
        TextWatcher textWatcher = new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                if (s.length() == limit) {
                    hideSoftKeyboard(activity);
                }
            }

            public void afterTextChanged(Editable editable) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

        };
        editText.addTextChangedListener(textWatcher);
        return textWatcher;
    }

    public static void hideKeyboard(Activity activity) {
        View view = activity.findViewById(android.R.id.content);
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


    public static float getDensity(Context context) {
        DisplayMetrics metrics = new DisplayMetrics();
        ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getMetrics(metrics);
        return metrics.density;
    }

    public static int getWidth(Context context) {
        Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE))
                .getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.x;
    }

    public static int getHeight(Context context) {
        Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE))
                .getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.y;
    }

    public static int convertDpToPx(Context context, int dp) {
        return Math.round(dp * (context.getResources().getDisplayMetrics().xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }


    public static SpannableStringBuilder getStyleText(Context context, int position, String text, int positionFinal) {
        Typeface font = ResourcesCompat.getFont(context, R.font.breviabold);

        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(text);
        spannableStringBuilder.setSpan(new CustomTypefaceSpan(font), position, positionFinal, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        return spannableStringBuilder;
    }

    public static SpannableStringBuilder getStyleText(Context context, String completeText, String textBold) {

        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(completeText);
        spannableStringBuilder.setSpan(new TextAppearanceSpan(context, R.style.FontBold), completeText.length() - textBold.length(), completeText.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);

        return spannableStringBuilder;
    }


    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }

    public static void addRadioButton(Context context, RadioGroupComponent radioGroup, int id, boolean check, int sizeDp) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        RadioButton rbSlider = new RadioButton(context);//(RadioButton) inflater.inflate(R.layout.component_radio_button, null);
        rbSlider.setId(id);
        rbSlider.setButtonDrawable(android.R.color.transparent);
        RadioGroup.LayoutParams layoutParams = new RadioGroup.LayoutParams(AppUtils.dpToPx(sizeDp), AppUtils.dpToPx(sizeDp));
        layoutParams.setMargins(AppUtils.dpToPx(6), 0, 0, 0);
        rbSlider.setLayoutParams(layoutParams);
        rbSlider.setChecked(check);
        radioGroup.addView(rbSlider);

    }


    public static String formatPrice(Double price) {
        DecimalFormatSymbols simbolos = new DecimalFormatSymbols();
        simbolos.setDecimalSeparator('.');
        simbolos.setGroupingSeparator(',');
        NumberFormat numberFormat = new DecimalFormat("#,##0.00", simbolos);
        return numberFormat.format(price);
    }

    public static String formatPriceWithoutDecimal(Double price) {
        DecimalFormatSymbols simbolos = new DecimalFormatSymbols();
        simbolos.setDecimalSeparator('.');
        simbolos.setGroupingSeparator(',');
        NumberFormat numberFormat = new DecimalFormat("#,##0", simbolos);
        return numberFormat.format(price);
    }

    public static String formatNumber(String originalString) {
        if (originalString.trim().length() > 0) {
            Long longval;
            if (originalString.contains(",")) {
                originalString = originalString.replaceAll(",", "");
            }
            longval = Long.parseLong(originalString.trim());
            DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
            if (longval < 10000) {
                formatter.applyPattern("#,###.##");
            }
            return formatter.format(longval);
        }
        return originalString;
    }

    public static void updateTextWithFormat(EditText editText) {
        editText.addTextChangedListener(new TextWatcher() {
            private boolean startEdit = false;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //Do nothing
            }

            @Override
            public void onTextChanged(CharSequence text, int start, int before, int count) {
                startEdit = !startEdit;
                if (startEdit) {
                    editText.setText(formatNumber(text.toString()));
                    editText.setSelection(editText.getText().length());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                //Do nothing
            }
        });
    }

    public static void openBrowser(Context context, String url) {
        try {
            if (!url.startsWith("http://") && !url.startsWith("https://"))
                url = "http://" + url;
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            context.startActivity(i);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean isConnected(Context context) {
        if (context == null)
            return false;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Network[] networks = connectivityManager.getAllNetworks();
            NetworkInfo networkInfo;
            for (Network mNetwork : networks) {
                networkInfo = connectivityManager.getNetworkInfo(mNetwork);
                if (networkInfo.getState().equals(NetworkInfo.State.CONNECTED)) {
                    return true;
                }
            }
        } else {
            if (connectivityManager != null) {
                NetworkInfo[] info = connectivityManager.getAllNetworkInfo();
                if (info != null) {
                    for (NetworkInfo anInfo : info) {
                        if (anInfo.getState() == NetworkInfo.State.CONNECTED) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public static BitmapDescriptor getIconBitmapDescriptor(Context context, @ColorRes int colorRes) {
        int color = ContextCompat.getColor(context, colorRes);
        float[] hsv = new float[3];
        Color.colorToHSV(color, hsv);
        return BitmapDescriptorFactory.defaultMarker(hsv[0]);
    }

    public static void setGradient(int startColor, int endColor, float radius, View view) {
        GradientDrawable drawable = new GradientDrawable(
                GradientDrawable.Orientation.TOP_BOTTOM,
                new int[]{startColor, endColor});
        drawable.setCornerRadius(radius);
        view.setBackground(drawable);
    }

    public static boolean isSmallScreen(Context context) {
        Log.d("AQUI", "" + getWidth(context));
        int screenSize = context.getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK;
        return screenSize == Configuration.SCREENLAYOUT_SIZE_NORMAL;
    }

    public static ArrayList<CommunicationViewModel> encontrarPrimeroCommunicatons(ArrayList<CommunicationViewModel> communications, AppPreferences sharedPreferences, String push) {
        ArrayList<CommunicationViewModel> filtrado = new ArrayList<>();
        if (push == null || push.isEmpty()) {
            return filtrarCommunicatons(communications, sharedPreferences);
        } else {
            for (int i = 0; i < communications.size(); i++) {
                if (Integer.parseInt(push) == communications.get(i).idCommunication) {

                    if (sharedPreferences.getInt("USER" + communications.get(i).idCommunication + "") == 0) {
                        filtrado.add(communications.get(i));
                    }

                }
            }
            if (filtrado.size() == 0) {
                return filtrarCommunicatons(communications, sharedPreferences);
            } else {
                for (int i = 0; i < communications.size(); i++) {
                    if (filtrado.get(0).idCommunication != communications.get(i).idCommunication) {
                        if (sharedPreferences.getInt("USER" + communications.get(i).idCommunication + "") == 0) {
                            filtrado.add(communications.get(i));
                        }
                    }
                }
                return filtrado;
            }
        }
    }

    public static ArrayList<CommunicationViewModel> filtrarCommunicatons(ArrayList<CommunicationViewModel> communications, AppPreferences sharedPreferences) {
        ArrayList<CommunicationViewModel> filtrado = new ArrayList<>();
        for (int i = 0; i < communications.size(); i++) {
            if (sharedPreferences.getInt("USER" + communications.get(i).idCommunication + "") == 0) {
                filtrado.add(communications.get(i));
            }
        }
        return filtrado;
    }

    public static BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    public static void dialogIsConnect(Context context) {
        new AlertDialog.Builder(context)
                .setTitle(R.string.app_name)
                .setCancelable(false)
                .setMessage(R.string.exception_no_internet_error_view)
                .setPositiveButton(R.string.dialog_accept, (dialogInterface, i) -> dialogInterface.dismiss())
                .show();
    }

    public static void openPlayStore(Context context, String packageName) {
        try {
            if (isPackageInstalled(context, packageName)) {
                final PackageManager packageManager = context.getPackageManager();
                Intent intent = packageManager.getLaunchIntentForPackage(packageName);
                context.startActivity(intent);
            } else {
                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(context.getString(R.string.play_store_app, packageName))));
            }

        } catch (android.content.ActivityNotFoundException anfe) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(context.getString(R.string.play_store_app, packageName))));
        }
    }

    public static boolean isPackageInstalled(Context context, String packageName) {
        final PackageManager packageManager = context.getPackageManager();
        Intent intent = packageManager.getLaunchIntentForPackage(packageName);
        if (intent == null) {
            return false;
        }
        List<ResolveInfo> list = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        return list.size() > 0;
    }

    public static void openUrl(Context context, String url) {
        Uri uriUrl = Uri.parse(url);
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        context.startActivity(launchBrowser);
    }

    public static void sendToPlayStoreForUpdate(Activity activity) {
        activity.finish();
        activity.getApplicationContext().startActivity(new Intent(Intent.ACTION_VIEW,
                Uri.parse("https://play.google.com/store/apps/details?id=pe.solera.romerogroupapp&hl=en")).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    public static String getFirstNCharacters(String s) {
        return s.substring(0, Math.min(s.length(), 100));
    }


    public static boolean checkRunningProcesses() {
        boolean returnValue = false;
        try {
            Process process = Runtime.getRuntime().exec("ps");
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            int read;
            char[] buffer = new char[4096];
            StringBuilder output = new StringBuilder();
            while ((read = reader.read(buffer)) > 0) {
                output.append(buffer, 0, read);
            }
            reader.close();
            Timber.tag("fridaserver").d(output.toString());
            if (output.toString().contains("frida-server")) {
                Timber.tag("fridaserver").d("Frida Server process found!");
                returnValue = true;
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return returnValue;
    }
}
