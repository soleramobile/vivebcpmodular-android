package com.bcp.benefits.util.fcm;

import android.content.Context;
import android.content.Intent;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.bcp.benefits.ui.splash.SplashActivity;
import com.bcp.benefits.util.AppPreferences;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.jetbrains.annotations.NotNull;

import java.util.Map;

import static com.bcp.benefits.util.Constants.ID_COMMUNICATION;

public class AppFireBaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(@NotNull RemoteMessage remoteMessage) {
        if (remoteMessage.getNotification() != null) {
            handleNotification(remoteMessage.getNotification().getBody(), remoteMessage.getNotification().getTitle());
        }

        if (remoteMessage.getData().size() > 0) {
            handleDataMessage(remoteMessage.getData());
        }
    }

    private void handleNotification(String message, String title) {
        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
            // app is in foreground, broadcast the push message
            Intent pushNotification = new Intent(Config.PUSH_MESSAGE);
            pushNotification.putExtra("message", message);
            pushNotification.putExtra("title", title);
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
        }
    }

    private void handleDataMessage(Map<String, String> json) {
        try {
            if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
                // app is in foreground, broadcast the push message
                Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);

                pushNotification.putExtra(ID_COMMUNICATION, json.get(ID_COMMUNICATION));
                LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
            } else {
                Intent resultIntent = new Intent(getApplicationContext(), SplashActivity.class);
                resultIntent.putExtra(ID_COMMUNICATION, json.get(ID_COMMUNICATION));
                showNotificationMessage(getApplicationContext(), json.get("title"), json.get("body"), "0", resultIntent);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
        NotificationUtils notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }

    @Override
    public void onNewToken(@NotNull String s) {
        super.onNewToken(s);

        // Saving reg id to shared preferences
        storeRegIdInPref(s);

        Intent registrationComplete = new Intent(Config.REGISTRATION_COMPLETE);
        registrationComplete.putExtra("token", s);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    private void storeRegIdInPref(String token) {
        AppPreferences appPreferences = new AppPreferences(getApplicationContext());
        appPreferences.savePreference("regId", token);
    }
}
