package com.bcp.benefits.util.imageloader;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import androidx.annotation.DrawableRes;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

public class PicassoImageLoader implements ImageLoader {

  private Picasso picasso;

  public PicassoImageLoader(Picasso picasso) {
    this.picasso = picasso;
  }

  @Override
  public void load(String url, ImageView imageView) {
    picasso.load(url).into(imageView);
  }

  @Override
  public void load(String url, ImageView imageView, Callback callback) {
    picasso.load(url).into(imageView, callback);
  }

  @Override
  public void load(String url, ImageView imageView, Drawable placeholder) {
    picasso.load(url).placeholder(placeholder).into(imageView);
  }

  @Override
  public void load(String url, ImageView imageView, @DrawableRes int placeholder) {
    picasso.load(url).placeholder(placeholder).into(imageView);
  }

  @Override
  public void loadCircular(String url, ImageView imageView, int placeholder) {
    picasso.load(url).transform(new CircleTransform()).placeholder(placeholder).into(imageView);
  }
}
