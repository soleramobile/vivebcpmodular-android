package com.bcp.benefits.util;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.Task;
import com.bcp.benefits.R;

/**
 * Created by emontesinos on 22/05/19.
 **/
public class LocationController {

    private static final int LOCATION_PERMISSION_REQUEST = 1000;
    private static final int REQUEST_CHECK_SETTINGS = 0x1;

    private Context context;
    private GoogleApiClient googleApiClient;
    private Location lastLocation;
    private LocationRequest locationRequest;
    private LocationSettingsRequest.Builder settingsApiBuilder;
    private LocationControllerListener locationManagerListener;

    private int intervalTime;

    public LocationController(Context context, int intervalTime, LocationControllerListener locationManagerListener) {
        if (context == null)
            throw new IllegalArgumentException("Context can't be Null");

        if (locationManagerListener == null)
            throw new IllegalArgumentException("LocationManagerListener can't be Null");

        this.intervalTime = intervalTime;
        this.context = context;
        this.locationManagerListener = locationManagerListener;
    }

    public void connect() {
        validateLocationPermission();
    }

    public void disconnect() {
        stopLocationUpdates();
        if (googleApiClient != null) {
            googleApiClient.disconnect();
        }
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull int[] grantResults) {
        if (requestCode == LOCATION_PERMISSION_REQUEST) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                settingGoogleLocationAPI();
            else
                locationManagerListener.onLocationPermissionDenied();
        }
    }

    public void onActivityResult(int requestCode, int resultCode) {
        if (requestCode == REQUEST_CHECK_SETTINGS) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    startLocationUpdates();
                    break;
                case Activity.RESULT_CANCELED:
                    locationManagerListener.onGpsUnavailable();
                    break;
            }
        }
    }

    public void checkGspAvailability() {
        SettingsClient client = LocationServices.getSettingsClient(context);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(settingsApiBuilder.build());
        task.addOnSuccessListener((Activity) context, locationSettingsResponse -> startLocationUpdates());
        task.addOnFailureListener((Activity) context, e -> {
            int statusCode = ((ApiException) e).getStatusCode();
            switch (statusCode) {
                case CommonStatusCodes.RESOLUTION_REQUIRED:
                    try {
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        resolvable.startResolutionForResult((Activity) context, REQUEST_CHECK_SETTINGS);
                    } catch (IntentSender.SendIntentException sendEx) {
                        sendEx.printStackTrace();
                    }
                    break;
                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                    showActiveGPSDialog();
                    break;
            }
        });
    }

    private void validateLocationPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int locationPermissionCheck = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION);
            if (locationPermissionCheck == PackageManager.PERMISSION_GRANTED) {
                settingGoogleLocationAPI();
            } else {
                askLocationPermission();
            }
        } else {
            settingGoogleLocationAPI();
        }
    }

    private void askLocationPermission() {
        String permissions[] = {Manifest.permission.ACCESS_FINE_LOCATION};
        ActivityCompat.requestPermissions((Activity) context, permissions, LOCATION_PERMISSION_REQUEST);
    }

    private void settingGoogleLocationAPI() {
        googleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(@Nullable Bundle bundle) throws SecurityException {
                        createLocationRequest();
                    }

                    @Override
                    public void onConnectionSuspended(int i) {
                        locationManagerListener.onError(LocationManagerError.GOOGLE_API_CONNECTION_SUSPENDED);
                    }
                })
                .addOnConnectionFailedListener(connectionResult ->
                        locationManagerListener.onError(LocationManagerError.GOOGLE_API_CONNECTION_FAILED))
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
    }

    private LocationCallback myLocationCallback = new LocationCallback() {

        @Override
        public void onLocationResult(LocationResult result) {
            if (result.getLastLocation() == null)
                return;

            if (lastLocation == null) {
                locationManagerListener.onGetLocationCompleted(result.getLastLocation());
            }

            lastLocation = result.getLastLocation();
            locationManagerListener.onLocationUpdate(lastLocation);
        }

        @Override
        public void onLocationAvailability(LocationAvailability locationAvailability) {
            super.onLocationAvailability(locationAvailability);
            if (!locationAvailability.isLocationAvailable()) {
                stopLocationUpdates();
                locationManagerListener.onGpsUnavailable();
            }
        }
    };

    private void createLocationRequest() throws SecurityException {
        locationRequest = new LocationRequest();
        locationRequest.setInterval(intervalTime);
        locationRequest.setFastestInterval(intervalTime / 2);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setMaxWaitTime(intervalTime * 2);
        settingsApiBuilder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        checkGspAvailability();
    }

    private void startLocationUpdates() throws SecurityException {
        LocationServices.getFusedLocationProviderClient(context).getLastLocation()
                .addOnSuccessListener(location -> {
                    lastLocation = location;
                    if (lastLocation != null) {
                        locationManagerListener.onGetLocationCompleted(lastLocation);
                    }

                    if (googleApiClient != null)
                        LocationServices.getFusedLocationProviderClient(context).requestLocationUpdates(locationRequest, myLocationCallback, null);
                })
                .addOnFailureListener(e -> {
                    if (googleApiClient != null)
                        LocationServices.getFusedLocationProviderClient(context).requestLocationUpdates(locationRequest, myLocationCallback, null);
                });
    }

    private void stopLocationUpdates() throws SecurityException {
        if (googleApiClient != null)
            LocationServices.getFusedLocationProviderClient(context).removeLocationUpdates(myLocationCallback);
    }

    private void showActiveGPSDialog() {
        if (AppUtils.isConnected(context)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(context.getString(R.string.discounts_gps_unavailable_message))
                    .setTitle(context.getString(R.string.app_name));
            builder.setPositiveButton("Activar", (dialogInterface, i) -> {
                dialogInterface.dismiss();
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                context.startActivity(intent);
            });
            builder.setNegativeButton("Cancelar", (dialogInterface, i) -> {
                dialogInterface.dismiss();
                locationManagerListener.onGpsUnavailable();
            });
            builder.setCancelable(false);
            AlertDialog dialog = builder.create();
            dialog.show();
        } else {
            Toast.makeText(context, context.getString(R.string.exception_airplane_mode_off), Toast.LENGTH_LONG).show();
        }
    }

    public enum LocationManagerError {
        GOOGLE_API_CONNECTION_FAILED, GOOGLE_API_CONNECTION_SUSPENDED
    }

    public interface LocationControllerListener {
        void onLocationPermissionDenied();

        void onGpsUnavailable();

        void onGetLocationCompleted(Location location);

        void onLocationUpdate(Location location);

        void onError(LocationManagerError error);
    }
}
