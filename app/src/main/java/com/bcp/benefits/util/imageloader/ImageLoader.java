package com.bcp.benefits.util.imageloader;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import androidx.annotation.DrawableRes;

import com.squareup.picasso.Callback;

public interface ImageLoader {
  void load(String url, ImageView imageView);
  void load(String url, ImageView imageView, Callback callback);
  void load(String url, ImageView imageView, Drawable placeholder);
  void load(String url, ImageView imageView, @DrawableRes int placeholder);
  void loadCircular(String url, ImageView imageView, @DrawableRes int placeholder);
}
