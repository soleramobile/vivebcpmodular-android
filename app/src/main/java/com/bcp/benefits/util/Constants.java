package com.bcp.benefits.util;

public class Constants {

    //HomeViewModel
    public static final String HOME_JSON = "homejson";
    public static final String OTHER_JSON = "otherlistjson";
    public static final String OTHER_TWO_JSON = "otherTwolistjson";
    public static final String VALIDATED_GO = "1";
    public static final String LOGIN_GO = "2";
    public static final String EMPTY = "";


    //JSON key names
    public static final String CONTENT = "content";
    public static final String TITLE = "title";
    public static final String ID = "id";
    public static final String ICON = "icon";
    public static final String TITLE_COLOR = "titleColor";
    public static final String COLORS = "colors";
    public static final String START_COLOR = "startColor";
    public static final String END_COLOR = "endColor";
    public static final String BORDER_COLOR = "borderColor";
    public static final String NONE = "none";

    //MiBanco Items
    public static final String ID_DISCOUNTS = "1";
    public static final String ID_EDUCATION = "2";
    public static final String ID_HEALTH = "3";
    public static final String ID_FINANCE = "4";
    public static final String ID_OTHERS = "5";

    public static final String SPACE = " ";

    public static final String VALIDATE_VIEW = "validateView";

    // document type
    public static final String TYPE_ID_DNI = "DNI";
    public static final String TYPE_ID_CE = "CE";
    public static final String TYPE = "type_doc";

    //calendar constants
    public static final int MODO_COMPLETO = 1;
    public static final int MODO_SIN_DIA = 2;
    public static final int MODO_SIN_DIA_MES = 3;
    public static final String DATE = "date";
    public static final String OLD = "old";
    public static final String FUTURE = "futuro";
    public static final String ID_HOME = "id";
    public static final String DAY = "day";
    public static final String MONTH = "month";
    public static final String MODE = "mode";
    public static final String ANDROID = "android";
    public static final String DATE_START = "date_start";
    public static final String YEAR_MONTH_DAY = "dd/MM/yyyy";
    public static final String MONTH_YEAR = "MMMM 'del' yyyy";
    public static final String YEAR_ = "yyyy '- anual'";


    public static final String GOLDEN = "1";
    public static final String LICENCE = "2";
    public static final String MOTHER = "3";
    public static final String BUSES = "4";
    public static final String VOLUNTEERING = "5";
    public static final String CONFIRMATION = "6";
    //identifier benefit
    public static final int GOLDEN_TICKETS = 0;
    public static final int TICKETS_TO_APPROVED = 1;
    public static final int IDENTIFIER_TICKET = 10;
    public static final int IDENTIFIER_LICENCE = 11;
    public static final int IDENTIFIER_VOLUNTEETING = 12;
    public static final String TICKETS_DATA = "ticketsData";
    public static final String TICKETS_DATA_APPROVED = "ticketsDataApproved";

    public static final String ID_COMMUNICATION = "idCommunication";

    //indentifier dialog button

    public static final String IDENTIFIER_DIALOG = "identifierDialog";
    public static final String IDENTIFIER_SESION_DIALOG = "identifierSesion";
    public static final String IDENTIFIER_PERMISSION_DIALOG = "identifierPermission";
    public static final String IDENTIFIR_MOTHER_DIALOG = "identifierMother";
    public static final String IDENTIFIR_LICENCE_FORCE_APROVAL_DIALOG = "identifierTicketForceAproval";
    public static final String IDENTIFIER_LICENCE_REFUSE_DIALOG = "iidentifierLicencerefuse";
    public static final String IDENTIFIER_LICENCE_APROVAL_DIALOG = "iidentifierLicenceAproval";
    public static final String IDENTIFIER_TICKETS_REFUSE_DIALOG = "identifierTicketrefuse";
    public static final String IDENTIFIER_TICKETS_FORCE_APROVAL_DIALOG = "identifierLicenceForceAproval";
    public static final String IDENTIFIER_TICKETS_APROVAL_DIALOG = "identifierTicketAproval";



    //indentifier dialog VUNTEERING

    public static final String IDENTIFIER_TICKETS_VOLUNTEERING_APROVAL_DIALOG = "IDENTIFIER_TICKETS_VOLUNTEERING_APROVAL_DIALOG";
    public static final String IDENTIFIER_TICKETS_VOLUNTEERING_REFUSE_DIALOG = "IDENTIFIER_TICKETS_VOLUNTEERING_REFUSE_DIALOG";








    public static final String IDENTIFIER_DIALOG_TERMS = "identifierDialogTerms";
    public static final String TEXT_TERMS = "termstext";
    public static final String MESSAGE_SERVICE = "messageService";
    public static final String ID_CAMPAIGN_LICENCE = "idCampaignLicence";
    public static final String ID_CAMPAIGN_TICKET = "idCampaignTicket";

    //identifier dialog simple

    public static final String IDENTIFIER_APROVED_FINACIAL_DIALOG = "identifierAprovedFinancial";
    public static final String TITLE_TO_DIALOG_OPTIONS = "titleToDialogOptions";
    public static final String LIST_TO_DIALOG_OPTIONS="listToDialogOptions";
    public static final String IS_ROLE = "isRole";

    //campaing

    public static final String ID_CAMPAING = "idCampaign";


    public static final String SCALE = "scale";
    public static final String OWNED = "owned";


    public static final String VOLUNTEERING_DATA = "ticketVolunteeringViewModel";

    public final static String HEADNAME = "headName";
    public final static String IDTICKET = "idTicket";
    public final static String SHOWRADIO = "showRadio";
    public final static String STATUS_TICKET_APPROVED = "approved";
    public final static String STATUS_TICKET_PENDING = "pending";

    public final static String TYPE_MEDIC = "medic";
    public final static String TYPE_BIRDAY = "cumpleanios";
    public final static String TYPE_KILL = "muerte";
    public final static String TYPE_MATERNITY = "maternidad";


    //response code from activityResult
    public static final int RESULT_TICKET = 20;
    public static final int RESULT_TICKET_VOLUNTEERING = 30;
    public static final int RESULT_PERMISSION = 21;

    //Licenece Request
    public final static String SELECT_CAMERA = "Tomar foto";
    public final static String SELECT_GALERY = "Cargar imagen";

    //duration animation

    public static final int DURATION_ANIMATION = 500;

    //Aguinaldos Activity
    public static final String DETAIL_PARAM = "detail";
    public static final String BUTTON_1 = "button_1";
    public static final String BUTTON_2 = "button_2";
    public static final String PRODUCTS_PARAM = "products";
    public static final String LOCATIONS_PARAM = "locations";
    public static final String LOCATION_DETAIL_PARAM = "subsidiaries";
    public static final String ID_CAMPAIGN_PARAM = "id_campaign";
    public static final String DATA_TITLE_CAMPAIGN_VOLUNTEERING = "DATA_TITLE_CAMPAIGN_VOLUNTEERING";
    public static final String DATA_DESCRIPTION_CAMPAIGN_VOLUNTEERING = "DATA_DESCRIPTION_CAMPAIGN_VOLUNTEERING";
    public static final String ID_VOLUNTEERING_DETAIL = "ID_VOLUNTEERING_DETAIL";
    public static final String USED_PARAM = "used";
    public static final String SHOW_LOCATION_PARAM = "show_location";
    public static final String SELECT_PRODUCTS = "select_products";
    public static final String USAGE_DETAIL_PARAM = "usage_detail";
    public static final String ENABLE_ROLE = "enableRole";
    public static final String ENABLE_LOCAL = "enableLocal";


    //identifier campanias
    public static final String IDENTIFIER_CAMPAIGN = "identifierCampaign";
    public static final int IDENTIFIER_CAMPAIGN_OFERTA = 11;
    public static final int IDENTIFIER_CAMPAIGN_OFERTADOS = 12;
    public static final int IDENTIFIER_CAMPAIGN_INFORMATIVA = 13;
    public static final int IDENTIFIER_CAMPAIGN_ENLAZADA = 14;


    //vies health
    public static final String PRODUCT_DETAIL = "detail";
    public static final String CLINIC_ID_PARAM = "clinic_id";
    public static final String PRODUCT_ID_PARAM = "product_id";

    public static final String ID_WAZE = "com.waze";
    public static final String ID_MAPS = "com.google.android.apps.maps";
    public static final String NEARYOU_FRAGMENT_TAG = "nearyou_tag";
    public static final String DISCOUNT_TYPE_PARAM = "discount_type";

    public static final String DISCOUNT_PARAM = "discount_id";
    public static final String SUBSIDIARY_PARAM = "subsidiary_id";
    public static final String DISCOUNT_TYPE_PARAMA = "discount_type";
    public static final String ENTER_TYPE = "enter_type";


    public static final String CATEGORY_ID = "category_id";
    public static final String PROVINCE_ID = "province_id";
    public static final String DISTRICT_ID = "district_id";
    public static final String FLAG_CAMPAIGNS = "flag_campaign";


    public static final String DISCOUNT_ID_PARAM = "disc_id";
    public static final String USAGE_ID_PARAM = "usage_id";
    public static final String REVIEW_FORM_PARAM = "review_form";
    public static final String ID_PROVIDER = "id_provider";
    public static final String BENEFIT_NAME = "benefit_name";


    public static final String ID_CAMPAIGN_LOCATION = "idcampaign";
    public static final String ID_LOCATION = "idLocation";
    public static final String PROVINCE_TITLE = "PROVINCE_TITLE";
    public static final String DATA_LOCATION_RESULT = "dataLocation";
    public static final String ID_TYPE = "idtype";
    public static final String LIST_TYPE_DEPARTMENT = "listTypeDicountDepartmen";


    // consume view
    public static final String SUBSIDIARY_ID_PARAM = "subs_id";
    public static final String CAPTION_VALUE_PARAM = "caption_value";
    public static final String CAPTION_PARAM = "caption";
    public static final String PROVIDER_PARAM = "provider";


    //discount
    public static final String INDENTIFIER_FRAGMENT = "identifierFragment";
    public static final String INDENTIFIER_FILTER = "identifierFilter";

    public static final String TOKEN_REGISTRADO = "MiBanco_token_registrado";


    public static final String CHANNEL_ID = "VERBOSE_NOTIFICATION";
    public static final int NOTIFICATION_ID = 1;

    public static final CharSequence VERBOSE_NOTIFICATION_CHANNEL_NAME = "Verbose WorkManager Notifications";
    public static final String VERBOSE_NOTIFICATION_CHANNEL_DESCRIPTION = "Shows notifications whenever work starts";

    public static final CharSequence NOTIFICATION_TITLE = "WorkRequest Starting";
    public static final String ALARM_WORK_TAG = "ALARM_WORK_TAG";

    public static final String ALARM_TITLE = "ALARM_TITLE";

    //Update

    public static final Integer UPDATE_CODE = 200;



    public static final String DATA_LOCAL_VOLUNTEERING = "DATA_LOCAL_VOLUNTEERING";
    public static final String DATA_CAMPAIGN_VOLUNTEERING = "DATA_CAMPAIGN_VOLUNTEERING";

    public static final String TYPE_LOCAL = "TYPE_LOCAL";
    public static final String TYPE_ROL = "TYPE_ROL";
    public static final String TYPE_SELECTION = "TYPE_SELECTION";
}
