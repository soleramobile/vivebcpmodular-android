package com.bcp.benefits.viewmodel;

import pe.solera.benefit.main.entity.User;

/**
 * Created by emontesinos.
 **/


public class UserViewModel {


    public final String documentNumber;
    public final int documentType;
    public final String employeeId;
    public final String birthDate;
    public final String name;
    public final String firstSurname;
    public final String secondSurname;
    public final String position;
    public final String email;
    public String phone;
    public String personalEmail;
    public final String imageUrl;
    public final String barCode;

    public UserViewModel(String documentNumber, int documentType, String employeeId, String birthDate,
                         String name, String firstSurname, String secondSurname, String position,
                         String email, String phone, String personalEmail, String imageUrl, String barCode) {
        this.documentNumber = documentNumber;
        this.documentType = documentType;
        this.employeeId = employeeId;
        this.birthDate = birthDate;
        this.name = name;
        this.firstSurname = firstSurname;
        this.secondSurname = secondSurname;
        this.position = position;
        this.email = email;
        this.phone = phone;
        this.personalEmail = personalEmail;
        this.imageUrl = imageUrl;
        this.barCode = barCode;
    }

    public UserViewModel(User user) {
        this.documentNumber = user.getDocumentNumber();
        this.documentType = user.getDocumentType();
        this.employeeId = user.getEmployeeId();
        this.birthDate = user.getBirthDate();
        this.name = user.getName();
        this.firstSurname = user.getFirstSurname();
        this.secondSurname = user.getSecondSurname();
        this.position = user.getPosition();
        this.email = user.getEmail();
        this.phone = user.getPhone();
        this.personalEmail = user.getPersonalEmail();
        this.imageUrl = user.getImageUrl();
        this.barCode = user.getBarCode();
    }


    public String getDocumentNumber() {
        return documentNumber;
    }

    public int getDocumentType() {
        return documentType;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public String getName() {
        return name;
    }

    public String getFirstSurname() {
        return firstSurname;
    }

    public String getSecondSurname() {
        return secondSurname;
    }

    public String getPosition() {
        return position;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPersonalEmail() {
        return personalEmail;
    }

    public void setPersonalEmail(String personalEmail) {
        this.personalEmail = personalEmail;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getBarCode() {
        return barCode;
    }

    @Override
    public String toString() {
        return "UserViewModel{" +
                "documentNumber='" + documentNumber + '\'' +
                ", documentType=" + documentType +
                ", employeeId='" + employeeId + '\'' +
                ", birthDate='" + birthDate + '\'' +
                ", name='" + name + '\'' +
                ", firstSurname='" + firstSurname + '\'' +
                ", secondSurname='" + secondSurname + '\'' +
                ", position='" + position + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", barCode='" + barCode + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                '}';
    }


}
