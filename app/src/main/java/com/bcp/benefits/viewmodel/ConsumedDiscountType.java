package com.bcp.benefits.viewmodel;

/*
    Created by: Flavia Figueroa
    Email: ffigueroa@solera.pe
    
    12/4/20 - 11:53
    Solera Mobile
*/

public enum ConsumedDiscountType{
    DNI(1, "dni"), CODIGO(2, "codigo"), PDF(3, "pdf"), LINK(4, "link");
    private int typeId;
    private String name;

    ConsumedDiscountType(int typeId, String name) {
        this.typeId = typeId;
        this.name = name;
    }

    public int getTypeId() {
        return typeId;
    }

    public String getName() {
        return name;
    }
}
