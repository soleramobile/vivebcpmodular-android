package com.bcp.benefits.viewmodel;

import java.util.ArrayList;
import java.util.List;

import pe.solera.benefit.main.entity.DynamicReferredField;

/**
 * Created by emontesinos on 20/05/19.
 **/

public class DynamicFieldValueViewModel {
    private int fieldId;
    private String value;
    private String fieldName;
    private int parentId;
    private List<String> propertyList;

    private transient boolean valid;

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public List<String> getPropertyList() {
        return propertyList;
    }

    public void setPropertyList(List<String> propertyList) {
        this.propertyList = propertyList;
    }

    public DynamicFieldValueViewModel(int fieldId, String value) {
        this.fieldId = fieldId;
        this.value = value;
        this.valid = true;
    }

    public DynamicFieldValueViewModel(int fieldId, String value, boolean valid) {
        this.fieldId = fieldId;
        this.value = value;
        this.valid = valid;
    }

    public static List<DynamicReferredField> toDynamicList(List<DynamicFieldValueViewModel> dynamicFields) {
        List<DynamicReferredField> list = new ArrayList<>();
        if (dynamicFields != null) {
            for (DynamicFieldValueViewModel fieldValueViewModel : dynamicFields) {
                list.add(fieldValueViewModel.toDynamicReferredField());
            }
        }
        return list;
    }

    public DynamicReferredField toDynamicReferredField() {
        return new DynamicReferredField(getFieldId(), getFieldName(), getValue(), getParentId(), getPropertyList());
    }

    public int getFieldId() {
        return fieldId;
    }

    public void setFieldId(int fieldId) {
        this.fieldId = fieldId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }
}
