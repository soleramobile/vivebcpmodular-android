package com.bcp.benefits.viewmodel;

import android.content.Context;
import android.text.SpannableStringBuilder;
import android.text.Spanned;

import androidx.core.content.res.ResourcesCompat;

import com.bcp.benefits.R;
import com.bcp.benefits.components.CustomTypefaceSpan;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pe.solera.benefit.main.entity.Discount;
import pe.solera.benefit.main.entity.DiscountList;

/**
 * Created by jmarkstar on 18/11/2017.
 */

public class DiscountViewModel {
    private int idDiscount;
    private int idSubsidiary;
    private String value;
    private String caption;
    private String provider;
    private String image;
    private String group;
    private String restriction;
    private int score;
    private int usageCount;
    private int reviewCount;
    private boolean showGroup;

    private double latitude;
    private double longitude;

    private String description;
    private String terms;
    private List<SubsidiaryViewModel> subsidiaries;
    private String webpage;
    private String pdfUrl;

    private Boolean canBeReviewed;
    private Boolean canBeUsed;

    private int idProvider;

    private TypeViewHolder typeViewHolder;

    public DiscountViewModel(TypeViewHolder typeViewHolder) {
        this.typeViewHolder = typeViewHolder;
    }

    public DiscountViewModel() {
        this.idDiscount = 0;
        this.idSubsidiary = 0;
        this.value = null;
        this.caption = null;
        this.provider = null;
        this.image = null;
        this.group = null;
        this.score = 0;
        this.description = null;
        this.terms = null;
        this.subsidiaries = null;
        this.usageCount = 0;
        this.reviewCount = 0;
        this.latitude = 0.0;
        this.longitude = 0.0;
        this.showGroup = false;
        this.webpage = null;
        this.pdfUrl = null;
        this.canBeReviewed = null;
        this.canBeUsed = null;
        this.restriction = null;
        this.idProvider = -1;
    }

    public DiscountViewModel(int idDiscount, int idSubsidiary, String value, String caption,
                             String provider, String image, String group, int score, int usageCount,
                             int reviewCount, boolean showGroup, String webpage,
                             Boolean canBeReviewed, Boolean canBeUsed, String restriction, int idProvider) {
        this.idDiscount = idDiscount;
        this.idSubsidiary = idSubsidiary;
        this.value = value;
        this.caption = caption;
        this.provider = provider;
        this.image = image;
        this.group = group;
        this.score = score;
        this.description = null;
        this.terms = null;
        this.subsidiaries = null;
        this.usageCount = usageCount;
        this.reviewCount = reviewCount;
        this.latitude = 0.0;
        this.longitude = 0.0;
        this.showGroup = showGroup;
        this.webpage = webpage;
        this.pdfUrl = null;
        this.canBeReviewed = canBeReviewed;
        this.canBeUsed = canBeUsed;
        this.restriction = restriction;
        this.idProvider = idProvider;
    }

    public DiscountViewModel(int idDiscount, int idSubsidiary, String value, String caption,
                             String provider, String image, String group, int score, int usageCount,
                             int reviewCount, boolean showGroup, double latitude, double longitude,
                             String description, String terms,
                             ArrayList<SubsidiaryViewModel> subsidiaries, String webpage,
                             String pdfUrl, Boolean canBeReviewed, Boolean canBeUsed,
                             String restriction, int idProvider) {
        this.idDiscount = idDiscount;
        this.idSubsidiary = idSubsidiary;
        this.value = value;
        this.caption = caption;
        this.provider = provider;
        this.image = image;
        this.group = group;
        this.score = score;
        this.usageCount = usageCount;
        this.reviewCount = reviewCount;
        this.showGroup = showGroup;
        this.latitude = latitude;
        this.longitude = longitude;
        this.description = description;
        this.terms = terms;
        this.subsidiaries = subsidiaries;
        this.webpage = webpage;
        this.pdfUrl = pdfUrl;
        this.canBeReviewed = canBeReviewed;
        this.canBeUsed = canBeUsed;
        this.restriction = restriction;
        this.idProvider = idProvider;
    }

    public DiscountViewModel(Discount discount) {
        this.idDiscount = discount.getIdDiscount();
        this.idSubsidiary = discount.getIdSubsidiary();
        this.value = discount.getValue();
        this.caption = discount.getCaption();
        this.provider = discount.getProvider();
        this.image = discount.getImage() != null ? discount.getImage() : "";
        this.group = discount.getGroup();
        this.score = discount.getScore();
        this.description = discount.getDescription();
        this.terms = discount.getTerms();
        this.subsidiaries = discount.getSubsidiaries() != null
                ? SubsidiaryViewModel.toSubsidiaryList(discount.getSubsidiaries())
                : new ArrayList<>();
        this.usageCount = discount.getUsageCount();
        this.reviewCount = discount.getReviewCount();
        this.latitude = discount.getLatitude();
        this.longitude = discount.getLongitude();
        this.showGroup = discount.isShowGroup();
        this.webpage = discount.getWebPage();
        this.pdfUrl = discount.getPdfUrl();
        this.canBeReviewed = discount.getCanBeReviewed();
        this.canBeUsed = discount.getCanBeUsed();
        this.restriction = discount.getRestriction();
        this.typeViewHolder = TypeViewHolder.DISCOUNT;
        this.idProvider = discount.getIdProvider();
    }

    public int getIdDiscount() {
        return idDiscount;
    }

    public void setIdDiscount(int idDiscount) {
        this.idDiscount = idDiscount;
    }

    public int getIdSubsidiary() {
        return idSubsidiary;
    }

    public void setIdSubsidiary(int idSubsidiary) {
        this.idSubsidiary = idSubsidiary;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getRestriction() {
        return restriction;
    }

    public void setRestriction(String restriction) {
        this.restriction = restriction;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getUsageCount() {
        return usageCount;
    }

    public SpannableStringBuilder getUsage(Context context) {
        String textBold = usageCount == 0
                ? context.getString(R.string.nothing_usage_discount)
                : context.getString(R.string.usage_number_discount, usageCount);
        String textNormal = usageCount == 0
                ? context.getString(R.string.be_the_first)
                : context.getString(usageCount == 1
                ? R.string.one_usage_discount : R.string.usage_discount);
        String peopleNumberUsage = context.getString(R.string.usage_general_discount,
                textBold, textNormal);

        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(peopleNumberUsage);
        spannableStringBuilder.setSpan(
                new CustomTypefaceSpan(ResourcesCompat.getFont(context, R.font.flexo_bold)),
                0, textBold.length(),
                Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        spannableStringBuilder.setSpan(new CustomTypefaceSpan(ResourcesCompat.getFont(context,
                R.font.flexo_regular)), textBold.length() + 1, peopleNumberUsage.length(),
                Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        return spannableStringBuilder;
    }

    public void setUsageCount(int usageCount) {
        this.usageCount = usageCount;
    }

    public int getReviewCount() {
        return reviewCount;
    }

    public SpannableStringBuilder getReview(Context context) {
        String textBold = reviewCount == 0
                ? context.getString(R.string.nothing_qualification_discount)
                : context.getString(R.string.usage_number_discount, reviewCount);
        String textNormal = reviewCount == 0
                ? context.getString(R.string.be_the_first)
                : context.getString(reviewCount == 1
                ? R.string.one_qualification_discount : R.string.qualification_discount);
        String peopleNumberUsage = context.getString(R.string.usage_general_discount,
                textBold, textNormal);

        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(peopleNumberUsage);
        spannableStringBuilder.setSpan(
                new CustomTypefaceSpan(ResourcesCompat.getFont(context, R.font.flexo_bold)),
                0, textBold.length(),
                Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        spannableStringBuilder.setSpan(new CustomTypefaceSpan(ResourcesCompat.getFont(context,
                R.font.flexo_regular)), textBold.length() + 1, peopleNumberUsage.length(),
                Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        return spannableStringBuilder;
    }

    public void setReviewCount(int reviewCount) {
        this.reviewCount = reviewCount;
    }

    public boolean isShowGroup() {
        return showGroup;
    }

    public void setShowGroup(boolean showGroup) {
        this.showGroup = showGroup;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTerms() {
        return terms;
    }

    public void setTerms(String terms) {
        this.terms = terms;
    }

    public List<SubsidiaryViewModel> getSubsidiaries() {
        return subsidiaries;
    }

    public void setSubsidiaries(List<SubsidiaryViewModel> subsidiaries) {
        this.subsidiaries = subsidiaries;
    }

    public String getWebpage() {
        return webpage;
    }

    public void setWebpage(String webpage) {
        this.webpage = webpage;
    }

    public String getPdfUrl() {
        return pdfUrl;
    }

    public void setPdfUrl(String pdfUrl) {
        this.pdfUrl = pdfUrl;
    }

    public Boolean getCanBeReviewed() {
        return canBeReviewed;
    }

    public void setCanBeReviewed(Boolean canBeReviewed) {
        this.canBeReviewed = canBeReviewed;
    }

    public Boolean getCanBeUsed() {
        return canBeUsed;
    }

    public void setCanBeUsed(Boolean canBeUsed) {
        this.canBeUsed = canBeUsed;
    }

    public int getIdProvider() {
        return idProvider;
    }

    public void setIdProvider(int idProvider) {
        this.idProvider = idProvider;
    }

    public TypeViewHolder getTypeViewHolder() {
        return typeViewHolder;
    }

    public void setTypeViewHolder(TypeViewHolder typeViewHolder) {
        this.typeViewHolder = typeViewHolder;
    }

    public static List<DiscountViewModel>
    toDiscountListViewModels(DiscountList discountList, DiscountTypeViewModel typeViewModel) {
        List<DiscountViewModel> discountViewModels = new ArrayList<>();
        for (Discount discount : discountList.getDiscountList()) {
            discountViewModels.add(new DiscountViewModel(discount));
        }
        if (!discountList.getPagination().getHasNextPage()) {
            discountViewModels.add(new DiscountViewModel(TypeViewHolder.PROPOSE));
        }
        if (typeViewModel == DiscountTypeViewModel.DISCOUNT
                && discountList.getPagination().getNextPage() == 2) {
            discountViewModels.add(0, new DiscountViewModel(TypeViewHolder.ALERT));
        }
        return discountViewModels;
    }

    public enum TypeViewHolder {
        LOADING(1), DISCOUNT(2), PROPOSE(3), ALERT(4);
        private final int value;
        private static Map map = new HashMap();

        TypeViewHolder(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        static {
            for (TypeViewHolder pageType : TypeViewHolder.values()) {
                map.put(pageType.value, pageType);
            }
        }

        public static TypeViewHolder valueOf(int pageType) {
            return (TypeViewHolder) map.get(pageType);
        }
    }
}
