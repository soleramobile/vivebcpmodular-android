package com.bcp.benefits.viewmodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import pe.solera.benefit.main.entity.State;


/**
 * Created by emontesinos on 28/05/19.
 **/

public class StateViewModel implements Serializable {
    private int idProvince;
    private String name;
    private List<DistrictViewModel> districts;

    public StateViewModel() {
    }

    public StateViewModel(int idProvince, String name) {
        this.idProvince = idProvince;
        this.name = name;
    }

    public StateViewModel(State state) {
        this.idProvince = state.getIdProvince();
        this.name = state.getName();
        this.districts = state.getDistricts() != null
                ? DistrictViewModel.toDistrictViewModels(state.getDistricts()) : new ArrayList<>();
    }


    public int getIdProvince() {
        return idProvince;
    }

    public void setIdProvince(int idProvince) {
        this.idProvince = idProvince;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<DistrictViewModel> getDistricts() {
        return districts;
    }

    public void setDistricts(List<DistrictViewModel> districts) {
        this.districts = districts;
    }

    public static List<StateViewModel> toStateViewModel(List<State> stateList) {
        List<StateViewModel> stateViewModels = new ArrayList<>();
        if (stateList != null) {
            for (State state : stateList) {
                stateViewModels.add(new StateViewModel(state));
            }
        }
        return stateViewModels;
    }

}
