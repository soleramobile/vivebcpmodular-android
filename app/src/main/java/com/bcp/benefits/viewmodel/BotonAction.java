package com.bcp.benefits.viewmodel;

/*
    Created by: Flavia Figueroa
    Email: ffigueroa@solera.pe
    
    12/2/20 - 16:15
    Solera Mobile
*/

public enum BotonAction {
    LLEGAR("llegar"), LLAMAR("llamar"), WEB("ir a sitio web"), DESCARGAR("descargar documento");
    private String action;

    BotonAction(String action) {
        this.action = action;
    }

    public String getAction() {
        return action;
    }
}
