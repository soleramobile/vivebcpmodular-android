package com.bcp.benefits.viewmodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import pe.solera.benefit.main.entity.Period;

/**
 * Created by emontesinos on 20/05/19.
 **/

public class PeriodViewModel implements Serializable {

    private int idPeriod;
    private String description;

    public PeriodViewModel() {
    }

    public PeriodViewModel(int idPeriod, String description) {
        this.idPeriod = idPeriod;
        this.description = description;
    }

    public int getIdPeriod() {
        return idPeriod;
    }

    public void setIdPeriod(int idPeriod) {
        this.idPeriod = idPeriod;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static List<PeriodViewModel> toPeriodModelList(List<Period> periodList) {
        List<PeriodViewModel> periodModelList = new ArrayList<>();
        for (Period period : periodList) {
            periodModelList.add(PeriodViewModel.toPeriodModel(period));
        }
        return periodModelList;
    }

    public static PeriodViewModel toPeriodModel(Period period) {
        return new PeriodViewModel(period.getIdPeriod(), period.getDescription());
    }
}
