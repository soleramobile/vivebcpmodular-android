package com.bcp.benefits.viewmodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import pe.solera.benefit.main.entity.LocalVolunteering;

public class LocalVolunteeringViewModel implements Serializable {

    private int idVolunteer;
    private String name;
    private String date;
    private String closeAttendance;

    public LocalVolunteeringViewModel(int idVolunteer, String name, String date, String closeAttendance) {
        this.idVolunteer = idVolunteer;
        this.name = name;
        this.date = date;
        this.closeAttendance = closeAttendance;
    }

    public int getIdVolunteer() {
        return idVolunteer;
    }

    public void setIdVolunteer(int idVolunteer) {
        this.idVolunteer = idVolunteer;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCloseAttendance() {
        return closeAttendance;
    }

    public void setCloseAttendance(String closeAttendance) {
        this.closeAttendance = closeAttendance;
    }

    public LocalVolunteeringViewModel(LocalVolunteering data) {
        this.idVolunteer = data.getIdVolunteer();
        this.name = data.getName()!=null?data.getName():"";
        this.date = data.getDate()!=null?data.getDate():"";
        this.closeAttendance = data.getClosedAttendance();
    }

    public int getId() {
        return idVolunteer;
    }

    public void setId(int id) {
        this.idVolunteer = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static List<LocalVolunteeringViewModel> toCampaignListLocal(List<LocalVolunteering> campaigns) {
        List<LocalVolunteeringViewModel> list = new ArrayList<>();
        if (campaigns != null) {
            for (LocalVolunteering campaign : campaigns) {
                list.add(new LocalVolunteeringViewModel(campaign));
            }
        }
        return list;
    }
}
