package com.bcp.benefits.viewmodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import pe.solera.benefit.main.entity.ApprovalTicket;

/**
 * Created by emontesinos on 02/05/19.
 **/

public class ForApprovalViewModel implements Serializable {

    private int idTicket;
    private String durationText;
    private String durationDetailText;
    private String usageDate;
    private UserTicketViewModel user;

    public ForApprovalViewModel() {
    }

    public ForApprovalViewModel(int idTicket, String durationText, String durationDetailText,
                                String usageDate, UserTicketViewModel user) {
        this.idTicket = idTicket;
        this.durationText = durationText;
        this.durationDetailText = durationDetailText;
        this.usageDate = usageDate;
        this.user = user;
    }

    public ForApprovalViewModel(ApprovalTicket approvalTicket) {
        this.idTicket = approvalTicket.getIdTicket();
        this.durationText = approvalTicket.getDurationText();
        this.durationDetailText = approvalTicket.getDurationDetailText();
        this.usageDate = approvalTicket.getUsageDate();
        this.user = approvalTicket.getUser() != null?new UserTicketViewModel(approvalTicket.getUser()):null;
    }

    public int getIdTicket() {
        return idTicket;
    }

    public void setIdTicket(int idTicket) {
        this.idTicket = idTicket;
    }

    public String getDurationText() {
        return durationText;
    }

    public void setDurationText(String durationText) {
        this.durationText = durationText;
    }

    public String getDurationDetailText() {
        return durationDetailText;
    }

    public void setDurationDetailText(String durationDetailText) {
        this.durationDetailText = durationDetailText;
    }

    public String getUsageDate() {
        return usageDate;
    }

    public void setUsageDate(String usageDate) {
        this.usageDate = usageDate;
    }

    public UserTicketViewModel getUser() {
        return user;
    }

    public void setUser(UserTicketViewModel user) {
        this.user = user;
    }

    public static List<ForApprovalViewModel> toForApprovalViewModelList(
            List<ApprovalTicket> approvalTickets) {
        List<ForApprovalViewModel> list = new ArrayList<>();
        if (approvalTickets != null) {
            for (ApprovalTicket ticket : approvalTickets) {
                list.add(new ForApprovalViewModel(ticket));
            }
        }
        return list;
    }
}
