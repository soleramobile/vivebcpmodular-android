package com.bcp.benefits.viewmodel;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ernestogaspard on 23/04/19.
 **/

public class HomeViewModel {

    private String id;
    private String title;
    private String icon;
    private String titleColor;
    private HashMap colors;
    private ArrayList<HomeViewModel> content;

    public HomeViewModel() {
    }

    public HomeViewModel(String id, String title, String icon, String titleColor, HashMap colors) {
        this.id = id;
        this.title = title;
        this.icon = icon;
        this.titleColor = titleColor;
        this.colors = colors;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getTitleColor() {
        return titleColor;
    }

    public void setTitleColor(String titleColor) {
        this.titleColor = titleColor;
    }

    public HashMap getColors() {
        return colors;
    }

    public void setColors(HashMap colors) {
        this.colors = colors;
    }

    public ArrayList<HomeViewModel> getContent() {
        return content;
    }

    public void setContent(ArrayList<HomeViewModel> content) {
        this.content = content;
    }
}
