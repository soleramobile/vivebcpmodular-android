package com.bcp.benefits.viewmodel;

public class TypeDocViewModel {

    private String typeDoc;

    private int valor;

    public TypeDocViewModel(String typeDoc, int valor) {
        this.typeDoc = typeDoc;
        this.valor = valor;
    }

    public String getTypeDoc() {
        return typeDoc;
    }

    public int getValor() {
        return valor;
    }


    public void setTypeDoc(String typeDoc) {
        this.typeDoc = typeDoc;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }
}
