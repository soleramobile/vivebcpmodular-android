package com.bcp.benefits.viewmodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import pe.solera.benefit.main.entity.CampaignVolunteering;

public class CampaignVolunteeringViewModel implements Serializable {

    private int idCampaign;
    private String campaignName;
    private Integer enableLocal;

    public CampaignVolunteeringViewModel(int idCampaign, String campaignName, Integer enableLocal) {
        this.idCampaign = idCampaign;
        this.campaignName = campaignName;
        this.enableLocal = enableLocal;
    }

    public CampaignVolunteeringViewModel(CampaignVolunteering data) {
        this.idCampaign = data.getIdCampaign();
        this.campaignName = data.getCampaignName()!=null?data.getCampaignName():"";
        this.enableLocal = data.getEnabledLocal();
    }

    public int getId() {
        return idCampaign;
    }

    public void setId(int id) {
        this.idCampaign = id;
    }

    public String getName() {
        return campaignName;
    }

    public void setName(String name) {
        this.campaignName = name;
    }

    public Integer getEnableLocal() {
        return enableLocal;
    }

    public void setEnableLocal(Integer enableLocal) {
        this.enableLocal = enableLocal;
    }

    public static List<CampaignVolunteeringViewModel> toCampaignList(List<CampaignVolunteering> campaigns) {


        List<CampaignVolunteeringViewModel> list = new ArrayList<>();
        if (campaigns != null) {
            for (CampaignVolunteering campaign : campaigns) {
                list.add(new CampaignVolunteeringViewModel(campaign));
            }
        }
        return list;
    }
}
