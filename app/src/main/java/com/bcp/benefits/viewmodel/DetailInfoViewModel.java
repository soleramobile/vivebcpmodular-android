package com.bcp.benefits.viewmodel;

import java.io.Serializable;

import pe.solera.benefit.main.entity.DetailCampaign;

/**
 * Created by emontesinos on 13/05/19.
 **/

public class DetailInfoViewModel implements Serializable {
    private String title;
    private String subTitle;
    private String caption;
    private String image;
    private String idCampaign;


    public DetailInfoViewModel(DetailCampaign detailCampaign) {
        this.title = detailCampaign.getTitle() != null? detailCampaign.getTitle():"";
        this.subTitle = detailCampaign.getSubTitle()!= null ? detailCampaign.getSubTitle():"";
        this.caption = detailCampaign.getCaption()!= null ? detailCampaign.getCaption():"";
        this.image = detailCampaign.getImage()!= null ? detailCampaign.getImage():"";
        this.idCampaign = detailCampaign.getIdCampaign()!= null ? detailCampaign.getIdCampaign():"";
    }

    public String getIdCampaign() {
        return idCampaign;
    }

    public void setIdCampaign(String idCampaign) {
        this.idCampaign = idCampaign;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public DetailInfoViewModel() {
    }


}
