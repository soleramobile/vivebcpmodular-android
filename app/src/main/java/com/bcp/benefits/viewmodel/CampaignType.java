package com.bcp.benefits.viewmodel;

/*
    Created by: Flavia Figueroa
    Email: ffigueroa@solera.pe
    
    12/4/20 - 18:21
    Solera Mobile
*/

public enum CampaignType {
    PRODUCTO(1, "Producto"), INFORMATIVO(2, "Informativo"),
    DESCUENTO(3, "Descuento"), DINAMICO(4, "Dinámico"),
    VOLUNTARIADO(5, "Voluntariado");

    private int idType;
    private String name;

    CampaignType(int idType, String name) {
        this.idType = idType;
        this.name = name;
    }

    public int getIdType() {
        return idType;
    }

    public String getName() {
        return name;
    }
}
