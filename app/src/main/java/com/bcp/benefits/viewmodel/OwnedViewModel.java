package com.bcp.benefits.viewmodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import pe.solera.benefit.main.entity.OwnTicket;

/**
 * Created by emontesinos on 02/05/19.
 **/

public class OwnedViewModel implements Serializable {

    private int idTicket;
    private String usageDate;
    private String durationText;
    private String statusText;
    private boolean showButton;
    private boolean showDurationDetail;
    private boolean isReleaseable;
    private String departmentHeadName;
    private String image;
    private String usageDateFormatted;

    public OwnedViewModel() {
    }

    public OwnedViewModel(int idTicket, String usageDate, String durationText, String statusText,
                          boolean showButton, boolean showDurationDetail, boolean isReleaseable,
                          String departmentHeadName, String image, String usageDateFormatted) {
        this.idTicket = idTicket;
        this.usageDate = usageDate;
        this.durationText = durationText;
        this.statusText = statusText;
        this.showButton = showButton;
        this.showDurationDetail = showDurationDetail;
        this.isReleaseable = isReleaseable;
        this.departmentHeadName = departmentHeadName;
        this.image = image;
        this.usageDateFormatted = usageDateFormatted;
    }

    public OwnedViewModel(OwnTicket ownTicket) {
        this.idTicket = ownTicket.getIdTicket();
        this.usageDate = ownTicket.getUsageDate();
        this.durationText = ownTicket.getDurationText();
        this.statusText = ownTicket.getStatusText();
        this.showButton = ownTicket.isShowButton();
        this.showDurationDetail = ownTicket.isShowDurationDetail();
        this.isReleaseable = ownTicket.isRealizable();
        this.departmentHeadName = ownTicket.getDepartmentHeadName();
        this.image = ownTicket.getImage();
        this.usageDateFormatted = ownTicket.getUsageDateFormatted();
    }


    public boolean isReleaseable() {
        return isReleaseable;
    }

    public void setReleaseable(boolean releaseable) {
        isReleaseable = releaseable;
    }

    public int getIdTicket() {
        return idTicket;
    }

    public void setIdTicket(int idTicket) {
        this.idTicket = idTicket;
    }

    public String getUsageDate() {
        return usageDate;
    }

    public void setUsageDate(String usageDate) {
        this.usageDate = usageDate;
    }

    public String getDurationText() {
        return durationText;
    }

    public void setDurationText(String durationText) {
        this.durationText = durationText;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public boolean getShowButton() {
        return showButton;
    }

    public void setShowButton(boolean showButton) {
        this.showButton = showButton;
    }

    public boolean getShowDurationDetail() {
        return showDurationDetail;
    }

    public void setShowDurationDetail(boolean showDurationDetail) {
        this.showDurationDetail = showDurationDetail;
    }

    public String getDepartmentHeadName() {
        return departmentHeadName;
    }

    public void setDepartmentHeadName(String departmentHeadName) {
        this.departmentHeadName = departmentHeadName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUsageDateFormatted() {
        return usageDateFormatted;
    }

    public void setUsageDateFormatted(String usageDateFormatted) {
        this.usageDateFormatted = usageDateFormatted;
    }

    public static List<OwnedViewModel> toOwnedList(List<OwnTicket> ownTickets) {
        List<OwnedViewModel> list = new ArrayList<>();
        if (ownTickets != null) {
            for (OwnTicket ticket : ownTickets) {
                list.add(toOwnTicketViewModel(ticket));
            }
        }
        return list;
    }

    public static OwnedViewModel toOwnTicketViewModel(OwnTicket ownTicket) {
        return new OwnedViewModel(ownTicket);
    }
}