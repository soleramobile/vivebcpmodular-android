package com.bcp.benefits.viewmodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import pe.solera.benefit.main.entity.Ticket;
import pe.solera.benefit.main.entity.VolunteeringTicket;

/**
 * Created by emontesinos on 02/05/19.
 **/

public class VolunteeringTicketViewModel implements Serializable {

    private boolean validateTipeUser;
    private List<TicketVolunteeringViewModel> listTicket;
    private List<TicketRequestVolunteeringViewModel> listTicketApprove;


    public boolean isValidateTipeUser() {
        return validateTipeUser;
    }

    public void setValidateTipeUser(boolean validateTipeUser) {
        this.validateTipeUser = validateTipeUser;
    }

    public VolunteeringTicketViewModel(VolunteeringTicket volunteeringTicket) {
        this.validateTipeUser = volunteeringTicket.getApprover()!=null? volunteeringTicket.getApprover():false;
        this.listTicket =volunteeringTicket.getListTicket() != null? TicketVolunteeringViewModel.toTicketVolunteeringList(volunteeringTicket.getListTicket()):new ArrayList<>();
        this.listTicketApprove = volunteeringTicket.getListTicketApprove() != null? TicketRequestVolunteeringViewModel.toForApprovalVolunteerignViewModelList(volunteeringTicket.getListTicketApprove()): new ArrayList<>();
    }

    public List<TicketVolunteeringViewModel> getListTicket() {
        return listTicket;
    }

    public void setListTicket(List<TicketVolunteeringViewModel> listTicket) {
        this.listTicket = listTicket;
    }

    public List<TicketRequestVolunteeringViewModel> getListTicketApprove() {
        return listTicketApprove;
    }

    public void setListTicketApprove(List<TicketRequestVolunteeringViewModel> listTicketApprove) {
        this.listTicketApprove = listTicketApprove;
    }
}