package com.bcp.benefits.viewmodel;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

import pe.solera.benefit.main.entity.Product;

/**
 * Created by emontesinos on 13/05/19.
 **/

public class ButtonProductViewModel implements Parcelable {
    private boolean active;
    private String text;
    private int idVolunteer = 0;

    public int getIdVolunteer() {
        return idVolunteer;
    }

    public void setIdVolunteer(int idVolunteer) {
        this.idVolunteer = idVolunteer;
    }

    public ButtonProductViewModel(boolean active, String text, int idVolunteer) {
        this.active = active;
        this.text = text;
        this.idVolunteer = idVolunteer;
    }

    public ButtonProductViewModel(Product product) {
        this.active = product.isActive();
        this.text = product.getText();
        this.idVolunteer = product.getIdVolunteer();

    }

    public static List<ButtonProductViewModel> toProductList(List<Product> products){
        List<ButtonProductViewModel> list = new ArrayList<>();
        if (products != null) {
            for (Product product : products) {
                list.add(new ButtonProductViewModel(product));
            }
        }
        return list;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.active ? (byte) 1 : (byte) 0);
        dest.writeString(this.text);
    }

    public ButtonProductViewModel() {
    }

    protected ButtonProductViewModel(Parcel in) {
        this.active = in.readByte() != 0;
        this.text = in.readString();
    }

    public static final Parcelable.Creator<ButtonProductViewModel> CREATOR = new Parcelable.Creator<ButtonProductViewModel>() {
        @Override
        public ButtonProductViewModel createFromParcel(Parcel source) {
            return new ButtonProductViewModel(source);
        }

        @Override
        public ButtonProductViewModel[] newArray(int size) {
            return new ButtonProductViewModel[size];
        }
    };
}
