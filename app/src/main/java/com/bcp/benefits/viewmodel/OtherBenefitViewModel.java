package com.bcp.benefits.viewmodel;

import java.util.ArrayList;
import java.util.HashMap;

public class OtherBenefitViewModel {

    private  String id;
    private String title;
    private String icon;
    private HashMap colors;
    private ArrayList<OtherBenefitViewModel> content;

    public HashMap getColors() {
        return colors;
    }

    public void setColors(HashMap colors) {
        this.colors = colors;
    }

    public OtherBenefitViewModel() {

    }

    public OtherBenefitViewModel(String id, String title, String icon, HashMap colors) {
        this.id = id;
        this.title = title;
        this.icon = icon;
        this.colors = colors;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ArrayList<OtherBenefitViewModel> getContent() {
        return content;
    }

    public void setContent(ArrayList<OtherBenefitViewModel> content) {
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
