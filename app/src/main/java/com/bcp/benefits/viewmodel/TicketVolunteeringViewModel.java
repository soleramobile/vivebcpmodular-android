package com.bcp.benefits.viewmodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import pe.solera.benefit.main.entity.TicketVolunteering;

public class TicketVolunteeringViewModel implements Serializable {
    private  int idVolunteerCollaborator;
    private  String nameTicket_status;
    private int idTicketStatus;
    private  String nameVolunteer_time;
    private  String dateRequest;
    private  String timeRequest;
    private  String urlImage;

    public int getIdTicketStatus() {
        return idTicketStatus;
    }

    public void setIdTicketStatus(int idTicketStatus) {
        this.idTicketStatus = idTicketStatus;
    }

    public TicketVolunteeringViewModel(int idVolunteerCollaborator, String nameTicket_status, int idTicketStatus, String nameVolunteer_time, String dateRequest, String timeRequest, String urlImage) {
        this.idVolunteerCollaborator = idVolunteerCollaborator;
        this.nameTicket_status = nameTicket_status;
        this.idTicketStatus = idTicketStatus;
        this.nameVolunteer_time = nameVolunteer_time;
        this.dateRequest = dateRequest;
        this.timeRequest = timeRequest;
        this.urlImage = urlImage;
    }


    public static List<TicketVolunteeringViewModel> toTicketVolunteeringList(List<TicketVolunteering> TicketVolunteerings) {
        List<TicketVolunteeringViewModel> list = new ArrayList<>();
        if (TicketVolunteerings != null) {
            for (TicketVolunteering response : TicketVolunteerings) {
                list.add(toOwnTicketVolunteerignViewModel(response));
            }
        }
        return list;
    }

    public static TicketVolunteeringViewModel toOwnTicketVolunteerignViewModel(TicketVolunteering ticketVolunteering) {
        return new TicketVolunteeringViewModel(
                ticketVolunteering.getIdVolunteerCollaborator(),
                ticketVolunteering.getNameTicket_status()!=null?ticketVolunteering.getNameTicket_status():"",
                ticketVolunteering.getIdTicketStatus(),
                ticketVolunteering.getNameVolunteer_time()!=null?ticketVolunteering.getNameVolunteer_time():"",
                ticketVolunteering.getDateRequest()!=null?ticketVolunteering.getDateRequest():"",
                ticketVolunteering.getTimeRequest()!=null?ticketVolunteering.getTimeRequest():"",
                ticketVolunteering.getUrlImage()!=null?ticketVolunteering.getUrlImage():""
        );
    }


    public int getIdVolunteerCollaborator() {
        return idVolunteerCollaborator;
    }

    public void setIdVolunteerCollaborator(int idVolunteerCollaborator) {
        this.idVolunteerCollaborator = idVolunteerCollaborator;
    }

    public String getNameTicket_status() {
        return nameTicket_status;
    }

    public void setNameTicket_status(String nameTicket_status) {
        this.nameTicket_status = nameTicket_status;
    }

    public String getNameVolunteer_time() {
        return nameVolunteer_time;
    }

    public void setNameVolunteer_time(String nameVolunteer_time) {
        this.nameVolunteer_time = nameVolunteer_time;
    }

    public String getDateRequest() {
        return dateRequest;
    }

    public void setDateRequest(String dateRequest) {
        this.dateRequest = dateRequest;
    }

    public String getTimeRequest() {
        return timeRequest;
    }

    public void setTimeRequest(String timeRequest) {
        this.timeRequest = timeRequest;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }
}
