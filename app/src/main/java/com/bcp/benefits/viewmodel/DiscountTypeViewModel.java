package com.bcp.benefits.viewmodel;

import java.io.Serializable;

import pe.solera.benefit.main.entity.DiscountType;

/**
 * Created by emontesinos on 22/05/19.
 **/

public enum DiscountTypeViewModel implements Serializable {
    DISCOUNT("discount", 1), EDUTATION("education", 2);

    private int index;
    private String value;

    DiscountTypeViewModel(String value, int index) {
        this.value = value;
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

    public String getValue() {
        return value;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static DiscountType getDiscountType(DiscountTypeViewModel discountTypeViewModel) {
        if (discountTypeViewModel == DiscountTypeViewModel.DISCOUNT)
            return DiscountType.DISCOUNT;
        else
            return DiscountType.EDUTATION;
    }
}
