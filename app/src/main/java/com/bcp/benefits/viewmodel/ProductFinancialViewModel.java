package com.bcp.benefits.viewmodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import pe.solera.benefit.main.entity.FinancialProduct;

/**
 * Created by emontesinos on 20/05/19.
 **/
public class ProductFinancialViewModel implements Serializable {

    private int idProduct;
    private String name;
    private String approvedAmount;
    private String interest;
    private String disclaimer;
    private String periodUnit;
    private boolean showAmountField;
    private List<ConditionViewModel> conditions;
    private List<PeriodViewModel> periodOptions;
    private String link;
    private String amountLabel;

    public ProductFinancialViewModel() {
    }

    public ProductFinancialViewModel(int idProduct, String name, String approvedAmount,
                                     String interest, String disclaimer, String periodUnit,
                                     boolean showAmountField, List<ConditionViewModel> conditions,
                                     List<PeriodViewModel> periodOptions, String link, String amountLabel) {
        this.idProduct = idProduct;
        this.name = name;
        this.approvedAmount = approvedAmount;
        this.interest = interest;
        this.disclaimer = disclaimer;
        this.periodUnit = periodUnit;
        this.showAmountField = showAmountField;
        this.conditions = conditions;
        this.periodOptions = periodOptions;
        this.link = link;
        this.amountLabel = amountLabel;
    }


    public int getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(int idProduct) {
        this.idProduct = idProduct;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getApprovedAmount() {
        return approvedAmount;
    }

    public void setApprovedAmount(String approvedAmount) {
        this.approvedAmount = approvedAmount;
    }

    public String getInterest() {
        return interest;
    }

    public void setInterest(String interest) {
        this.interest = interest;
    }

    public String getDisclaimer() {
        return disclaimer;
    }

    public void setDisclaimer(String disclaimer) {
        this.disclaimer = disclaimer;
    }

    public String getPeriodUnit() {
        return periodUnit;
    }

    public void setPeriodUnit(String periodUnit) {
        this.periodUnit = periodUnit;
    }

    public boolean isShowAmountField() {
        return showAmountField;
    }

    public void setShowAmountField(boolean showAmountField) {
        this.showAmountField = showAmountField;
    }

    public List<ConditionViewModel> getConditions() {
        return conditions;
    }

    public void setConditions(List<ConditionViewModel> conditions) {
        this.conditions = conditions;
    }

    public List<PeriodViewModel> getPeriodOptions() {
        return periodOptions;
    }

    public void setPeriodOptions(List<PeriodViewModel> periodOptions) {
        this.periodOptions = periodOptions;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getAmountLabel() {
        return amountLabel;
    }

    public void setAmountLabel(String amountLabel) {
        this.amountLabel = amountLabel;
    }

    public static List<ProductFinancialViewModel> toList(List<FinancialProduct> productList) {
        List<ProductFinancialViewModel> productFinancialViewModels = new ArrayList<>();
        for (FinancialProduct product : productList) {
            productFinancialViewModels.add(ProductFinancialViewModel.productFinancialViewModel(product));
        }
        return productFinancialViewModels;
    }

    private static ProductFinancialViewModel productFinancialViewModel(FinancialProduct product) {
        return new ProductFinancialViewModel(product.getIdProduct(), product.getName(),
                product.getApprovedAmount(), product.getInterest(), product.getDisclaimer(),
                product.getPeriodUnit(), product.isShowAmountField(),
                ConditionViewModel.toConditionsViewModels(product.getConditions()),
                PeriodViewModel.toPeriodModelList(product.getPeriodOptions()), product.getLink(),
                product.getAmountLabel());
    }

}
