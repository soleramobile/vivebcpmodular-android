package com.bcp.benefits.viewmodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import pe.solera.benefit.main.entity.Condition;

/**
 * Created by emontesinos on 20/05/19.
 **/

public class ConditionViewModel implements Serializable {

    private String idCondition;
    private String description;
    private boolean checked;

    public ConditionViewModel() {
    }

    public ConditionViewModel(String idCondition, String description, boolean checked) {
        this.idCondition = idCondition;
        this.description = description;
        this.checked = checked;
    }

    public ConditionViewModel(Condition condition) {
    }

    public String getIdCondition() {
        return idCondition;
    }

    public void setIdCondition(String idCondition) {
        this.idCondition = idCondition;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public static List<ConditionViewModel> toConditionsViewModels(List<Condition> conditionList) {
        List<ConditionViewModel> conditionModelList = new ArrayList<>();
        for (Condition condition : conditionList) {
            conditionModelList.add(ConditionViewModel.toConditionModel(condition));
        }
        return conditionModelList;
    }

    public static ConditionViewModel toConditionModel(Condition condition) {
        return new ConditionViewModel(condition.getIdCondition(), condition.getDescription(),
                condition.isChecked());
    }

    public static List<Condition> toCondition(List<ConditionViewModel> conditionModels) {
        List<Condition> conditionList = new ArrayList<>();
        for (ConditionViewModel condition : conditionModels) {
            conditionList.add(ConditionViewModel.toCondition(condition));
        }
        return conditionList;
    }

    public static Condition toCondition(ConditionViewModel condition) {
        return new Condition(condition.getIdCondition(), condition.getDescription(),
                condition.isChecked());
    }
}

