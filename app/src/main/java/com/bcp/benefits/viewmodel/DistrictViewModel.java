package com.bcp.benefits.viewmodel;

import java.util.ArrayList;
import java.util.List;

import pe.solera.benefit.main.entity.District;

/**
 * Created by emontesinos on 28/05/19.
 **/

public class DistrictViewModel {

    private int idDistrict;
    private String name;

    public DistrictViewModel(int idDistrict, String name) {
        this.idDistrict = idDistrict;
        this.name = name;
    }

    public DistrictViewModel() {
    }


    public DistrictViewModel(District district) {
        this.idDistrict = district.getIdDistrict();
        this.name = district.getName();
    }

    public int getIdDistrict() {
        return idDistrict;
    }

    public void setIdDistrict(int idDistrict) {
        this.idDistrict = idDistrict;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static List<DistrictViewModel> toDistrictViewModels(List<District> districtList) {
        List<DistrictViewModel> viewModelList = new ArrayList<>();
        if (districtList != null) {
            for (District district : districtList) {
                viewModelList.add(new DistrictViewModel(district));
            }
        }
        return viewModelList;
    }
}
