package com.bcp.benefits.viewmodel;

import java.io.Serializable;

import pe.solera.benefit.main.entity.User;

/**
 * Created by emontesinos on 02/05/19.
 **/

public class UserTicketViewModel implements Serializable {
    private String name;
    private String position;

    public UserTicketViewModel() {
    }

    public UserTicketViewModel(String name, String position) {
        this.name = name;
        this.position = position;
    }

    public UserTicketViewModel(User user) {
        this.name = user.getName();
        this.position = user.getPosition();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
}
