package com.bcp.benefits.viewmodel;

import java.util.List;

import pe.solera.benefit.main.entity.BusSchedule;

public class BusScheduleViewModel {
    private List<BusScheduleItemViewModel> schedule;
    private String defaultStart;

    public BusScheduleViewModel(BusSchedule busSchedule) {
        this.schedule = BusScheduleItemViewModel.toList(busSchedule.getSchedule());
        this.defaultStart = busSchedule.getDefaultStart();
    }


    public List<BusScheduleItemViewModel> getSchedule() {
        return schedule;
    }

    public void setSchedule(List<BusScheduleItemViewModel> schedule) {
        this.schedule = schedule;
    }

    public String getDefaultStart() {
        return defaultStart;
    }

    public void setDefaultStart(String defaultStart) {
        this.defaultStart = defaultStart;
    }
}
