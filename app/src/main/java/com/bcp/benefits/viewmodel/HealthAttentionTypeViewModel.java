package com.bcp.benefits.viewmodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import pe.solera.benefit.main.entity.HealthAttentionType;

/**
 * Created by emontesinos on 22/05/19.
 **/

public class HealthAttentionTypeViewModel implements Serializable {
    private Integer idAttentionType;
    private String description;
    private String name;
    private String coverage;
    private String amount;

    private HealthAttentionTypeViewModel(Integer idAttentionType, String description) {
        this.idAttentionType = idAttentionType;
        this.description = description;
        this.coverage = null;
        this.amount = null;
        this.name = null;
    }

    private HealthAttentionTypeViewModel(Integer idAttentionType, String description, String name,
                                         String coverage, String amount) {
        this.idAttentionType = idAttentionType;
        this.description = description;
        this.name = name;
        this.coverage = coverage;
        this.amount = amount;
    }

    private HealthAttentionTypeViewModel(HealthAttentionType healthAttentionType) {
        this.idAttentionType = healthAttentionType.getIdAttentionType();
        this.description = healthAttentionType.getDescription();
        this.coverage = healthAttentionType.getCoverage();
        this.amount = healthAttentionType.getAmount();
        this.name = healthAttentionType.getName();
    }

    public Integer getIdAttentionType() {
        return idAttentionType;
    }

    public void setIdAttentionType(Integer idAttentionType) {
        this.idAttentionType = idAttentionType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCoverage() {
        return coverage;
    }

    public void setCoverage(String coverage) {
        this.coverage = coverage;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public static List<HealthAttentionTypeViewModel> toHealthAttentionTypeViewModels(
            List<HealthAttentionType> healthAttentionTypes) {
        List<HealthAttentionTypeViewModel> healthAttentionTypeViewModels = new ArrayList<>();
        if (healthAttentionTypes != null) {
            for (HealthAttentionType type : healthAttentionTypes) {
                healthAttentionTypeViewModels.add(new HealthAttentionTypeViewModel(type));
            }
        }
        return healthAttentionTypeViewModels;
    }
}
