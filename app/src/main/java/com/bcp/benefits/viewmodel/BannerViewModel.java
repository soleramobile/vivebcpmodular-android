package com.bcp.benefits.viewmodel;

import java.util.ArrayList;
import java.util.List;

import pe.solera.benefit.main.entity.Banner;

/**
 * Created by emontesinos on 20/05/19.
 **/

public class BannerViewModel {

    private int id;
    private String link;

    public BannerViewModel() {
    }

    public BannerViewModel(int id, String link) {
        this.id = id;
        this.link = link;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public static List<BannerViewModel> bannerModels(List<Banner> bannerList) {
        List<BannerViewModel> bannerModels = new ArrayList<>();
        for (Banner banner : bannerList) {
            bannerModels.add(new BannerViewModel(banner.getId(), banner.getImage()));
        }
        return bannerModels;
    }
}
