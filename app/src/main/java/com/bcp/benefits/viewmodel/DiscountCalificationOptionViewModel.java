package com.bcp.benefits.viewmodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import pe.solera.benefit.main.entity.DiscountQualificationOption;

/**
 * Created by emontesinos on 12/06/19.
 */

public class DiscountCalificationOptionViewModel implements Serializable {
    public final Integer idReviewOption;
    public final String description;

    public DiscountCalificationOptionViewModel(Integer idReviewOption, String description) {
        this.idReviewOption = idReviewOption;
        this.description = description;
    }

    public DiscountCalificationOptionViewModel(DiscountQualificationOption discountQualificationOption) {
        this.idReviewOption = discountQualificationOption.getIdReviewOption();
        this.description = discountQualificationOption.getDescription();
    }

    public Integer getIdReviewOption() {
        return idReviewOption;
    }

    public String getDescription() {
        return description;
    }


    public static List<DiscountCalificationOptionViewModel> toCalificationOptionList(List<DiscountQualificationOption> discountQualificationList) {
        List<DiscountCalificationOptionViewModel> list = new ArrayList<>();
        if (discountQualificationList != null) {
            for (DiscountQualificationOption discountQualification : discountQualificationList) {
                list.add(new DiscountCalificationOptionViewModel(discountQualification));
            }
        }
        return list;
    }
}
