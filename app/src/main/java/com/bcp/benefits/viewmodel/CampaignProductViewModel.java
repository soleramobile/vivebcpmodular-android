package com.bcp.benefits.viewmodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import pe.solera.benefit.main.entity.CampaignProduct;

/**
 * Created by emontesinos on 13/05/19.
 **/

public class CampaignProductViewModel implements Serializable {
    private Integer idCampaignProduct;
    private String title;
    private String image;
    public boolean Selected;
    private List<String> descriptions = null;


    public CampaignProductViewModel(Integer idCampaignProduct, String title, String image, boolean selected, List<String> descriptions) {
        this.idCampaignProduct = idCampaignProduct;
        this.title = title;
        this.image = image;
        Selected = selected;
        this.descriptions = descriptions;
    }

    public CampaignProductViewModel(CampaignProduct campaignProduct) {
        this.idCampaignProduct = campaignProduct.getIdCampaignProduct();
        this.title = campaignProduct.getTitle();
        this.image = campaignProduct.getImage();
        this.Selected = campaignProduct.isSelected();
        this.descriptions = campaignProduct.getDescriptions() != null?
                toCampaignDescriptionList(campaignProduct.getDescriptions()): new ArrayList<>();
    }


    public static List<String> toCampaignDescriptionList(List<String> strings){
        List<String> list = new ArrayList<>();
        if (strings != null) {
            for (String strings1 : strings) {
                list.add(new String(strings1));
            }
        }
        return list;
    }

    public static List<CampaignProductViewModel> toCampaignProductList(List<CampaignProduct> campaignProducts){
        List<CampaignProductViewModel> list = new ArrayList<>();
        if (campaignProducts != null) {
            for (CampaignProduct campaignProduct : campaignProducts) {
                list.add(new CampaignProductViewModel(campaignProduct));
            }
        }
        return list;
    }


    public Integer getIdCampaignProduct() {
        return idCampaignProduct;
    }

    public void setIdCampaignProduct(Integer idCampaignProduct) {
        this.idCampaignProduct = idCampaignProduct;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<String> getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(List<String> descriptions) {
        this.descriptions = descriptions;
    }


    public CampaignProductViewModel() {
    }

}
