package com.bcp.benefits.viewmodel;

import pe.solera.benefit.main.entity.Calculate;

/**
 * Created by emontesinos on 21/05/19.
 **/

public class CalculateViewModel {

    private double amount;
    private String tcea;
    private String interest;

    public CalculateViewModel() {
    }

    public CalculateViewModel(double amount, String tcea, String interest) {
        this.amount = amount;
        this.tcea = tcea;
        this.interest = interest;
    }

    public CalculateViewModel(Calculate calculate) {
        this.amount = calculate.getAmount();
        this.tcea = calculate.getTca();
        this.interest = calculate.getInterest();
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getTcea() {
        return tcea;
    }

    public void setTcea(String tcea) {
        this.tcea = tcea;
    }


    public String getInterest() {
        return interest;
    }

    public void setInterest(String interest) {
        this.interest = interest;
    }
}
