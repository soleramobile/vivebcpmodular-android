package com.bcp.benefits.viewmodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import pe.solera.benefit.main.entity.Clinic;

/**
 * Created by emontesinos on 23/05/19.
 **/

public class ClinicViewModel implements Serializable {
    private Integer idClinic;
    private String name;
    private Double latitude;
    private Double longitude;
    private String phone;
    private String address;
    private String webpage;
    private String coverage;
    private String amount;
    private List<HealthAttentionTypeViewModel> attentionTypes;

    private ClinicViewModel(Integer idClinic, String name, Double latitude, Double longitude,
                            String coverage, String amount) {
        this.idClinic = idClinic;
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.coverage = coverage;
        this.amount = amount;
        this.phone = null;
        this.address = null;
        this.webpage = null;
        this.attentionTypes = null;
    }

    private ClinicViewModel(Integer idClinic, String name, Double latitude, Double longitude,
                            String phone, String address, String webpage, String coverage,
                            String amount, List<HealthAttentionTypeViewModel> attentionTypes) {
        this.idClinic = idClinic;
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.phone = phone;
        this.address = address;
        this.webpage = webpage;
        this.coverage = coverage;
        this.amount = amount;
        this.attentionTypes = attentionTypes;
    }

    public ClinicViewModel(Clinic clinic) {
        this.idClinic = clinic.getIdClinic();
        this.name = clinic.getName();
        this.latitude = clinic.getLatitude();
        this.longitude = clinic.getLongitude();
        this.coverage = clinic.getCoverage();
        this.amount = clinic.getAmount();
        this.phone = clinic.getPhone();
        this.address = clinic.getAddress();
        this.webpage = clinic.getWebpage();
        this.attentionTypes = clinic.getAttentionTypes() != null
                ? HealthAttentionTypeViewModel.toHealthAttentionTypeViewModels(
                clinic.getAttentionTypes()) : new ArrayList<>();
    }


    public Integer getIdClinic() {
        return idClinic;
    }

    public void setIdClinic(Integer idClinic) {
        this.idClinic = idClinic;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getWebpage() {
        return webpage;
    }

    public void setWebpage(String webpage) {
        this.webpage = webpage;
    }

    public String getCoverage() {
        return coverage;
    }

    public void setCoverage(String coverage) {
        this.coverage = coverage;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public List<HealthAttentionTypeViewModel> getAttentionTypes() {
        return attentionTypes;
    }

    public void setAttentionTypes(List<HealthAttentionTypeViewModel> attentionTypes) {
        this.attentionTypes = attentionTypes;
    }

    public static List<ClinicViewModel> toClinicViewModels(List<Clinic> clinics) {
        List<ClinicViewModel> clinicViewModels = new ArrayList<>();
        if (clinics != null) {
            for (Clinic clinic : clinics) {
                clinicViewModels.add(new ClinicViewModel(clinic));
            }
        }
        return clinicViewModels;
    }
}
