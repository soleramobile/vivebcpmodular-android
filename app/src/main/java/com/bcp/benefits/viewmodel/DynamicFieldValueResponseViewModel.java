package com.bcp.benefits.viewmodel;

import java.util.ArrayList;
import java.util.List;

import pe.solera.benefit.main.entity.DynamicReferredField;

public class DynamicFieldValueResponseViewModel {

    private int id;
    private String fieldName;
    private String fieldValue;
    private int parentId;
    private List<String> propertyList;


    public DynamicFieldValueResponseViewModel(int id, String fieldName, String fieldValue, int parentId, List<String> propertyList) {
        this.id = id;
        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
        this.parentId = parentId;
        this.propertyList = propertyList;
    }
    public DynamicFieldValueResponseViewModel(DynamicReferredField dynamicReferredField) {
        this.id = dynamicReferredField.getId();
        this.fieldName = dynamicReferredField.getFieldName();
        this.fieldValue = dynamicReferredField.getFieldValue();
        this.parentId = dynamicReferredField.getParentId();
        this.propertyList = dynamicReferredField.getPropertyList() !=null?
                toStrignList(dynamicReferredField.getPropertyList()): new ArrayList<>();
    }

    public static List<String> toStrignList(List<String> strings) {
        List<String> list = new ArrayList<>();
        if (strings != null) {
            for (String strin : strings) {
                list.add(new String(strin));
            }
        }
        return list;
    }

    public static List<DynamicFieldValueResponseViewModel> toDynamicFieldValueList(List<DynamicReferredField> dynamicReferredFields) {
        List<DynamicFieldValueResponseViewModel> list = new ArrayList<>();
        if (dynamicReferredFields != null) {
            for (DynamicReferredField dynamicReferred : dynamicReferredFields) {
                list.add(new DynamicFieldValueResponseViewModel(dynamicReferred));
            }
        }
        return list;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public List<String> getPropertyList() {
        return propertyList;
    }

    public void setPropertyList(List<String> propertyList) {
        this.propertyList = propertyList;
    }
}
