package com.bcp.benefits.viewmodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import pe.solera.benefit.main.entity.TicketRequestVolunteering;
import pe.solera.benefit.main.entity.TicketVolunteering;

public class TicketRequestVolunteeringViewModel implements Serializable {
    private  int idVolunteerCollaborator;
    private  String fullNameCollaborator;
    private  String nameTicket_status;
    private  String typeVolunteerTime;
    private  String functionName;
    private  String nameVolunteer_time;
    private  String dateRequest;
    private  String timeRequest;

    public TicketRequestVolunteeringViewModel(int idVolunteerCollaborator, String fullNameCollaborator, String nameTicket_status,String typeVolunteerTime,String functionName, String nameVolunteer_time, String dateRequest, String timeRequest) {
        this.idVolunteerCollaborator = idVolunteerCollaborator;
        this.fullNameCollaborator = fullNameCollaborator;
        this.nameTicket_status = nameTicket_status;
        this.typeVolunteerTime = typeVolunteerTime;
        this.functionName = functionName;
        this.nameVolunteer_time = nameVolunteer_time;
        this.dateRequest = dateRequest;
        this.timeRequest = timeRequest;
    }

    public String getTypeVolunteerTime() {
        return typeVolunteerTime;
    }

    public void setTypeVolunteerTime(String typeVolunteerTime) {
        this.typeVolunteerTime = typeVolunteerTime;
    }

    public String getFunctionName() {
        return functionName;
    }

    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }

    public static List<TicketRequestVolunteeringViewModel> toForApprovalVolunteerignViewModelList(List<TicketRequestVolunteering> ticketRequestVolunteerings) {
        List<TicketRequestVolunteeringViewModel> list = new ArrayList<>();
        if (ticketRequestVolunteerings != null) {
            for (TicketRequestVolunteering response : ticketRequestVolunteerings) {
                list.add(toForApprovalVolunteerignViewModel(response));
            }
        }
        return list;
    }

    public static TicketRequestVolunteeringViewModel toForApprovalVolunteerignViewModel(TicketRequestVolunteering ticketRequestVolunteering) {
        return new TicketRequestVolunteeringViewModel(
               ticketRequestVolunteering.getIdVolunteerCollaborator(),
                ticketRequestVolunteering.getFullNameCollaborator()!=null?ticketRequestVolunteering.getFullNameCollaborator():"",
                ticketRequestVolunteering.getNameTicket_status()!=null?ticketRequestVolunteering.getNameTicket_status():"",
                ticketRequestVolunteering.getTypeVolunteerTime()!=null?ticketRequestVolunteering.getTypeVolunteerTime():"",
                ticketRequestVolunteering.getFunctionName()!=null?ticketRequestVolunteering.getFunctionName():"",
                ticketRequestVolunteering.getNameVolunteer_time()!=null?ticketRequestVolunteering.getNameVolunteer_time():"",
                ticketRequestVolunteering.getDateRequest()!=null?ticketRequestVolunteering.getDateRequest():"",
                ticketRequestVolunteering.getTimeRequest()!=null?ticketRequestVolunteering.getTimeRequest():""
        );
    }



    public int getIdVolunteerCollaborator() {
        return idVolunteerCollaborator;
    }

    public void setIdVolunteerCollaborator(int idVolunteerCollaborator) {
        this.idVolunteerCollaborator = idVolunteerCollaborator;
    }

    public String getFullNameCollaborator() {
        return fullNameCollaborator;
    }

    public void setFullNameCollaborator(String fullNameCollaborator) {
        this.fullNameCollaborator = fullNameCollaborator;
    }

    public String getNameTicket_status() {
        return nameTicket_status;
    }

    public void setNameTicket_status(String nameTicket_status) {
        this.nameTicket_status = nameTicket_status;
    }

    public String getNameVolunteer_time() {
        return nameVolunteer_time;
    }

    public void setNameVolunteer_time(String nameVolunteer_time) {
        this.nameVolunteer_time = nameVolunteer_time;
    }

    public String getDateRequest() {
        return dateRequest;
    }

    public void setDateRequest(String dateRequest) {
        this.dateRequest = dateRequest;
    }

    public String getTimeRequest() {
        return timeRequest;
    }

    public void setTimeRequest(String timeRequest) {
        this.timeRequest = timeRequest;
    }
}
