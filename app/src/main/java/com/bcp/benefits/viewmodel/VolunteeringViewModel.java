package com.bcp.benefits.viewmodel;

import java.util.ArrayList;
import java.util.List;

import pe.solera.benefit.main.entity.ListLocal;
import pe.solera.benefit.main.entity.ListRole;
import pe.solera.benefit.main.entity.Volunteering;

public class VolunteeringViewModel {
    private int idVolunteer;
    private String nameVolunteer;
    private String address;
    private int capacity;
    private int idCampaign;
    private int idCollaborator;
    private String nameCollaboratorResponsable;
    private int idVolunteerTime;
    private String nameVolunteerTime;
    private String date;
    private String time;
    private List<ListRoleViewModel> listRole;
    private List<ListRoleViewModel> listLocal;
    private String local;
    private String role;
    private int availableVolunteer;
    private String imageLocal;
    private String message;
    private String optionRole;
    private String optionLocal;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public VolunteeringViewModel(int idVolunteer, String nameVolunteer, String address, int capacity, int idCampaign, int idCollaborator, String nameCollaboratorResponsable, int idVolunteerTime, String nameVolunteerTime, String date, String time, List<ListRoleViewModel> listRole, List<ListRoleViewModel> listLocal, String local, String role, int availableVolunteer, String imageLocal, String message, String optionRole, String optionLocal) {
        this.idVolunteer = idVolunteer;
        this.nameVolunteer = nameVolunteer;
        this.address = address;
        this.capacity = capacity;
        this.idCampaign = idCampaign;
        this.idCollaborator = idCollaborator;
        this.nameCollaboratorResponsable = nameCollaboratorResponsable;
        this.idVolunteerTime = idVolunteerTime;
        this.nameVolunteerTime = nameVolunteerTime;
        this.date = date;
        this.time = time;
        this.listRole = listRole;
        this.listLocal = listLocal;
        this.local = local;
        this.role = role;
        this.availableVolunteer = availableVolunteer;
        this.imageLocal = imageLocal;
        this.message = message;
        this.optionRole = optionRole;
        this.optionLocal = optionLocal;
    }

    public VolunteeringViewModel(Volunteering volunteering) {
        this.idVolunteer = volunteering.getIdVolunteer();
        this.nameVolunteer = volunteering.getNameVolunteer()!=null?volunteering.getNameVolunteer():"";
        this.address = volunteering.getAddress()!=null?volunteering.getAddress():"";
        this.capacity = volunteering.getCapacity();
        this.idCampaign = volunteering.getIdCampaign();
        this.idCollaborator = volunteering.getIdCollaborator();
        this.nameCollaboratorResponsable = volunteering.getNameCollaboratorResponsable()!=null?volunteering.getNameCollaboratorResponsable():"";
        this.idVolunteerTime = volunteering.getIdVolunteerTime();
        this.nameVolunteerTime = volunteering.getNameVolunteerTime()!=null?volunteering.getNameVolunteerTime():"";
        this.date = volunteering.getDate()!=null?volunteering.getDate():"";
        this.time = volunteering.getTime()!=null?volunteering.getTime():"";
        this.listLocal = ListRoleViewModel.toListRole(volunteering.getListLocal());
        this.listRole = ListRoleViewModel.toListRole(volunteering.getListRole());
        this.local = volunteering.getLocal();
        this.role = volunteering.getRole();
        this.availableVolunteer = volunteering.getAvailableVolunteer();
        this.imageLocal = volunteering.getImageLocal()!=null?volunteering.getImageLocal():"";
        this.message = volunteering.getMessage()!=null?volunteering.getMessage():"";
        this.optionRole = volunteering.getOptionRole();
        this.optionLocal = volunteering.getOptionLocal();
    }

    public int getIdVolunteer() {
        return idVolunteer;
    }

    public void setIdVolunteer(int idVolunteer) {
        this.idVolunteer = idVolunteer;
    }

    public String getNameVolunteer() {
        return nameVolunteer;
    }

    public void setNameVolunteer(String nameVolunteer) {
        this.nameVolunteer = nameVolunteer;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setListRole(List<ListRoleViewModel> listRole) {
        this.listRole = listRole;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getIdCampaign() {
        return idCampaign;
    }

    public void setIdCampaign(int idCampaign) {
        this.idCampaign = idCampaign;
    }

    public int getIdCollaborator() {
        return idCollaborator;
    }

    public void setIdCollaborator(int idCollaborator) {
        this.idCollaborator = idCollaborator;
    }

    public String getNameCollaboratorResponsable() {
        return nameCollaboratorResponsable;
    }

    public void setNameCollaboratorResponsable(String nameCollaboratorResponsable) {
        this.nameCollaboratorResponsable = nameCollaboratorResponsable;
    }

    public int getIdVolunteerTime() {
        return idVolunteerTime;
    }

    public void setIdVolunteerTime(int idVolunteerTime) {
        this.idVolunteerTime = idVolunteerTime;
    }

    public String getNameVolunteerTime() {
        return nameVolunteerTime;
    }

    public void setNameVolunteerTime(String nameVolunteerTime) {
        this.nameVolunteerTime = nameVolunteerTime;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public List<ListRoleViewModel> getListRole() {
        return listRole;
    }

    public List<ListRoleViewModel> getListLocal() {
        return listLocal;
    }

    public String getLocal() {
        return local;
    }

    public String getRole() {
        return role;
    }

    public int getAvailableVolunteer() {
        return availableVolunteer;
    }

    public void setAvailableVolunteer(int availableVolunteer) {
        this.availableVolunteer = availableVolunteer;
    }

    public String getImageLocal() {
        return imageLocal;
    }

    public void setImageLocal(String imageLocal) {
        this.imageLocal = imageLocal;
    }

    public String getOptionRole() {
        return optionRole;
    }

    public void setOptionRole(String optionRole) {
        this.optionRole = optionRole;
    }

    public String getOptionLocal() {
        return optionLocal;
    }

    public void setOptionLocal(String optionLocal) {
        this.optionLocal = optionLocal;
    }

    public static List<VolunteeringViewModel> toVolunteeringList(List<Volunteering> Volunteerings) {
        List<VolunteeringViewModel> list = new ArrayList<>();
        if (Volunteerings != null) {
            for (Volunteering response : Volunteerings) {
                list.add(new VolunteeringViewModel(response));
            }
        }
        return list;
    }
}
