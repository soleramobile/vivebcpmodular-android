package com.bcp.benefits.viewmodel;

import java.io.Serializable;

import pe.solera.benefit.main.entity.CampaignInfo;

/**
 * Created by emontesinos on 13/05/19.
 **/

public class ListInfoViewModel implements Serializable {
    private String title;
    private String subTitle;
    private String caption;
    private String buttonTextInactive;
    private String buttonTextActive;
    private String image;
    private boolean usage;

    public ListInfoViewModel(CampaignInfo campaignInfo) {
        this.title = campaignInfo.getTitle();
        this.subTitle = campaignInfo.getSubTitle();
        this.caption = campaignInfo.getCaption();
        this.buttonTextInactive = campaignInfo.getButtonTextInactive();
        this.buttonTextActive = campaignInfo.getButtonTextActive();
        this.image = campaignInfo.getImage();
        this.usage = campaignInfo.isUsage();
    }

    public String getTitle() {
        return title!= null && !title.isEmpty() ? title : "";
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getButtonTextInactive() {
        return buttonTextInactive;
    }

    public void setButtonTextInactive(String buttonTextInactive) {
        this.buttonTextInactive = buttonTextInactive;
    }

    public String getButtonTextActive() {
        return buttonTextActive;
    }

    public void setButtonTextActive(String buttonTextActive) {
        this.buttonTextActive = buttonTextActive;
    }

    public boolean isUsage() {
        return usage;
    }

    public void setUsage(boolean usage) {
        this.usage = usage;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


    public ListInfoViewModel() {
    }

}
