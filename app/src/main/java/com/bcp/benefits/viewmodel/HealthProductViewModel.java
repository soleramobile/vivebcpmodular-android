package com.bcp.benefits.viewmodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import pe.solera.benefit.main.entity.HealthProduct;

/**
 * Created by emontesinos on 22/05/19.
 **/

public class HealthProductViewModel implements Serializable {

    private Integer idProduct;
    private String productType;
    private String productName;
    private String productLink;
    private Boolean inUse;
    private List<HealthProductItemViewModel> items;
    private String phone;
    private String url;
    private List<HealthAttentionTypeViewModel> attentionTypes;


    public HealthProductViewModel(Integer idProduct, String productName,
                                  List<HealthAttentionTypeViewModel> attentionTypes, String phone,
                                  String url, String productLink, Boolean inUse, String productType) {
        this.idProduct = idProduct;
        this.productName = productName;
        this.attentionTypes = attentionTypes;
        this.productType = productType;
        this.productLink = null;
        this.inUse = inUse;
        this.items = null;
        this.phone = null;
        this.url = null;
    }

    public HealthProductViewModel(Integer idProduct, String productName,
                                  List<HealthProductItemViewModel> items) {
        this.idProduct = idProduct;
        this.productName = productName;
        this.items = items;
        this.productType = null;
        this.productLink = null;
        this.inUse = null;
        this.attentionTypes = null;
        this.phone = null;
        this.url = null;

    }

    public HealthProductViewModel(HealthProduct healthProduct) {
        this.idProduct = healthProduct.getIdProduct();
        this.productName = healthProduct.getProductName();
        this.items = healthProduct.getItems() != null
                ? HealthProductItemViewModel.toHealthProductItemViewModels(healthProduct.getItems())
                : new ArrayList<>();
        this.productType = healthProduct.getProductType();
        this.productLink = healthProduct.getProductLink();
        this.inUse = healthProduct.getInUse();
        this.attentionTypes = healthProduct.getAttentionTypes() != null
                ? HealthAttentionTypeViewModel.toHealthAttentionTypeViewModels(
                healthProduct.getAttentionTypes()) : new ArrayList<>();
        this.phone = healthProduct.getPhone();
        this.url = healthProduct.getUrl();

    }

    public void setIdProduct(Integer idProduct) {
        this.idProduct = idProduct;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public void setProductLink(String productLink) {
        this.productLink = productLink;
    }

    public void setInUse(Boolean inUse) {
        this.inUse = inUse;
    }

    public void setItems(List<HealthProductItemViewModel> items) {
        this.items = items;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setAttentionTypes(List<HealthAttentionTypeViewModel> attentionTypes) {
        this.attentionTypes = attentionTypes;
    }

    public Integer getIdProduct() {
        return idProduct;
    }

    public String getProductType() {
        return productType;
    }

    public String getProductName() {
        return productName;
    }

    public String getProductLink() {
        return productLink;
    }

    public Boolean getInUse() {
        return inUse;
    }

    public List<HealthProductItemViewModel> getItems() {
        return items;
    }

    public String getPhone() {
        return phone;
    }

    public String getUrl() {
        return url;
    }

    public List<HealthAttentionTypeViewModel> getAttentionTypes() {
        return attentionTypes;
    }

    public static List<HealthProductViewModel> toHealthProductViewModel(List<HealthProduct> healthProducts) {
        List<HealthProductViewModel> modelList = new ArrayList<>();
        if (healthProducts != null) {
            for (HealthProduct product : healthProducts) {
                modelList.add(new HealthProductViewModel(product));
            }
        }
        return modelList;
    }
}
