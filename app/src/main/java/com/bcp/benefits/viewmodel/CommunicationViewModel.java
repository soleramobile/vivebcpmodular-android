package com.bcp.benefits.viewmodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import pe.solera.benefit.main.entity.Communication;


public class CommunicationViewModel implements Serializable {


    public final Integer idCommunication;
    public final String action;
    public final DetailInfoViewModel campaignContent;
    public final String content;
    public final String image;
    public final String buttonText;
    public final String buttonColor;
    public final Boolean isButton;

    public Integer getIdCommunication() {
        return idCommunication;
    }

    public String getAction() {
        return action;
    }

    public DetailInfoViewModel getCampaignContent() {
        return campaignContent;
    }

    public String getContent() {
        return content;
    }

    public String getImage() {
        return image;
    }

    public String getButtonText() {
        return buttonText;
    }

    public String getButtonColor() {
        return buttonColor;
    }

    public Boolean getButton() {
        return isButton;
    }

    public CommunicationViewModel(Integer idCommunication, String action, DetailInfoViewModel campaignContent,
                                  String content, String image, String buttonText, String buttonColor, Boolean isButton) {
        this.idCommunication = idCommunication;
        this.action = action;
        this.campaignContent = campaignContent;
        this.content = content;
        this.image = image;
        this.buttonText = buttonText;
        this.buttonColor = buttonColor;
        this.isButton = isButton;
    }

    public CommunicationViewModel(Communication communication) {
        this.idCommunication = communication.getIdCommunication() != null ? communication.getIdCommunication() : 0;
        this.action = communication.getAction() != null ? communication.getAction() : "";
        this.campaignContent = communication.getCampaignContent() != null ? new DetailInfoViewModel(communication.getCampaignContent()) : null;
        this.content = communication.getContent();
        this.image = communication.getImage() != null ? communication.getImage() : "";
        this.buttonText = communication.getButtonText() != null ? communication.getButtonText() : "";
        this.buttonColor = communication.getButtonColor() != null ? communication.getButtonColor() : "";
        this.isButton = communication.getButton() != null ? communication.getButton() : false;
    }

    public static List<CommunicationViewModel> toOwnedList(List<Communication> communications) {
        List<CommunicationViewModel> list = new ArrayList<>();
        if (communications != null) {
            for (Communication communication : communications) {
                list.add(new CommunicationViewModel(communication));
            }
        }
        return list;
    }

}
