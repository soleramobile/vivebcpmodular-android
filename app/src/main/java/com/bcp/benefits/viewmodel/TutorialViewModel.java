package com.bcp.benefits.viewmodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import pe.solera.benefit.main.entity.Banner;

/**
 * Created by emontesinos.
 **/

public class TutorialViewModel implements Serializable {
    private String id;
    private String title;
    private String colorButton;
    private String image;
    private String background;

    public TutorialViewModel(String id, String title, String colorButton, String image, String background) {
        this.id = id;
        this.title = title;
        this.colorButton = colorButton;
        this.image = image;
        this.background = background;
    }

    public TutorialViewModel(String id, String background) {
        this.id = id;
        this.background = background;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getColorButton() {
        return colorButton;
    }

    public void setColorButton(String colorButton) {
        this.colorButton = colorButton;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public static List<TutorialViewModel> tutorialViewModelList(List<Banner> bannerList) {
        List<TutorialViewModel> tutorialViewModels = new ArrayList<>();
        if (bannerList != null) {
            for (Banner banner : bannerList) {
                tutorialViewModels.add(new TutorialViewModel(String.valueOf(banner.getId()),
                        banner.getUrl()));
            }
        }
        return tutorialViewModels;
    }
}
