package com.bcp.benefits.viewmodel;
/* 
   Created by: Flavia Figueroa
               13/02/2020
         Solera Mobile
*/


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import pe.solera.benefit.main.entity.ListLocal;

public class ListLocalViewModel implements Serializable {
    private Integer idLocalVolunteer;
    private String local;

    public ListLocalViewModel(Integer idLocalVolunteer, String local) {
        this.idLocalVolunteer = idLocalVolunteer;
        this.local = local;
    }

    ListLocalViewModel(ListLocal list){
        this.idLocalVolunteer = list.getIdLocalVolunteer();
        this.local = list.getLocal();
    }

    public Integer getIdLocalVolunteer() {
        return idLocalVolunteer;
    }

    public String getLocal() {
        return local;
    }

    public static List<ListLocalViewModel> toListLocal(List<ListLocal> listLocal){
        List<ListLocalViewModel> list = new ArrayList<>();
        if (listLocal!= null){
            for (ListLocal local: listLocal){
                list.add(toListLocalViewModel(local));
            }
        }
        return list;
    }

    public static ListLocalViewModel toListLocalViewModel(ListLocal listLocal){
        return new ListLocalViewModel(listLocal);
    }
}
