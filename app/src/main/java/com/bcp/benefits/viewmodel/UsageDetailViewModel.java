package com.bcp.benefits.viewmodel;

import android.os.Parcel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import pe.solera.benefit.main.entity.UsageDetail;

/**
 * Created by emontesinos on 13/05/19.
 **/

public class UsageDetailViewModel implements Serializable {


    private List<Integer> products;
    private SubsidiaryViewModel subsidiary;


    public UsageDetailViewModel(UsageDetail usageDetail) {
        this.products = usageDetail.getProducts() != null ? getProducts(usageDetail.getProducts()): new ArrayList();
        this.subsidiary = usageDetail.getSubsidiary() != null ? new SubsidiaryViewModel(usageDetail.getSubsidiary()): null;
    }

    public SubsidiaryViewModel getSubsidiary() {
        return subsidiary;
    }

    public void setSubsidiary(SubsidiaryViewModel subsidiary) {
        this.subsidiary = subsidiary;
    }

    public List<Integer> getProducts() {
        return products;
    }

    public List<Integer> getProducts(List<Integer> integers) {
        List<Integer> list = new ArrayList<>();
        if (integers != null) {
            for (Integer integer : integers) {
                list.add( new Integer(integer));
            }
        }
        return list;
    }

    public void setProducts(List<Integer> products) {
        this.products = products;
    }



    protected UsageDetailViewModel(Parcel in) {
        this.products = new ArrayList<Integer>();
        in.readList(this.products, Integer.class.getClassLoader());
        this.subsidiary = in.readParcelable(SubsidiaryViewModel.class.getClassLoader());
    }

}
