package com.bcp.benefits.viewmodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import pe.solera.benefit.main.entity.CampaignLocation;

/**
 * Created by emontesinos on 13/05/19.
 **/

public class CampaignLocationViewModel implements Serializable {
    private Integer idLocation;
    private String description;
    private boolean collect;
    private boolean selected;
    private String textCollect;
    private List<SubsidiaryViewModel> subsidiaries = null;



    public CampaignLocationViewModel() {
    }

    public CampaignLocationViewModel(Integer idLocation, String description, boolean collect,
                                     boolean selected, String textCollect,
                                     ArrayList<SubsidiaryViewModel> subsidiaries) {
        this.idLocation = idLocation;
        this.description = description;
        this.collect = collect;
        this.selected = selected;
        this.textCollect = textCollect;
        this.subsidiaries = subsidiaries;
    }

    public CampaignLocationViewModel(CampaignLocation campaignLocation) {

        this.idLocation = campaignLocation.getIdLocation();
        this.description = campaignLocation.getDescription();
        this.collect = campaignLocation.isCollect();
        this.selected = campaignLocation.isSelected();
        this.textCollect = campaignLocation.getTextCollect();
        this.subsidiaries =campaignLocation.getSubsidiaries()!=null?
                SubsidiaryViewModel.toSubsidiaryList(campaignLocation.getSubsidiaries()): new ArrayList<>();

    }

    public static List<CampaignLocationViewModel> toLocationList(List<CampaignLocation> campaignLocations) {
        List<CampaignLocationViewModel> list = new ArrayList<>();
        if (campaignLocations != null) {
            for (CampaignLocation location : campaignLocations) {
                list.add(toLocationViewModel(location));
            }
        }
        return list;
    }


    public static CampaignLocationViewModel toLocationViewModel(CampaignLocation campaignLocation) {
        return new CampaignLocationViewModel(campaignLocation);
    }

    public Integer getIdLocation() {
        return idLocation;
    }

    public void setIdLocation(Integer idLocation) {
        this.idLocation = idLocation;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isCollect() {
        return collect;
    }

    public void setCollect(boolean collect) {
        this.collect = collect;
    }

    public String getTextCollect() {
        return textCollect;
    }

    public void setTextCollect(String textCollect) {
        this.textCollect = textCollect;
    }

    public List<SubsidiaryViewModel> getSubsidiares() {
        return subsidiaries;
    }

    public void setSubsidiares(ArrayList<SubsidiaryViewModel> subsidiaries) {
        this.subsidiaries = subsidiaries;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

}
