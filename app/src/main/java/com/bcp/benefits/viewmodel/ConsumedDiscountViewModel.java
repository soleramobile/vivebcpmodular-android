package com.bcp.benefits.viewmodel;

import java.util.ArrayList;
import java.util.List;

import pe.solera.benefit.main.entity.ConsumeDiscount;

/**
 * Created by emontesinos on 12/06/19.
 */

public class ConsumedDiscountViewModel {
    public final Integer idUsage;
    public final String barCode;
    public final List<DiscountCalificationViewModel> reviewForm;
    public ConsumedDiscountType idType;
    public final String usageUrl;

    public ConsumedDiscountViewModel(ConsumeDiscount consumeDiscount) {
        this.idUsage = consumeDiscount.getIdUsage();
        this.barCode = consumeDiscount.getBarCode();
        this.reviewForm =consumeDiscount.getReviewForm() != null?
                DiscountCalificationViewModel.toCalificationList(consumeDiscount.getReviewForm()): new ArrayList<>();
        switch (consumeDiscount.getIdType()){
            case 1:
                this.idType = ConsumedDiscountType.DNI;
                break;
            case 2:
                this.idType = ConsumedDiscountType.CODIGO;
                break;
            case 3:
                this.idType = ConsumedDiscountType.PDF;
                break;
            case 4:
                this.idType = ConsumedDiscountType.LINK;
                break;
        }
        this.usageUrl = consumeDiscount.getUsageUrl();
    }
}

