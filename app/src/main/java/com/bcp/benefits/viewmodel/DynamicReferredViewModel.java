package com.bcp.benefits.viewmodel;



import java.util.ArrayList;
import java.util.List;

import pe.solera.benefit.main.entity.DynamicReferred;


public class DynamicReferredViewModel {

    private int id;
    private List<DynamicFieldValueResponseViewModel> fieldValueList;




    public DynamicReferredViewModel() {
        this.id = id;
        this.fieldValueList = fieldValueList;
    }

    public DynamicReferredViewModel(DynamicReferred dynamicReferred) {
        this.id = dynamicReferred.getId();
        this.fieldValueList = dynamicReferred != null ?
                DynamicFieldValueResponseViewModel.toDynamicFieldValueList(dynamicReferred.getValues()):new ArrayList<>();
    }


    public static List<DynamicReferredViewModel> toDynamicReferredList(List<DynamicReferred> dynamicReferredFields) {
        List<DynamicReferredViewModel> list = new ArrayList<>();
        if (dynamicReferredFields != null) {
            for (DynamicReferred dynamicReferred : dynamicReferredFields) {
                list.add(new DynamicReferredViewModel(dynamicReferred));
            }
        }
        return list;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<DynamicFieldValueResponseViewModel> getFieldValueList() {
        return fieldValueList;
    }

    public void setFieldValueList(List<DynamicFieldValueResponseViewModel> fieldValueList) {
        this.fieldValueList = fieldValueList;
    }
}
