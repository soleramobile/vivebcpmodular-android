package com.bcp.benefits.viewmodel;

import java.util.ArrayList;
import java.util.List;

import pe.solera.benefit.main.entity.Boss;

/**
 * Created by emontesinos on 22/05/19.
 **/

public class BossViewModel {
    public final Integer idUser;
    public final String name;

    public Integer getIdUser() {
        return idUser;
    }

    public String getName() {
        return name;
    }

    public BossViewModel(Integer idUser, String name) {
        this.idUser = idUser;
        this.name = name;
    }


    public BossViewModel(Boss boss) {
        this.idUser = boss.getIdUser();
        this.name = boss.getName();
    }



    public static List<BossViewModel> toBossList(List<Boss> bosses) {
        List<BossViewModel> list = new ArrayList<>();
        if (bosses != null) {
            for (Boss boss : bosses) {
                list.add(new BossViewModel(boss));
            }
        }
        return list;
    }

}
