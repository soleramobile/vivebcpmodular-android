package com.bcp.benefits.viewmodel;
/* 
   Created by: Flavia Figueroa
               26/02/2020
         Solera Mobile
*/


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import pe.solera.benefit.main.entity.ListCollaborator;

public class ListCollaboratorViewModel implements Serializable {
    public int idVolunteerCollaborator;
    public String collaboratorName;
    public String documentNumber;
    public int attendance;
    public String commentary;

    public ListCollaboratorViewModel(int idVolunteerCollaborator, String collaboratorName, String documentNumber, int attendance, String commentary) {
        this.idVolunteerCollaborator = idVolunteerCollaborator;
        this.collaboratorName = collaboratorName;
        this.documentNumber = documentNumber;
        this.attendance = attendance;
        this.commentary = commentary;
    }

    public ListCollaboratorViewModel(ListCollaborator listRole) {
        this.idVolunteerCollaborator = listRole.getIdVolunteerCollaborator();
        this.collaboratorName = listRole.getCollaboratorName();
        this.documentNumber = listRole.getDocumentNumber();
        this.attendance = listRole.getAttendance();
        this.commentary = listRole.getCommentary();
    }

    public ListCollaboratorViewModel(ListCollaborator data, int count) {
        this.idVolunteerCollaborator = data.getIdVolunteerCollaborator();
        this.collaboratorName = data.getCollaboratorName()!=null?count+"  "+data.getCollaboratorName():"";
        this.documentNumber = data.getDocumentNumber()!=null?data.getDocumentNumber():"";
        this.attendance = data.getAttendance();
        this.commentary = data.getCommentary()!=null?data.getCommentary():"";
    }

    public int getIdVolunteerCollaborator() {
        return idVolunteerCollaborator;
    }

    public void setIdVolunteerCollaborator(int idVolunteerCollaborator) {
        this.idVolunteerCollaborator = idVolunteerCollaborator;
    }

    public String getCollaboratorName() {
        return collaboratorName;
    }

    public void setCollaboratorName(String collaboratorName) {
        this.collaboratorName = collaboratorName;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public int getAttendance() {
        return attendance;
    }

    public void setAttendance(int attendance) {
        this.attendance = attendance;
    }

    public String getCommentary() {
        return commentary;
    }

    public void setCommentary(String commentary) {
        this.commentary = commentary;
    }

    public static List<ListCollaboratorViewModel> toListCollaborator(List<ListCollaborator> listCollaborators){
        int count = 1;
        List<ListCollaboratorViewModel> list = new ArrayList<>();
        if (listCollaborators != null){
            for (ListCollaborator campaign: listCollaborators){
                list.add(toListCollaboratorViewModel(campaign, count));
                count++;
            }
        }
        return list;
    }

    public static ListCollaboratorViewModel toListCollaboratorViewModel(ListCollaborator listRole, int count){
        return new ListCollaboratorViewModel(listRole, count);
    }
}
