package com.bcp.benefits.viewmodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import pe.solera.benefit.main.entity.HealthProductItem;

/**
 * Created by emontesinos on 22/05/19.
 **/

public class HealthProductItemViewModel implements Serializable {

    private String description;
    private String extras;
    private String coverage;
    private String amount;
    private String disclaimer;

    public HealthProductItemViewModel() {
    }

    public HealthProductItemViewModel(HealthProductItem item) {
        this.description = item.getDescription();
        this.extras = item.getExtras();
        this.coverage = item.getCoverage();
        this.amount = item.getAmount();
        this.disclaimer = item.getDisclaimer();
    }

    private HealthProductItemViewModel(String description, String extras, String coverage,
                                       String amount, String disclaimer) {
        this.description = description;
        this.extras = extras;
        this.coverage = coverage;
        this.amount = amount;
        this.disclaimer = disclaimer;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getExtras() {
        return extras;
    }

    public void setExtras(String extras) {
        this.extras = extras;
    }

    public String getCoverage() {
        return coverage;
    }

    public void setCoverage(String coverage) {
        this.coverage = coverage;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDisclaimer() {
        return disclaimer;
    }

    public void setDisclaimer(String disclaimer) {
        this.disclaimer = disclaimer;
    }

    public static List<HealthProductItemViewModel> toHealthProductItemViewModels(
            List<HealthProductItem> healthProductItems) {
        List<HealthProductItemViewModel> healthProductItemViewModels = new ArrayList<>();
        if (healthProductItems != null) {
            for (HealthProductItem item : healthProductItems) {
                healthProductItemViewModels.add(new HealthProductItemViewModel(item));
            }
        }
        return healthProductItemViewModels;
    }
}
