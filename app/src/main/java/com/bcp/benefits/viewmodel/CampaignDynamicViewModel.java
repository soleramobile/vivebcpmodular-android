package com.bcp.benefits.viewmodel;




import java.util.List;

import pe.solera.benefit.main.entity.CampaignDynamic;

/**
 * Created by emontesinos on 20/05/19.
 **/

public class CampaignDynamicViewModel {


    private String title;
    private String subTitle;
    private String disclaimer;
    private String termsConditions;
    private String buttonTextSend;
    private List<DynamicFieldViewModel> dynamicFieldModelList;
    private boolean viewReferees;


    public CampaignDynamicViewModel(String title, String subTitle, String disclaimer, String termsConditions,
                                    String buttonTextSend, List<DynamicFieldViewModel> dynamicFieldModelList,
                                    boolean viewReferees) {
        this.title = title;
        this.subTitle = subTitle;
        this.disclaimer = disclaimer;
        this.termsConditions = termsConditions;
        this.buttonTextSend = buttonTextSend;
        this.dynamicFieldModelList = dynamicFieldModelList;
        this.viewReferees = viewReferees;
    }

    public CampaignDynamicViewModel(CampaignDynamic campaignDynamic) {
        this.title = campaignDynamic.getTitle();
        this.subTitle = campaignDynamic.getSubTitle();
        this.disclaimer = campaignDynamic.getDisclaimer();
        this.termsConditions = campaignDynamic.getTermsConditions();
        this.buttonTextSend = campaignDynamic.getButtonTextSend();
        this.dynamicFieldModelList = campaignDynamic.getDynamicFields()!= null ? DynamicFieldViewModel.toDynamicList(campaignDynamic.getDynamicFields()): null;
        this.viewReferees = campaignDynamic.getViewReferees();
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getDisclaimer() {
        return disclaimer;
    }

    public void setDisclaimer(String disclaimer) {
        this.disclaimer = disclaimer;
    }

    public String getTermsConditions() {
        return termsConditions;
    }

    public void setTermsConditions(String termsConditions) {
        this.termsConditions = termsConditions;
    }

    public String getButtonTextSend() {
        return buttonTextSend;
    }

    public void setButtonTextSend(String buttonTextSend) {
        this.buttonTextSend = buttonTextSend;
    }

    public List<DynamicFieldViewModel> getDynamicFieldModelList() {
        return dynamicFieldModelList;
    }

    public void setDynamicFieldModelList(List<DynamicFieldViewModel> dynamicFieldModelList) {
        this.dynamicFieldModelList = dynamicFieldModelList;
    }

    public boolean isViewReferees() {
        return viewReferees;
    }

    public void setViewReferees(boolean viewReferees) {
        this.viewReferees = viewReferees;
    }
}
