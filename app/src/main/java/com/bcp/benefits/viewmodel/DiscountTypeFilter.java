package com.bcp.benefits.viewmodel;

import java.io.Serializable;

/**
 * Created by emontesinos on 22/05/19.
 **/

public enum DiscountTypeFilter implements Serializable {
    LISTFILTER("listFilter", 1), MAPSFILTER("listMaps", 2);

    private int index;
    private String value;

    DiscountTypeFilter(String value, int index) {
        this.value = value;
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

    public String getValue() {
        return value;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static DiscountTypeFilter getDiscountType(DiscountTypeFilter discountTypeFilter) {
        if (discountTypeFilter == DiscountTypeFilter.LISTFILTER)
            return DiscountTypeFilter.LISTFILTER;
        else
            return DiscountTypeFilter.MAPSFILTER;
    }
}
