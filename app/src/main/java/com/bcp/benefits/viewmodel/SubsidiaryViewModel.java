package com.bcp.benefits.viewmodel;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import pe.solera.benefit.main.entity.Subsidiary;

/**
 * Created by emontesinos on 13/05/19.
 **/

public class SubsidiaryViewModel implements Serializable {
    private String idSubsidiary;
    private String name;
    private String address;
    private String phone;
    private String latitude;
    private String longitude;
    private String description;
    private String subsidiaryDetail;
    private int idSubsidiaryCampaign;
    private String agency;
    private boolean selected;

    public SubsidiaryViewModel(String idSubsidiary, String name, String address, String phone,
                               String latitude, String longitude, String description,
                               String subsidiaryDetail, int idSubsidiaryCampaign, String agency) {
        this.idSubsidiary = idSubsidiary;
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.latitude = latitude;
        this.longitude = longitude;
        this.description = description;
        this.subsidiaryDetail = subsidiaryDetail;
        this.idSubsidiaryCampaign = idSubsidiaryCampaign;
        this.agency = agency;
    }

    public SubsidiaryViewModel(Subsidiary subsidiary) {
        this.idSubsidiary = subsidiary.getIdSubsidiary() != null ? subsidiary.getIdSubsidiary() : "";
        this.name = subsidiary.getName()!= null ? subsidiary.getName() : "";
        this.address = subsidiary.getAddress()!= null ? subsidiary.getAddress() : "";
        this.phone = subsidiary.getPhone()!= null ? subsidiary.getPhone() : "";
        this.latitude = subsidiary.getLatitude()!= null ? subsidiary.getLatitude() : "";
        this.longitude = subsidiary.getLongitude()!= null ? subsidiary.getLongitude() : "";
        this.description = subsidiary.getDescription()!= null ? subsidiary.getDescription() : "";
        this.subsidiaryDetail = subsidiary.getSubsidiaryDetail()!= null ? subsidiary.getSubsidiaryDetail() : "";
        this.idSubsidiaryCampaign = subsidiary.getIdSubsidiaryCampaign();
        this.agency = subsidiary.getAgency()!= null ? subsidiary.getAgency() : "";
    }

    public String getIdSubsidiary() {
        return idSubsidiary;
    }

    public void setIdSubsidiary(String idSubsidiary) {
        this.idSubsidiary = idSubsidiary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSubsidiaryDetail() {
        return subsidiaryDetail;
    }

    public void setSubsidiaryDetail(String subsidiaryDetail) {
        this.subsidiaryDetail = subsidiaryDetail;
    }

    public int getIdSubsidiaryCampaign() {
        return idSubsidiaryCampaign;
    }

    public void setIdSubsidiaryCampaign(int idSubsidiaryCampaign) {
        this.idSubsidiaryCampaign = idSubsidiaryCampaign;
    }

    public String getAgency() {
        return agency;
    }

    public void setAgency(String agency) {
        this.agency = agency;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public static List<SubsidiaryViewModel> toSubsidiaryList(List<Subsidiary> subsidiaryList) {
        List<SubsidiaryViewModel> list = new ArrayList<>();
        if (subsidiaryList != null) {
            for (Subsidiary location : subsidiaryList) {
                list.add(toSubsidiaryViewModel(location));
            }
        }
        return list;
    }

    public static SubsidiaryViewModel toSubsidiaryViewModel(Subsidiary subsidiary) {
        return new SubsidiaryViewModel(subsidiary);
    }
}
