package com.bcp.benefits.viewmodel;

import java.util.ArrayList;
import java.util.List;

import pe.solera.benefit.main.entity.Bus;

public class BusViewModel {
    private int idBusType;
    private String description;
    private boolean isSelected;

    public BusViewModel(Bus bus) {
        this.idBusType = bus.getIdBusType();
        this.description = bus.getDescription();
        this.isSelected = false;
    }

    public int getIdBusType() {
        return idBusType;
    }

    public void setIdBusType(int idBusType) {
        this.idBusType = idBusType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public static List<BusViewModel> toList(List<Bus> busList) {
        List<BusViewModel> busViewModels = new ArrayList<>();
        for (Bus model : busList) {
            busViewModels.add(new BusViewModel(model));
        }
        if (busViewModels.size() > 0)
            busViewModels.get(0).setSelected(true);
        return busViewModels;
    }
}
