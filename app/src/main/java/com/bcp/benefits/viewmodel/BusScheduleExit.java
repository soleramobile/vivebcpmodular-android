package com.bcp.benefits.viewmodel;

import java.util.ArrayList;
import java.util.List;

public class BusScheduleExit {
    private String busSchedule;
    private boolean selected;

    public BusScheduleExit(String busSchedule) {
        this.busSchedule = busSchedule;
        this.selected = false;
    }

    public String getBusSchedule() {
        return busSchedule;
    }

    public void setBusSchedule(String busSchedule) {
        this.busSchedule = busSchedule;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public static List<BusScheduleExit> toList(List<String> stringList) {
        List<BusScheduleExit> exitList = new ArrayList<>();
        for (String item : stringList) {
            exitList.add(new BusScheduleExit(item));
        }
        if (exitList.size() > 0)
            exitList.get(0).setSelected(true);
        return exitList;
    }
}
