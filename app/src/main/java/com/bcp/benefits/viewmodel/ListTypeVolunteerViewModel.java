package com.bcp.benefits.viewmodel;
/* 
   Created by: Flavia Figueroa
               25/02/2020
         Solera Mobile
*/


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import pe.solera.benefit.main.entity.ListType;

public class ListTypeVolunteerViewModel implements Serializable {
    private Integer idVolunteerType;
    private String name;

    public ListTypeVolunteerViewModel(Integer idVolunteerType, String name) {
        this.idVolunteerType = idVolunteerType;
        this.name = name;
    }

    public ListTypeVolunteerViewModel(ListType responseData) {
        this.idVolunteerType = responseData.getIdVolunteerType();
        this.name = responseData.getName();
    }

    public Integer getIdVolunteerType() {
        return idVolunteerType;
    }

    public void setIdVolunteerType(Integer idVolunteerType) {
        this.idVolunteerType = idVolunteerType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static List<ListTypeVolunteerViewModel> toListTypeVolunteer(List<ListType> campaigns) {


        List<ListTypeVolunteerViewModel> list = new ArrayList<>();
        if (campaigns != null) {
            for (ListType campaign : campaigns) {
                list.add(new ListTypeVolunteerViewModel(campaign));
            }
        }
        return list;
    }
}
