package com.bcp.benefits.viewmodel;


import java.util.ArrayList;
import java.util.List;

import pe.solera.benefit.main.entity.DynamicField;

/**
 * Created by emontesinos on 20/05/19.
 **/

public class DynamicFieldViewModel {

    public static final int TYPE_SWITCH = 1;
    public static final int TYPE_STRING = 2;
    public static final int TYPE_NUMBER = 3;

    public static final int VALIDATION_TYPE_LENGTH = 1;
    public static final int VALIDATION_TYPE_VALUE = 2;

    private int id;
    private int type;
    private String label;
    private int min;
    private int max;
    private String keyboard;
    private List<String> values;
    private List<DynamicFieldViewModel> dynamicFieldModelList;
    private int validationType;

    public DynamicFieldViewModel(int id, int type, String label, int min, int max, String keyboard, List<String> values,
                                 List<DynamicFieldViewModel> dynamicFieldModelList, int validationType) {
        this.id = id;
        this.type = type;
        this.label = label;
        this.min = min;
        this.max = max;
        this.keyboard = keyboard;
        this.values = values;
        this.dynamicFieldModelList = dynamicFieldModelList;
        this.validationType = validationType;
    }

    public DynamicFieldViewModel(DynamicField dynamicField) {
        this.id = dynamicField.getId();
        this.type = dynamicField.getType();
        this.label = dynamicField.getLabel();
        this.min = dynamicField.getMin()!= null ? dynamicField.getMin() : 0;
        this.max = dynamicField.getMax()!= null ? dynamicField.getMax() : 0;
        this.keyboard = dynamicField.getKeyboard();
        this.values = dynamicField.getValues()!= null ? toValuesList(dynamicField.getValues()): new ArrayList<>();
        this.dynamicFieldModelList =dynamicField.getDynamicFieldList() != null ?
                toDynamicList(dynamicField.getDynamicFieldList()): new ArrayList<>();
        this.validationType = dynamicField.getValidationType()!= null ? dynamicField.getValidationType(): 0;
    }

    public static List<String> toValuesList(List<String> values) {
        List<String> list = new ArrayList<>();
        if (values != null) {
            for (String value : values) {
                list.add(new String(value));
            }
        }
        return list;
    }


    public static List<DynamicFieldViewModel> toDynamicList(List<DynamicField> dynamicFields) {
        List<DynamicFieldViewModel> list = new ArrayList<>();
        if (dynamicFields != null) {
            for (DynamicField dynamicField: dynamicFields) {
                list.add(new DynamicFieldViewModel(dynamicField));
            }
        }
        return list;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public String getKeyboard() { return keyboard; }

    public void setKeyboard(String keyboard) { this.keyboard = keyboard; }

    public List<DynamicFieldViewModel> getDynamicFieldModelList() {
        return dynamicFieldModelList;
    }

    public void setDynamicFieldModelList(List<DynamicFieldViewModel> dynamicFieldModelList) {
        this.dynamicFieldModelList = dynamicFieldModelList;
    }

    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }

    public int getValidationType() {
        return validationType;
    }

    public void setValidationType(int validationType) {
        this.validationType = validationType;
    }
}
