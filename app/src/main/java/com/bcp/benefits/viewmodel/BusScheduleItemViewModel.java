package com.bcp.benefits.viewmodel;

import java.util.ArrayList;
import java.util.List;

import pe.solera.benefit.main.entity.BusScheduleItem;

public class BusScheduleItemViewModel {
    private List<BusScheduleExit> start;
    private String arrival;
    private boolean isSelected;

    public BusScheduleItemViewModel(BusScheduleItem busScheduleItem) {
        this.start = BusScheduleExit.toList(busScheduleItem.getStart());
        this.arrival = busScheduleItem.getArrival();
        isSelected = false;
    }


    public List<BusScheduleExit> getStart() {
        return start;
    }

    public void setStart(List<BusScheduleExit> start) {
        this.start = start;
    }

    public String getArrival() {
        return arrival;
    }

    public void setArrival(String arrival) {
        this.arrival = arrival;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public static List<BusScheduleItemViewModel> toList(List<BusScheduleItem> busScheduleItems) {
        List<BusScheduleItemViewModel> list = new ArrayList<>();
        for (BusScheduleItem item : busScheduleItems) {
            list.add(new BusScheduleItemViewModel(item));
        }
        if (!list.isEmpty())
            list.get(0).setSelected(true);
        return list;
    }
}
