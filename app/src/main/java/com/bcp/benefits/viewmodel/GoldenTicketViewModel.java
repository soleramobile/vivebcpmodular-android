package com.bcp.benefits.viewmodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import pe.solera.benefit.main.entity.Ticket;

/**
 * Created by emontesinos on 02/05/19.
 **/

public class GoldenTicketViewModel implements Serializable {
    private boolean isDepartmentHead;
    private List<OwnedViewModel> owned;
    private List<ForApprovalViewModel> forApproval;

    public GoldenTicketViewModel() {
    }

    public GoldenTicketViewModel(boolean isDepartmentHead, List<OwnedViewModel> owned,
                                 List<ForApprovalViewModel> forApproval) {
        this.isDepartmentHead = isDepartmentHead;
        this.owned = owned;
        this.forApproval = forApproval;
    }

    public GoldenTicketViewModel(Ticket ticket) {
        this.isDepartmentHead = ticket.isDepartmentHead();
        this.owned = OwnedViewModel.toOwnedList(ticket.getOwnTickets());
        this.forApproval = ticket.getApprovalTickets() != null?
                ForApprovalViewModel.toForApprovalViewModelList(ticket.getApprovalTickets()): new ArrayList<>();
    }


    public List<OwnedViewModel> getOwned() {
        return owned;
    }

    public void setOwned(List<OwnedViewModel> owned) {
        this.owned = owned;
    }

    public List<ForApprovalViewModel> getForApproval() {
        return forApproval;
    }

    public void setForApproval(List<ForApprovalViewModel> forApproval) {
        this.forApproval = forApproval;
    }

    public boolean isDepartmentHead() {
        return isDepartmentHead;
    }

    public void setDepartmentHead(boolean departmentHead) {
        isDepartmentHead = departmentHead;
    }

}