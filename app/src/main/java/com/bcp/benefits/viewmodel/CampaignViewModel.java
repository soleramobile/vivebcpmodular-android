package com.bcp.benefits.viewmodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import pe.solera.benefit.main.entity.Campaign;

/**
 * Created by emontesinos on 13/05/19.
 **/

public class CampaignViewModel implements Serializable {
    private Integer idCampaign;
    private CampaignType idCampaignType;
    private Integer idVolunteerType;
    public Integer enableLocal;
    public Integer enableRole;
    private ListInfoViewModel listInfo;
    private DetailInfoViewModel detailInfo;
    private UsageDetailViewModel usageDetail;
    private LocationDetailViewModel detailLocation;
    private boolean showLocations;
    private boolean selectProducts;
    private List<ButtonProductViewModel> buttons;
    private String messageVolunteer;
    private List<CampaignProductViewModel> products;
    private List<SubsidiaryViewModel> subsidiares;
    private Integer idDiscount;
    private Integer idSubsidiary;
    private Integer idDiscountCategory;
    private Integer idProvince;
    private Integer idDistrict;
    private Integer idDiscountType;


    public CampaignViewModel(Integer idCampaign, CampaignType idCampaignType, Integer idVolunteerType, Integer enableLocal, Integer enableRole, ListInfoViewModel listInfo, DetailInfoViewModel
            detailInfo, UsageDetailViewModel usageDetail, LocationDetailViewModel detailLocation, boolean showLocations,
                             boolean selectProducts, List<ButtonProductViewModel> buttons,String messageVolunteer, List<CampaignProductViewModel> products,
                             List<SubsidiaryViewModel> subsidiares, Integer idDiscount, Integer idSubsidiary,
                             Integer idDiscountCategory, Integer idProvince, Integer idDistrict, Integer idDiscountType) {
        this.idCampaign = idCampaign;
        this.idCampaignType = idCampaignType;
        this.idVolunteerType = idVolunteerType;
        this.enableLocal = enableLocal;
        this.enableRole = enableRole;
        this.listInfo = listInfo;
        this.detailInfo = detailInfo;
        this.usageDetail = usageDetail;
        this.detailLocation = detailLocation;
        this.showLocations = showLocations;
        this.selectProducts = selectProducts;
        this.buttons = buttons;
        this.messageVolunteer = messageVolunteer;
        this.products = products;
        this.subsidiares = subsidiares;
        this.idDiscount = idDiscount;
        this.idSubsidiary = idSubsidiary;
        this.idDiscountCategory = idDiscountCategory;
        this.idProvince = idProvince;
        this.idDistrict = idDistrict;
        this.idDiscountType = idDiscountType;
    }

    private CampaignViewModel(Campaign campaign) {
        this.idCampaign = campaign.getIdCampaign();
        switch (campaign.getIdCampaignType()){
            case 1:
                this.idCampaignType = CampaignType.PRODUCTO;
                break;
            case 2:
                this.idCampaignType = CampaignType.INFORMATIVO;
                break;
            case 3:
                this.idCampaignType = CampaignType.DESCUENTO;
                break;
            case 4:
                this.idCampaignType = CampaignType.DINAMICO;
                break;
            case 5:
                this.idCampaignType = CampaignType.VOLUNTARIADO;
                break;
        }
        this.idVolunteerType = campaign.getIdVolunteerType();
        this.enableLocal = campaign.getEnableLocal();
        this.enableRole = campaign.getEnableRole();
        this.listInfo = new ListInfoViewModel(campaign.getCampaignInfo());
        this.detailInfo = campaign.getDetailCampaign() != null
                ? new DetailInfoViewModel(campaign.getDetailCampaign()) : null;
        this.usageDetail = campaign.getUsageDetail() != null
                ? new UsageDetailViewModel(campaign.getUsageDetail()) : null;
        this.detailLocation = campaign.getLocationDetail() != null
                ? new LocationDetailViewModel(campaign.getLocationDetail()) : null;
        this.showLocations = campaign.isShowLocations();
        this.selectProducts = campaign.isSelectProducts();
        this.buttons = campaign.getProducts() != null
                ? ButtonProductViewModel.toProductList(campaign.getProducts()) : new ArrayList<>();
        this.products = campaign.getCampaignProducts() != null
                ? CampaignProductViewModel.toCampaignProductList(campaign.getCampaignProducts())
                : new ArrayList<>();
        this.messageVolunteer = campaign.getMessageVolunteer();
        this.subsidiares = campaign.getSubsidiaries() != null
                ? SubsidiaryViewModel.toSubsidiaryList(campaign.getSubsidiaries())
                : new ArrayList<>();
        this.idDiscount = campaign.getIdDiscount() != null ? campaign.getIdDiscount() : 0;
        this.idSubsidiary = campaign.getIdSubsidiary() != null ? campaign.getIdSubsidiary() : 0;
        this.idDiscountCategory = campaign.getIdDiscountCategory() != null ? campaign.getIdDiscountCategory() : 0;
        this.idProvince = campaign.getIdProvince() != null ? campaign.getIdProvince() : 0;
        this.idDistrict = campaign.getIdDistrict() != null ? campaign.getIdDistrict() : 0;
        this.idDiscountType = campaign.getIdDiscountType() != null ? campaign.getIdDiscountType() : 0;
    }


    public Integer getEnableLocal() {
        return enableLocal;
    }

    public Integer getEnableRole() {
        return enableRole;
    }

    public LocationDetailViewModel getDetailLocation() {
        return detailLocation;
    }

    public List<ButtonProductViewModel> getButtons() {
        return buttons;
    }

    public void setButtons(ArrayList<ButtonProductViewModel> buttons) {
        this.buttons = buttons;
    }

    public void setDetailLocation(LocationDetailViewModel detailLocation) {
        this.detailLocation = detailLocation;
    }

    public boolean isSelectProducts() {
        return selectProducts;
    }

    public void setSelectProducts(boolean selectProducts) {
        this.selectProducts = selectProducts;
    }

    public boolean isShowLocations() {
        return showLocations;
    }

    public void setShowLocations(boolean showLocations) {
        this.showLocations = showLocations;
    }

    public UsageDetailViewModel getUsageDetail() {
        return usageDetail;
    }

    public void setUsageDetail(UsageDetailViewModel usageDetail) {
        this.usageDetail = usageDetail;
    }

    public Integer getIdCampaign() {
        return idCampaign;
    }

    public void setIdCampaign(Integer idCampaign) {
        this.idCampaign = idCampaign;
    }

    public CampaignType getIdCampaignType() {
        return idCampaignType;
    }

    public void setIdCampaignType(CampaignType idCampaignType) {
        this.idCampaignType = idCampaignType;
    }

    public ListInfoViewModel getListInfo() {
        return listInfo;
    }

    public void setListInfo(ListInfoViewModel listInfo) {
        this.listInfo = listInfo;
    }

    public DetailInfoViewModel getDetailInfo() {
        return detailInfo;
    }

    public void setDetailInfo(DetailInfoViewModel detailInfo) {
        this.detailInfo = detailInfo;
    }

    public List<CampaignProductViewModel> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<CampaignProductViewModel> products) {
        this.products = products;
    }

    public String getMessageVolunteer() {
        return messageVolunteer;
    }

    public void setMessageVolunteer(String messageVolunteer) {
        this.messageVolunteer = messageVolunteer;
    }

    public List<SubsidiaryViewModel> getSubsidiares() {
        return subsidiares;
    }

    public void setSubsidiares(ArrayList<SubsidiaryViewModel> subsidiares) {
        this.subsidiares = subsidiares;
    }

    public Integer getIdDiscount() {
        return idDiscount;
    }

    public void setIdDiscount(Integer idDiscount) {
        this.idDiscount = idDiscount;
    }

    public Integer getIdSubsidiary() {
        return idSubsidiary;
    }

    public void setIdSubsidiary(Integer idSubsidiary) {
        this.idSubsidiary = idSubsidiary;
    }

    public Integer getIdDiscountCategory() {
        return idDiscountCategory;
    }

    public void setIdDiscountCategory(Integer idDiscountCategory) {
        this.idDiscountCategory = idDiscountCategory;
    }

    public Integer getIdProvince() {
        return idProvince;
    }

    public void setIdProvince(Integer idProvince) {
        this.idProvince = idProvince;
    }

    public Integer getIdDistrict() {
        return idDistrict;
    }

    public void setIdDistrict(Integer idDistrict) {
        this.idDistrict = idDistrict;
    }

    public Integer getIdDiscountType() {
        return idDiscountType;
    }

    public void setIdDiscountType(Integer idDiscountType) {
        this.idDiscountType = idDiscountType;
    }


    public static List<CampaignViewModel> toCampaignList(List<Campaign> campaigns) {
        List<CampaignViewModel> list = new ArrayList<>();
        if (campaigns != null) {
            for (Campaign campaign : campaigns) {
                list.add(new CampaignViewModel(campaign));
            }
        }
        return list;
    }
}
