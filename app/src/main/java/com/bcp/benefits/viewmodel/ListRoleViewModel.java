package com.bcp.benefits.viewmodel;
/* 
   Created by: Flavia Figueroa
               13/02/2020
         Solera Mobile
*/


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import pe.solera.benefit.main.entity.ListRole;

public class ListRoleViewModel {
    private Integer idRoleVolunteer;
    private String name;

    public ListRoleViewModel(Integer idRoleVolunteer, String name) {
        this.idRoleVolunteer = idRoleVolunteer;
        this.name = name;
    }

    ListRoleViewModel(ListRole listRole){
        this.idRoleVolunteer = listRole.getIdRoleVolunteer();
        this.name = listRole.getName();
    }

    public Integer getIdRoleVolunteer() {
        return idRoleVolunteer;
    }

    public String getName() {
        return name;
    }

    public static List<ListRoleViewModel> toListRole(List<ListRole> listRole){
        List<ListRoleViewModel> list = new ArrayList<>();
        if (listRole != null){
            for (ListRole role: listRole){
                list.add(toListRoleViewModel(role));
            }
        }
        return list;
    }

    public static ListRoleViewModel toListRoleViewModel(ListRole listRole){
        return new ListRoleViewModel(listRole);
    }

}
