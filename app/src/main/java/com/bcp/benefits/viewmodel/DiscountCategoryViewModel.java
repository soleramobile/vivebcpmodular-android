package com.bcp.benefits.viewmodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import pe.solera.benefit.main.entity.DiscountCategory;

/**
 * Created by emontesinos on 28/05/19.
 **/


public class DiscountCategoryViewModel implements Serializable {
    private int idCategory;
    private int idType;
    private String categoryName;

    public DiscountCategoryViewModel() {
    }

    public DiscountCategoryViewModel(int idCategory, int idType, String categoryName) {
        this.idCategory = idCategory;
        this.idType = idType;
        this.categoryName = categoryName;
    }

    public DiscountCategoryViewModel(DiscountCategory discountCategory) {
        this.idCategory = discountCategory.getIdCategory();
        this.idType = discountCategory.getIdType();
        this.categoryName = discountCategory.getCategoryName();
    }

    public int getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(int idCategory) {
        this.idCategory = idCategory;
    }

    public int getIdType() {
        return idType;
    }

    public void setIdType(int idType) {
        this.idType = idType;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public static List<DiscountCategoryViewModel> toDiscountCategoryViewModelList(
            List<DiscountCategory> categories) {
        List<DiscountCategoryViewModel> categoryViewModels = new ArrayList<>();
        if (categories != null) {
            for (DiscountCategory category : categories) {
                categoryViewModels.add(new DiscountCategoryViewModel(category));
            }
        }
        return categoryViewModels;
    }
}
