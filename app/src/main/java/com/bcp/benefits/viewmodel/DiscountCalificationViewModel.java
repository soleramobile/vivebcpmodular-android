package com.bcp.benefits.viewmodel;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import pe.solera.benefit.main.entity.DiscountQualification;

/**
 * Created by emontesinos on 12/06/19.
 */

public class DiscountCalificationViewModel implements Serializable {
    public  String question;
    public  Integer commentAvailable;
    public  List<DiscountCalificationOptionViewModel> options;

    public DiscountCalificationViewModel(String question, Integer commentAvailable, List<DiscountCalificationOptionViewModel> options) {
        this.question = question;
        this.commentAvailable = commentAvailable;
        this.options = options;
    }

    public DiscountCalificationViewModel(DiscountQualification discountQualification) {
        this.question = discountQualification.getQuestion();
        this.commentAvailable = discountQualification.getCommentAvailable();
        this.options = discountQualification.getOptions() != null ?
                DiscountCalificationOptionViewModel.toCalificationOptionList(discountQualification.getOptions()): new ArrayList<>();
    }

    public static List<DiscountCalificationViewModel> toCalificationList(List<DiscountQualification> discountQualificationList) {
        List<DiscountCalificationViewModel> list = new ArrayList<>();
        if (discountQualificationList != null) {
            for (DiscountQualification discountQualification : discountQualificationList) {
                list.add(new DiscountCalificationViewModel(discountQualification));
            }
        }
        return list;
    }


    public String getQuestion() {
        return question;
    }

    public Integer getCommentAvailable() {
        return commentAvailable;
    }

    public List<DiscountCalificationOptionViewModel> getOptions() {
        return options;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public void setCommentAvailable(Integer commentAvailable) {
        this.commentAvailable = commentAvailable;
    }

    public void setOptions(List<DiscountCalificationOptionViewModel> options) {
        this.options = options;
    }

}


