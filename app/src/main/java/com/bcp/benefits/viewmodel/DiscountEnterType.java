package com.bcp.benefits.viewmodel;

/*
    Created by: Flavia Figueroa
    Email: ffigueroa@solera.pe
    
    12/2/20 - 11:03
    Solera Mobile
*/

import com.bcp.benefits.analytics.AnalyticsTags;

public enum DiscountEnterType {
    LISTA(AnalyticsTags.Common.Values.lista), MAPA(AnalyticsTags.Common.Values.mapa), CAMPANA(AnalyticsTags.Common.Values.campana);

    private String type;

    DiscountEnterType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
