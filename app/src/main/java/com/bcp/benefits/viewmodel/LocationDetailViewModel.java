package com.bcp.benefits.viewmodel;

import android.os.Parcel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import pe.solera.benefit.main.entity.LocationDetail;

/**
 * Created by emontesinos on 13/05/19.
 **/

public class LocationDetailViewModel implements Serializable {
    private String title;
    private String disclaimer;
    private List<CampaignLocationViewModel> locations = null;


    public LocationDetailViewModel(LocationDetail locationDetail) {
        this.title = locationDetail.getTitle();
        this.disclaimer = locationDetail.getDisclaimer();
        this.locations = locationDetail.getLocations() != null ?
                CampaignLocationViewModel.toLocationList(locationDetail.getLocations()): new ArrayList<>();
    }

    protected LocationDetailViewModel(Parcel in) {
        title = in.readString();
        disclaimer = in.readString();
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDisclaimer() {
        return disclaimer;
    }

    public void setDisclaimer(String disclaimer) {
        this.disclaimer = disclaimer;
    }

    public List<CampaignLocationViewModel> getLocations() {
        return locations;
    }

    public void setLocations(List<CampaignLocationViewModel> locations) {
        this.locations = locations;
    }



}
