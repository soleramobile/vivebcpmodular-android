package com.bcp.benefits.viewmodel;

/**
 * Created by emontesinos on 21/05/19.
 **/

public class LastActionViewModel {

    public static final int OPTIONAL_AMOUNT = 1;
    public static final int AMOUNT = 2;
    public static final int PERIOD = 3;
    public static final int CONDITION = 4;
    public static final int CREDIT_TYPE = 5;
    public static final int CREDIT_AMOUNT = 6;

    private int id;
    private Object action;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Object getAction() {
        return action;
    }

    public void setAction(Object action) {
        this.action = action;
    }
}
